import React from 'react';
import * as productCheckStatus from './../constants/productCheckStatus';
import * as packageStatus from '../constants/packageStatus';
import * as logisticsReceiveStatus from './../constants/logisticsReceiveStatus';
import * as logisticsDeliverStatus from './../constants/logisticsDeliverStatus';
import * as productDeliveryStatus from './../constants/productDeliveryStatus';


export default class ProductRenderer {
	renderProductDeliveryStatus = (status, intl) => {
		switch (status) {
			default:
			case productDeliveryStatus.SENT_BACK:
				return <span
					style={ { color: '#ED1B23' } }>{ intl.formatMessage({ id: 'packages.delivery.send_back' }) }</span>;
			case productDeliveryStatus.WAIT:
				return <span
					style={ { color: '#F2994A' } }>{ intl.formatMessage({ id: 'packages-check.status.waiting' }) }</span>;
			case productDeliveryStatus.SEND:
				return <span
					style={ { color: '#2F80ED' } }>{ intl.formatMessage({ id: 'products-delivery.submitter.send' }) }</span>;
			case productDeliveryStatus.IN_PACK:
				return <span
					style={ { color: '#2F80ED' } }>{ intl.formatMessage({ id: 'packages.receive-status.package' }) }</span>;
			case productDeliveryStatus.IN_AIRPORT:
				return <span
					style={ { color: '#2F80ED' } }>{ intl.formatMessage({ id: 'packages.receive-status.airport' }) }</span>;
			case productDeliveryStatus.SENT:
				return <span
					style={ { color: '#219653' } }>{ intl.formatMessage({ id: 'packages.delivery.sent' }) }</span>;
		}
	};

	renderProductCheckStatus = (status, intl) => {
		switch (status) {
			default:
			case productCheckStatus.DEFECT:
				return <span>{ intl.formatMessage({ id: 'products-check.selector.defect' }) }</span>;
			case productCheckStatus.ERROR:
				return <span
					style={ { color: '#2F80ED' } }>{ intl.formatMessage({ id: 'products-check.selector.wrong' }) }</span>;
			case productCheckStatus.WAIT:
				return <span
					style={ { color: '#F2994A' } }>{ intl.formatMessage({ id: 'packages-check.filter.date' }) }</span>;
			case productCheckStatus.GOOD:
				return <span
					style={ { color: '#219653' } }>{ intl.formatMessage({ id: 'products-check.selector.ok' }) }</span>;
		}
	};

	renderLogisticsReceiveStatus = (status, intl) => {
		switch (status) {
			default:
			case logisticsReceiveStatus.WAIT:
				return <span style={ { color: '#F2994A' } }>В ожидании</span>;
			case logisticsReceiveStatus.RECEIVED:
				return <span
					style={ { color: '#219653' } }>{ intl.formatMessage({ id: 'packages-check.status.received' }) }</span>;
		}
	};

	renderLogisticsDeliverStatus = (status, intl) => {
		switch (status) {
			default:
			case logisticsDeliverStatus.WAIT:
				return <span style={ { color: '#F2994A' } }>В ожидании</span>;
			case logisticsDeliverStatus.SENT:
				return <span
					style={ { color: '#2F80ED' } }>{ intl.formatMessage({ id: 'packages.delivery.sent' }) }</span>;
			case logisticsDeliverStatus.DELIVERED:
				return <span style={ { color: '#219653' } }>Доставлен</span>;
		}
	};

	renderPackageStatus = (status, intl) => {
		switch (status) {
			default:
			case packageStatus.WAIT:
				return <span
					style={ { color: '#F2994A' } }>{ intl.formatMessage({ id: 'packages-check.filter.date' }) }</span>;
			case packageStatus.ORDERED:
				return <span
					style={ { color: '#2F80ED' } }>{ intl.formatMessage({ id: 'packages-check.status.ordered' }) }</span>;
			case packageStatus.RECEIVED:
				return <span
					style={ { color: '#219653' } }>{ intl.formatMessage({ id: 'packages-check.status.received' }) }</span>;
		}
	};
}