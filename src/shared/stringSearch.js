import React, { useRef } from 'react';
import { Button, Input, Space } from 'antd';
import SearchOutlined from '@ant-design/icons/lib/icons/SearchOutlined';

export const useTextFilter = (dataIndex) => {
	const searchInput = useRef();

	return {
		filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
			<div style={ { padding: 8 } }>
				<Input
					ref={ searchInput }
					placeholder={ `Search ${ dataIndex }` }
					value={ selectedKeys[0] }
					onChange={ e => setSelectedKeys(e.target.value ? [e.target.value] : []) }
					onPressEnter={ () => handleSearch(selectedKeys, confirm, dataIndex) }
					style={ { width: 188, marginBottom: 8, display: 'block' } }
				/>
				<Space>
					<Button
						type="primary"
						onClick={ () => handleSearch(selectedKeys, confirm, dataIndex) }
						icon={ <SearchOutlined/> }
						size="small"
						style={ { width: 90 } }
					>
						Найти
					</Button>
					<Button onClick={ () => handleReset(clearFilters) } size="small" style={ { width: 90 } }>
						Сброс
					</Button>
				</Space>
			</div>
		),
		filterIcon: filtered => <SearchOutlined style={ { color: filtered ? '#1890ff' : undefined } }/>,
		onFilter: (value, record) => record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
	};
};

const handleSearch = (selectedKeys, confirm, dataIndex) => {
	confirm();
};

const handleReset = clearFilters => {
	clearFilters();
};