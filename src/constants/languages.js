import * as Languages from './locales';

export const TR = 'tr';
export const RU = 'ru';

const localeMap = {
	[Languages.RU]: 'ru',
	[Languages.TR]: 'tr',
};

export default localeMap;