export const CASH = 0;
export const ELKART = 100;
export const MAESTRO = 101;
export const MASTERCARD = 102;
export const VISA = 103;
export const OMONEY = 200;
export const PAY24 = 201;
export const DEMIRBANK = 300;
