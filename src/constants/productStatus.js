export const CANCELED = -1;
export const WAIT = 1;
export const IN_PROCESS = 2;
export const DONE = 3;
export const REFUND = 4;
