export const BEGIN = 0;
export const DEPARTMENT = 1;
export const CATEGORY = 2;
export const COLOR = 3;
export const CONTENT = 4;
export const END = 5;
export const APPROVED = 100;