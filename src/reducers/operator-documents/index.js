import { Map } from 'immutable';
import * as Actions from '../../actions/operator-documents';

const initialState = new Map({
	fetching: false,
	data: [],
	selected: {},
	products: {},
	all: [],
	hierarchy: {}
});

export default (state = initialState, action) => {
	switch (action.type) {
		case Actions.FETCH_OPERATOR_DOCUMENTS:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_FETCH_OPERATOR_DOCUMENTS:
			state = state.update('fetching', () => false);
			state = state.update('data', () => action.data);
			return state;

		case Actions.FAILURE_FETCH_OPERATOR_DOCUMENTS:
			state = state.update('fetching', () => false);
			state = state.update('data', () => []);
			return state;

		case Actions.FETCH_OPERATOR_DOCUMENT_HIERARCHY:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_FETCH_OPERATOR_DOCUMENT_HIERARCHY:
			state = state.update('fetching', () => false);
			state = state.update('hierarchy', () => action.data);
			return state;

		case Actions.FAILURE_FETCH_OPERATOR_DOCUMENT_HIERARCHY:
			state = state.update('fetching', () => false);
			state = state.update('hierarchy', () => {
			});
			return state;

		case Actions.FETCH_OPERATOR_DOCUMENT_HIERARCHY_AFTER_BULK:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_FETCH_OPERATOR_DOCUMENT_HIERARCHY_AFTER_BULK:
			state = state.update('fetching', () => false);
			return state;

		case Actions.FAILURE_FETCH_OPERATOR_DOCUMENT_HIERARCHY_AFTER_BULK:
			state = state.update('fetching', () => false);
			return state;

		case Actions.FETCH_ALL_OPERATOR_DOCUMENTS:
			state = state.update('fetching', () => true);
			return state;

		case Actions.SUCCESS_FETCH_ALL_OPERATOR_DOCUMENTS:
			state = state.update('fetching', () => false);
			state = state.update('all', () => action.data);
			return state;

		case Actions.FAILURE_FETCH_ALL_OPERATOR_DOCUMENTS:
			state = state.update('fetching', () => false);
			state = state.update('all', () => []);
			return state;

		case Actions.FETCH_OPERATOR_DOCUMENT:
			state = state.update('fetching', () => true);
			return state;

		case Actions.SUCCESS_FETCH_OPERATOR_DOCUMENT:
			state = state.update('fetching', () => false);
			state = state.update('selected', () => action.data);
			return state;

		case Actions.FAILURE_FETCH_OPERATOR_DOCUMENT:
			state = state.update('fetching', () => false);
			state = state.update('selected', () => {
			});
			return state;

		case Actions.FETCH_OPERATOR_PRODUCTS_BY_CATEGORY:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_FETCH_OPERATOR_PRODUCTS_BY_CATEGORY:
			state = state.update('fetching', () => false);
			state = state.update('products', () => action.data);
			return state;

		case Actions.FAILURE_FETCH_OPERATOR_PRODUCTS_BY_CATEGORY:
			state = state.update('fetching', () => false);
			state = state.update('products', () => []);
			return state;

		case Actions.PROCESS_OPERATOR_PRODUCT:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_PROCESS_OPERATOR_PRODUCT:
			state = state.update('fetching', () => false);
			let indexOfListToUpdate = state.get('products').products.findIndex(listItem => {
				return listItem.id === action.data.id;
			});
			state.getIn(['products', 'products'])[indexOfListToUpdate] = action.data;
			return state;

		case Actions.FAILURE_PROCESS_OPERATOR_PRODUCT:
			state = state.update('fetching', () => false);
			return state;

		default:
			return state;
	}
}
