import { Map } from 'immutable';
import * as Actions from '../../actions/documents';

const initialState = new Map({
	fetching: false,
	brands: [],
	documents: [],
	statistics: {},
	operators: [],
	hierarchy: {},
	products: {
		products: [], count: 0, pages: 0, step: 0
	},
});

export default (state = initialState, action) => {
	switch (action.type) {

		case Actions.FETCH_DOCUMENT_HIERARCHY:
			state = state.update('fetching', () => true);
			return state;

		case Actions.SUCCESS_FETCH_DOCUMENT_HIERARCHY:
			state = state.update('fetching', () => false);
			state = state.update('hierarchy', () => action.data);
			return state;

		case Actions.FAILURE_FETCH_DOCUMENT_HIERARCHY:
			state = state.update('fetching', () => false);
			state = state.update('hierarchy', () => {
			});
			return state;

		case Actions.FETCH_PRODUCTS_BY_CATEGORY:
			state = state.update('fetching', () => true);
			return state;

		case Actions.SUCCESS_FETCH_PRODUCTS_BY_CATEGORY:
			state = state.update('fetching', () => false);
			state = state.update('products', () => action.data);
			return state;

		case Actions.FAILURE_FETCH_PRODUCTS_BY_CATEGORY:
			state = state.update('fetching', () => false);
			state = state.update('products', () => {
			});
			return state;

		case Actions.FETCH_DOC_BRANDS:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_FETCH_DOC_BRANDS:
			state = state.update('fetching', () => false);
			state = state.update('brands', () => action.data);
			return state;

		case Actions.FAILURE_FETCH_DOC_BRANDS:
			state = state.update('fetching', () => false);
			state = state.update('brands', () => []);
			return state;

		case Actions.FETCH_DOCS:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_FETCH_DOCS:
			state = state.update('fetching', () => false);
			state = state.update('documents', () => action.data);
			return state;

		case Actions.FAILURE_FETCH_DOCS:
			state = state.update('fetching', () => false);
			state = state.update('documents', () => []);
			return state;

		case Actions.FETCH_STATISTICS:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_FETCH_STATISTICS:
			state = state.update('fetching', () => false);
			state = state.update('statistics', () => action.data);
			return state;

		case Actions.FAILURE_FETCH_STATISTICS:
			state = state.update('fetching', () => false);
			state = state.update('statistics', () => []);
			return state;

		case Actions.FETCH_OPERATORS:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_FETCH_OPERATORS:
			state = state.update('fetching', () => false);
			state = state.update('operators', () => action.data);
			return state;

		case Actions.FAILURE_FETCH_OPERATORS:
			state = state.update('fetching', () => false);
			state = state.update('operators', () => []);
			return state;

		case Actions.CREATE_DOC:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_CREATE_DOC:
			state = state.update('fetching', () => false);
			return state;

		case Actions.FAILURE_CREATE_DOC:
			state = state.update('fetching', () => false);
			return state;

		case Actions.SET_NEW:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_SET_NEW:
			state = state.update('fetching', () => false);
			let toUpdate = state.get('documents').findIndex(listItem => {
				return listItem.id === action.data.id;
			});
			state.getIn(['documents'])[toUpdate] = action.data;
			return state;

		case Actions.FAILURE_SET_NEW:
			state = state.update('fetching', () => false);
			return state;

		default:
			return state;
	}
}