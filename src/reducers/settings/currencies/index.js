import { Map } from 'immutable';
import * as Actions from '../../../actions/currencies';

const initialState = new Map({
	fetching: false,
	data: []
});

export default (state = initialState, action) => {
	switch (action.type) {
		case Actions.FETCH_CURRENCIES:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_FETCH_CURRENCIES:
			state = state.update('fetching', () => false);
			state = state.update('data', () => action.data);
			return state;

		case Actions.FAILURE_FETCH_CURRENCIES:
			state = state.update('fetching', () => false);
			state = state.update('data', () => []);
			return state;

		case Actions.FETCH_EXCHANGES:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_FETCH_EXCHANGES:
			state = state.update('fetching', () => false);
			state = state.update('data', () => parseData(action.data));
			return state;

		default:
			return state;
	}

	function parseData(exchanges) {
		const currencies = state.get('data');

		const createExchanges = (id, name, currencies) => {
			const exchanges = [];
			const exchangables = currencies.filter(function (obj) {
				return obj.id !== id;
			});
			exchangables.map(e => exchanges.push({
				id: null,
				from: id,
				fromName: name,
				to: e.id,
				toName: e.name,
				value: null,
				values: [],
				date: new Date()
			}));
			return exchanges;
		};

		const insertExchanges = (exchange) => {
			const foundExchange = currencies.find(currency => currency.id === exchange.from_currency.id).exchanges.find(ex => ex.to === exchange.to_currency.id);
			foundExchange.id = exchange.id;
			foundExchange.from = exchange.from_currency.id;
			foundExchange.to = exchange.to_currency.id;
			foundExchange.value = exchange.value;
			foundExchange.values = exchange.values;
		};

		currencies.map(currency => currency.exchanges = createExchanges(currency.id, currency.name, currencies));
		exchanges.map(exchange => insertExchanges(exchange));
		return currencies;
	}
}