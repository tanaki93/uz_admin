import { Map } from 'immutable';
import * as Actions from '../../actions/categories';

const initialState = new Map({
	fetching: false,
	data: [],
	selected: {}
});

export default (state = initialState, action) => {
	switch (action.type) {
		case Actions.FETCH_CATEGORIES:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_FETCH_CATEGORIES:
			state = state.update('fetching', () => false);
			state = state.update('data', () => action.data);
			return state;

		case Actions.FAILURE_FETCH_CATEGORIES:
			state = state.update('fetching', () => false);
			state = state.update('data', () => []);
			return state;

		case Actions.FETCH_CATEGORY:
			state = state.update('fetching', () => true);
			return state;

		case Actions.FAILURE_FETCH_CATEGORY:
			state = state.update('fetching', () => false);
			state = state.update('selected', () => {
			});
			return state;

		case Actions.SUCCESS_FETCH_CATEGORY:
			state = state.update('fetching', () => false);
			state = state.update('selected', () => action.data);
			return state;

		default:
			return state;
	}
}
