import { Map } from 'immutable';
import * as Actions from '../../../actions/landing';

const initialState = new Map({
	fetching: false,
	data: []
});

export default (state = initialState, action) => {
	switch (action.type) {
		case Actions.FETCH_BRANDS:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_FETCH_BRANDS:
			state = state.update('fetching', () => false);
			state = state.update('data', () => action.data);
			return state;

		case Actions.FAILURE_FETCH_BRANDS:
			state = state.update('fetching', () => false);
			state = state.update('data', () => []);
			return state;

		case Actions.UPDATE_BRANDS:
			return state;

		case Actions.SUCCESS_UPDATE_BRANDS:
			return state;

		case Actions.FAILURE_UPDATE_BRANDS:
			return state;

		default:
			return state;
	}
}
