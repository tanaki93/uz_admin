import { Map } from 'immutable';
import * as Actions from '../../actions/profile';

const initialState = new Map({
	fetching: false,
	data: {},
	orders: []
});

export default (state = initialState, action) => {
	switch (action.type) {
		case Actions.GET_USER_PROFILE:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_GET_USER_PROFILE:
			state = state.update('fetching', () => false);
			state = state.update('data', () => action.data.profile);
			state = state.update('orders', () => action.data.orders);
			return state;

		case Actions.FAILURE_GET_USER_PROFILE:
			state = state.update('fetching', () => false);
			state = state.update('data', () => {
			});
			state = state.update('orders', () => []);
			return state;

		default:
			return state;
	}
}
