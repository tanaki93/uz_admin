import { Map } from 'immutable';
import * as Actions from '../../../actions/brands';

const initialState = new Map({
	fetching: false,
	data: [],
	brandDepartments: {}
});

export default (state = initialState, action) => {
	switch (action.type) {
		case Actions.FETCH_BRANDS:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_FETCH_BRANDS:
			state = state.update('fetching', () => false);
			state = state.update('data', () => action.data);
			return state;

		case Actions.FAILURE_FETCH_BRANDS:
			state = state.update('fetching', () => false);
			state = state.update('data', () => []);
			return state;

		case Actions.FETCH_BRAND:
			state = state.update('fetching', () => true);
			return state;

		case Actions.SUCCESS_FETCH_BRAND:
			state = state.update('fetching', () => false);
			action.data.departments.map(entry => entry.department = entry.department !== null ? entry.department.id : null);
			action.data.departments.map(entry => entry.categories.map(catEntry => catEntry.category = catEntry.category !== null ? catEntry.category.id : null));
			state = state.update('brandDepartments', () => action.data);
			return state;

		case Actions.FAILURE_FETCH_BRAND:
			state = state.update('fetching', () => false);
			state = state.update('brandDepartments', () => {
			});
			return state;

		case Actions.REFRESH_BRAND:
			return state;

		case Actions.SUCCESS_REFRESH_BRAND:
			return state;

		case Actions.FAILURE_REFRESH_BRAND:
			return state;

		default:
			return state;
	}
}
