import { Map } from 'immutable';
import * as Actions from '../../../actions/classes-base';

const initialState = new Map({
	fetching: false,
	data: [],
	selected: {}
});

export default (state = initialState, action) => {
	switch (action.type) {
		case Actions.FETCH_CLASSES_BASE:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_FETCH_CLASSES_BASE:
			state = state.update('fetching', () => false);
			state = state.update('data', () => action.data);
			return state;

		case Actions.FAILURE_FETCH_CLASSES_BASE:
			state = state.update('fetching', () => false);
			state = state.update('data', () => []);
			return state;

		case Actions.FETCH_CLASS_BASE:
			state = state.update('fetching', () => true);
			return state;

		case Actions.FAILURE_FETCH_CLASS_BASE:
			state = state.update('fetching', () => false);
			state = state.update('selected', () => {
			});
			return state;

		case Actions.SUCCESS_FETCH_CLASS_BASE:
			state = state.update('fetching', () => false);
			state = state.update('selected', () => action.data);
			return state;

		default:
			return state;
	}
}
