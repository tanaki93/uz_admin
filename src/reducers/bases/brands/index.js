import { Map } from 'immutable';
import * as Actions from '../../../actions/brands-base';

const initialState = new Map({
	fetching: false,
	data: []
});

export default (state = initialState, action) => {
	switch (action.type) {
		case Actions.FETCH_BRANDS_BASE:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_FETCH_BRANDS_BASE:
			state = state.update('fetching', () => false);
			state = state.update('data', () => action.data);
			return state;

		case Actions.FAILURE_FETCH_BRANDS_BASE:
			state = state.update('fetching', () => false);
			state = state.update('data', () => []);
			return state;
		default:
			return state;
	}
}
