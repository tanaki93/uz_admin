import { Map } from 'immutable';
import * as Actions from '../../../actions/departments-base';

const initialState = new Map({
	fetching: false,
	data: [],
	selected: {}
});

export default (state = initialState, action) => {
	switch (action.type) {
		case Actions.FETCH_DEPARTMENTS_BASE:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_FETCH_DEPARTMENTS_BASE:
			state = state.update('fetching', () => false);
			state = state.update('data', () => action.data);
			return state;

		case Actions.FAILURE_FETCH_DEPARTMENTS_BASE:
			state = state.update('fetching', () => false);
			state = state.update('data', () => []);
			return state;

		case Actions.FETCH_DEPARTMENT_BASE:
			state = state.update('fetching', () => true);
			return state;

		case Actions.FAILURE_FETCH_DEPARTMENT_BASE:
			state = state.update('fetching', () => false);
			state = state.update('selected', () => {
			});
			return state;

		case Actions.SUCCESS_FETCH_DEPARTMENT_BASE:
			state = state.update('fetching', () => false);
			state = state.update('selected', () => action.data);
			return state;

		default:
			return state;
	}
}
