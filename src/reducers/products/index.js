import { Map } from 'immutable';
import * as Actions from '../../actions/products';

const initialState = new Map({
	fetching: false,
	data: [],
	count: 0,
	pages: 0,
	current: 0
});

export default (state = initialState, action) => {
	switch (action.type) {
		case Actions.FETCH_PRODUCTS:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_FETCH_PRODUCTS:
			state = state.update('fetching', () => false);
			state = state.update('data', () => action.data.products);
			state = state.update('count', () => action.data.count);
			state = state.update('pages', () => action.data.pages);
			state = state.update('current', () => action.data.page);
			return state;

		case Actions.FAILURE_FETCH_PRODUCTS:
			state = state.update('fetching', () => false);
			state = state.update('data', () => []);
			state = state.update('count', () => 0);
			state = state.update('pages', () => 0);
			state = state.update('current', () => 0);
			return state;

		case Actions.ACTIVATE_PRODUCT:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_ACTIVATE_PRODUCT:
			state = state.update('fetching', () => false);
			let toUpdate = state.get('data').findIndex(listItem => {
				return listItem.id === action.data.id;
			});
			state.getIn(['data'])[toUpdate] = action.data;
			return state;

		case Actions.FAILURE_ACTIVATE_PRODUCT:
			state = state.update('fetching', () => false);
			return state;

		default:
			return state;
	}
}
