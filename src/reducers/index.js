import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import applicationReducer from './applicationReducer';


export default combineReducers({
	routing: routerReducer,
	application: applicationReducer
});
