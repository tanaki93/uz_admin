import { Map } from 'immutable';
import * as Actions from '../../actions/orders';

const initialState = new Map({
	fetching: false,
	fetchingClients: false,
	data: [],
	clients: []
});

export default (state = initialState, action) => {
	switch (action.type) {
		case Actions.FETCH_ORDERS:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_FETCH_ORDERS:
			state = state.update('fetching', () => false);
			state = state.update('data', () => action.data);
			return state;

		case Actions.FAILURE_FETCH_ORDERS:
			state = state.update('fetching', () => false);
			state = state.update('data', () => []);
			return state;

		case Actions.FETCH_CLIENTS:
			return state = state.update('fetchingClients', () => true);

		case Actions.SUCCESS_FETCH_CLIENTS:
			state = state.update('fetchingClients', () => false);
			state = state.update('clients', () => action.data);
			return state;

		case Actions.FAILURE_FETCH_CLIENTS:
			state = state.update('fetchingClients', () => false);
			state = state.update('clients', () => []);
			return state;

		case Actions.ASSIGN_MYSELF:
			state = state.update('fetching', () => true);
			return state;

		case Actions.SUCCESS_ASSIGN_MYSELF:
			state = state.update('fetching', () => false);
			return state;

		case Actions.FAILURE_ASSIGN_MYSELF:
			state = state.update('fetching', () => false);
			return state;

		default:
			return state;
	}
}
