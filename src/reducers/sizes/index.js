import { Map } from 'immutable';
import * as Actions from '../../actions/sizes';

const initialState = new Map({
	fetching: false,
	data: []
});

export default (state = initialState, action) => {
	switch (action.type) {
		case Actions.FETCH_SIZES:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_FETCH_SIZES:
			state = state.update('fetching', () => false);
			state = state.update('data', () => action.data);
			return state;

		case Actions.FAILURE_FETCH_SIZES:
			state = state.update('fetching', () => false);
			state = state.update('data', () => []);
			return state;

		default:
			return state;
	}
}
