import { Map } from 'immutable';
import authReducer from './auth';
import brandsReducer from './handbook/brands';
import departmentsReducer from './handbook/departments';
import departmentsBaseReducer from './bases/departments';
import classesBaseReducer from './bases/classes';
import categoriesBaseReducer from './bases/categories';
import contentsBaseReducer from './bases/contents';
import brandsBaseReducer from './bases/brands';
import sizesBaseReducer from './bases/sizes';
import colorsBaseReducer from './bases/colors';
import categoriesReducer from './categories';
import documentsReducer from './documents';
import operatorDocumentsReducer from './operator-documents';
import productsReducer from './products';
import iziProductsReducer from './izi-products';
import colorsReducer from './handbook/colors';
import sizesReducer from './sizes';
import usersReducer from './settings/users';
import currenciesReducer from './settings/currencies';
import languagesReducer from './settings/languages';
import countriesReducer from './settings/countries';
import citiesReducer from './settings/cities';
import pointsReducer from './settings/points';
import ordersReducer from './orders';
import orderDetailsReducer from './order-details';
import packagesCheckReducer from './controller/packages-check';
import productsCheckReducer from './controller/products-check';
import productDeliveryReducer from './controller/product-delivery';
import packetsReducer from './controller/packets';
import profileReducer from './profile';
import logisticsReducer from './logistics';
import logisticsManagementReducer from './logistics-management';
import refundReducer from './refund';
import marketingReducer from './marketing';
import landingBrandsReducer from './landing/brands';
import sliderReducer from './landing/slider';
import landingCategoriesReducer from './landing/categories';
import * as Locale from '../constants/locales';
import * as localeActions from '../actions/locale';

const initialState = Map({
	languageId: localStorage.getItem('locale') ? +localStorage.getItem('locale') : Locale.RU,
	auth: new Map({
		fetching: false,
		token: undefined,
		status: 0,
		user: {
			id: 0
		}
	}),
	brands: new Map({
		fetching: false,
		data: [],
		brandDepartments: {}
	}),
	departments: new Map({
		fetching: false,
		data: [],
		selected: {}
	}),
	departmentsBase: new Map({
		fetching: false,
		data: [],
		selected: {}
	}),
	classesBase: new Map({
		fetching: false,
		data: [],
		selected: {}
	}),
	colorsBase: new Map({
		fetching: false,
		data: []
	}),
	categories: new Map({
		fetching: false,
		data: [],
		selected: {}
	}),
	categoriesBase: new Map({
		fetching: false,
		parents: [],
		data: [],
		selected: {},
		parentSelected: {}
	}),
	documents: new Map({
		fetching: false,
		brands: [],
		documents: [],
		statistics: {},
		operators: [],
		hierarchy: {},
		products: {
			products: [], count: 0, pages: 0, step: 0
		},
	}),
	operatorDocuments: new Map({
		fetching: false,
		data: [],
		selected: {},
		products: {},
		all: [],
		hierarchy: {}
	}),
	products: new Map({
		fetching: false,
		data: [],
		count: 0,
		pages: 0,
		current: 0
	}),
	iziProducts: new Map({
		fetching: false,
		data: [],
		count: 0,
		pages: 0,
		current: 0
	}),
	marketing: new Map({
		fetching: false,
		data: [],
		count: 0,
		pages: 0,
		current: 0
	}),
	users: new Map({
		fetching: false,
		data: [],
	}),
	currencies: new Map({
		fetching: false,
		data: [],
	}),
	slider: new Map({
		fetching: false,
		data: [],
	}),
	sizes: new Map({
		fetching: false,
		data: [],
	}),
	languages: new Map({
		fetching: false,
		data: [],
	}),
	countries: new Map({
		fetching: false,
		data: [],
	}),
	cities: new Map({
		fetching: false,
		data: [],
	}),
	points: new Map({
		fetching: false,
		data: [],
	}),
	contents: new Map({
		fetching: false,
		data: [],
	}),
	orders: new Map({
		fetching: false,
		fetchingClients: false,
		data: [],
		clients: []
	}),
	orderDetails: new Map({
		fetching: false,
		data: {}
	}),
	packagesCheck: new Map({
		fetching: false,
		data: []
	}),
	productsCheck: new Map({
		fetching: false,
		data: []
	}),
	productDelivery: new Map({
		fetching: false,
		data: []
	}),
	packets: new Map({
		fetching: false,
		data: []
	}),
	logistics: new Map({
		fetching: false,
		data: []
	}),
	logisticsManagement: new Map({
		fetching: false,
		data: {}
	}),
	profile: new Map({
		fetching: false,
		data: {},
		orders: []
	}),
	brandsBaseReducer: new Map({
		fetching: false,
		data: []
	}),
	refund: new Map({
		fetching: false,
		data: []
	}),
	landingBrands: new Map({
		fetching: false,
		data: []
	})
});

export default (state = initialState, action) => {
	state = state.update('auth', (s) => authReducer(s, action));
	state = state.update('brands', (s) => brandsReducer(s, action));
	state = state.update('departments', (s) => departmentsReducer(s, action));
	state = state.update('departmentsBase', (s) => departmentsBaseReducer(s, action));
	state = state.update('classesBase', (s) => classesBaseReducer(s, action));
	state = state.update('categoriesBase', (s) => categoriesBaseReducer(s, action));
	state = state.update('contentsBase', (s) => contentsBaseReducer(s, action));
	state = state.update('operatorDocuments', (s) => operatorDocumentsReducer(s, action));
	state = state.update('colorsBase', (s) => colorsBaseReducer(s, action));
	state = state.update('sizesBase', (s) => sizesBaseReducer(s, action));
	state = state.update('categories', (s) => categoriesReducer(s, action));
	state = state.update('colors', (s) => colorsReducer(s, action));
	state = state.update('sizes', (s) => sizesReducer(s, action));
	state = state.update('documents', (s) => documentsReducer(s, action));
	state = state.update('products', (s) => productsReducer(s, action));
	state = state.update('iziProducts', (s) => iziProductsReducer(s, action));
	state = state.update('users', (s) => usersReducer(s, action));
	state = state.update('currencies', (s) => currenciesReducer(s, action));
	state = state.update('languages', (s) => languagesReducer(s, action));
	state = state.update('countries', (s) => countriesReducer(s, action));
	state = state.update('cities', (s) => citiesReducer(s, action));
	state = state.update('points', (s) => pointsReducer(s, action));
	state = state.update('orders', (s) => ordersReducer(s, action));
	state = state.update('orderDetails', (s) => orderDetailsReducer(s, action));
	state = state.update('packagesCheck', (s) => packagesCheckReducer(s, action));
	state = state.update('productsCheck', (s) => productsCheckReducer(s, action));
	state = state.update('productDelivery', (s) => productDeliveryReducer(s, action));
	state = state.update('packets', (s) => packetsReducer(s, action));
	state = state.update('logistics', (s) => logisticsReducer(s, action));
	state = state.update('logisticsManagement', (s) => logisticsManagementReducer(s, action));
	state = state.update('brandsBase', (s) => brandsBaseReducer(s, action));
	state = state.update('profile', (s) => profileReducer(s, action));
	state = state.update('refund', (s) => refundReducer(s, action));
	state = state.update('marketing', (s) => marketingReducer(s, action));
	state = state.update('landingBrands', (s) => landingBrandsReducer(s, action));
	state = state.update('slider', (s) => sliderReducer(s, action));
	state = state.update('landingCategories', (s) => landingCategoriesReducer(s, action));

	switch (action.type) {
		case localeActions.CHANGE_LOCALE:
			localStorage.setItem('locale', action.data.languageId);
			state = state.update('languageId', () => action.data.languageId);
			break;

		default:
			return state
	}

	return state;
}