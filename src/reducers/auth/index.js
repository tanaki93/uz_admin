import { Map } from 'immutable';
import * as AuthActions from '../../actions/auth';
import * as AuthMap from '../../constants/auth';

const initialState = new Map({
	fetching: false,
	token: undefined,
	status: 0,
	user: {}
});

export default (state = initialState, action) => {
	switch (action.type) {
		case AuthActions.FETCH_AUTH:
		case AuthActions.FETCH_REAUTH:
			state = state.update('status', () => AuthMap.INIT);
			return state = state.update('fetching', () => true);

		case AuthActions.SUCCESS_FETCH_AUTH:
			localStorage.setItem('Token', action.data.token);
			state = state.update('fetching', () => false);
			state = state.update('status', () => AuthMap.AUTHENTICATED);
			state = state.update('user', () => action.data.user);
			return state;

		case AuthActions.SUCCESS_FETCH_REAUTH:
			state = state.update('fetching', () => false);
			state = state.update('status', () => AuthMap.AUTHENTICATED);
			state = state.update('user', () => action.data.user);
			return state;

		case AuthActions.FAILURE_FETCH_AUTH:
		case AuthActions.FAILURE_FETCH_REAUTH:
			localStorage.removeItem('Token');
			state = action.data === 'Пользователь не найден' ?
				state.update('status', () => AuthMap.WRONG_AUTH) :
				state.update('status', () => AuthMap.INTERNAL_ERROR);
			state = state.update('fetching', () => false);
			state = state.update('user', () => {
			});
			return state;

		case AuthActions.LOSE_AUTH:
			if (localStorage.getItem('Token') !== undefined || sessionStorage.getItem('Token') !== null || sessionStorage.getItem('Token') !== '') {
				localStorage.removeItem('Token');
			}
			return initialState;

		default:
			return state;
	}
}
