import { Map } from 'immutable';
import * as Actions from '../../actions/izi-products';

const initialState = new Map({
	fetching: false,
	data: [],
	count: 0,
	pages: 0
});

export default (state = initialState, action) => {
	switch (action.type) {
		case Actions.FETCH_IZI_PRODUCTS:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_FETCH_IZI_PRODUCTS:
			state = state.update('fetching', () => false);
			state = state.update('data', () => parseProducts(action.data.products));
			state = state.update('count', () => action.data.count);
			state = state.update('pages', () => action.data.pages);
			return state;

		case Actions.FAILURE_FETCH_IZI_PRODUCTS:
			state = state.update('fetching', () => false);
			state = state.update('data', () => []);
			state = state.update('count', () => 0);
			state = state.update('pages', () => 0);
			return state;

		case Actions.EDIT_PRODUCT:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_EDIT_PRODUCT:
			state = state.update('fetching', () => false);
			let indexOfListToUpdate = state.get('data').findIndex(listItem => {
				return listItem.id === action.data.id;
			});
			state.getIn(['data'])[indexOfListToUpdate] = parseProduct(action.data);
			return state;

		case Actions.FAILURE_EDIT_PRODUCT:
			state = state.update('fetching', () => false);
			return state;

		case Actions.ACTIVATE_IZI_PRODUCT:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_ACTIVATE_IZI_PRODUCT:
			state = state.update('fetching', () => false);
			let toUpdate = state.get('data').findIndex(listItem => {
				return listItem.id === action.data.id;
			});
			state.getIn(['data'])[toUpdate] = parseProduct(action.data);
			return state;

		case Actions.FAILURE_ACTIVATE_IZI_PRODUCT:
			state = state.update('fetching', () => false);
			return state;

		case Actions.HIT_IZI_PRODUCT:
			return state = state.update('fetching', () => true);

		case Actions.SUCCESS_HIT_IZI_PRODUCT:
			state = state.update('fetching', () => false);
			const update = state.get('data').findIndex(listItem => {
				return listItem.id === action.data.id;
			});
			state.getIn(['data'])[update] = parseProduct(action.data);
			return state;

		case Actions.FAILURE_HIT_IZI_PRODUCT:
			state = state.update('fetching', () => false);
			return state;


		default:
			return state;
	}

	function parseProduct(product) {
		return {
			is_sellable: product.is_sellable,
			is_hit: product.is_hit,
			id: product.id,
			prices: product.prices,
			department: product.department,
			category: product.category,
			colour: product.colour,
			content: product.content || { name: '', code: '' },
			title: product.vend_product.title,
			brand: product.vend_product.brand,
			description: product.vend_product.description,
			discount_price: product.vend_product.discount_price,
			original_price: product.vend_product.original_price,
			selling_price: product.vend_product.selling_price + ' TRY',
			izi_price: product.prices[0].selling_price + ' UZ',
			images: product.vend_product.images,
			link: product.link,
			izi_link: product.izi_link,
			descriptions: product.descriptions,
			updated_at: product.updated_at,
			created_at: product.created_at,
		};
	}

	function parseProducts(products) {
		const parsed = [];
		products.map(product => parsed.push({
			is_sellable: product.is_sellable,
			is_hit: product.is_hit,
			id: product.id,
			prices: product.prices,
			department: product.department,
			category: product.category,
			colour: product.colour,
			content: product.content || { name: '', code: '' },
			title: product.vend_product.title,
			brand: product.vend_product.brand,
			description: product.vend_product.description,
			discount_price: product.vend_product.discount_price,
			original_price: product.vend_product.original_price,
			selling_price: product.vend_product.selling_price + ' TRY',
			izi_price: product.prices[0].selling_price + ' UZ',
			images: product.vend_product.images,
			link: product.link,
			izi_link: product.izi_link,
			descriptions: product.descriptions,
			updated_at: product.updated_at,
			created_at: product.created_at,
		}));
		return parsed;
	}
}
