import React from 'react';
import { withRouter } from 'react-router-dom';
import {
	fetchDocuments as fetchDocumentsAction,
	fetchDocument as fetchDocumentAction,
	fetchAllDocuments as fetchAllDocumentsAction,
	assignDocument as assignDocumentAction
} from './../../actions/creators/operator-documents';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import DocumentsComponent from '../../components/operator';

class Documents extends React.Component {
	componentDidMount() {
		this.fetchDocuments();
	}

	fetchDocuments = () => {
		this.props.fetchDocumentsAction();
		this.props.fetchAllDocumentsAction();
	};

	assignDocument = (id) => {
		this.props.assignDocumentAction(id).then(() => this.fetchDocuments());
	};

	render() {
		const { fetching, documents, all } = this.props;

		return (
			<DocumentsComponent
				fetching={ fetching }
				documents={ documents }
				all={ all }
				fetchDocuments={ this.fetchDocuments }
				assignDocument={ this.assignDocument }
			/>
		);
	}
}

const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['operatorDocuments', 'fetching']),
	documents: state.application.getIn(['operatorDocuments', 'data']),
	all: state.application.getIn(['operatorDocuments', 'all']),
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchDocumentsAction,
	fetchAllDocumentsAction,
	fetchDocumentAction,
	assignDocumentAction,
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(Documents)
);
