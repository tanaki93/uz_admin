import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import {
	fetchProductsByCategory as fetchProductsAction,
	fetchDocumentHierarchy as fetchDocumentHierarchyAction,
	processProducts as processProductsAction,
	processProduct as processProductAction,
	bulkProducts as bulkProductsAction,
	fetchDocumentHierarchyAfterBulk as fetchDocumentHierarchyAfterBulkAction
} from '../../../actions/creators/operator-documents';
import {
	fetchContents as fetchContentsAction,
	createContent as createContentAction
} from '../../../actions/creators/contents-base';
import {
	fetchLanguages as fetchLanguagesAction
} from './../../../actions/creators/languages';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import DocumentProducts from '../../../components/operator/products';
import * as processingSteps from './../../../constants/processingSteps';

const Document = (props) => {
	const [_products, setProducts] = useState([]);
	const [count, setCount] = useState(0);

	const documentId = props.match.params['documentId'];
	const {
		products, hierarchy, fetching, languages, contents, fetchDocumentHierarchyAction, fetchProductsAction,
		processProductAction, bulkProductsAction, fetchDocumentHierarchyAfterBulkAction,
		createContentAction, fetchLanguagesAction
	} = props;


	useEffect(() => {
		if (products.products) {
			setProducts([...products.products]);
			setCount(products.count);
		}
	}, [props]);

	useEffect(() => {
		fetchDocumentHierarchy(documentId);
		fetchProducts(documentId, { page: 1 });
		props.fetchContentsAction();
		fetchLanguages();
		if (hierarchy.step === processingSteps.END) {
			props.history.push('/');
		}
	}, []);

	const fetchDocumentHierarchy = (id) => {
		fetchDocumentHierarchyAction(id);
	};

	const processProducts = (docId) => {
		fetchDocumentHierarchyAfterBulkAction(docId).then(() => {
			fetchProducts(docId);
			fetchDocumentHierarchy(docId);
		});
	};

	const bulkProducts = (docId, data) => {
		bulkProductsAction(docId, data).then(() => fetchProducts(docId));
	};

	const fetchProducts = (id, params) => {
		fetchProductsAction(id, params);
	};

	const processProduct = (productId, data) => {
		const documentId = props.match.params['documentId'];
		processProductAction(documentId, productId, data);
	};

	const fetchLanguages = () => {
		fetchLanguagesAction();
	};

	const createContentBase = (data) => {
		const documentId = props.match.params['documentId'];
		createContentAction(data).then(() => fetchDocumentHierarchy(documentId));
	};


	return (
		<DocumentProducts
			count={ count }
			fetching={ fetching }
			documentId={ documentId }
			products={ _products }
			hierarchy={ hierarchy }
			fetchProducts={ fetchProducts }
			processProducts={ processProducts }
			processProduct={ processProduct }
			bulkProducts={ bulkProducts }
			createContentBase={ createContentBase }
			languages={ languages }
			contents={ contents }
		/>
	);
};


const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['operatorDocuments', 'fetching']),
	products: state.application.getIn(['operatorDocuments', 'products']),
	hierarchy: state.application.getIn(['operatorDocuments', 'hierarchy']),
	contents: state.application.getIn(['contentsBase', 'data']),
	languages: state.application.getIn(['languages', 'data']),
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchDocumentHierarchyAction,
	fetchProductsAction,
	processProductsAction,
	processProductAction,
	bulkProductsAction,
	fetchDocumentHierarchyAfterBulkAction,
	createContentAction,
	fetchLanguagesAction,
	fetchContentsAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(Document)
);
