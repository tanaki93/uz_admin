import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import AdminPage from 'containers/documents/list';
import OperatorPage from 'containers/operator';

class MainPage extends React.Component {
	componentDidMount() {
		this.props.user.user_type === 5 && this.props.history.push('/packages-check');
		this.props.user.user_type === 6 && this.props.history.push('/marketing');
	}

	render() {
		return (
			this.props.user?.user_type !== 1 ? <OperatorPage/> : <AdminPage/>
		);
	}
}

const mapStateToProps = (state) => ({
	user: state.application.getIn(['auth', 'user'])
});

export default withRouter(
	connect(mapStateToProps)(MainPage)
);