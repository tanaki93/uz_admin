import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { auth as authAction } from 'actions/creators/auth';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import AuthComponent from 'components/auth';

class Auth extends Component {
	UNSAFE_componentWillReceiveProps(nextProps) {
		if (this.props.location.state) {
			this.props.history.push(this.props.location.state.from.pathname);
		} else {
			this.props.history.push('/');
		}
	}

	auth = (data) => {
		this.props.authAction(data);
	};

	render() {
		const { fetching, status } = this.props;
		return (
			<div className='login-page'>
				<AuthComponent auth={ this.auth } fetching={ fetching } status={ status }/>
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['auth', 'fetching']),
	status: state.application.getIn(['auth', 'status'])
});

const mapDispatchToProps = dispatch => bindActionCreators({
	authAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(Auth)
);