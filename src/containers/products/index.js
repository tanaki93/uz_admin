import React from 'react';
import { withRouter } from 'react-router-dom';
import {
	fetchProducts as fetchProductsAction,
	activateProduct as activateProductAction
} from './../../actions/creators/products';
import { fetchBrands as fetchBrandsAction } from './../../actions/creators/brands';
import { fetchColors as fetchColorsAction } from './../../actions/creators/colors';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ProductsComponent from '../../components/products';

class Products extends React.Component {
	componentDidMount() {
		this.fetchProducts('', '', '', '', '', 1);
		this.fetchBrands();
		this.fetchColors();
	}

	fetchProducts = (brandId, departmentId, categoryId, colourId, query, page) => {
		this.props.fetchProductsAction(brandId, departmentId, categoryId, colourId, query, page);
	};

	fetchBrands = () => {
		this.props.fetchBrandsAction();
	};

	fetchColors = () => {
		this.props.fetchColorsAction();
	};

	activateProduct = (id, active) => {
		this.props.activateProductAction(id, { is_active: active });
	};

	render() {
		const {
			fetching, products, count, pages, current, hierarchy,
			fetchingBrands, brands, fetchingColors, colors
		} = this.props;

		return (
			<ProductsComponent
				fetching={ fetching }
				products={ products }
				count={ count }
				pages={ pages }
				current={ current }
				hierarchy={ hierarchy }
				fetchingBrands={ fetchingBrands }
				brands={ brands }
				fetchingColors={ fetchingColors }
				colors={ colors }
				fetchProducts={ this.fetchProducts }
				activateProduct={ this.activateProduct }
			/>
		);
	}
}

const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['products', 'fetching']),
	products: state.application.getIn(['products', 'data']),
	count: state.application.getIn(['products', 'count']),
	pages: state.application.getIn(['products', 'pages']),
	current: state.application.getIn(['products', 'current']),
	fetchingBrands: state.application.getIn(['brands', 'fetching']),
	brands: state.application.getIn(['brands', 'data']),
	fetchingColors: state.application.getIn(['colors', 'fetching']),
	colors: state.application.getIn(['colors', 'data']),
	hierarchy: state.application.getIn(['documents', 'hierarchy'])
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchProductsAction,
	fetchBrandsAction,
	fetchColorsAction,
	activateProductAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(Products)
);
