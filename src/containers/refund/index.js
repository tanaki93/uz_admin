import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import RefundComponent from '../../components/refund/company';
import {
	fetchRefunds as fetchRefundsAction,
	changeRefundStatus as changeRefundStatusAction
} from '../../actions/creators/refund';

class ProductDelivery extends Component {
	componentDidMount() {
		this.fetchRefunds();
	}

	fetchRefunds = () => {
		this.props.fetchRefundsAction();
	};

	changeRefundStatus = (refundId, status) => {
		this.props.changeRefundStatusAction(refundId, status).then(() => this.fetchRefunds());
	};

	render() {
		const { fetching, data } = this.props;

		return (
			<RefundComponent data={ data.filter(item => item.refund_type === 1) } fetching={ fetching }
			                 changeRefundStatus={ this.changeRefundStatus }/>
		);
	}
}

const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['refund', 'fetching']),
	data: state.application.getIn(['refund', 'data'])
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchRefundsAction,
	changeRefundStatusAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(ProductDelivery)
);