import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import LogisticsComponent from '../../components/logistics/list';
import {
	fetchLogistics as fetchLogisticsAction
} from '../../actions/creators/logistics';

class Logistics extends Component {
	componentDidMount() {
		this.fetchLogistics();
	}

	fetchLogistics = (query) => {
		this.props.fetchLogisticsAction(query);
	};

	render() {
		const { fetching, data } = this.props;
		return (
			<LogisticsComponent
				fetching={ fetching }
				data={ data }
				fetchLogistics={ this.fetchLogistics }
			/>
		);
	}
}

const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['logistics', 'fetching']),
	data: state.application.getIn(['logistics', 'data'])
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchLogisticsAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(Logistics)
);