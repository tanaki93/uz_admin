import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import LogisticsManagementComponent from '../../components/logistics/item';
import {
	fetchLogisticsOrder as fetchLogisticsOrderAction
} from '../../actions/creators/logistics';
import {
	changeLogisticsStatus as changeLogisticsStatusAction,
	createDeliveryStatus as createDeliveryStatusAction
} from '../../actions/creators/logistics-management';

class LogisticsManagement extends Component {
	componentDidMount() {
		const orderId = this.props.match.params['id'];

		this.fetchLogisticsOrder(orderId);
	}

	fetchLogisticsOrder = (orderId) => {
		this.props.fetchLogisticsOrderAction(orderId);
	};

	changeLogisticsStatus = (productId, option, value) => {
		const orderId = this.props.match.params['id'];
		this.props.changeLogisticsStatusAction(productId, option, value).then(() => this.fetchLogisticsOrder(orderId));
	};

	createDeliveryStatus = (productId, shippingService) => {
		const orderId = this.props.match.params['id'];
		this.props.createDeliveryStatusAction(productId, shippingService).then(() => this.fetchLogisticsOrder(orderId));
	};

	render() {
		const { fetching, data } = this.props;

		return (
			<LogisticsManagementComponent
				fetching={ fetching }
				data={ data }
				changeLogisticsStatus={ this.changeLogisticsStatus }
				createDeliveryStatus={ this.createDeliveryStatus }
			/>
		);
	}
}

const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['logisticsManagement', 'fetching']),
	data: state.application.getIn(['logisticsManagement', 'data'])
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchLogisticsOrderAction,
	changeLogisticsStatusAction,
	createDeliveryStatusAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(LogisticsManagement)
);