import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
	fetchSlider as fetchSliderAction,
	createSlider as createSliderAction,
	editSlider as editSliderAction
} from '../../../actions/creators/slider';
import Slider from '../../../components/landing/slider';

const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['slider', 'fetching']),
	data: state.application.getIn(['slider', 'data'])
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchSliderAction,
	createSliderAction,
	editSliderAction
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Slider);