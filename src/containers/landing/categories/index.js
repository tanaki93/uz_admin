import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
	fetchLandingCategories as fetchLandingCategoriesAction,
	updateLandingCategory as updateLandingCategoryAction,
	updateLandingSubcategory as updateLandingSubcategoryAction
} from '../../../actions/creators/landingCategories';
import Categories from '../../../components/landing/categories';

const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['landingCategories', 'fetching']),
	data: state.application.getIn(['landingCategories', 'data'])
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchLandingCategoriesAction,
	updateLandingCategoryAction,
	updateLandingSubcategoryAction
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Categories);