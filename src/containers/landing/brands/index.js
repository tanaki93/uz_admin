import React, { useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Component from 'components/landing/brands';
import {
	fetchLandingBrands as fetchLandingBrandsAction,
	updateLandingBrand as updateLandingBrandAction
} from 'actions/creators/landing';

const LandingBrands = (props) => {

	const { fetchLandingBrandsAction, updateLandingBrandAction, fetching, data } = props;

	useEffect(() => {
		fetchBrands();
	}, [fetchLandingBrandsAction]);

	const fetchBrands = () => {
		fetchLandingBrandsAction();
	};

	const updateLandingBrand = (brandId, data) => {
		updateLandingBrandAction(brandId, data).then(() => fetchBrands());
	};

	return (
		<Component data={ data } fetching={ fetching } update={ updateLandingBrand }/>
	);
};

const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['landingBrands', 'fetching']),
	data: state.application.getIn(['landingBrands', 'data'])
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchLandingBrandsAction,
	updateLandingBrandAction
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(LandingBrands);