import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import CurrencyComponent from '../../../components/settings/currencies';
import {
	fetchCurrencies as fetchCurrenciesAction,
	createCurrency as createCurrencyAction,
	editCurrency as editCurrencyAction,
	deleteCurrency as deleteCurrencyAction,
	fetchExchanges as fetchExchangesAction,
	createExchange as createExchangeAction,
	editExchange as editExchangeAction
} from '../../../actions/creators/currencies';

class Currencies extends Component {
	componentDidMount() {
		this.fetchCurrencies();
	}

	fetchCurrencies = () => {
		this.props.fetchCurrenciesAction().then(() => this.fetchExchanges());
	};

	deleteCurrency = (id) => {
		this.props.deleteCurrencyAction(id).then(() => this.fetchCurrencies());
	};

	createCurrency = (data) => {
		this.props.createCurrencyAction(data).then(() => this.fetchCurrencies());
	};

	editCurrency = (id, data) => {
		this.props.editCurrencyAction(id, data).then(() => this.fetchCurrencies());
	};

	fetchExchanges = () => {
		this.props.fetchExchangesAction();
	};

	createExchange = (data) => {
		this.props.createExchangeAction(data).then(() => this.fetchExchanges());
	};

	editExchange = (id, data) => {
		this.props.editExchangeAction(id, data).then(() => this.fetchExchanges());
	};

	render() {
		const { fetching, data, fetchingExchanges, dataExchanges } = this.props;

		return (
			<CurrencyComponent
				createCurrency={ this.createCurrency }
				editCurrency={ this.editCurrency }
				deleteCurrency={ this.deleteCurrency }
				fetchExchanges={ this.fetchExchanges }
				createExchange={ this.createExchange }
				editExchange={ this.editExchange }
				fetching={ fetching }
				data={ data }
				fetchingExchanges={ fetchingExchanges }
				dataExchanges={ dataExchanges }
			/>
		);
	}
}

const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['currencies', 'fetching']),
	data: state.application.getIn(['currencies', 'data']),
	fetchingExchanges: state.application.getIn(['exchanges', 'fetching']),
	dataExchanges: state.application.getIn(['exchanges', 'data']),
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchCurrenciesAction,
	createCurrencyAction,
	editCurrencyAction,
	deleteCurrencyAction,
	fetchExchangesAction,
	createExchangeAction,
	editExchangeAction,
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(Currencies)
);
