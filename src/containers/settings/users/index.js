import React, { useEffect } from 'react';
import {
	fetchUsers as fetchUsersAction,
	createUser as createUserAction,
	deleteUser as deleteUserAction,
	editUser as editUserAction
} from 'actions/creators/users';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import UsersComponent from 'components/settings/users/list';

const Users = (props) => {
	const { fetching, data, createUserAction, deleteUserAction, editUserAction, fetchUsersAction } = props;

	useEffect(() => {
		fetchUsers();
	}, []);

	const createUser = (data) => {
		createUserAction(data).then(() => fetchUsers());
	};

	const deleteUser = (id) => {
		deleteUserAction(id).then(() => fetchUsers());
	};

	const editUser = (id, data) => {
		editUserAction(id, data).then(() => fetchUsers());
	};

	const fetchUsers = () => {
		fetchUsersAction();
	};

	return (
		<UsersComponent
			fetching={ fetching } data={ data }
			createUser={ createUser }
			deleteUser={ deleteUser }
			editUser={ editUser }
		/>
	);
};

const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['users', 'fetching']),
	data: state.application.getIn(['users', 'data']),
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchUsersAction,
	createUserAction,
	deleteUserAction,
	editUserAction
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Users);