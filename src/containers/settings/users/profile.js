import React, { useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getUserProfile as getUserProfileAction } from 'actions/creators/profile';
import ProfileComponent from 'components/settings/users/profile';

const Profile = (props) => {
	const { match, fetching, data, orders, getUserProfileAction } = props;

	useEffect(() => {
		getUserProfile(match.params['userId']);
	}, [match]);

	const getUserProfile = (userId) => {
		getUserProfileAction(userId);
	};

	return (
		<ProfileComponent
			fetching={ fetching } data={ data } orders={ orders }
		/>
	);
};

const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['profile', 'fetching']),
	data: state.application.getIn(['profile', 'data']),
	orders: state.application.getIn(['profile', 'orders']),
});

const mapDispatchToProps = dispatch => bindActionCreators({
	getUserProfileAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(Profile)
);
