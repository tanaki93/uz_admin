import React, { useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Component from '../../../components/settings/points';
import {
	fetchCities as fetchCitiesAction
} from '../../../actions/creators/cities';
import {
	fetchPoints as fetchPointsAction,
	createPoint as createPointAction,
	editPoint as editPointAction,
	deletePoint as deletePointAction
} from '../../../actions/creators/points';

const Points = (props) => {
	const {
		fetching, data, cities,
		fetchCitiesAction, fetchPointsAction, createPointAction, editPointAction, deletePointAction
	} = props;

	useEffect(() => {
		fetchCities();
		fetchPoints();
	}, []);

	const fetchCities = () => {
		fetchCitiesAction();
	};

	const fetchPoints = () => {
		fetchPointsAction();
	};

	const createPoint = (data) => {
		createPointAction(data).then(() => fetchPoints());
	};

	const editPoint = (id, data) => {
		editPointAction(id, data).then(() => fetchPoints());
	};

	const deletePoint = (id) => {
		deletePointAction(id).then(() => fetchPoints());
	};

	return (
		<Component
			fetching={ fetching } data={ data } cities={ cities }
			createPoint={ createPoint }
			editPoint={ editPoint }
			deletePoint={ deletePoint }
		/>
	);
};

const mapStateToProps = (state) => ({
	cities: state.application.getIn(['cities', 'data']),
	fetching: state.application.getIn(['points', 'fetching']),
	data: state.application.getIn(['points', 'data'])
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchCitiesAction,
	fetchPointsAction,
	createPointAction,
	editPointAction,
	deletePointAction
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Points);
