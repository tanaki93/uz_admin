import React, { useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import CountryComponent from '../../../components/settings/countries';
import {
	fetchCountries as fetchCountriesAction,
	createCountry as createCountryAction,
	editCountry as editCountryAction,
	deleteCountry as deleteCountryAction
} from '../../../actions/creators/countries';
import {
	fetchCurrencies as fetchCurrenciesAction,
	createCurrency as createCurrencyAction,
} from '../../../actions/creators/currencies';
import {
	fetchLanguages as fetchLanguagesAction,
	createLanguage as createLanguageAction,
} from '../../../actions/creators/languages';

const Countries = (props) => {
	const {
		fetching, data, currencies, languages,
		fetchCountriesAction, createCountryAction, editCountryAction, fetchCurrenciesAction, fetchLanguagesAction,
		createLanguageAction, createCurrencyAction, deleteCountryAction
	} = props;

	useEffect(() => {
		fetchCountries();
		fetchCurrencies();
		fetchLanguages();
	}, []);

	const fetchCurrencies = () => {
		fetchCurrenciesAction();
	};

	const fetchLanguages = () => {
		fetchLanguagesAction();
	};

	const fetchCountries = () => {
		fetchCountriesAction();
	};

	const deleteCountry = (id) => {
		deleteCountryAction(id).then(() => fetchCountries());
	};

	const createLanguage = (data) => {
		createLanguageAction(data).then(() => fetchLanguages());
	};

	const createCurrency = (data) => {
		createCurrencyAction(data).then(() => fetchCurrencies());
	};

	const createCountry = (data) => {
		createCountryAction(data).then(() => fetchCountries());
	};

	const editCountry = (id, data) => {
		editCountryAction(id, data).then(() => fetchCountries());
	};

	return (
		<CountryComponent
			fetching={ fetching } languages={ languages } currencies={ currencies } data={ data }
			deleteCountry={ deleteCountry }
			createLanguage={ createLanguage }
			createCountry={ createCountry }
			createCurrency={ createCurrency }
			editCountry={ editCountry }
		/>
	);
};

const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['countries', 'fetching']),
	data: state.application.getIn(['countries', 'data']),
	currencies: state.application.getIn(['currencies', 'data']),
	languages: state.application.getIn(['languages', 'data']),
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchCountriesAction,
	createCountryAction,
	editCountryAction,
	fetchCurrenciesAction,
	fetchLanguagesAction,
	createLanguageAction,
	createCurrencyAction,
	deleteCountryAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(Countries)
);
