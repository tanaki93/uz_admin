import React, { useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Component from '../../../components/settings/cities';
import {
	fetchCountries as fetchCountriesAction
} from '../../../actions/creators/countries';
import {
	fetchCities as fetchCitiesAction,
	createCity as createCityAction,
	editCity as editCityAction,
	deleteCity as deleteCityAction,
} from '../../../actions/creators/cities';

const Cities = (props) => {
	const {
		fetching, data, countries,
		fetchCountriesAction, fetchCitiesAction, createCityAction, editCityAction, deleteCityAction
	} = props;

	useEffect(() => {
		fetchCountries();
		fetchCities();
	}, []);

	const fetchCountries = () => {
		fetchCountriesAction();
	};

	const fetchCities = () => {
		fetchCitiesAction();
	};

	const createCity = (data) => {
		createCityAction(data).then(() => fetchCities());
	};

	const editCity = (id, data) => {
		editCityAction(id, data).then(() => fetchCities());
	};

	const deleteCity = (id) => {
		deleteCityAction(id).then(() => fetchCities());
	};

	return (
		<Component
			fetching={ fetching } data={ data } countries={ countries }
			createCity={ createCity }
			editCity={ editCity }
			deleteCity={ deleteCity }
		/>
	);
};

const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['cities', 'fetching']),
	data: state.application.getIn(['cities', 'data']),
	countries: state.application.getIn(['countries', 'data'])
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchCountriesAction,
	fetchCitiesAction,
	createCityAction,
	editCityAction,
	deleteCityAction
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Cities);
