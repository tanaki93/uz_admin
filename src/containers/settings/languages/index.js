import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
	fetchLanguages as fetchLanguagesAction,
	createLanguage as createLanguageAction,
	editLanguage as editLanguageAction,
	deleteLanguage as deleteLanguageAction
} from '../../../actions/creators/languages';
import LanguagesComponent from '../../../components/settings/languages';

class Languages extends Component {
	componentDidMount() {
		this.fetchLanguages();
	}

	fetchLanguages = () => {
		this.props.fetchLanguagesAction();
	};

	createLanguage = (data) => {
		this.props.createLanguageAction(data).then(() => this.fetchLanguages());
	};

	editLanguage = (id, data) => {
		this.props.editLanguageAction(id, data).then(() => this.fetchLanguages());
	};

	deleteLanguage = (id) => {
		this.props.deleteLanguageAction(id).then(() => this.fetchLanguages());
	};

	render() {
		const { fetching, data } = this.props;

		return (
			<>
				<LanguagesComponent fetching={ fetching } data={ data } createLanguage={ this.createLanguage }
				                    editLanguage={ this.editLanguage } deleteLanguage={ this.deleteLanguage }/>
			</>
		);
	}
}

const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['languages', 'fetching']),
	data: state.application.getIn(['languages', 'data']),
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchLanguagesAction,
	createLanguageAction,
	editLanguageAction,
	deleteLanguageAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(Languages)
);
