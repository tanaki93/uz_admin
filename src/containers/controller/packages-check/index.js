import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PackagesCheckComponent from 'components/controller/packages-check';
import {
	fetchPackages as fetchPackagesAction,
	changePackageStatus as changePackageStatusAction
} from 'actions/creators/packages-check';

class PackagesCheck extends Component {
	componentDidMount() {
		this.fetchPackages();
	}

	fetchPackages = (query) => {
		this.props.fetchPackagesAction(query);
	};

	changePackageStatus = (packageId, status) => {
		this.props.changePackageStatusAction(packageId, status).then(() => this.fetchPackages());
	};

	render() {
		const { fetching, data } = this.props;

		return (
			<PackagesCheckComponent
				fetching={ fetching }
				data={ data }
				fetchPackages={ this.fetchPackages }
				changePackageStatus={ this.changePackageStatus }
			/>
		);
	}
}


const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['packagesCheck', 'fetching']),
	data: state.application.getIn(['packagesCheck', 'data'])
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchPackagesAction,
	changePackageStatusAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(PackagesCheck)
);