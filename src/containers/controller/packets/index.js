import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PacketsComponent from 'components/controller/packets';
import {
	fetchPackets as fetchPacketsAction,
	changePacketStatus as changePacketStatusAction,
	createFlight as createFlightAction,
} from 'actions/creators/packets';

class Packets extends Component {
	componentDidMount() {
		this.fetchPackets();
	}

	fetchPackets = (query) => {
		this.props.fetchPacketsAction(query);
	};

	changePacketStatus = (packetId, status) => {
		this.props.changePacketStatusAction(packetId, status).then(() => this.fetchPackets());
	};

	createFlight = (packetId, flightNo, money) => {
		this.props.createFlightAction(packetId, flightNo, money).then(() => this.fetchPackets());
	};

	render() {
		const { fetching, data } = this.props;

		return (
			<PacketsComponent
				fetching={ fetching }
				data={ data }
				fetchPackets={ this.fetchPackets }
				changePacketStatus={ this.changePacketStatus }
				createFlight={ this.createFlight }
			/>
		);
	}
}

const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['packets', 'fetching']),
	data: state.application.getIn(['packets', 'data'])
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchPacketsAction,
	changePacketStatusAction,
	createFlightAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(Packets)
);