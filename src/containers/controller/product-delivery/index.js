import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ProductDeliveryComponent from 'components/controller/product-delivery';
import {
	fetchDeliveryProducts as fetchDeliveryProductsAction,
	createPackage as createPackageAction
} from 'actions/creators/product-delivery';

class ProductDelivery extends Component {
	componentDidMount() {
		this.fetchDeliveryProducts();
	}

	fetchDeliveryProducts = (query) => {
		this.props.fetchDeliveryProductsAction(query);
	};

	createPackage = (order_products, weight, money) => {
		this.props.createPackageAction(order_products, weight, money).then(() => this.fetchDeliveryProducts());
	};

	render() {
		const { fetching, data } = this.props;

		return (
			<ProductDeliveryComponent
				fetching={ fetching }
				data={ data }
				fetchDeliveryProducts={ this.fetchDeliveryProducts }
				createPackage={ this.createPackage }
			/>
		);
	}
}

const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['productDelivery', 'fetching']),
	data: state.application.getIn(['productDelivery', 'data'])
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchDeliveryProductsAction,
	createPackageAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(ProductDelivery)
);