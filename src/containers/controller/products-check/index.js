import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ProductsCheckComponent from 'components/controller/products-check';
import {
	fetchProductsToCheck as fetchProductsToCheckAction,
	changeProductStatus as changeProductStatusAction,
	setProductComment as setProductCommentAction
} from 'actions/creators/products-check';

class ProductsCheck extends Component {
	componentDidMount() {
		this.fetchProductsToCheck();
	}

	fetchProductsToCheck = (query) => {
		this.props.fetchProductsToCheckAction(query);
	};

	changeProductStatus = (productId, status) => {
		this.props.changeProductStatusAction(productId, status);
	};

	setProductComment = (productId, comment) => {
		this.props.setProductCommentAction(productId, comment);
	};

	render() {
		const { fetching, data } = this.props;

		return (
			<ProductsCheckComponent
				fetchProductsToCheck={ this.fetchProductsToCheck }
				changeProductStatus={ this.changeProductStatus }
				setProductComment={ this.setProductComment }
				fetching={ fetching }
				data={ data }
			/>
		);
	}
}

const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['productsCheck', 'fetching']),
	data: state.application.getIn(['productsCheck', 'data'])
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchProductsToCheckAction,
	changeProductStatusAction,
	setProductCommentAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(ProductsCheck)
);
