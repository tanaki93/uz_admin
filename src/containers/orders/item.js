import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import OrderDetailsComponent from '../../components/orders/item';
import {
	fetchOrder as fetchOrderAction,
	changeProductStatus as changeProductStatusAction,
	changeStage as changeStageAction,
} from '../../actions/creators/order-details';
import {
	changeOrderStatus as changeOrderStatusAction
} from '../../actions/creators/orders';

class OrderDetails extends Component {
	componentDidMount() {
		const orderId = this.props.match.params['orderId'];
		this.fetchOrder(orderId);
	}

	fetchOrder = (orderId) => {
		this.props.fetchOrderAction(orderId);
	};

	changeOrderStatus = (orderId, status) => {
		this.props.changeOrderStatusAction(orderId, status).then(() => this.props.history.push('/orders'));
	};

	changeProductStatus = (productId, option, value) => {
		const orderId = this.props.match.params['orderId'];
		this.props.changeProductStatusAction(productId, option, value).then(() => this.fetchOrder(orderId));
	};

	changeStage = (productId, stage) => {
		const orderId = this.props.match.params['orderId'];
		this.props.changeStageAction(productId, stage).then(() => this.fetchOrder(orderId));
	};


	render() {
		const { fetching, data } = this.props;

		return (
			<OrderDetailsComponent
				fetching={ fetching }
				data={ data }
				changeProductStatus={ this.changeProductStatus }
				changeOrderStatus={ this.changeOrderStatus }
				changeStage={ this.changeStage }
			/>
		);
	}
}

const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['orderDetails', 'fetching']),
	data: state.application.getIn(['orderDetails', 'data'])
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchOrderAction,
	changeProductStatusAction,
	changeStageAction,
	changeOrderStatusAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(OrderDetails)
);