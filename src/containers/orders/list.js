import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import OrdersComponent from '../../components/orders/list';
import {
	fetchOrders as fetchOrdersAction,
	fetchClients as fetchClientsAction,
	assignMyself as assignMyselfAction
} from '../../actions/creators/orders';

class Orders extends Component {
	componentDidMount() {
		this.fetchOrders();
		this.fetchClients();
	}

	fetchOrders = (query) => {
		this.props.fetchOrdersAction(query);
	};

	fetchClients = () => {
		this.props.fetchClientsAction();
	};

	assignMyself = (orderId) => {
		this.props.assignMyselfAction(orderId).then(() => this.fetchOrders());
	};

	render() {
		const {
			fetching, data, fetchingClients, clients
		} = this.props;

		return (
			<OrdersComponent
				fetching={ fetching }
				data={ data }
				fetchingClients={ fetchingClients }
				clients={ clients }
				fetchOrders={ this.fetchOrders }
				assignMyself={ this.assignMyself }
			/>
		);
	}
}

const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['orders', 'fetching']),
	data: state.application.getIn(['orders', 'data']),
	fetchingClients: state.application.getIn(['orders', 'fetchingClients']),
	clients: state.application.getIn(['orders', 'clients'])
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchOrdersAction,
	fetchClientsAction,
	assignMyselfAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(Orders)
);