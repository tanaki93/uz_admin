import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ReturnComponent from '../../components/orders/return';
import { fetchOrder as fetchOrderAction, refund as refundAction } from '../../actions/creators/order-details';

class OrderReturn extends Component {
	componentDidMount() {
		const orderId = this.props.match.params['orderId'];
		this.fetchOrder(orderId);
	}

	fetchOrder = (orderId) => {
		this.props.fetchOrderAction(orderId);
	};

	refund = (data) => {
		this.props.refundAction(data).then(this.props.history.push('/refund'));
	};

	render() {
		const { fetching, data } = this.props;

		return (
			<ReturnComponent
				fetching={ fetching }
				data={ data }
				refund={ this.refund }
			/>
		);
	}
}

const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['orderDetails', 'fetching']),
	data: state.application.getIn(['orderDetails', 'data'])
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchOrderAction,
	refundAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(OrderReturn)
);