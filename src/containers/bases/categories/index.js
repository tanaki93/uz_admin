import React from 'react';
import { withRouter } from 'react-router-dom';
import {
	fetchCategories as fetchCategoriesAction,
	createCategory as createCategoryAction,
	editCategory as editCategoryAction,
	deleteCategory as deleteCategoryAction,
	uploadCategoryImage as uploadCategoryImageAction
} from 'actions/creators/categories-base';
import {
	fetchLanguages as fetchLanguagesAction
} from 'actions/creators/languages';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import CategoriesComponent from 'components/bases/categories';

class CategoriesBase extends React.Component {
	componentDidMount() {
		this.fetchCategories();
		this.fetchLanguages();
	}

	fetchCategories = () => {
		this.props.fetchCategoriesAction(this.props.match.params.parentId);
	};

	fetchLanguages = () => {
		this.props.fetchLanguagesAction();
	};

	createCategory = (id, data) => {
		this.props.createCategoryAction(id, data).then(() => this.fetchCategories());
	};

	editCategory = (id, data) => {
		this.props.editCategoryAction(id, data).then(() => this.fetchCategories());
	};

	deleteCategory = (id) => {
		this.props.deleteCategoryAction(id).then(() => this.fetchCategories());
	};

	uploadCategoryImage = (id, data) => {
		this.props.uploadCategoryImageAction(id, data);
	};

	render() {
		const { fetching, data, languages } = this.props;
		const parentId = this.props.match.params.parentId;
		const departmentId = this.props.match.params.departmentId;

		return (
			<CategoriesComponent
				createCategory={ this.createCategory }
				editCategory={ this.editCategory }
				deleteCategory={ this.deleteCategory }
				uploadCategoryImage={ this.uploadCategoryImage }
				departmentId={ departmentId }
				parentId={ parentId }
				fetching={ fetching }
				data={ data }
				languages={ languages }
			/>
		);
	}
}


const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['categoriesBase', 'fetching']),
	data: state.application.getIn(['categoriesBase', 'data']),
	languages: state.application.getIn(['languages', 'data']),
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchCategoriesAction,
	createCategoryAction,
	editCategoryAction,
	fetchLanguagesAction,
	deleteCategoryAction,
	uploadCategoryImageAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(CategoriesBase)
);