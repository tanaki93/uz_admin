import React from 'react';
import { withRouter } from 'react-router-dom';
import {
	fetchDepartments as fetchDepartmentsAction,
	createDepartment as createDepartmentAction,
	editDepartment as editDepartmentAction,
	deleteDepartment as deleteDepartmentAction
} from 'actions/creators/departments-base';
import {
	fetchLanguages as fetchLanguagesAction
} from 'actions/creators/languages';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import DepartmentsComponent from 'components/bases/departments';

class DepartmentBase extends React.Component {
	componentDidMount() {
		this.fetchDepartments();
		this.fetchLanguages();
	}

	fetchDepartments = () => {
		this.props.fetchDepartmentsAction();
	};

	fetchLanguages = () => {
		this.props.fetchLanguagesAction();
	};

	createDepartmentBase = (data) => {
		this.props.createDepartmentAction(data).then(() => this.fetchDepartments());
	};

	editDepartmentBase = (id, data) => {
		this.props.editDepartmentAction(id, data).then(() => this.fetchDepartments());
	};

	deleteDepartment = (id) => {
		this.props.deleteDepartmentAction(id).then(() => this.fetchDepartments());
	};

	render() {
		const { fetching, data, languages } = this.props;

		return (
			<DepartmentsComponent
				createDepartment={ this.createDepartmentBase }
				editDepartment={ this.editDepartmentBase }
				deleteDepartment={ this.deleteDepartment }
				fetching={ fetching }
				data={ data }
				languages={ languages }
			/>
		);
	}
}


const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['departmentsBase', 'fetching']),
	data: state.application.getIn(['departmentsBase', 'data']),
	languages: state.application.getIn(['languages', 'data']),
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchDepartmentsAction,
	createDepartmentAction,
	editDepartmentAction,
	fetchLanguagesAction,
	deleteDepartmentAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(DepartmentBase)
);