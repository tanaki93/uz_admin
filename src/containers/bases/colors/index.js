import React from 'react';
import { withRouter } from 'react-router-dom';
import {
	fetchColors as fetchColorsAction,
	createColor as createColorAction,
	editColor as editColorAction,
	deleteColor as deleteColorAction,
} from 'actions/creators/colors-base';
import {
	fetchLanguages as fetchLanguagesAction
} from 'actions/creators/languages';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ColorsComponent from 'components/bases/colors';

class Colors extends React.Component {
	componentDidMount() {
		this.fetchColors();
		this.fetchLanguages();
	}

	fetchColors = () => {
		this.props.fetchColorsAction('');
	};

	fetchLanguages = () => {
		this.props.fetchLanguagesAction();
	};

	createColor = (data) => {
		this.props.createColorAction(data).then(() => this.fetchColors());
	};

	editColor = (id, data) => {
		this.props.editColorAction(id, data).then(() => this.fetchColors());
	};

	deleteColor = (id) => {
		this.props.deleteColorAction(id).then(() => this.fetchColors());
	};

	render() {
		const { fetching, data, languages } = this.props;

		return (
			<ColorsComponent
				createColor={ this.createColor }
				editColor={ this.editColor }
				deleteColor={ this.deleteColor }
				fetching={ fetching }
				data={ data }
				languages={ languages }
			/>
		);
	}
}


const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['colorsBase', 'fetching']),
	data: state.application.getIn(['colorsBase', 'data']),
	languages: state.application.getIn(['languages', 'data']),
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchColorsAction,
	createColorAction,
	editColorAction,
	deleteColorAction,
	fetchLanguagesAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(Colors)
);