import React from 'react';
import { withRouter } from 'react-router-dom';
import {
	fetchClasses as fetchClassesAction,
	createClass as createClassAction,
	editClass as editClassAction,
	deleteClass as deleteClassAction
} from 'actions/creators/classes-base';
import {
	fetchLanguages as fetchLanguagesAction
} from 'actions/creators/languages';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ClassesComponent from 'components/bases/classes';

class ClassesBase extends React.Component {
	componentDidMount() {
		this.fetchClasses();
		this.fetchLanguages();
	}

	fetchClasses = () => {
		this.props.fetchClassesAction(this.props.match.params.departmentId);
	};

	fetchLanguages = () => {
		this.props.fetchLanguagesAction();
	};

	createClass = (id, data) => {
		this.props.createClassAction(id, data).then(() => this.fetchClasses());
	};

	editClass = (id, data) => {
		this.props.editClassAction(id, data).then(() => this.fetchClasses());
	};

	deleteClass = (id) => {
		this.props.deleteClassAction(id).then(() => this.fetchClasses());
	};

	render() {
		const { fetching, data, languages } = this.props;
		const departmentId = this.props.match.params.departmentId;

		return (
			<ClassesComponent
				createClass={ this.createClass }
				editClass={ this.editClass }
				deleteClass={ this.deleteClass }
				departmentId={ departmentId }
				fetching={ fetching }
				data={ data }
				languages={ languages }
			/>
		);
	}
}


const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['classesBase', 'fetching']),
	data: state.application.getIn(['classesBase', 'data']),
	languages: state.application.getIn(['languages', 'data']),
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchClassesAction,
	createClassAction,
	editClassAction,
	fetchLanguagesAction,
	deleteClassAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(ClassesBase)
);