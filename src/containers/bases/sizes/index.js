import React from 'react';
import { withRouter } from 'react-router-dom';
import {
	fetchSizes as fetchSizesAction,
	createSize as createSizeAction,
	deleteSize as deleteSizeAction,
	editSize as editSizeAction,
} from 'actions/creators/sizes-base';
import {
	fetchLanguages as fetchLanguagesAction
} from 'actions/creators/languages';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import SizesComponent from 'components/bases/sizes';

class Sizes extends React.Component {
	componentDidMount() {
		this.fetchSizes();
		this.fetchLanguages();
	}

	fetchSizes = () => {
		this.props.fetchSizesAction('');
	};

	fetchLanguages = () => {
		this.props.fetchLanguagesAction();
	};

	createSize = (data) => {
		this.props.createSizeAction(data).then(() => this.fetchSizes());
	};

	editSize = (id, data) => {
		this.props.editSizeAction(id, data).then(() => this.fetchSizes());
	};

	deleteSize = (id) => {
		this.props.deleteSizeAction(id).then(() => this.fetchSizes());
	};

	render() {
		const { fetching, data, languages } = this.props;

		return (
			<SizesComponent
				createSize={ this.createSize }
				editSize={ this.editSize }
				deleteSize={ this.deleteSize }
				fetching={ fetching }
				data={ data }
				languages={ languages }
			/>
		);
	}
}


const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['sizesBase', 'fetching']),
	data: state.application.getIn(['sizesBase', 'data']),
	languages: state.application.getIn(['languages', 'data']),
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchSizesAction,
	createSizeAction,
	deleteSizeAction,
	editSizeAction,
	fetchLanguagesAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(Sizes)
);