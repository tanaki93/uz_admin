import React from 'react';
import { withRouter } from 'react-router-dom';
import {
	fetchContents as fetchContentsAction,
	createContent as createContentAction,
	editContent as editContentAction,
	deleteContent as deleteContentAction
} from 'actions/creators/contents-base';
import {
	fetchLanguages as fetchLanguagesAction
} from 'actions/creators/languages';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ContentsComponent from 'components/bases/contents';

class ContentBase extends React.Component {
	componentDidMount() {
		this.fetchContents();
		this.fetchLanguages();
	}

	fetchContents = () => {
		this.props.fetchContentsAction();
	};

	fetchLanguages = () => {
		this.props.fetchLanguagesAction();
	};

	createContentBase = (data) => {
		this.props.createContentAction(data).then(() => this.fetchContents());
	};

	editContentBase = (id, data) => {
		this.props.editContentAction(id, data).then(() => this.fetchContents());
	};

	deleteContent = (id) => {
		this.props.deleteContentAction(id).then(() => this.fetchContents());
	};

	render() {
		const { fetching, data, languages } = this.props;

		return (
			<ContentsComponent
				createContent={ this.createContentBase }
				editContent={ this.editContentBase }
				deleteContent={ this.deleteContent }
				fetching={ fetching }
				data={ data }
				languages={ languages }
			/>
		);
	}
}


const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['contentsBase', 'fetching']),
	data: state.application.getIn(['contentsBase', 'data']),
	languages: state.application.getIn(['languages', 'data']),
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchContentsAction,
	createContentAction,
	editContentAction,
	fetchLanguagesAction,
	deleteContentAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(ContentBase)
);