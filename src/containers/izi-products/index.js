import React from 'react';
import { withRouter } from 'react-router-dom';
import {
	fetchProducts as fetchProductsAction,
	editProduct as editProductAction,
	editDescription as editDescriptionAction,
	activateProduct as activateProductAction,
	hitProduct as hitProductAction
} from './../../actions/creators/izi-products';
import {
	fetchContents as fetchContentsAction,
} from './../../actions/creators/contents-base';
import {
	fetchBrands as fetchBrandsAction
} from './../../actions/creators/brands-base';
import { fetchDepartments as fetchDepartmentsAction } from './../../actions/creators/departments';
import { fetchColors as fetchColorsAction } from './../../actions/creators/colors-base';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ProductsComponent from '../../components/izi-products';

class Products extends React.Component {
	componentDidMount() {
		this.fetchProducts('', '', '', '', 1);
		this.fetchDepartments();
		this.fetchColors();
		this.fetchContents();
		this.fetchBrands();
	}

	fetchProducts = (brandId, departmentId, categoryId, colourId, query, page) => {
		this.props.fetchProductsAction(brandId, departmentId, categoryId, colourId, query, page);
	};

	fetchBrands = () => {
		this.props.fetchBrandsAction();
	};

	editDescription = (productId, data) => {
		this.props.editDescriptionAction(productId, data).then(()=>this.fetchProducts('', '', '', '', '',1))
	};

	fetchDepartments = () => {
		this.props.fetchDepartmentsAction();
	};

	fetchColors = () => {
		this.props.fetchColorsAction();
	};

	fetchContents = () => {
		this.props.fetchContentsAction();
	};

	editProduct = (productId, option, id) => {
		this.props.editProductAction(productId, { option, id });
	};

	activateProduct = (id, active) => {
		this.props.activateProductAction(id, { is_sellable: active });
	};

	hitProduct = (id, active) => {
		this.props.hitProductAction(id, {is_hit: active})
	};

	render() {
		const {
			fetching, products, count, pages, current, hierarchy,
			fetchingDepartments, departments, fetchingColors, colors,
			fetchingContents, contents, fetchingBrands, brands
		} = this.props;

		return (
			<ProductsComponent
				fetching={ fetching }
				products={ products }
				count={ count }
				pages={ pages }
				current={ current }
				hierarchy={ hierarchy }
				fetchingDepartments={ fetchingDepartments }
				departments={ departments }
				fetchingContents={ fetchingContents }
				contents={ contents }
				fetchingColors={ fetchingColors }
				colors={ colors }
				fetchingBrands={ fetchingBrands }
				brands={ brands }
				fetchProducts={ this.fetchProducts }
				editProduct={ this.editProduct }
				hitProduct={ this.hitProduct }
				editDescription={ this.editDescription }
				activateProduct={ this.activateProduct }
			/>
		);
	}
}

const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['iziProducts', 'fetching']),
	products: state.application.getIn(['iziProducts', 'data']),
	count: state.application.getIn(['iziProducts', 'count']),
	pages: state.application.getIn(['iziProducts', 'pages']),
	current: state.application.getIn(['iziProducts', 'current']),
	fetchingDepartments: state.application.getIn(['departments', 'fetching']),
	departments: state.application.getIn(['departments', 'data']),
	fetchingColors: state.application.getIn(['colorsBase', 'fetching']),
	colors: state.application.getIn(['colorsBase', 'data']),
	fetchingContents: state.application.getIn(['contentsBase', 'fetching']),
	contents: state.application.getIn(['contentsBase', 'data']),
	fetchingBrands: state.application.getIn(['brandsBase', 'fetching']),
	brands: state.application.getIn(['brandsBase', 'data']),
	hierarchy: state.application.getIn(['documents', 'hierarchy'])
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchProductsAction,
	fetchDepartmentsAction,
	editProductAction,
	editDescriptionAction,
	fetchContentsAction,
	fetchColorsAction,
	activateProductAction,
	fetchBrandsAction,
	hitProductAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(Products)
);
