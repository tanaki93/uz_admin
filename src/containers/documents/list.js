import React, { useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { fetchDocuments as fetchDocumentsAction, setNew as setNewAction } from 'actions/creators/documents';
import { fetchBrands2 as fetchBrandsAction } from 'actions/creators/brands';
import { fetchUsers as fetchUsersAction } from 'actions/creators/users';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Component from 'components/documents/list';
import * as userTypes from 'constants/userType';

const Documents = (props) => {
	const { fetching, documents, fetchingBrands, brands, fetchingUsers, users, fetchDocumentsAction, fetchBrandsAction, fetchUsersAction, setNewAction } = props;

	useEffect(() => {
		fetchDocumentsAction();
		fetchBrandsAction();
		fetchUsersAction();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const fetchDocuments = (userId, brandId, from, to) => {
		fetchDocumentsAction(userId, brandId, from, to);
	};

	const setNew = (documentId, isNew) => {
		setNewAction(documentId, isNew);
	};

	return (
		<Component
			fetching={ fetching }
			fetchingBrands={ fetchingBrands }
			fetchingUsers={ fetchingUsers }
			documents={ documents }
			brands={ brands }
			users={ users }
			fetchDocuments={ fetchDocuments }
			setNew={ setNew }
		/>
	);
};

const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['documents', 'fetching']),
	documents: state.application.getIn(['documents', 'documents']),
	fetchingBrands: state.application.getIn(['brands', 'fetching']),
	brands: state.application.getIn(['brands', 'data']),
	fetchingUsers: state.application.getIn(['users', 'fetching']),
	users: state.application.getIn(['users', 'data']).filter(u => u.user_type < userTypes.CLIENT),
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchDocumentsAction,
	fetchBrandsAction,
	fetchUsersAction,
	setNewAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(Documents)
);