import React from 'react';
import { withRouter } from 'react-router-dom';
import {
	fetchProductsByCategory as fetchProductsAction,
	fetchDocumentHierarchy as fetchDocumentHierarchyAction,
	sendBack as sendBackAction,
	approve as approveAction,
} from 'actions/creators/documents';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import CheckComponent from 'components/documents/check';

class Check extends React.Component {
	componentDidMount() {
		const documentId = this.props.match.params['documentId'];

		this.fetchDocumentHierarchy(documentId);
		this.fetchProducts(documentId, {page: 1});
	}

	fetchDocumentHierarchy = (id) => {
		this.props.fetchDocumentHierarchyAction(id);
	};

	fetchProducts = (id, params) => {
		this.props.fetchProductsAction(id, params);
	};

	sendBack = (documentId, data) => {
		this.props.sendBackAction(documentId, { comment: data }).then(() => this.props.history.push('/'));
	};

	approve = (documentId) => {
		this.props.approveAction(documentId).then(() => this.props.history.push('/'));
	};

	render() {
		const { products, hierarchy, fetching } = this.props;
		const documentId = this.props.match.params['documentId'];

		return (
			<CheckComponent
				fetching={ fetching }
				documentId={ documentId }
				products={ products }
				hierarchy={ hierarchy }
				fetchProducts={ this.fetchProducts }
				sendBack={ this.sendBack }
				approve={ this.approve }
			/>
		);
	}
}


const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['documents', 'fetching']),
	products: state.application.getIn(['documents', 'products']),
	hierarchy: state.application.getIn(['documents', 'hierarchy']),
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchDocumentHierarchyAction,
	fetchProductsAction,
	sendBackAction,
	approveAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(Check)
);
