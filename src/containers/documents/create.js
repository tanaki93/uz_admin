import React, { useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
	fetchDocumentBrands as fetchDocumentBrandsAction,
	fetchOperators as fetchOperatorsAction,
	createDocuments as createDocumentsAction,
	fetchDocuments as fetchDocumentsAction
} from 'actions/creators/documents';
import Component from 'components/documents/create';

const Create = (props) => {
	const { fetching, brands, operators, fetchDocumentBrandsAction, fetchOperatorsAction, createDocumentsAction, fetchDocumentsAction } = props;

	useEffect(() => {
		fetchDocumentBrandsAction();
		fetchOperatorsAction();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const fetchDocuments = (userId, brandId, from, to) => {
		fetchDocumentsAction(userId, brandId, from, to);
	};

	const createDocuments = (data) => {
		createDocumentsAction(data).then(() => {
			fetchDocuments();
		});
	};

	return (
		<Component
			fetching={ fetching }
			brands={ brands }
			operators={ operators }
			createDocuments={ createDocuments }
		/>
	);
};

const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['documents', 'fetching']),
	brands: state.application.getIn(['documents', 'brands']),
	operators: state.application.getIn(['documents', 'operators']),
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchDocumentBrandsAction,
	fetchOperatorsAction,
	createDocumentsAction,
	fetchDocumentsAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(Create)
);