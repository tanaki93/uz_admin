import React from 'react';
import ContentsComponent from '../../../components/handbook/contents';

class Contents extends React.Component {

	render() {
		return (
			<ContentsComponent/>
		);
	}
}

export default Contents;