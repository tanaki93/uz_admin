import React from 'react';
import { withRouter } from 'react-router-dom';
import {
	fetchBrand as fetchBrandAction,
} from '../../../actions/creators/brands';
import {
	createDepartment as createDepartmentAction,
	editDepartment as editDepartmentAction,
	deleteDepartment as deleteDepartmentAction
} from '../../../actions/creators/departments';
import {
	createDepartment as createIziDepartmentAction,
	fetchDepartments as fetchDepartmentsAction
} from '../../../actions/creators/departments-base';
import {
	editCategory as editCategoryAction,
	fetchCategories as fetchCategoriesAction,
	deleteCategory as deleteCategoryAction,
	createCategory as createCategoryAction,
} from '../../../actions/creators/categories';
import {
	createCategory as createIziCategoryAction
} from '../../../actions/creators/categories-base';
import {
	createClass as createClassAction,
	fetchClasses as fetchClassesAction
} from '../../../actions/creators/classes-base';
import {
	fetchLanguages as fetchLanguagesAction
} from '../../../actions/creators/languages';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import DepartmentsComponent from '../../../components/handbook/departments';

class Departments extends React.Component {
	componentDidMount() {
		this.fetchBrand();
	}

	fetchBrand = () => {
		this.props.fetchBrandAction(this.props.match.params.brandId);
		this.fetchDepartments();
		this.fetchCategories();
		this.fetchLanguages();
	};

	editDepartment = (departmentId, data) => {
		this.props.editDepartmentAction(departmentId, data).then(() => this.fetchBrand());
	};

	editCategory = (categoryId, data) => {
		this.props.editCategoryAction(categoryId, data).then(() => this.fetchBrand());
	};

	fetchDepartments = () => {
		this.props.fetchDepartmentsAction();
	};

	assignDepartment = (departmentId, iziDepartmentId) => {
		this.props.createDepartmentAction(departmentId, { department_id: iziDepartmentId }).then(() => this.fetchBrand());
	};

	createDepartment = (departmentId, iziDepartmentName) => {
		this.props.createDepartmentAction(departmentId, { name: iziDepartmentName }).then(() => this.fetchBrand());
	};

	deleteDepartment = (categoryId) => {
		this.props.deleteDepartmentAction(categoryId).then(() => this.fetchBrand());
	};

	fetchCategories = () => {
		this.props.fetchCategoriesAction();
	};

	assignCategory = (categoryId, iziCategoryId) => {
		this.props.createCategoryAction(categoryId, { category_id: iziCategoryId }).then(() => this.fetchBrand());
	};

	createCategory = (categoryId, iziCategoryName) => {
		this.props.createCategoryAction(categoryId, { name: iziCategoryName }).then(() => this.fetchBrand());
	};

	fetchLanguages = () => {
		this.props.fetchLanguagesAction();
	};

	deleteCategory = (categoryId) => {
		this.props.deleteCategoryAction(categoryId).then(() => this.fetchBrand());
	};

	fetchClasses = (id) => {
		this.props.fetchClassesAction(id);
	};

	createIziDepartment = (data) => {
		this.props.createIziDepartmentAction(data).then(() => this.fetchDepartments());
	};

	createIziCategory = (id, data) => {
		this.props.createIziCategoryAction(id, data).then(() => this.fetchCategories());
	};

	createIziClass = (id, data) => {
		this.props.createClassAction(id, data).then(() => this.fetchClasses(id));
	};

	render() {
		const { fetching, brandDepartments, departmentsFetching, departments, categories, languages, parents } = this.props;

		return (
			<DepartmentsComponent
				fetching={ fetching }
				brandDepartments={ brandDepartments }
				departmentsFetching={ departmentsFetching }
				assignDepartment={ this.assignDepartment }
				createDepartment={ this.createDepartment }
				deleteDepartment={ this.deleteDepartment }
				editDepartment={ this.editDepartment }
				departments={ departments }
				languages={ languages }
				parents={ parents }
				categories={ categories }
				assignCategory={ this.assignCategory }
				createCategory={ this.createCategory }
				deleteCategory={ this.deleteCategory }
				editCategory={ this.editCategory }
				fetchClasses={ this.fetchClasses }
				createIziDepartment={ this.createIziDepartment }
				createIziCategory={ this.createIziCategory }
				createIziClass={ this.createIziClass }
			/>
		);
	}
}


const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['brands', 'fetching']),
	brandDepartments: state.application.getIn(['brands', 'brandDepartments']),
	departmentsFetching: state.application.getIn(['departmentsBase', 'fetching']),
	departments: state.application.getIn(['departmentsBase', 'data']),
	categoriesFetching: state.application.getIn(['categories', 'fetching']),
	categories: state.application.getIn(['categories', 'data']),
	parents: state.application.getIn(['classesBase', 'data']),
	languages: state.application.getIn(['languages', 'data']),
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchBrandAction,
	editDepartmentAction,
	fetchDepartmentsAction,
	createDepartmentAction,
	editCategoryAction,
	fetchCategoriesAction,
	createCategoryAction,
	deleteDepartmentAction,
	deleteCategoryAction,
	fetchClassesAction,
	fetchLanguagesAction,
	createIziDepartmentAction,
	createIziCategoryAction,
	createClassAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(Departments)
);
