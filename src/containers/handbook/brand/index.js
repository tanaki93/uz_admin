import React from 'react';
import { withRouter } from 'react-router-dom';
import {
	fetchBrands2 as fetchBrandsAction,
	refreshBrand as refreshBrandAction,
	createBrand as createBrandAction,
	editBrand as editBrandAction
} from '../../../actions/creators/brands';
import {
	fetchCurrencies as fetchCurrenciesAction
} from '../../../actions/creators/currencies';
import {
	fetchCountries as fetchCountriesAction
} from '../../../actions/creators/countries';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import BrandComponent from '../../../components/handbook/brand';

class Brand extends React.Component {
	componentDidMount() {
		this.fetchBrands();
		this.fetchCurrencies();
		this.fetchCountries();
	}

	fetchCountries = () => {
		this.props.fetchCountriesAction();
	};

	fetchBrands = () => {
		this.props.fetchBrandsAction();
	};

	fetchCurrencies = () => {
		this.props.fetchCurrenciesAction();
	};

	refreshBrand = (brandId) => {
		this.props.refreshBrandAction(brandId).then(() => this.fetchBrands());
	};

	createBrand = (data) => {
		this.props.createBrandAction(data).then(() => this.fetchBrands());
	};

	editBrand = (id, data) => {
		this.props.editBrandAction(id, data).then(() => this.fetchBrands());
	};

	render() {
		const { fetching, data, selected, brandRefresh, currencies, countries } = this.props;

		return (
			<BrandComponent
				refreshBrand={ this.refreshBrand }
				createBrand={ this.createBrand }
				fetchBrands={ this.fetchBrands }
				editBrand={ this.editBrand }
				fetching={ fetching }
				data={ data }
				selected={ selected }
				brandRefresh={ brandRefresh }
				currencies={ currencies }
				countries={ countries }
			/>
		);
	}
}


const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['brands', 'fetching']),
	data: state.application.getIn(['brands', 'data']),
	currencies: state.application.getIn(['currencies', 'data']),
	countries: state.application.getIn(['countries', 'data']),
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchBrandsAction,
	refreshBrandAction,
	createBrandAction,
	editBrandAction,
	fetchCurrenciesAction,
	fetchCountriesAction
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(Brand)
);