import React from 'react';
import { withRouter } from 'react-router-dom';
import {
	fetchColors as fetchColorsAction,
	createColor as createColorAction,
} from '../../../actions/creators/colors';
import {
	fetchColors as fetchIziColorsAction,
	createColor as createIziColorAction
} from '../../../actions/creators/colors-base';
import {
	fetchLanguages as fetchLanguagesAction
} from '../../../actions/creators/languages';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ColorsComponent from '../../../components/handbook/colors';

class Departments extends React.Component {
	componentDidMount() {
		this.fetchColors();
		this.fetchIziColors();
		this.fetchLanguages();
	}

	fetchColors = () => {
		this.props.fetchColorsAction();
	};

	fetchIziColors = () => {
		this.props.fetchIziColorsAction();
	};

	fetchLanguages = () => {
		this.props.fetchLanguagesAction();
	};

	createColor = (data) => {
		this.props.createIziColorAction(data).then(() => this.fetchIziColors());
	};

	assignColor = (colorId, iziColorId) => {
		this.props.createColorAction(colorId, { colour_id: iziColorId }).then(() => this.fetchColors());
	};

	render() {
		const { fetching, colors, iziColorsFetching, iziColors, languages } = this.props;

		return (
			<ColorsComponent fetching={ fetching } colors={ colors } iziColorsFetching={ iziColorsFetching }
			                 assignColor={ this.assignColor } createColor={ this.createColor }
			                 iziColors={ iziColors } languages={ languages }/>
		);
	}
}


const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['colors', 'fetching']),
	colors: state.application.getIn(['colors', 'data']),
	iziColorsFetching: state.application.getIn(['colorsBase', 'fetching']),
	iziColors: state.application.getIn(['colorsBase', 'data']),
	languages: state.application.getIn(['languages', 'data']),
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchColorsAction,
	fetchIziColorsAction,
	fetchLanguagesAction,
	createIziColorAction,
	createColorAction,
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(Departments)
);
