import React from 'react';
import { withRouter } from 'react-router-dom';
import {
	fetchSizes as fetchSizesAction,
	createSize as createSizeAction,
} from '../../../actions/creators/sizes';
import {
	fetchSizes as fetchIziSizesAction,
	createSize as createIziSizeAction
} from '../../../actions/creators/sizes-base';
import {
	fetchLanguages as fetchLanguagesAction
} from '../../../actions/creators/languages';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import SizesComponent from '../../../components/handbook/sizes';

class Departments extends React.Component {
	componentDidMount() {
		this.fetchSizes();
		this.fetchIziSizes();
		this.fetchLanguages();
	}

	fetchSizes = () => {
		this.props.fetchSizesAction();
	};

	fetchIziSizes = () => {
		this.props.fetchIziSizesAction();
	};

	fetchLanguages = () => {
		this.props.fetchLanguagesAction();
	};

	createSize = (data) => {
		this.props.createIziSizeAction(data).then(() => this.fetchIziSizes());
	};

	assignSize = (sizeId, iziSizeId) => {
		this.props.createSizeAction(sizeId, { size_id: iziSizeId }).then(() => this.fetchSizes());
	};

	render() {
		const { fetching, sizes, iziSizesFetching, iziSizes, languages } = this.props;

		return (
			<SizesComponent
				fetching={ fetching }
				sizes={ sizes } iziSizesFetching={ iziSizesFetching } assignSize={ this.assignSize }
				createSize={ this.createSize }
				iziSizes={ iziSizes } languages={ languages }
			/>
		);
	}
}


const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['sizes', 'fetching']),
	sizes: state.application.getIn(['sizes', 'data']),
	iziSizesFetching: state.application.getIn(['sizesBase', 'fetching']),
	iziSizes: state.application.getIn(['sizesBase', 'data']),
	languages: state.application.getIn(['languages', 'data']),
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchSizesAction,
	createSizeAction,
	fetchLanguagesAction,
	fetchIziSizesAction,
	createIziSizeAction,
}, dispatch);

export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(Departments)
);
