import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
	hitProduct as hitProductAction
} from './../../actions/creators/izi-products';
import {
	fetchMarketingOnly as fetchMarketingOnlyAction,
	setMarketing as setMarketingAction
} from './../../actions/creators/marketing';
import { fetchDepartments as fetchDepartmentsAction } from '../../actions/creators/departments';
import { fetchContents as fetchContentsAction } from '../../actions/creators/contents-base';
import { fetchColors as fetchColorsAction } from '../../actions/creators/colors-base';
import { fetchBrands as fetchBrandsAction } from '../../actions/creators/brands-base';
import ProductsComponent from '../../components/marketing';

const Marketing = (props) => {
	const {
		fetching, products, count, pages, current, fetchingDepartments,
		departments, fetchingColors, colors, fetchingContents, contents, fetchingBrands, brands, hierarchy,
		fetchMarketingOnlyAction, fetchDepartmentsAction, fetchContentsAction, fetchColorsAction, fetchBrandsAction,
		setMarketingAction, hitProductAction
	} = props;

	useEffect(() => {
		fetchProducts();
		fetchBrandsAction();
		fetchDepartmentsAction();
		fetchColorsAction();
		fetchContentsAction();
	}, []);

	const fetchProducts = (data) => {
		fetchMarketingOnlyAction(data);
	};

	const setMarketing = (productId, data) => {
		setMarketingAction(productId, data);
	};

	const hitProduct = (id, active) => {
		hitProductAction(id, {is_hit: active})
	};

	return (
		<ProductsComponent
			marketing
			fetching={ fetching }
			products={ products }
			count={ count }
			pages={ pages }
			current={ current }
			hierarchy={ hierarchy }
			fetchingDepartments={ fetchingDepartments }
			departments={ departments }
			fetchingContents={ fetchingContents }
			contents={ contents }
			fetchingColors={ fetchingColors }
			colors={ colors }
			fetchingBrands={ fetchingBrands }
			brands={ brands }
			fetchProducts={ fetchProducts }
			setMarketing={ setMarketing }
			hitProduct={ hitProduct }
		/>
	);
};


const mapStateToProps = (state) => ({
	fetching: state.application.getIn(['marketing', 'fetching']),
	products: state.application.getIn(['marketing', 'data']),
	count: state.application.getIn(['marketing', 'count']),
	pages: state.application.getIn(['marketing', 'pages']),
	current: state.application.getIn(['marketing', 'current']),
	fetchingDepartments: state.application.getIn(['departments', 'fetching']),
	departments: state.application.getIn(['departments', 'data']),
	fetchingColors: state.application.getIn(['colorsBase', 'fetching']),
	colors: state.application.getIn(['colorsBase', 'data']),
	fetchingContents: state.application.getIn(['contentsBase', 'fetching']),
	contents: state.application.getIn(['contentsBase', 'data']),
	fetchingBrands: state.application.getIn(['brandsBase', 'fetching']),
	brands: state.application.getIn(['brandsBase', 'data']),
	hierarchy: state.application.getIn(['documents', 'hierarchy'])
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchMarketingOnlyAction,
	fetchDepartmentsAction,
	fetchContentsAction,
	fetchColorsAction,
	fetchBrandsAction,
	setMarketingAction,
	hitProductAction
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Marketing);