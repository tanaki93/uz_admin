export const CHANGE_LOCALE = 'CHANGE_LOCALE';

export function changeLocale(data) {
	return { type: CHANGE_LOCALE, data: data };
}
