export const FETCH_DOC_BRANDS = 'FETCH_DOC_BRANDS';
export const SUCCESS_FETCH_DOC_BRANDS = 'SUCCESS_FETCH_DOC_BRANDS';
export const FAILURE_FETCH_DOC_BRANDS = 'FAILURE_FETCH_DOC_BRANDS';

export const FETCH_DOCS = 'FETCH_DOCS';
export const SUCCESS_FETCH_DOCS = 'SUCCESS_FETCH_DOCS';
export const FAILURE_FETCH_DOCS = 'FAILURE_FETCH_DOCS';

export const CREATE_DOC = 'CREATE_DOC';
export const SUCCESS_CREATE_DOC = 'SUCCESS_CREATE_DOC';
export const FAILURE_CREATE_DOC = 'FAILURE_CREATE_DOC';

export const FETCH_STATISTICS = 'FETCH_STATISTICS';
export const SUCCESS_FETCH_STATISTICS = 'SUCCESS_FETCH_STATISTICS';
export const FAILURE_FETCH_STATISTICS = 'FAILURE_FETCH_STATISTICS';

export const FETCH_OPERATORS = 'FETCH_OPERATORS';
export const SUCCESS_FETCH_OPERATORS = 'SUCCESS_FETCH_OPERATORS';
export const FAILURE_FETCH_OPERATORS = 'FAILURE_FETCH_OPERATORS';

export const FETCH_DOCUMENT_HIERARCHY = 'FETCH_DOCUMENT_HIERARCHY';
export const SUCCESS_FETCH_DOCUMENT_HIERARCHY = 'SUCCESS_FETCH_DOCUMENT_HIERARCHY';
export const FAILURE_FETCH_DOCUMENT_HIERARCHY = 'FAILURE_FETCH_DOCUMENT_HIERARCHY';

export const SEND_BACK = 'SEND_BACK';
export const SUCCESS_SEND_BACK = 'SUCCESS_SEND_BACK';
export const FAILURE_SEND_BACK = 'FAILURE_SEND_BACK';

export const APPROVE = 'APPROVE';
export const SUCCESS_APPROVE = 'SUCCESS_APPROVE';
export const FAILURE_APPROVE = 'FAILURE_APPROVE';

export const FETCH_PRODUCTS_BY_CATEGORY = 'FETCH_PRODUCTS_BY_CATEGORY';
export const SUCCESS_FETCH_PRODUCTS_BY_CATEGORY = 'SUCCESS_FETCH_PRODUCTS_BY_CATEGORY';
export const FAILURE_FETCH_PRODUCTS_BY_CATEGORY = 'FAILURE_FETCH_PRODUCTS_BY_CATEGORY';

export const SET_NEW = 'SET_NEW';
export const SUCCESS_SET_NEW = 'SUCCESS_SET_NEW';
export const FAILURE_SET_NEW = 'FAILURE_SET_NEW';