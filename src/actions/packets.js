export const FETCH_PACKETS = 'FETCH_PACKETS';
export const SUCCESS_FETCH_PACKETS = 'SUCCESS_FETCH_PACKETS';
export const FAILURE_FETCH_PACKETS = 'FAILURE_FETCH_PACKETS';

export const FETCH_PACKET = 'FETCH_PACKET';
export const SUCCESS_FETCH_PACKET = 'SUCCESS_FETCH_PACKET';
export const FAILURE_FETCH_PACKET = 'FAILURE_FETCH_PACKET';

export const CHANGE_PACKET_STATUS = 'CHANGE_PACKET_STATUS';
export const SUCCESS_CHANGE_PACKET_STATUS = 'SUCCESS_CHANGE_PACKET_STATUS';
export const FAILURE_CHANGE_PACKET_STATUS = 'FAILURE_CHANGE_PACKET_STATUS';

export const CREATE_FLIGHT = 'CREATE_FLIGHT';
export const SUCCESS_CREATE_FLIGHT = 'SUCCESS_CREATE_FLIGHT';
export const FAILURE_CREATE_FLIGHT = 'FAILURE_CREATE_FLIGHT';
