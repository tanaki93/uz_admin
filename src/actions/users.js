export const FETCH_USERS = 'FETCH_USERS';
export const SUCCESS_FETCH_USERS = 'SUCCESS_FETCH_USERS';
export const FAILURE_FETCH_USERS = 'FAILURE_FETCH_USERS';

export const CREATE_USER = 'CREATE_USER';
export const SUCCESS_CREATE_USER = 'SUCCESS_CREATE_USER';
export const FAILURE_CREATE_USER = 'FAILURE_CREATE_USER';

export const EDIT_USER = 'EDIT_USER';
export const SUCCESS_EDIT_USER = 'SUCCESS_EDIT_USER';
export const FAILURE_EDIT_USER = 'FAILURE_EDIT_USER';

export const DELETE_USER = 'DELETE_USER';
export const SUCCESS_DELETE_USER = 'SUCCESS_DELETE_USER';
export const FAILURE_DELETE_USER = 'FAILURE_DELETE_USER';
