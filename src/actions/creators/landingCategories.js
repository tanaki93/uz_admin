import * as Actions from '../landingCategories';
import { CALL_API } from '../../middleware/api';

export const fetchLandingCategories = () => {
	return (dispatch) => dispatch({
		type: Actions.FETCH_LANDING_CATEGORIES,
		[CALL_API]: {
			endpoint: `/api/v1/landing/categories/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_LANDING_CATEGORIES,
				Actions.FAILURE_FETCH_LANDING_CATEGORIES
			]
		}
	});
}

export const updateLandingCategory = (categoryId, data) => {
	return (dispatch) => dispatch({
		type: Actions.UPDATE_LANDING_CATEGORY,
		[CALL_API]: {
			endpoint: `/api/v1/landing/categories/${categoryId}/`,
			method: 'put',
			data,
			types: [
				Actions.SUCCESS_UPDATE_LANDING_CATEGORY,
				Actions.FAILURE_UPDATE_LANDING_CATEGORY
			]
		}
	});
}

export const updateLandingSubcategory = (categoryId, data) => {
	return (dispatch) => dispatch({
		type: Actions.UPDATE_LANDING_SUBCATEGORY,
		[CALL_API]: {
			endpoint: `/api/v1/landing/categories/${categoryId}/`,
			method: 'post',
			data,
			types: [
				Actions.SUCCESS_UPDATE_LANDING_SUBCATEGORY,
				Actions.FAILURE_UPDATE_LANDING_SUBCATEGORY
			]
		}
	});
}