import * as Actions from '../departments-base';
import { CALL_API } from '../../middleware/api';

export function fetchDepartments() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_DEPARTMENTS_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izi/departments/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_DEPARTMENTS_BASE,
				Actions.FAILURE_FETCH_DEPARTMENTS_BASE
			]
		}
	});
}

export function createDepartment(data) {
	return (dispatch) => dispatch({
		type: Actions.CREATE_DEPARTMENT_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/departments/`,
			method: 'post',
			types: [
				Actions.SUCCESS_CREATE_DEPARTMENT_BASE,
				Actions.FAILURE_CREATE_DEPARTMENT_BASE
			],
			data
		}
	});
}

export function fetchDepartment(department_id) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_DEPARTMENT_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/departments/${ department_id }/`,
			method: 'get',
			types: [
				Actions.FAILURE_FETCH_DEPARTMENT_BASE,
				Actions.SUCCESS_FETCH_DEPARTMENT_BASE
			]
		}
	});
}

export function deleteDepartment(department_id) {
	return (dispatch) => dispatch({
		type: Actions.DELETE_DEPARTMENT_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/departments/${ department_id }/`,
			method: 'delete',
			types: [
				Actions.FAILURE_DELETE_DEPARTMENT_BASE,
				Actions.SUCCESS_DELETE_DEPARTMENT_BASE
			]
		}
	});
}

export function editDepartment(department_id, data) {
	return (dispatch) => dispatch({
		type: Actions.EDIT_DEPARTMENT_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/departments/${ department_id }/`,
			method: 'put',
			types: [
				Actions.SUCCESS_EDIT_DEPARTMENT_BASE,
				Actions.FAILURE_EDIT_DEPARTMENT_BASE
			],
			data
		}
	});
}