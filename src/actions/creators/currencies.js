import * as Actions from '../currencies';
import { CALL_API } from '../../middleware/api';

export function fetchCurrencies() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_CURRENCIES,
		[CALL_API]: {
			endpoint: `/api/v1/admin/currencies/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_CURRENCIES,
				Actions.FAILURE_FETCH_CURRENCIES
			]
		}
	})
}

export function createCurrency(data) {
	return (dispatch) => dispatch({
		type: Actions.CREATE_CURRENCY,
		[CALL_API]: {
			endpoint: `/api/v1/admin/currencies/`,
			method: 'post',
			types: [
				Actions.SUCCESS_CREATE_CURRENCY,
				Actions.FAILURE_CREATE_CURRENCY
			],
			data
		}
	})
}

export function editCurrency(currencyId, data) {
	return (dispatch) => dispatch({
		type: Actions.EDIT_CURRENCY,
		[CALL_API]: {
			endpoint: `/api/v1/admin/currencies/${currencyId}/`,
			method: 'put',
			types: [
				Actions.SUCCESS_EDIT_CURRENCY,
				Actions.FAILURE_EDIT_CURRENCY
			],
			data
		}
	})
}

export function deleteCurrency(currencyId) {
	return (dispatch) => dispatch({
		type: Actions.EDIT_CURRENCY,
		[CALL_API]: {
			endpoint: `/api/v1/admin/currencies/${currencyId}/`,
			method: 'delete',
			types: [
				Actions.SUCCESS_EDIT_CURRENCY,
				Actions.FAILURE_EDIT_CURRENCY
			]
		}
	})
}



export function fetchExchanges() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_EXCHANGES,
		[CALL_API]: {
			endpoint: `/api/v1/admin/exchanges/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_EXCHANGES,
				Actions.FAILURE_FETCH_EXCHANGES
			]
		}
	})
}

export function createExchange(data) {
	return (dispatch) => dispatch({
		type: Actions.CREATE_EXCHANGE,
		[CALL_API]: {
			endpoint: `/api/v1/admin/exchanges/`,
			method: 'post',
			types: [
				Actions.SUCCESS_CREATE_EXCHANGE,
				Actions.FAILURE_CREATE_EXCHANGE
			],
			data
		}
	})
}

export function editExchange(exchangeId, data) {
	return (dispatch) => dispatch({
		type: Actions.EDIT_EXCHANGE,
		[CALL_API]: {
			endpoint: `/api/v1/admin/exchanges/${exchangeId}/`,
			method: 'put',
			types: [
				Actions.SUCCESS_EDIT_EXCHANGE,
				Actions.FAILURE_EDIT_EXCHANGE
			],
			data
		}
	})
}