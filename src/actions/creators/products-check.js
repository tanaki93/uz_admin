import * as Actions from '../products-check';
import { CALL_API } from '../../middleware/api';

export function fetchProductsToCheck(query) {
	let _query = '';
	let number = '';
	let dateFrom = '';
	let dateTo = '';

	if (query) {
		_query = query.query || '';
		number = query.number || '';
		dateFrom = query.dateFrom || '';
		dateTo = query.dateTo || '';
	}

	return (dispatch) => dispatch({
		type: Actions.FETCH_PRODUCTS_TO_CHECK,
		[CALL_API]: {
			endpoint: `/api/v1/manager/checking/product/?query=${ _query }&number=${ number }&date_from=${ dateFrom }&date_to=${ dateTo }`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_PRODUCTS_TO_CHECK,
				Actions.FAILURE_FETCH_PRODUCTS_TO_CHECK
			]
		}
	});
}

export function fetchProductToCheck(productId) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_PRODUCT_TO_CHECK,
		[CALL_API]: {
			endpoint: `/api/v1/manager/checking/product/${ productId }/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_PRODUCT_TO_CHECK,
				Actions.FAILURE_FETCH_PRODUCT_TO_CHECK
			]
		}
	});
}

export function changeProductStatus(productId, status) {
	return (dispatch) => dispatch({
		type: Actions.CHANGE_PRODUCT_STATUS,
		[CALL_API]: {
			endpoint: `/api/v1/manager/checking/product/${ productId }/`,
			method: 'put',
			types: [
				Actions.SUCCESS_CHANGE_PRODUCT_STATUS,
				Actions.FAILURE_CHANGE_PRODUCT_STATUS
			],
			data: { status }
		}
	});
}

export function setProductComment(productId, comment) {
	return (dispatch) => dispatch({
		type: Actions.SET_PRODUCT_COMMENT,
		[CALL_API]: {
			endpoint: `/api/v1/manager/checking/product/${ productId }/`,
			method: 'post',
			types: [
				Actions.SUCCESS_SET_PRODUCT_COMMENT,
				Actions.FAILURE_SET_PRODUCT_COMMENT
			],
			data: { comment }
		}
	});
}