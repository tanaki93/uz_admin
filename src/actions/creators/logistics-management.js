import * as Actions from '../logistics-management';
import { CALL_API } from '../../middleware/api';

export function changeLogisticsStatus(productId, option, value) {
	return (dispatch) => dispatch({
		type: Actions.CHANGE_LOGISTICS_PRODUCT_STATUS,
		[CALL_API]: {
			endpoint: `/api/v1/manager/logistic/orders/product/${productId}/`,
			method: 'put',
			types: [
				Actions.SUCCESS_CHANGE_LOGISTICS_PRODUCT_STATUS,
				Actions.FAILURE_CHANGE_LOGISTICS_PRODUCT_STATUS
			],
			data: {option, value}
		}
	})
}

export function createDeliveryStatus(productId, shippingService) {
	return (dispatch) => dispatch({
		type: Actions.CREATE_DELIVERY_SERVICE,
		[CALL_API]: {
			endpoint: `/api/v1/manager/logistic/orders/product/${productId}/`,
			method: 'post',
			types: [
				Actions.SUCCESS_CREATE_DELIVERY_SERVICE,
				Actions.FAILURE_CREATE_DELIVERY_SERVICE
			],
			data: {shipping_service: shippingService}
		}
	})
}
