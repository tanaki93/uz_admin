import * as Actions from '../documents';
import { CALL_API } from '../../middleware/api';

export function fetchDocumentBrands() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_DOC_BRANDS,
		[CALL_API]: {
			endpoint: `/api/v1/admin/document/brands/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_DOC_BRANDS,
				Actions.FAILURE_FETCH_DOC_BRANDS
			]
		}
	});
}

export function fetchDocuments(userId, brandId, from, to) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_DOCS,
		[CALL_API]: {
			endpoint: `/api/v1/admin/document/list/?user_id=${userId || ''}&brand_id=${brandId || ''}&date_from=${from || ''}&date_to=${to || ''}`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_DOCS,
				Actions.FAILURE_FETCH_DOCS
			]
		}
	});
}

export function fetchStatistics() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_STATISTICS,
		[CALL_API]: {
			endpoint: `/api/v1/admin/document/statistics/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_STATISTICS,
				Actions.FAILURE_FETCH_STATISTICS
			]
		}
	});
}

export function fetchOperators() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_OPERATORS,
		[CALL_API]: {
			endpoint: `/api/v1/admin/document/users/?role=2`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_OPERATORS,
				Actions.FAILURE_FETCH_OPERATORS
			]
		}
	});
}

export function createDocuments(data) {
	return (dispatch) => dispatch({
		type: Actions.CREATE_DOC,
		[CALL_API]: {
			endpoint: `/api/v1/admin/document/brands/`,
			method: 'put',
			types: [
				Actions.SUCCESS_CREATE_DOC,
				Actions.FAILURE_CREATE_DOC
			],
			data
		}
	});
}

export function fetchDocumentHierarchy(document_id) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_DOCUMENT_HIERARCHY,
		[CALL_API]: {
			endpoint: `/api/v1/admin/documents/${ document_id }/process/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_DOCUMENT_HIERARCHY,
				Actions.FAILURE_FETCH_DOCUMENT_HIERARCHY
			]
		}
	});
}

export function sendBack(document_id, data) {
	return (dispatch) => dispatch({
		type: Actions.SEND_BACK,
		[CALL_API]: {
			endpoint: `/api/v1/admin/documents/${ document_id }/process/`,
			method: 'put',
			types: [
				Actions.SUCCESS_SEND_BACK,
				Actions.FAILURE_SEND_BACK
			],
			data
		}
	});
}

export function approve(document_id) {
	return (dispatch) => dispatch({
		type: Actions.APPROVE,
		[CALL_API]: {
			endpoint: `/api/v1/admin/documents/${ document_id }/process/`,
			method: 'post',
			types: [
				Actions.SUCCESS_APPROVE,
				Actions.FAILURE_APPROVE
			]
		}
	});
}

export function fetchProductsByCategory(document_id, params) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_PRODUCTS_BY_CATEGORY,
		[CALL_API]: {
			endpoint: `/api/v1/admin/documents/${ document_id }/process/products/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_PRODUCTS_BY_CATEGORY,
				Actions.FAILURE_FETCH_PRODUCTS_BY_CATEGORY
			],
			params
		}
	});
}

export function setNew(documentId, isNew) {
	return (dispatch) => dispatch({
		type: Actions.SET_NEW,
		[CALL_API]: {
			endpoint: `/api/v1/admin/document/${ documentId }/`,
			method: 'PUT',
			types: [
				Actions.SUCCESS_SET_NEW,
				Actions.FAILURE_SET_NEW
			],
			data: { is_new: isNew }
		}
	});
}