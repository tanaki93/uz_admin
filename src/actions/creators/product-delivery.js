import * as Actions from '../product-delivery';
import { CALL_API } from '../../middleware/api';

export function fetchDeliveryProducts(query) {
	let number = '';
	let dateFrom = '';
	let dateTo = '';

	if (query) {
		number = query.number || '';
		dateFrom = query.dateFrom || '';
		dateTo = query.dateTo || '';
	}

	return (dispatch) => dispatch({
		type: Actions.FETCH_DELIVERY_PRODUCTS,
		[CALL_API]: {
			endpoint: `/api/v1/manager/delivery/product/?number=${ number }&date_from=${ dateFrom }&date_to=${ dateTo }`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_DELIVERY_PRODUCTS,
				Actions.FAILURE_FETCH_DELIVERY_PRODUCTS
			]
		}
	});
}

export function createPackage(order_products, weight) {
	return (dispatch) => dispatch({
		type: Actions.CREATE_PACKAGE,
		[CALL_API]: {
			endpoint: `/api/v1/manager/delivery/product/`,
			method: 'post',
			types: [
				Actions.SUCCESS_CREATE_PACKAGE,
				Actions.FAILURE_CREATE_PACKAGE
			],
			data: { order_products, weight, money: 0 }
		}
	});
}