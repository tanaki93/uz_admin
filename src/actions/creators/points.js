import * as Actions from '../points';
import { CALL_API } from '../../middleware/api';

export function fetchPoints() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_POINTS,
		[CALL_API]: {
			endpoint: `/api/v1/admin/points/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_POINTS,
				Actions.FAILURE_FETCH_POINTS
			]
		}
	})
}

export function createPoint(data) {
	return (dispatch) => dispatch({
		type: Actions.CREATE_POINT,
		[CALL_API]: {
			endpoint: `/api/v1/admin/points/`,
			method: 'post',
			types: [
				Actions.SUCCESS_CREATE_POINT,
				Actions.FAILURE_CREATE_POINT
			],
			data
		}
	})
}

export function editPoint(pointId, data) {
	return (dispatch) => dispatch({
		type: Actions.EDIT_POINT,
		[CALL_API]: {
			endpoint: `/api/v1/admin/points/${pointId}/`,
			method: 'put',
			types: [
				Actions.SUCCESS_EDIT_POINT,
				Actions.FAILURE_EDIT_POINT
			],
			data
		}
	})
}

export function deletePoint(pointId) {
	return (dispatch) => dispatch({
		type: Actions.DELETE_POINT,
		[CALL_API]: {
			endpoint: `/api/v1/admin/points/${pointId}/`,
			method: 'DELETE',
			types: [
				Actions.SUCCESS_DELETE_POINT,
				Actions.FAILURE_DELETE_POINT
			]
		}
	})
}