import * as Actions from '../sizes';
import { CALL_API } from '../../middleware/api';

export function fetchSizes() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_SIZES,
		[CALL_API]: {
			endpoint: `/api/v1/operator/trendyol/sizes/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_SIZES,
				Actions.FAILURE_FETCH_SIZES
			]
		}
	});
}

export function createSize(size_id, data) {
	return (dispatch) => dispatch({
		type: Actions.CREATE_SIZE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/trendyol/sizes/${ size_id }/`,
			method: 'post',
			types: [
				Actions.SUCCESS_CREATE_SIZE,
				Actions.FAILURE_CREATE_SIZE
			],
			data
		}
	});
}