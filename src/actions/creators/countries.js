import * as Actions from '../countries';
import { CALL_API } from '../../middleware/api';

export function fetchCountries() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_COUNTRIES,
		[CALL_API]: {
			endpoint: `/api/v1/admin/countries/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_COUNTRIES,
				Actions.FAILURE_FETCH_COUNTRIES
			]
		}
	})
}

export function createCountry(data) {
	return (dispatch) => dispatch({
		type: Actions.CREATE_COUNTRY,
		[CALL_API]: {
			endpoint: `/api/v1/admin/countries/`,
			method: 'post',
			types: [
				Actions.SUCCESS_CREATE_COUNTRY,
				Actions.FAILURE_CREATE_COUNTRY
			],
			data
		}
	})
}

export function editCountry(countryId, data) {
	return (dispatch) => dispatch({
		type: Actions.EDIT_COUNTRY,
		[CALL_API]: {
			endpoint: `/api/v1/admin/countries/${countryId}/`,
			method: 'put',
			types: [
				Actions.SUCCESS_EDIT_COUNTRY,
				Actions.FAILURE_EDIT_COUNTRY
			],
			data
		}
	})
}

export function deleteCountry(countryId) {
	return (dispatch) => dispatch({
		type: Actions.DELETE_COUNTRY,
		[CALL_API]: {
			endpoint: `/api/v1/admin/countries/${countryId}/`,
			method: 'DELETE',
			types: [
				Actions.SUCCESS_DELETE_COUNTRY,
				Actions.FAILURE_DELETE_COUNTRY
			]
		}
	})
}