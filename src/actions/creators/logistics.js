import * as Actions from '../logistics';
import { CALL_API } from '../../middleware/api';

export function fetchLogistics(query) {
	let number = '';
	let dateFrom = '';
	let dateTo = '';

	if (query) {
		number = query.number || '';
		dateFrom = query.dateFrom || '';
		dateTo = query.dateTo || '';
	}

	return (dispatch) => dispatch({
		type: Actions.FETCH_LOGISTICS,
		[CALL_API]: {
			endpoint: `/api/v1/manager/logistic/orders/?number=${ number }&date_from=${ dateFrom }&date_to=${ dateTo }`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_LOGISTICS,
				Actions.FAILURE_FETCH_LOGISTICS
			]
		}
	})
}

export function fetchLogisticsOrder(orderId) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_LOGISTICS_ORDER,
		[CALL_API]: {
			endpoint: `/api/v1/manager/logistic/orders/${orderId}/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_LOGISTICS_ORDER,
				Actions.FAILURE_FETCH_LOGISTICS_ORDER
			]
		}
	})
}
