import * as Actions from '../slider';
import { CALL_API } from '../../middleware/api';

export function fetchSlider() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_SLIDER,
		[CALL_API]: {
			endpoint: `/api/v1/landing/sliders/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_SLIDER,
				Actions.FAILURE_FETCH_SLIDER
			]
		}
	});
}

export function createSlider(data) {
	return (dispatch) => dispatch({
		type: Actions.CREATE_SLIDER,
		[CALL_API]: {
			endpoint: `/api/v1/landing/sliders/`,
			method: 'post',
			data,
			types: [
				Actions.SUCCESS_CREATE_SLIDER,
				Actions.FAILURE_CREATE_SLIDER
			]
		}
	});
}

export function editSlider(sliderId, data) {
	return (dispatch) => dispatch({
		type: Actions.EDIT_SLIDER,
		[CALL_API]: {
			endpoint: `/api/v1/landing/sliders/${sliderId}/`,
			method: 'put',
			data,
			types: [
				Actions.SUCCESS_EDIT_SLIDER,
				Actions.FAILURE_EDIT_SLIDER
			]
		}
	});
}