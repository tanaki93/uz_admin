import * as Actions from '../classes-base';
import { CALL_API } from '../../middleware/api';

export function fetchClasses(department_id) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_CLASSES_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/departments/${department_id}/parents/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_CLASSES_BASE,
				Actions.FAILURE_FETCH_CLASSES_BASE
			]
		}
	})
}

export function createClass(department_id, data) {
	return (dispatch) => dispatch({
		type: Actions.CREATE_CLASS_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/departments/${department_id}/parents/`,
			method: 'post',
			types: [
				Actions.SUCCESS_CREATE_CLASS_BASE,
				Actions.FAILURE_CREATE_CLASS_BASE
			],
			data
		}
	})
}

export function fetchClass(parent_id) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_CLASS_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/parents/${parent_id}/`,
			method: 'get',
			types: [
				Actions.FAILURE_FETCH_CLASS_BASE,
				Actions.SUCCESS_FETCH_CLASS_BASE
			]
		}
	})
}

export function deleteClass(parent_id) {
	return (dispatch) => dispatch({
		type: Actions.DELETE_CLASS_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/parents/${parent_id}/`,
			method: 'delete',
			types: [
				Actions.FAILURE_DELETE_CLASS_BASE,
				Actions.SUCCESS_DELETE_CLASS_BASE
			]
		}
	})
}

export function editClass(parent_id, data) {
	return (dispatch) => dispatch({
		type: Actions.EDIT_CLASS_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/parents/${parent_id}/`,
			method: 'put',
			types: [
				Actions.SUCCESS_EDIT_CLASS_BASE,
				Actions.FAILURE_EDIT_CLASS_BASE
			],
			data
		}
	})
}