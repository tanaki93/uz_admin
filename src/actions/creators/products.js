import * as Actions from '../products';
import { CALL_API } from '../../middleware/api';

export function fetchProducts(brandId, departmentId, categoryId, colourId, query, page) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_PRODUCTS,
		[CALL_API]: {
			endpoint: `/api/v1/operator/vendor/products/?brand_id=${ brandId }&department_id=${ departmentId }&category_id=${ categoryId }&colour_id=${ colourId }&query=${ query }&page=${ page || 1 }`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_PRODUCTS,
				Actions.FAILURE_FETCH_PRODUCTS
			]
		}
	})
}

export function activateProduct(productId, data) {
	return (dispatch) => dispatch({
		type: Actions.ACTIVATE_PRODUCT,
		[CALL_API]: {
			endpoint: `/api/v1/operator/vendor/products/${productId}/`,
			method: 'PUT',
			types: [
				Actions.SUCCESS_ACTIVATE_PRODUCT,
				Actions.FAILURE_ACTIVATE_PRODUCT
			],
			data
		}
	})
}