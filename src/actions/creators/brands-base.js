import * as Actions from '../brands-base';
import { CALL_API } from '../../middleware/api';

export function fetchBrands() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_BRANDS_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/main/brands/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_BRANDS_BASE,
				Actions.FAILURE_FETCH_BRANDS_BASE
			]
		}
	});
}