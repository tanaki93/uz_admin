import * as Actions from '../packets';
import { CALL_API } from '../../middleware/api';

export function fetchPackets(query) {
	let number = '';
	let dateFrom = '';
	let dateTo = '';

	if (query) {
		number = query.number || '';
		dateFrom = query.dateFrom || '';
		dateTo = query.dateTo || '';
	}

	return (dispatch) => dispatch({
		type: Actions.FETCH_PACKETS,
		[CALL_API]: {
			endpoint: `/api/v1/manager/packets/?number=${ number }&date_from=${ dateFrom }&date_to=${ dateTo }`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_PACKETS,
				Actions.FAILURE_FETCH_PACKETS
			]
		}
	});
}

export function fetchPacket(packetId) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_PACKET,
		[CALL_API]: {
			endpoint: `/api/v1/manager/packets/${ packetId }/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_PACKET,
				Actions.FAILURE_FETCH_PACKET
			]
		}
	});
}

export function changePacketStatus(packetId, status) {
	return (dispatch) => dispatch({
		type: Actions.CHANGE_PACKET_STATUS,
		[CALL_API]: {
			endpoint: `/api/v1/manager/packets/${ packetId }/`,
			method: 'put',
			types: [
				Actions.SUCCESS_CHANGE_PACKET_STATUS,
				Actions.FAILURE_CHANGE_PACKET_STATUS
			],
			data: { received_status: status }
		}
	});
}

export function createFlight(packetId, flightNo, money) {
	return (dispatch) => dispatch({
		type: Actions.CREATE_FLIGHT,
		[CALL_API]: {
			endpoint: `/api/v1/manager/packets/${ packetId }/`,
			method: 'post',
			types: [
				Actions.SUCCESS_CREATE_FLIGHT,
				Actions.FAILURE_CREATE_FLIGHT
			],
			data: { flight_number: flightNo, money }
		}
	});
}