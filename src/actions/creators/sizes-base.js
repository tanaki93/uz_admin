import * as Actions from '../sizes-base';
import { CALL_API } from '../../middleware/api';

export function fetchSizes() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_SIZES_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/sizes/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_SIZES_BASE,
				Actions.FAILURE_FETCH_SIZES_BASE
			]
		}
	});
}

export function createSize(data) {
	return (dispatch) => dispatch({
		type: Actions.CREATE_SIZE_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/sizes/`,
			method: 'post',
			types: [
				Actions.SUCCESS_CREATE_SIZE_BASE,
				Actions.FAILURE_CREATE_SIZE_BASE
			],
			data
		}
	});
}

export function fetchSize(size_id) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_SIZE_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/sizes/${ size_id }/`,
			method: 'get',
			types: [
				Actions.FAILURE_FETCH_SIZE_BASE,
				Actions.SUCCESS_FETCH_SIZE_BASE
			]
		}
	});
}

export function deleteSize(size_id) {
	return (dispatch) => dispatch({
		type: Actions.DELETE_SIZE_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/sizes/${ size_id }/`,
			method: 'delete',
			types: [
				Actions.FAILURE_DELETE_SIZE_BASE,
				Actions.SUCCESS_DELETE_SIZE_BASE
			]
		}
	});
}

export function editSize(size_id, data) {
	return (dispatch) => dispatch({
		type: Actions.EDIT_SIZE_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/sizes/${ size_id }/`,
			method: 'put',
			types: [
				Actions.SUCCESS_EDIT_SIZE_BASE,
				Actions.FAILURE_EDIT_SIZE_BASE
			],
			data
		}
	});
}