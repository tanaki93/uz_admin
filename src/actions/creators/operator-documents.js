import * as Actions from '../operator-documents';
import { CALL_API } from '../../middleware/api';

export function fetchDocuments() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_OPERATOR_DOCUMENTS,
		[CALL_API]: {
			endpoint: `/api/v1/operator/documents/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_OPERATOR_DOCUMENTS,
				Actions.FAILURE_FETCH_OPERATOR_DOCUMENTS
			]
		}
	});
}

export function fetchAllDocuments() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_ALL_OPERATOR_DOCUMENTS,
		[CALL_API]: {
			endpoint: `/api/v1/operator/documents/all/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_ALL_OPERATOR_DOCUMENTS,
				Actions.FAILURE_FETCH_ALL_OPERATOR_DOCUMENTS
			]
		}
	});
}

export function assignDocument(documment_id) {
	return (dispatch) => dispatch({
		type: Actions.ASSIGN_OPERATOR_DOCUMENT,
		[CALL_API]: {
			endpoint: `/api/v1/operator/documents/${ documment_id }/`,
			method: 'put',
			types: [
				Actions.SUCCESS_ASSIGN_OPERATOR_DOCUMENT,
				Actions.FAILURE_ASSIGN_OPERATOR_DOCUMENT
			]
		}
	});
}

export function fetchDocument(document_id) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_OPERATOR_DOCUMENT,
		[CALL_API]: {
			endpoint: `/api/v1/operator/documents/${ document_id }/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_OPERATOR_DOCUMENT,
				Actions.FAILURE_FETCH_OPERATOR_DOCUMENT
			]
		}
	});
}

export function fetchDocumentHierarchy(document_id) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_OPERATOR_DOCUMENT_HIERARCHY,
		[CALL_API]: {
			endpoint: `/api/v1/operator/documents/${ document_id }/process/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_OPERATOR_DOCUMENT_HIERARCHY,
				Actions.FAILURE_FETCH_OPERATOR_DOCUMENT_HIERARCHY
			]
		}
	});
}

export function fetchDocumentHierarchyAfterBulk(document_id) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_OPERATOR_DOCUMENT_HIERARCHY_AFTER_BULK,
		[CALL_API]: {
			endpoint: `/api/v1/operator/documents/${ document_id }/process/`,
			method: 'post',
			types: [
				Actions.SUCCESS_FETCH_OPERATOR_DOCUMENT_HIERARCHY_AFTER_BULK,
				Actions.FAILURE_FETCH_OPERATOR_DOCUMENT_HIERARCHY_AFTER_BULK
			]
		}
	});
}

export function fetchProductsByCategory(document_id, params) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_OPERATOR_PRODUCTS_BY_CATEGORY,
		[CALL_API]: {
			endpoint: `/api/v1/operator/documents/${ document_id }/process/products/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_OPERATOR_PRODUCTS_BY_CATEGORY,
				Actions.FAILURE_FETCH_OPERATOR_PRODUCTS_BY_CATEGORY
			],
			params
		}
	});
}

export function processProducts(document_id, data) {
	return (dispatch) => dispatch({
		type: Actions.PROCESS_OPERATOR_DOCUMENT,
		[CALL_API]: {
			endpoint: `/api/v1/operator/documents/${ document_id }/process/products/`,
			method: 'post',
			types: [
				Actions.SUCCESS_PROCESS_OPERATOR_DOCUMENT,
				Actions.FAILURE_PROCESS_OPERATOR_DOCUMENT
			],
			data
		}
	});
}

export function processProduct(document_id, product_id, data) {
	return (dispatch) => dispatch({
		type: Actions.PROCESS_OPERATOR_PRODUCT,
		[CALL_API]: {
			endpoint: `/api/v1/operator/documents/${ document_id }/process/products/${ product_id }/`,
			method: 'put',
			types: [
				Actions.SUCCESS_PROCESS_OPERATOR_PRODUCT,
				Actions.FAILURE_PROCESS_OPERATOR_PRODUCT
			],
			data
		}
	});
}

export function bulkProducts(document_id, data) {
	return (dispatch) => dispatch({
		type: Actions.BULK_OPERATOR_DOCUMENT,
		[CALL_API]: {
			endpoint: `/api/v1/operator/documents/${ document_id }/process/products/`,
			method: 'put',
			types: [
				Actions.SUCCESS_BULK_OPERATOR_DOCUMENT,
				Actions.FAILURE_BULK_OPERATOR_DOCUMENT
			],
			data
		}
	});
}