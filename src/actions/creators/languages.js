import * as Actions from '../languages';
import { CALL_API } from '../../middleware/api';

export function fetchLanguages() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_LANGUAGES,
		[CALL_API]: {
			endpoint: `/api/v1/admin/languages/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_LANGUAGES,
				Actions.FAILURE_FETCH_LANGUAGES
			]
		}
	});
}

export function createLanguage(data) {
	return (dispatch) => dispatch({
		type: Actions.CREATE_LANGUAGE,
		[CALL_API]: {
			endpoint: `/api/v1/admin/languages/`,
			method: 'post',
			types: [
				Actions.SUCCESS_CREATE_LANGUAGE,
				Actions.FAILURE_CREATE_LANGUAGE
			],
			data
		}
	});
}

export function editLanguage(languageId, data) {
	return (dispatch) => dispatch({
		type: Actions.EDIT_LANGUAGE,
		[CALL_API]: {
			endpoint: `/api/v1/admin/languages/${ languageId }/`,
			method: 'put',
			types: [
				Actions.SUCCESS_EDIT_LANGUAGE,
				Actions.FAILURE_EDIT_LANGUAGE
			],
			data
		}
	});
}

export function deleteLanguage(languageId) {
	return (dispatch) => dispatch({
		type: Actions.DELETE_LANGUAGE,
		[CALL_API]: {
			endpoint: `/api/v1/admin/languages/${ languageId }/`,
			method: 'delete',
			types: [
				Actions.SUCCESS_DELETE_LANGUAGE,
				Actions.FAILURE_DELETE_LANGUAGE
			]
		}
	});
}