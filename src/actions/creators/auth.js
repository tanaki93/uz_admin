import * as Actions from '../auth';
import { CALL_API } from '../../middleware/api';

export function auth(data) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_AUTH,
		[CALL_API]: {
			endpoint: `/api/v1/login/`,
			method: 'post',
			types: [
				Actions.SUCCESS_FETCH_AUTH,
				Actions.FAILURE_FETCH_AUTH
			],
			data
		}
	});
}


export function reAuth() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_REAUTH,
		[CALL_API]: {
			endpoint: `/api/v1/reauth/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_REAUTH,
				Actions.FAILURE_FETCH_REAUTH
			]
		}
	});
}

export function loseAuth() {
	return { type: Actions.LOSE_AUTH };
}
