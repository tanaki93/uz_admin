import * as Actions from '../order-details';
import { CALL_API } from '../../middleware/api';

export function fetchOrder(orderId) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_ORDER,
		[CALL_API]: {
			endpoint: `/api/v1/manager/orders/${ orderId }/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_ORDER,
				Actions.FAILURE_FETCH_ORDER
			]
		}
	});
}

export function changeProductStatus(orderId, option, value) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_ORDER,
		[CALL_API]: {
			endpoint: `/api/v1/manager/orders/product/${ orderId }/`,
			method: 'put',
			types: [
				Actions.SUCCESS_FETCH_ORDER,
				Actions.FAILURE_FETCH_ORDER
			],
			data: { option, value }
		}
	});
}


export function changeStage(orderId, stage) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_ORDER,
		[CALL_API]: {
			endpoint: `/api/v1/manager/orders/product/${ orderId }/`,
			method: 'post',
			types: [
				Actions.SUCCESS_FETCH_ORDER,
				Actions.FAILURE_FETCH_ORDER
			],
			data: { stage }
		}
	});
}

export function refund(data) {
	return (dispatch) => dispatch({
		type: Actions.ADMIN_REFUND,
		[CALL_API]: {
			endpoint: `/api/v1/admin/refunds/`,
			method: 'post',
			types: [
				Actions.SUCCESS_ADMIN_REFUND,
				Actions.FAILURE_ADMIN_REFUND
			],
			data
		}
	});
}