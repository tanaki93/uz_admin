import * as Actions from '../refund';
import { CALL_API } from '../../middleware/api';

export function fetchRefunds() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_REFUNDS,
		[CALL_API]: {
			endpoint: `/api/v1/admin/refunds/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_REFUNDS,
				Actions.FAILURE_FETCH_REFUNDS
			]
		}
	});
}

export function changeRefundStatus(refundId, status) {
	return (dispatch) => dispatch({
		type: Actions.CHANGE_REFUND_STATUS,
		[CALL_API]: {
			endpoint: `/api/v1/admin/refunds/${ refundId }/`,
			method: 'put',
			types: [
				Actions.SUCCESS_CHANGE_REFUND_STATUS,
				Actions.FAILURE_CHANGE_REFUND_STATUS
			],
			data: { status }
		}
	});
}

export function changeClientRefundStatus(refundId, itemId, status, comment) {
	return (dispatch) => dispatch({
		type: Actions.CHANGE_REFUND_STATUS,
		[CALL_API]: {
			endpoint: `/api/v1/admin/refunds/${ refundId }/`,
			method: 'POST',
			types: [
				Actions.SUCCESS_CHANGE_REFUND_STATUS,
				Actions.FAILURE_CHANGE_REFUND_STATUS
			],
			data: { item_id: itemId, status, comment }
		}
	});
}