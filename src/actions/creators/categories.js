import * as Actions from '../categories';
import { CALL_API } from '../../middleware/api';

export function fetchCategories() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_CATEGORIES,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/categories/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_CATEGORIES,
				Actions.FAILURE_FETCH_CATEGORIES
			]
		}
	});
}

export function fetchCategory(category_id) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_CATEGORY,
		[CALL_API]: {
			endpoint: `/api/v1/operator/trendyol/categories/${ category_id }/`,
			method: 'get',
			types: [
				Actions.FAILURE_FETCH_CATEGORY,
				Actions.SUCCESS_FETCH_CATEGORY
			]
		}
	});
}

export function createCategory(category_id, data) {
	return (dispatch) => dispatch({
		type: Actions.CREATE_CATEGORY,
		[CALL_API]: {
			endpoint: `/api/v1/operator/trendyol/categories/${ category_id }/`,
			method: 'post',
			types: [
				Actions.SUCCESS_CREATE_CATEGORY,
				Actions.FAILURE_CREATE_CATEGORY
			],
			data
		}
	});
}

export function deleteCategory(category_id) {
	return (dispatch) => dispatch({
		type: Actions.DELETE_CATEGORY,
		[CALL_API]: {
			endpoint: `/api/v1/operator/trendyol/categories/${ category_id }/`,
			method: 'delete',
			types: [
				Actions.SUCCESS_DELETE_CATEGORY,
				Actions.FAILURE_DELETE_CATEGORY
			]
		}
	});
}

export function editCategory(category_id, data) {
	return (dispatch) => dispatch({
		type: Actions.EDIT_CATEGORY,
		[CALL_API]: {
			endpoint: `/api/v1/operator/trendyol/categories/${ category_id }/`,
			method: 'put',
			types: [
				Actions.SUCCESS_EDIT_CATEGORY,
				Actions.FAILURE_EDIT_CATEGORY
			],
			data
		}
	});
}