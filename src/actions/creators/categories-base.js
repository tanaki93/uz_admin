import * as Actions from '../categories-base';
import { CALL_API } from '../../middleware/api';

export function fetchCategories(parent_id) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_CATEGORIES_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/parents/${parent_id}/categories/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_CATEGORIES_BASE,
				Actions.FAILURE_FETCH_CATEGORIES_BASE
			]
		}
	})
}

export function createCategory(parent_id, data) {
	return (dispatch) => dispatch({
		type: Actions.CREATE_CATEGORY_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/parents/${parent_id}/categories/`,
			method: 'post',
			types: [
				Actions.SUCCESS_CREATE_CATEGORY_BASE,
				Actions.FAILURE_CREATE_CATEGORY_BASE
			],
			data
		}
	})
}

export function fetchCategory(category_id) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_CATEGORY_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/categories/${category_id}/`,
			method: 'get',
			types: [
				Actions.FAILURE_FETCH_CATEGORY_BASE,
				Actions.SUCCESS_FETCH_CATEGORY_BASE
			]
		}
	})
}

export function deleteCategory(category_id) {
	return (dispatch) => dispatch({
		type: Actions.DELETE_CATEGORY_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/categories/${ category_id }/`,
			method: 'delete',
			types: [
				Actions.FAILURE_DELETE_CATEGORY_BASE,
				Actions.SUCCESS_DELETE_CATEGORY_BASE
			]
		}
	})
}

export function editCategory(category_id, data) {
	return (dispatch) => dispatch({
		type: Actions.EDIT_CATEGORY_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/categories/${ category_id }/`,
			method: 'put',
			types: [
				Actions.SUCCESS_EDIT_CATEGORY_BASE,
				Actions.FAILURE_EDIT_CATEGORY_BASE
			],
			data
		}
	})
}

export function uploadCategoryImage(category_id, data) {
	return (dispatch) => dispatch({
		type: Actions.EDIT_CATEGORY_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/categories/${ category_id }/`,
			method: 'post',
			types: [
				Actions.SUCCESS_EDIT_CATEGORY_BASE,
				Actions.FAILURE_EDIT_CATEGORY_BASE
			],
			data
		}
	})
}