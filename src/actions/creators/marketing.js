import * as Actions from '../marketing';
import { CALL_API } from '../../middleware/api';

const encodeQueryData = (data) => {
	const ret = [];
	for (let d in data)
		ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
	return ret.join('&');
};

export const fetchMarketing = (query) => {
	let params = {};
	let endpoint = '/api/v1/marketolog/products/';
	if (query) {
		if (query.brandId) params['brand_id'] = query.brandId;
		if (query.departmentId) params['department_id'] = query.departmentId;
		if (query.categoryId) params['category_id'] = query.categoryId;
		if (query.colourId) params['colour_id'] = query.colourId;
		if (query.query) params['query'] = query.query;
		if (query.page) params['page'] = query.page;
		if (query.isMarketing) params['is_marketolog'] = query.isMarketing;
		endpoint += '?' + encodeQueryData(params);
	}


	return (dispatch) => dispatch({
		type: Actions.FETCH_MARKETING,
		[CALL_API]: {
			endpoint: endpoint,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_MARKETING,
				Actions.FAILURE_FETCH_MARKETING
			]
		}
	});
};


export const fetchMarketingOnly = (query) => {
	let params = {};
	let endpoint = '/api/v1/marketolog/products/?is_marketolog=1';
	if (query) {
		if (query.brandId) params['brand_id'] = query.brandId;
		if (query.departmentId) params['department_id'] = query.departmentId;
		if (query.categoryId) params['category_id'] = query.categoryId;
		if (query.colourId) params['colour_id'] = query.colourId;
		if (query.query) params['query'] = query.query;
		if (query.page) params['page'] = query.page;
		endpoint += encodeQueryData(params);
	}


	return (dispatch) => dispatch({
		type: Actions.FETCH_MARKETING,
		[CALL_API]: {
			endpoint: endpoint,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_MARKETING,
				Actions.FAILURE_FETCH_MARKETING
			]
		}
	});
};

export function setMarketing(productId, data) {
	return (dispatch) => dispatch({
		type: Actions.SET_MARKETING,
		[CALL_API]: {
			endpoint: `/api/v1/marketolog/products/${ productId }/`,
			method: 'put',
			types: [
				Actions.SUCCESS_SET_MARKETING,
				Actions.FAILURE_SET_MARKETING
			],
			data: { is_marketolog: data }
		}
	});
}