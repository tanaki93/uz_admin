import * as Actions from '../contents-base';
import { CALL_API } from '../../middleware/api';

export function fetchContents() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_CONTENTS_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/content/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_CONTENTS_BASE,
				Actions.FAILURE_FETCH_CONTENTS_BASE
			]
		}
	});
}

export function createContent(data) {
	return (dispatch) => dispatch({
		type: Actions.CREATE_CONTENT_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/content/`,
			method: 'post',
			types: [
				Actions.SUCCESS_CREATE_CONTENT_BASE,
				Actions.FAILURE_CREATE_CONTENT_BASE
			],
			data
		}
	});
}

export function fetchContent(content_id) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_CONTENT_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/content/${ content_id }/`,
			method: 'get',
			types: [
				Actions.FAILURE_FETCH_CONTENT_BASE,
				Actions.SUCCESS_FETCH_CONTENT_BASE
			]
		}
	});
}

export function deleteContent(content_id) {
	return (dispatch) => dispatch({
		type: Actions.DELETE_CONTENT_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/content/${ content_id }/`,
			method: 'delete',
			types: [
				Actions.FAILURE_DELETE_CONTENT_BASE,
				Actions.SUCCESS_DELETE_CONTENT_BASE
			]
		}
	});
}

export function editContent(content_id, data) {
	return (dispatch) => dispatch({
		type: Actions.EDIT_CONTENT_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/content/${ content_id }/`,
			method: 'put',
			types: [
				Actions.SUCCESS_EDIT_CONTENT_BASE,
				Actions.FAILURE_EDIT_CONTENT_BASE
			],
			data
		}
	});
}