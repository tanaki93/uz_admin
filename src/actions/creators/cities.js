import * as Actions from '../cities';
import { CALL_API } from '../../middleware/api';

export function fetchCities() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_CITIES,
		[CALL_API]: {
			endpoint: `/api/v1/admin/cities/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_CITIES,
				Actions.FAILURE_FETCH_CITIES
			]
		}
	})
}

export function createCity(data) {
	return (dispatch) => dispatch({
		type: Actions.CREATE_CITY,
		[CALL_API]: {
			endpoint: `/api/v1/admin/cities/`,
			method: 'post',
			types: [
				Actions.SUCCESS_CREATE_CITY,
				Actions.FAILURE_CREATE_CITY
			],
			data
		}
	})
}

export function editCity(cityId, data) {
	return (dispatch) => dispatch({
		type: Actions.EDIT_CITY,
		[CALL_API]: {
			endpoint: `/api/v1/admin/cities/${cityId}/`,
			method: 'put',
			types: [
				Actions.SUCCESS_EDIT_CITY,
				Actions.FAILURE_EDIT_CITY
			],
			data
		}
	})
}

export function deleteCity(cityId) {
	return (dispatch) => dispatch({
		type: Actions.DELETE_CITY,
		[CALL_API]: {
			endpoint: `/api/v1/admin/cities/${cityId}/`,
			method: 'DELETE',
			types: [
				Actions.SUCCESS_DELETE_CITY,
				Actions.FAILURE_DELETE_CITY
			]
		}
	})
}