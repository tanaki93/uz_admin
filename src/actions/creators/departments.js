import * as Actions from '../departments';
import { CALL_API } from '../../middleware/api';

export function fetchDepartments() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_DEPARTMENTS,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/departments/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_DEPARTMENTS,
				Actions.FAILURE_FETCH_DEPARTMENTS
			]
		}
	});
}

export function fetchDepartment(department_id) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_DEPARTMENT,
		[CALL_API]: {
			endpoint: `/api/v1/operator/trendyol/departments/${ department_id }/`,
			method: 'get',
			types: [
				Actions.FAILURE_FETCH_DEPARTMENT,
				Actions.SUCCESS_FETCH_DEPARTMENT
			]
		}
	});
}

export function createDepartment(department_id, data) {
	return (dispatch) => dispatch({
		type: Actions.CREATE_DEPARTMENT,
		[CALL_API]: {
			endpoint: `/api/v1/operator/trendyol/departments/${ department_id }/`,
			method: 'post',
			types: [
				Actions.SUCCESS_CREATE_DEPARTMENT,
				Actions.FAILURE_CREATE_DEPARTMENT
			],
			data
		}
	});
}

export function deleteDepartment(department_id) {
	return (dispatch) => dispatch({
		type: Actions.DELETE_DEPARTMENT,
		[CALL_API]: {
			endpoint: `/api/v1/operator/trendyol/departments/${ department_id }/`,
			method: 'delete',
			types: [
				Actions.SUCCESS_DELETE_DEPARTMENT,
				Actions.FAILURE_DELETE_DEPARTMENT
			]
		}
	});
}

export function editDepartment(department_id, data) {
	return (dispatch) => dispatch({
		type: Actions.EDIT_DEPARTMENT,
		[CALL_API]: {
			endpoint: `/api/v1/operator/trendyol/departments/${ department_id }/`,
			method: 'put',
			types: [
				Actions.SUCCESS_EDIT_DEPARTMENT,
				Actions.FAILURE_EDIT_DEPARTMENT
			],
			data
		}
	});
}