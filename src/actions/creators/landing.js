import * as Actions from '../landing';
import { CALL_API } from '../../middleware/api';

export const fetchLandingBrands = () => {
	return (dispatch) => dispatch({
		type: Actions.FETCH_BRANDS,
		[CALL_API]: {
			endpoint: `/api/v1/landing/brands/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_BRANDS,
				Actions.FAILURE_FETCH_BRANDS
			]
		}
	});
}

export const updateLandingBrand = (brandId, data) => {
	return (dispatch) => dispatch({
		type: Actions.UPDATE_BRANDS,
		[CALL_API]: {
			endpoint: `/api/v1/landing/brands/${brandId}/`,
			method: 'put',
			types: [
				Actions.SUCCESS_UPDATE_BRANDS,
				Actions.FAILURE_UPDATE_BRANDS
			],
			data
		}
	});
}
