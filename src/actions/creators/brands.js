import * as Actions from '../brands';
import { CALL_API } from '../../middleware/api';

export function fetchBrands() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_BRANDS,
		[CALL_API]: {
			endpoint: '/api/v1/operator/trendyol/brands/',
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_BRANDS,
				Actions.FAILURE_FETCH_BRANDS
			]
		}
	});
}


export function fetchBrands2() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_BRANDS,
		[CALL_API]: {
			endpoint: '/api/v1/operator/vendor/brands/',
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_BRANDS,
				Actions.FAILURE_FETCH_BRANDS
			]
		}
	});
}

export function fetchBrand(brand_id) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_BRAND,
		[CALL_API]: {
			endpoint: `/api/v1/operator/trendyol/brands/${ brand_id }/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_BRAND,
				Actions.FAILURE_FETCH_BRAND
			]
		}
	});
}

export function refreshBrand(brand_id) {
	return (dispatch) => dispatch({
		type: Actions.REFRESH_BRAND,
		[CALL_API]: {
			endpoint: `/api/v1/operator/trendyol/brands/${ brand_id }/refresh/`,
			method: 'post',
			types: [
				Actions.SUCCESS_REFRESH_BRAND,
				Actions.FAILURE_REFRESH_BRAND
			]
		}
	});
}

export function createBrand(data) {
	return (dispatch) => dispatch({
		type: Actions.CREATE_BRAND,
		[CALL_API]: {
			endpoint: '/api/v1/operator/trendyol/brands/',
			method: 'post',
			types: [
				Actions.SUCCESS_CREATE_BRAND,
				Actions.FAILURE_CREATE_BRAND
			],
			data
		}
	});
}

export function editBrand(brand_id, data) {
	return (dispatch) => dispatch({
		type: Actions.EDIT_BRAND,
		[CALL_API]: {
			endpoint: `/api/v1/operator/trendyol/brands/${ brand_id }/`,
			method: 'put',
			types: [
				Actions.SUCCESS_EDIT_BRAND,
				Actions.FAILURE_EDIT_BRAND
			],
			data
		}
	});
}