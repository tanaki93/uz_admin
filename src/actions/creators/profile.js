import * as Actions from '../profile';
import { CALL_API } from '../../middleware/api';

export function getUserProfile(userId) {
	return (dispatch) => dispatch({
		type: Actions.GET_USER_PROFILE,
		[CALL_API]: {
			endpoint: `/api/v1/admin/profiles/${userId}/`,
			method: 'get',
			types: [
				Actions.SUCCESS_GET_USER_PROFILE,
				Actions.FAILURE_GET_USER_PROFILE
			],
			data: {userId}
		}
	});
}