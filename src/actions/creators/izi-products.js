import * as Actions from '../izi-products';
import { CALL_API } from '../../middleware/api';

export function fetchProducts(brandId, departmentId, categoryId, colourId, query, page) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_IZI_PRODUCTS,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/products/?brand_id=${ brandId }&department_id=${ departmentId }&category_id=${ categoryId }&colour_id=${ colourId }&query=${ query }&page=${ page || 1 }`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_IZI_PRODUCTS,
				Actions.FAILURE_FETCH_IZI_PRODUCTS
			]
		}
	});
}

export function editProduct(productId, data) {
	return (dispatch) => dispatch({
		type: Actions.EDIT_PRODUCT,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/products/${ productId }/`,
			method: 'put',
			types: [
				Actions.SUCCESS_EDIT_PRODUCT,
				Actions.FAILURE_EDIT_PRODUCT
			],
			data
		}
	});
}

export function activateProduct(productId, data) {
	return (dispatch) => dispatch({
		type: Actions.ACTIVATE_IZI_PRODUCT,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/products/${ productId }`,
			method: 'POST',
			types: [
				Actions.SUCCESS_ACTIVATE_IZI_PRODUCT,
				Actions.FAILURE_ACTIVATE_IZI_PRODUCT
			],
			data
		}
	});
}

export function hitProduct(productId, data) {
	return (dispatch) => dispatch({
		type: Actions.HIT_IZI_PRODUCT,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/products/${ productId }`,
			method: 'delete',
			types: [
				Actions.SUCCESS_HIT_IZI_PRODUCT,
				Actions.FAILURE_HIT_IZI_PRODUCT
			],
			data
		}
	});
}

export function editDescription(productId, data) {
	return (dispatch) => dispatch({
		type: Actions.EDIT_DESCRIPTION,
		[CALL_API]: {
			endpoint: `/api/v1/admin/documents/${productId}/descriptions/`,
			method: 'PUT',
			types: [
				Actions.SUCCESS_EDIT_DESCRIPTION,
				Actions.FAILURE_EDIT_DESCRIPTION
			],
			data
		}
	})
}