import * as Actions from '../colors';
import { CALL_API } from '../../middleware/api';

export function fetchColors() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_COLORS,
		[CALL_API]: {
			endpoint: `/api/v1/operator/trendyol/colours/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_COLORS,
				Actions.FAILURE_FETCH_COLORS
			]
		}
	});
}

export function createColor(color_id, data) {
	return (dispatch) => dispatch({
		type: Actions.CREATE_COLOR,
		[CALL_API]: {
			endpoint: `/api/v1/operator/trendyol/colours/${ color_id }/`,
			method: 'post',
			types: [
				Actions.SUCCESS_CREATE_COLOR,
				Actions.FAILURE_CREATE_COLOR
			],
			data
		}
	});
}