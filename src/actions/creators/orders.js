import * as Actions from '../orders';
import { CALL_API } from '../../middleware/api';

export function fetchOrders(query) {
	let name = '';
	let id = '';
	let dateFrom = '';
	let dateTo = '';

	if (query) {
		name = query.name || '';
		id = query.id || '';
		dateFrom = query.dateFrom || '';
		dateTo = query.dateTo || '';
	}

	return (dispatch) => dispatch({
		type: Actions.FETCH_ORDERS,
		[CALL_API]: {
			endpoint: `/api/v1/manager/orders/?name=${ name }&id=${ id }&date_from=${ dateFrom }&date_to=${ dateTo }`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_ORDERS,
				Actions.FAILURE_FETCH_ORDERS
			]
		}
	});
}


export function fetchClients() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_CLIENTS,
		[CALL_API]: {
			endpoint: `/api/v1/manager/clients/?name=`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_CLIENTS,
				Actions.FAILURE_FETCH_CLIENTS
			]
		}
	});
}

export function assignMyself(orderId) {
	return (dispatch) => dispatch({
		type: Actions.ASSIGN_MYSELF,
		[CALL_API]: {
			endpoint: `/api/v1/manager/orders/${ orderId }/`,
			method: 'put',
			types: [
				Actions.SUCCESS_ASSIGN_MYSELF,
				Actions.FAILURE_ASSIGN_MYSELF
			]
		}
	});
}

export function changeOrderStatus(orderId, status) {
	return (dispatch) => dispatch({
		type: Actions.CHANGE_ORDER_STATUS,
		[CALL_API]: {
			endpoint: `/api/v1/manager/orders/${ orderId }/`,
			method: 'post',
			types: [
				Actions.SUCCESS_CHANGE_ORDER_STATUS,
				Actions.FAILURE_CHANGE_ORDER_STATUS
			],
			data: { status }
		}
	});
}