import * as Actions from '../packages-check';
import { CALL_API } from '../../middleware/api';

export function fetchPackages(query) {
	let id = '';
	let number = '';
	let dateFrom = '';
	let dateTo = '';

	if (query) {
		id = query.id || '';
		number = query.number || '';
		dateFrom = query.dateFrom || '';
		dateTo = query.dateTo || '';
	}

	return (dispatch) => dispatch({
		type: Actions.FETCH_PACKAGES_TO_CHECK,
		[CALL_API]: {
			endpoint: `/api/v1/manager/packages/?id=${ id }&number=${ number }&date_from=${ dateFrom }&date_to=${ dateTo }`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_PACKAGES,
				Actions.FAILURE_FETCH_PACKAGES
			]
		}
	});
}

export function fetchPackage(packageId) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_PACKAGE,
		[CALL_API]: {
			endpoint: `/api/v1/manager/packages/${ packageId }/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_PACKAGE,
				Actions.FAILURE_FETCH_PACKAGE
			]
		}
	});
}

export function changePackageStatus(packageId, status) {
	return (dispatch) => dispatch({
		type: Actions.CHANGE_PACKAGE_STATUS,
		[CALL_API]: {
			endpoint: `/api/v1/manager/packages/${ packageId }/`,
			method: 'put',
			types: [
				Actions.SUCCESS_CHANGE_PACKAGE_STATUS,
				Actions.FAILURE_CHANGE_PACKAGE_STATUS
			],
			data: { status }
		}
	});
}