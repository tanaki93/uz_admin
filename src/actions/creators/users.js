import * as Actions from '../users';
import { CALL_API } from '../../middleware/api';

export function fetchUsers() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_USERS,
		[CALL_API]: {
			endpoint: `/api/v1/admin/document/users/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_USERS,
				Actions.FAILURE_FETCH_USERS
			]
		}
	});
}

export function createUser(data) {
	return (dispatch) => dispatch({
		type: Actions.CREATE_USER,
		[CALL_API]: {
			endpoint: `/api/v1/admin/document/users/`,
			method: 'post',
			types: [
				Actions.SUCCESS_CREATE_USER,
				Actions.FAILURE_CREATE_USER
			],
			data
		}
	});
}

export function deleteUser(id) {
	return (dispatch) => dispatch({
		type: Actions.DELETE_USER,
		[CALL_API]: {
			endpoint: `/api/v1/admin/document/users/${id}/`,
			method: 'delete',
			types: [
				Actions.SUCCESS_DELETE_USER,
				Actions.FAILURE_DELETE_USER
			]
		}
	});
}

export function editUser(id, data) {
	return (dispatch) => dispatch({
		type: Actions.EDIT_USER,
		[CALL_API]: {
			endpoint: `/api/v1/admin/document/users/${id}/`,
			method: 'put',
			types: [
				Actions.SUCCESS_EDIT_USER,
				Actions.FAILURE_EDIT_USER
			],
			data
		}
	});
}

