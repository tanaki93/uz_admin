import * as Actions from '../colors-base';
import { CALL_API } from '../../middleware/api';

export function fetchColors() {
	return (dispatch) => dispatch({
		type: Actions.FETCH_COLORS_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/colours/`,
			method: 'get',
			types: [
				Actions.SUCCESS_FETCH_COLORS_BASE,
				Actions.FAILURE_FETCH_COLORS_BASE
			]
		}
	});
}

export function createColor(data) {
	return (dispatch) => dispatch({
		type: Actions.CREATE_COLOR_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/colours/`,
			method: 'post',
			types: [
				Actions.SUCCESS_CREATE_COLOR_BASE,
				Actions.FAILURE_CREATE_COLOR_BASE
			],
			data
		}
	});
}

export function fetchColor(department_id) {
	return (dispatch) => dispatch({
		type: Actions.FETCH_COLOR_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/colours/${ department_id }/`,
			method: 'get',
			types: [
				Actions.FAILURE_FETCH_COLOR_BASE,
				Actions.SUCCESS_FETCH_COLOR_BASE
			]
		}
	});
}

export function deleteColor(department_id) {
	return (dispatch) => dispatch({
		type: Actions.DELETE_COLOR_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/colours/${ department_id }/`,
			method: 'delete',
			types: [
				Actions.FAILURE_DELETE_COLOR_BASE,
				Actions.SUCCESS_DELETE_COLOR_BASE
			]
		}
	});
}

export function editColor(department_id, data) {
	return (dispatch) => dispatch({
		type: Actions.EDIT_COLOR_BASE,
		[CALL_API]: {
			endpoint: `/api/v1/operator/izishop/colours/${ department_id }/`,
			method: 'put',
			types: [
				Actions.SUCCESS_EDIT_COLOR_BASE,
				Actions.FAILURE_EDIT_COLOR_BASE
			],
			data
		}
	});
}