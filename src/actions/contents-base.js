export const FETCH_CONTENTS_BASE = 'FETCH_CONTENTS_BASE';
export const SUCCESS_FETCH_CONTENTS_BASE = 'SUCCESS_FETCH_CONTENTS_BASE';
export const FAILURE_FETCH_CONTENTS_BASE = 'FAILURE_FETCH_CONTENTS_BASE';

export const FETCH_CONTENT_BASE = 'FETCH_CONTENT_BASE';
export const FAILURE_FETCH_CONTENT_BASE = 'FAILURE_FETCH_CONTENT_BASE';
export const SUCCESS_FETCH_CONTENT_BASE = 'SUCCESS_FETCH_CONTENT_BASE';

export const DELETE_CONTENT_BASE = 'DELETE_CONTENT_BASE';
export const FAILURE_DELETE_CONTENT_BASE = 'FAILURE_DELETE_CONTENT_BASE';
export const SUCCESS_DELETE_CONTENT_BASE = 'SUCCESS_DELETE_CONTENT_BASE';

export const CREATE_CONTENT_BASE = 'CREATE_CONTENT_BASE';
export const SUCCESS_CREATE_CONTENT_BASE = 'SUCCESS_CREATE_CONTENT_BASE';
export const FAILURE_CREATE_CONTENT_BASE = 'FAILURE_CREATE_DEPARTMEN_BASET';

export const EDIT_CONTENT_BASE = 'EDIT_CONTENT_BASE';
export const SUCCESS_EDIT_CONTENT_BASE = 'SUCCESS_EDIT_CONTENT_BASE';
export const FAILURE_EDIT_CONTENT_BASE = 'FAILURE_EDIT_CONTENT_BASE';
