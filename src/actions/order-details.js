export const FETCH_ORDER = 'FETCH_ORDER';
export const SUCCESS_FETCH_ORDER = 'SUCCESS_FETCH_ORDER';
export const FAILURE_FETCH_ORDER = 'FAILURE_FETCH_ORDER';

export const CHANGE_PRODUCT_STATUS = 'CHANGE_PRODUCT_STATUS';
export const SUCCESS_CHANGE_PRODUCT_STATUS = 'SUCCESS_CHANGE_PRODUCT_STATUS';
export const FAILURE_CHANGE_PRODUCT_STATUS = 'FAILURE_CHANGE_PRODUCT_STATUS';

export const CHANGE_STAGE = 'CHANGE_STAGE';
export const SUCCESS_CHANGE_STAGE = 'SUCCESS_CHANGE_STAGE';
export const FAILURE_CHANGE_STAGE = 'FAILURE_CHANGE_STAGE';

export const ADMIN_REFUND = 'ADMIN_REFUND';
export const SUCCESS_ADMIN_REFUND = 'SUCCESS_ADMIN_REFUND';
export const FAILURE_ADMIN_REFUND = 'FAILURE_ADMIN_REFUND';