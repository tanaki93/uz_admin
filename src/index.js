import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store, { history } from './store';
import './assets/styles/index.scss';
import { Router } from 'react-router';
import routes from './routes';
import { ConfigProvider } from 'antd';
import ru_RU from 'antd/lib/locale-provider/ru_RU';
import 'moment/locale/ru';
import LocaleProvider from './LocaleProvider.jsx';

require('./intl-polyfill');
// import createDevToolsWindow from './createDevToolsWindow';

const rootEl = document.getElementById('root');

ReactDOM.render(
	<Provider store={ store }>
		<ConfigProvider locale={ ru_RU }>
			<LocaleProvider>
				<Router history={ history }>
					{ routes() }
				</Router>
			</LocaleProvider>
		</ConfigProvider>
	</Provider>, rootEl
);

// if (process.env.NODE_ENV !== 'production') {
// 	createDevToolsWindow(store);
// }
