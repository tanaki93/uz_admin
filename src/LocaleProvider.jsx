import { connect } from 'react-redux';
import { IntlProvider } from 'react-intl';
import localeMap from './constants/languages';
import ruMessages from './localization/ru.json';
import trMessages from './localization/tr.json';

const localeMessagesMap = {
	ru: ruMessages,
	tr: trMessages
};

function select(state) {
	const locale = localeMap[state.application.get('languageId')];
	const messages = localeMessagesMap[locale];

	return {
		locale,
		messages
	};
}

export default connect(select)(IntlProvider);