import React from 'react';
import { Route, Switch, Redirect } from 'react-router';
import PageLayout from './components/_layout/PageLayout';
import Login from './containers/auth';
import Main from './containers/main';
import Brand from './containers/handbook/brand';
import Users from './containers/settings/users';
import Products from './containers/products';
import IziProducts from './containers/izi-products';
import Currencies from './containers/settings/currencies';
import Countries from './containers/settings/countries';
import Cities from './containers/settings/cities';
import Points from './containers/settings/points';
import Languages from './containers/settings/languages';
import Departments from './containers/handbook/departments';
import Colors from './containers/handbook/colors';
import Sizes from './containers/handbook/sizes';
import CategoriesBase from './containers/bases/categories';
import DepartmentBase from './containers/bases/department';
import ClassesBase from './containers/bases/classes';
import SizesBase from './containers/bases/sizes';
import ColorsBase from './containers/bases/colors';
import ContentsBase from './containers/bases/content';
import Checking from './containers/documents/check';
import CreateDocument from './containers/documents/create';
import OperatorDocument from './containers/operator/products';
import Contents from './containers/handbook/contents';
import Orders from './containers/orders/list';
import OrderDetails from './containers/orders/item';
import PackagesCheck from './containers/controller/packages-check';
import ProductsCheck from './containers/controller/products-check';
import ProductDelivery from './containers/controller/product-delivery';
import Packets from './containers/controller/packets';
import Logistics from './containers/logistics/list';
import LogisticsManagement from './containers/logistics/item';
import Profile from './containers/settings/users/profile';
import OrderReturn from './containers/orders/return';
import Refund from './containers/refund';
import ClientRefund from './containers/refund/client';
import Marketing from './containers/marketing';
import MarketingOnly from './containers/marketing/page';
import LandingBrands from './containers/landing/brands';
import Slider from './containers/landing/slider';
import LandingCategories from './containers/landing/categories';

const AdminRoute = ({ component: Component, ...rest }) => (
	<Route { ...rest } render={ props => (
		localStorage.getItem('Token')
			? (<PageLayout component={ Component } { ...props }/>)
			: (<Redirect to={ { pathname: '/login', state: { from: props.location } } }/>)
	) }/>
);

const routes = () => {
	return (
		<Switch>
			<AdminRoute exact path='/' component={ Main }/>
			<AdminRoute exact path='/brand' component={ Brand }/>
			<AdminRoute exact path='/products' component={ Products }/>
			<AdminRoute exact path='/izi-products' component={ IziProducts }/>
			<AdminRoute exact path='/currencies' component={ Currencies }/>
			<AdminRoute exact path='/countries' component={ Countries }/>
			<AdminRoute exact path='/cities' component={ Cities }/>
			<AdminRoute exact path='/points' component={ Points }/>
			<AdminRoute exact path='/languages' component={ Languages }/>
			<AdminRoute exact path='/users' component={ Users }/>
			<AdminRoute exact path='/users/:userId' component={ Profile }/>
			<AdminRoute exact path='/colors' component={ Colors }/>
			<AdminRoute exact path='/contents' component={ Contents }/>
			<AdminRoute exact path='/sizes' component={ Sizes }/>
			<AdminRoute exact path='/brand/:brandId/departments' component={ Departments }/>
			<AdminRoute exact path='/store/departments' component={ DepartmentBase }/>
			<AdminRoute exact path='/store/departments/:departmentId/parents' component={ ClassesBase }/>
			<AdminRoute exact path='/store/departments/:departmentId/parents/:parentId' component={ CategoriesBase }/>
			<AdminRoute exact path='/base/categories' component={ CategoriesBase }/>
			<AdminRoute exact path='/base/sizes' component={ SizesBase }/>
			<AdminRoute exact path='/base/colors' component={ ColorsBase }/>
			<AdminRoute exact path='/base/contents' component={ ContentsBase }/>
			<AdminRoute exact path='/operator-documents/:documentId' component={ OperatorDocument }/>
			<AdminRoute exact path='/documents/create' component={ CreateDocument }/>
			<AdminRoute exact path='/documents/:documentId' component={ Checking }/>
			<AdminRoute exact path='/orders' component={ Orders }/>
			<AdminRoute exact path='/orders/:orderId' component={ OrderDetails }/>
			<AdminRoute exact path='/orders/:orderId/refund' component={ OrderReturn }/>
			<AdminRoute exact path='/packages-check' component={ PackagesCheck }/>
			<AdminRoute exact path='/products-check' component={ ProductsCheck }/>
			<AdminRoute exact path='/product-delivery' component={ ProductDelivery }/>
			<AdminRoute exact path='/packages' component={ Packets }/>
			<AdminRoute exact path='/logistics' component={ Logistics }/>
			<AdminRoute exact path='/marketing' component={ Marketing }/>
			<AdminRoute exact path='/marketing/only' component={ MarketingOnly }/>
			<AdminRoute exact path='/refund' component={ Refund }/>
			<AdminRoute exact path='/refund/client' component={ ClientRefund }/>
			<AdminRoute exact path='/logistics/:id' component={ LogisticsManagement }/>
			<AdminRoute exact path='/landing/brands' component={ LandingBrands }/>
			<AdminRoute exact path='/landing/slider' component={ Slider }/>
			<AdminRoute exact path='/landing/categories' component={ LandingCategories }/>
			<Route exact path='/login' component={ Login }/>
		</Switch>
	);
};

export default routes;
