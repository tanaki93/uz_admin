import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import { Button, Modal } from 'antd';
import ReactToPrint from 'react-to-print';
import moment from 'moment';
import logo from '../../../assets/images/logo_black.svg';
import { useIntl } from 'react-intl';

const Receipt = (props) => {
	const ref = useRef();
	const intl = useIntl();
	const { visible, close, refundItem } = props;

	return (
		<Modal
			bodyStyle={ { padding: 0 } }
			visible={ visible }
			onOk={ close }
			onCancel={ close }
			footer={ [
				<Button key='back' onClick={ close }>
					Закрыть
				</Button>,
				<ReactToPrint
					trigger={ () => <Button type='primary'>Распечатать</Button> }
					content={ ref }
				/>
			] }
		>
			<div className='return-check' ref={ ref }>
				{
					refundItem &&
					<table className='return-check__brief'>
						<tbody>
						<tr>
							<td>Номер чека возврата</td>
							<td>{ 'RET' + refundItem.id }</td>
						</tr>
						<tr>
							<td>{ intl.formatMessage({ id: 'products-delivery.submitter.created-at' }) }</td>
							<td>{ moment(refundItem.created).format('DD/MM/YYYY | HH:mm') }</td>
						</tr>
						<tr>
							<td>Имя клиента</td>
							<td>{ refundItem.order.name }</td>
						</tr>
						</tbody>
					</table>
				}
				<table className='return-check__items'>
					<thead>
					<tr>
						<th>Товар</th>
						<th>Кол-во</th>
						<th>Цена товара</th>
						<th>Цена доставки</th>
					</tr>
					</thead>
					{
						refundItem &&
						<tbody>
						{
							refundItem.items.map(item =>
								<tr>
									<td>
										{
											`${ item.order_item.product.izi_category.languages.find(lang => lang.lang_code === 'RUS')?.translation } ${ item.order_item.product.brand?.name }`
										}
									</td>
									<td>{ item.order_item.amount }</td>
									<td>{ item.order_item.price } c</td>
									<td>{ item.order_item.shipping_price } c</td>
								</tr>
							)
						}
						</tbody>
					}
				</table>

				<p className='return-check__total'>
					Итого возвращено: { refundItem && refundItem.price }
				</p>
				<div className='return-check__footer'>
					Спасибо что выбрали нас!
					<img src={ logo } alt=''/>
				</div>
			</div>
		</Modal>
	);
};

Receipt.propTypes = {
	visible: PropTypes.bool.isRequired,
	close: PropTypes.func.isRequired,
	refundItem: PropTypes.object.isRequired
};

export default Receipt;