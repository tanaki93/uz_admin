import React, { useState } from 'react';
import * as PropTypes from 'prop-types';
import moment from 'moment';
import { Button, Descriptions, Table } from 'antd';
import { useIntl } from 'react-intl';
import Receipt from './Receipt';

const List = (props) => {
	const intl = useIntl();
	const [visible, setVisible] = useState(false);
	const [selected, setSelected] = useState(null);
	const { fetching, data, changeRefundStatus, filter } = props;

	let refundFilter = data;

	switch (filter) {
		default:
		case 0:
			refundFilter = data;
			break;
		case 1:
			refundFilter = data.filter(item => item.status === 1);
			break;
		case 2:
			refundFilter = data.filter(item => item.status === -1);
			break;
	}

	const refundItem = refundFilter.find(refund => refund.id === selected);

	const showModal = (value) => {
		setVisible(true);
		setSelected(value);
	};

	const close = () => {
		setVisible(false);
		setSelected(null);
	};

	const renderRefundStatus = (status) => {
		switch (status) {
			default:
			case 0:
				return <span style={ { color: '#F2994A' } }>
					{ intl.formatMessage({ id: 'packages-check.list.stage' }) }
				</span>;
			case -1:
				return <span style={ { color: '#ED1B23' } }>Не возвращен</span>;
			case 1:
				return <span style={ { color: '#219653' } }>Возвращен</span>;
		}
	};

	const columns = [
		{
			title: 'Заказ',
			dataIndex: ['order', 'id'],
			key: 'order.id',
		},
		{
			title: 'Номер возврата',
			dataIndex: 'id',
			key: 'refundId',
		},
		{
			title: 'Кол-во тов',
			dataIndex: 'items',
			key: 'order_count',
			render: order => order.length
		},
		{
			title: 'Дата добавления',
			dataIndex: 'created',
			key: 'order_date',
			render: date => moment(date).format('DD.MM.YYYY | HH:mm')
		},
		{
			title: 'Сумма возврата',
			dataIndex: 'price',
			key: 'order_price',
		},
		{
			title: 'Статус возврата',
			key: 'status',
			dataIndex: 'status',
			render: status => renderRefundStatus(status)
		},
		{
			key: 'status_changer',
			render: item =>
				<>
					<Button onClick={ () => changeRefundStatus(item.id, 1) } disabled={ item.status === 1 }
					        className={ 'submit-btn' }>Возвращен</Button>
					{ item.status === 1 && <Button onClick={ () => changeRefundStatus(item.id, -1) } type={ 'link' }>
						Отмена
					</Button> }
				</>
		},
		{
			key: 'check_print',
			render: item => <Button id={ item.id } disabled={ item.status !== 1 } className={ 'submit__button' }
			                        onClick={ () => showModal(item.id) }>
				Чек возврата
			</Button>
		},
	];

	const orderColumns = [
		{
			title: 'Товар',
			key: 'productName',
			dataIndex: 'order_item',
			render: entry => `${ entry.product.izi_category.languages.find(lang => lang.lang_code === 'RUS').translation || '' } ${ entry.product.brand.name || '' }`
		},
		{
			title: 'ID заказа товара',
			dataIndex: 'order_item.id'
		},
		{
			title: 'Кол-во',
			dataIndex: 'order_item.amount'
		},
		{
			title: 'Link IZI',
			dataIndex: ['order_item', 'izi_link'],
			render: link => <a href={ link }>Link</a>
		},
		{
			title: 'Link Vend',
			dataIndex: ['order_item', 'link', 'url'],
			render: link => <a href={ link }>Link</a>
		},
		{
			title: 'Цена товара',
			dataIndex: ['order_item', 'price']
		},
		{
			title: 'Цена доставки',
			dataIndex: ['order_item', 'shipping_price']
		},
	];


	return (
		<>
			<Receipt refundItem={ refundItem } visible={ visible } close={ close }/>
			<Table
				loading={ fetching }
				className={ 'izi-table' }
				columns={ columns }
				dataSource={ refundFilter }
				bordered
				size='small'
				rowKey={ 'id' }
				expandedRowRender={ record =>
					<div className={ 'refund-details' }>
						<Descriptions column={ { xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 } }>
							<Descriptions.Item label='Имя клиента'>
								{ record.order.name }
							</Descriptions.Item>
							<Descriptions.Item label='Почта'>
								{ record.order.email }
							</Descriptions.Item>
							<Descriptions.Item label='Номер телефона'>
								{ record.order.phone }
							</Descriptions.Item>
							<Descriptions.Item label='Адрес'>
								{ record.order.address }
							</Descriptions.Item>
						</Descriptions>
						<Table
							loading={ fetching }
							className={ 'izi-table' }
							columns={ orderColumns }
							dataSource={ record.items }
							bordered
							size='small'
							rowKey={ 'id' }
							pagination={ false }
						/>
					</div>
				}
			/>
		</>
	);
};

List.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	changeRefundStatus: PropTypes.func.isRequired,
	filter: PropTypes.number.isRequired
};

export default List;