import React, { useState } from 'react';
import * as PropTypes from 'prop-types';
import { Select } from 'antd';
import CreateDepartment from './CreateDepartment';

const DepartmentSelector = (props) => {
	const [searchValue, setSearchValue] = useState('');
	const { department, assignDepartment, createDepartment, departments, languages } = props;

	const onChange = (value) => {
		assignDepartment(department.id, value);
	};


	const onSearch = (val) => {
		setSearchValue(val)
	};

	return (
		<div>
			<Select
				onChange={ onChange }
				defaultValue={ department.department }
				style={ { width: 200 } }
				showSearch
				optionFilterProp='children'
				filterOption={ (input, option) => (
					option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
				) }
				onSearch={ onSearch }
			>
				<Select.Option value={ null }>Пусто</Select.Option>
				{ departments.map(iziDepartment =>
					<Select.Option value={ iziDepartment.id } key={ iziDepartment.id }>
						{ iziDepartment.languages.find(lang => lang.lang_code === 'RUS').translation }
					</Select.Option>
				) }
			</Select>
			<CreateDepartment createDepartment={ createDepartment } languages={ languages }/>
		</div>
	);
};

DepartmentSelector.propTypes = {
	department: PropTypes.object.isRequired,
	assignDepartment: PropTypes.func.isRequired,
	createDepartment: PropTypes.func.isRequired,
	departments: PropTypes.array.isRequired,
	languages: PropTypes.array.isRequired
};

export default DepartmentSelector;
