import React from 'react';
import * as PropTypes from 'prop-types';
import { PlusOutlined } from '@ant-design/icons';
import { Modal, Button, Input, Table } from 'antd';

class CreateDepartment extends React.Component {
	state = {
		visible: false,
		name: '',
		code: '',
		languages: []
	};

	componentDidMount() {
		const data = [];

		this.props.languages.map(language => data.push({
			lang_id: language.id,
			lang_name: language.name,
			lang_code: language.code,
			translation: null,
		}));

		this.setState({
			languages: data
		});
	}

	onChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};

	onTableChange = (id, e) => {
		const languages = this.state.languages;
		languages.map(language => language.lang_id === id && (language[e.target.name] = e.target.value));
		this.setState({ languages });
	};

	showModal = () => {
		this.setState({
			visible: true,
		});
	};

	handleOk = () => {
		const { name, languages, code } = this.state;
		const data = [];

		languages.map(language => data.push({
			lang_id: language.lang_id,
			translation: language.translation,
		}));

		this.props.createDepartment({ name, code, languages: data, position: null, is_active: false });

		this.setState({
			visible: false,
		});
	};

	handleCancel = e => {
		this.setState({
			visible: false,
		});
	};

	render() {
		const { name, code, languages } = this.state;

		const columns = [
			{
				title: 'Страна',
				dataIndex: 'lang_code'
			},
			{
				title: 'Перевод',
				render: language => <Input defaultValue={ language.translation } name='translation'
				                           onChange={ e => this.onTableChange(language.lang_id, e) }/>
			},
		];

		return (
			<>
				<Button type="primary" onClick={ this.showModal } icon={ <PlusOutlined/> }/>
				<Modal
					title="Создание отделения"
					visible={ this.state.visible }
					onOk={ this.handleOk }
					onCancel={ this.handleCancel }
				>
					<table className={ 'base-table' }>
						<thead>
						<tr>
							<td>Код</td>
							<td>Название</td>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td>
								<Input value={ code } name={ 'code' } onChange={ this.onChange }/>
							</td>
							<td>
								<Input value={ name } name={ 'name' } onChange={ this.onChange }/>
							</td>
						</tr>
						</tbody>
					</table>
					<Table className={ 'izi-table' } columns={ columns } dataSource={ languages } rowKey='lang_id'/>
				</Modal>
			</>
		);
	}
}

CreateDepartment.propTypes = {
	createDepartment: PropTypes.func.isRequired,
	languages: PropTypes.array.isRequired
};

export default CreateDepartment;