import React from 'react';
import * as PropTypes from 'prop-types';
import { EditOutlined } from '@ant-design/icons';
import { Form } from '@ant-design/compatible';

import { Modal, Button, Input, Switch } from 'antd';

class EditBrand extends React.Component {
	state = {
		visible: false,
		name: '',
		link: '',
		is_active: false,
		is_trend_yol: false
	};

	UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
		this.props.selectedBrand !== nextProps.selectedBrand &&
		this.setState({
			name: nextProps.selectedBrand.name,
			link: nextProps.selectedBrand.link,
			is_active: nextProps.selectedBrand.is_active,
			is_trend_yol: nextProps.selectedBrand.is_trend_yol,
		});
	}

	onChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};

	onCheck = (name, checked) => {
		this.setState({
			[name]: checked
		});
	};

	showModal = () => {
		this.setState({
			visible: true,
		});
		this.props.selectBrand();
	};

	handleOk = e => {
		const { name, link, is_trend_yol } = this.state;
		this.props.editBrand(this.props.selectedBrand.id, {
			name,
			link,
			is_active: this.props.selectedBrand.is_active,
			is_trend_yol
		});
		this.setState({
			visible: false,
		});
	};

	handleCancel = e => {
		this.setState({
			visible: false,
		});
	};

	render() {
		const formItemLayout = {
			labelCol: { span: 4 },
			wrapperCol: { span: 14 },
		};

		const { name, link, is_active, is_trend_yol } = this.state;

		return (
			<>
				<Button type="dashed" onClick={ this.showModal } icon={ <EditOutlined/> }>
					Изменить
				</Button>
				<Modal
					title="Изменение бренда"
					visible={ this.state.visible }
					onOk={ this.handleOk }
					onCancel={ this.handleCancel }
				>
					<Form layout='vertical'>
						<Form.Item label="Название" { ...formItemLayout }>
							<Input value={ name } name={ 'name' } onChange={ this.onChange }/>
						</Form.Item>
						<Form.Item label="Ссылка" { ...formItemLayout }>
							<Input value={ link } name={ 'link' } onChange={ this.onChange }/>
						</Form.Item>
						<Form.Item label="Trendyol?" { ...formItemLayout }>
							<Switch checked={ is_trend_yol } name={ 'is_trend_yol' }
							        onChange={ checked => this.onCheck('is_trend_yol', checked) }/>
						</Form.Item>
					</Form>
				</Modal>
			</>
		);
	}
}

EditBrand.propTypes = {
	editBrand: PropTypes.func.isRequired,
	selectBrand: PropTypes.func,
	selectedBrand: PropTypes.object.isRequired
};

export default EditBrand;