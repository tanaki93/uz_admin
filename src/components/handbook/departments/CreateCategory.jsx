import React from 'react';
import * as PropTypes from 'prop-types';
import { PlusOutlined } from '@ant-design/icons';
import { Modal, Button, Input, Table, Checkbox, Select } from 'antd';
import CreateClass from './CreateClass';

class CreateCategory extends React.Component {
	state = {
		visible: false,
		name: '',
		code: '',
		parentId: 0,
		languages: []
	};

	componentDidMount() {
		const data = [];

		this.props.languages.map(language => data.push({
			is_active: true,
			lang_id: language.id,
			lang_name: language.name,
			lang_code: language.code,
			translation: null,
		}));

		this.setState({
			languages: data
		});
	}

	onChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};


	onTableChange = (id, e) => {
		const languages = this.state.languages;
		languages.map(language => language.lang_id === id && (language[e.target.name] = e.target.value));
		this.setState({ languages });
	};

	showModal = () => {
		this.setState({
			visible: true,
		});
	};

	handleOk = () => {
		const { name, languages, code, parentId } = this.state;
		const data = [];

		languages.map(language => data.push({
			is_active: language.is_active,
			lang_id: language.lang_id,
			translation: language.translation,
		}));

		if (name && languages && parentId) {
			this.props.createCategory(parentId, {
				name,
				code,
				languages: data,
				position: null,
				is_active: true,
				parent_id: parentId
			});

			this.setState({
				visible: false,
			});
		}
	};

	handleCancel = e => {
		this.setState({
			visible: false,
		});
	};

	handleSelect = (value) => {
		this.setState({
			parentId: value
		});
	};

	render() {
		const { name, languages, code } = this.state;

		const columns = [
			{
				title: 'Актив',
				render: language => <Checkbox checked={ language.is_active } name='is_active'
				                              onChange={ e => this.onTableChange(language.lang_id, e) }/>
			},
			{
				title: 'Страна',
				dataIndex: 'lang_code'
			},
			{
				title: 'Перевод',
				render: language => <Input defaultValue={ language.translation } name='translation'
				                           onChange={ e => this.onTableChange(language.lang_id, e) }/>
			},
		];

		return (
			<>
				<Button type="primary" onClick={ this.showModal } icon={ <PlusOutlined/> }/>
				<Modal
					width={ '50vw' }
					title="Создание категории"
					visible={ this.state.visible }
					onOk={ this.handleOk }
					onCancel={ this.handleCancel }
				>
					<table className={ 'base-table' }>
						<thead>
						<tr>
							<td>Код</td>
							<td>Название</td>
							<td>Класс</td>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td style={ { width: '100px' } }>
								<Input value={ code } name={ 'code' } onChange={ this.onChange }/>
							</td>
							<td style={ { width: '200px' } }>
								<Input value={ name } name={ 'name' } onChange={ this.onChange }/>
							</td>
							<td style={ { width: '300px' } }>
								<div style={ { display: 'flex' } }>
									<Select onChange={ value => this.handleSelect(value) } style={ { width: '100%' } }>
										{ this.props.parents.map(parent =>
											<Select.Option value={ parent.id }>{ parent.name }</Select.Option>
										) }
									</Select>
									<CreateClass createClass={ this.props.createIziClass }
									             languages={ this.props.languages }
									             departmentId={ this.props.selectedDepartmentId }
									             selectedDepartmentName={ this.props.selectedDepartmentName }/>
								</div>
							</td>
						</tr>
						</tbody>
					</table>
					<Table className={ 'izi-table' } columns={ columns } dataSource={ languages } rowKey='lang_id'/>
				</Modal>
			</>
		);
	}
}

CreateCategory.propTypes = {
	createCategory: PropTypes.func.isRequired,
	createIziClass: PropTypes.func.isRequired,
	languages: PropTypes.array.isRequired,
	parents: PropTypes.array.isRequired,
	selectedDepartmentId: PropTypes.number.isRequired,
	selectedDepartmentName: PropTypes.string
};

export default CreateCategory;