import React from 'react';
import * as PropTypes from 'prop-types';
import { Typography, Table, Switch, Row, Col, Button } from 'antd';
import DepartmentSelector from './DepartmentSelector';
import CategoriesSelector from './CategoriesSelector';

const { Title } = Typography;

class Departments extends React.Component {
	state = {
		brandDepartments: [],
		selectedDepartmentCategories: [],
		selectedDepartmentId: null,
		selectedDepartmentName: '',
	};

	UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
		if (this.props.brandDepartments !== nextProps.brandDepartments) {
			this.setState({
				brandDepartments: nextProps.brandDepartments
			});
		}
	}

	triggerActive = (type, item, checked) => {
		type === 'department' &&
		this.props.editDepartment(item.id, {
			is_active: checked,
		});
		type === 'category' &&
		this.props.editCategory(item.id, {
			is_active: checked,
		});
	};

	selectDepartment = (id, name, categories) => {
		this.setState({
			selectedDepartmentId: id,
			selectedDepartmentName: name,
			selectedDepartmentCategories: categories
		});
		this.props.fetchClasses(id);
	};

	render() {
		const columns = [
			{
				title: 'Активен',
				render: department => <Switch defaultChecked={ department.is_active }
				                              onChange={ checked => this.triggerActive('department', department, checked) }/>
			},
			{
				title: 'Отделение поставщика',
				dataIndex: 'name'
			},
			{
				title: 'Отделение IZI',
				render: department => <DepartmentSelector
					departments={ this.props.departments }
					department={ department }
					languages={ this.props.languages }
					parents={ this.props.parents }
					assignDepartment={ this.props.assignDepartment }
					createDepartment={ this.props.createIziDepartment }

				/>
			},
			{
				render: department => <Button
					type={ department.department === this.state.selectedDepartmentId ? 'primary' : 'default' }
					onClick={ () => this.selectDepartment(department.department, department.name, department.categories) }> > </Button>
			}
		];


		const categoryColumns = [
			{
				title: 'Активен',
				render: category => <Switch defaultChecked={ category.is_active }
				                            onChange={ checked => this.triggerActive('category', category, checked) }/>
			},
			{
				title: 'Категория поставщика',
				dataIndex: 'name'
			},
			{
				title: 'Категория IZI',
				render: category => <CategoriesSelector
					createIziClass={ this.props.createIziClass }
					languages={ this.props.languages }
					parents={ this.props.parents }
					category={ category }
					selectedDepartmentId={ this.state.selectedDepartmentId }
					selectedDepartmentName={ this.state.selectedDepartmentName }
					iziCategories={ this.props.categories }
					assignCategory={ this.props.assignCategory }
					createCategory={ this.props.createIziCategory }
					deleteCategory={ this.props.deleteCategory }
				/>
			}
		];

		return (
			<>
				<Row type="flex" justify="space-between">
					<Col><Title level={ 3 }>Отделения</Title></Col>
				</Row>
				<br/>
				<Row type="flex" justify="space-between" gutter={ 8 }>
					<Col span={ 12 }>
						<Table className={ 'izi-table' } columns={ columns }
						       dataSource={ this.state.brandDepartments.departments } size='small'
						       loading={ this.props.fetching }
						       rowKey={ 'id' }/>
					</Col>
					{
						this.state.selectedDepartmentCategories.length > 0 &&
						<Col span={ 12 }>
							<Table className={ 'izi-table' } columns={ categoryColumns }
							       dataSource={ this.state.selectedDepartmentCategories }
							       size='small' rowKey={ 'id' }/>
						</Col>
					}

				</Row>
			</>
		);
	}
}

Departments.propTypes = {
	editDepartment: PropTypes.func.isRequired,
	assignDepartment: PropTypes.func.isRequired,
	createDepartment: PropTypes.func.isRequired,
	deleteDepartment: PropTypes.func.isRequired,
	assignCategory: PropTypes.func.isRequired,
	createCategory: PropTypes.func.isRequired,
	deleteCategory: PropTypes.func.isRequired,
	editCategory: PropTypes.func.isRequired,
	createIziDepartment: PropTypes.func.isRequired,
	createIziCategory: PropTypes.func.isRequired,
	createIziClass: PropTypes.func.isRequired,
	fetchClasses: PropTypes.func.isRequired,
	fetching: PropTypes.bool.isRequired,
	brandDepartments: PropTypes.object.isRequired,
	departmentsFetching: PropTypes.bool.isRequired,
	departments: PropTypes.array.isRequired,
	categories: PropTypes.array.isRequired,
	languages: PropTypes.array.isRequired,
	parents: PropTypes.array.isRequired,
};

export default Departments;
