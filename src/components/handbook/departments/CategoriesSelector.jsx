import React, { useState } from 'react';
import * as PropTypes from 'prop-types';
import { Select } from 'antd';
import CreateCategory from './CreateCategory';

const CategoriesSelector = (props) => {
	const { category, assignCategory, createCategory, deleteCategory, createIziClass, iziCategories, parents, languages, selectedDepartmentId, selectedDepartmentName } = props;
	const [searchValue, setSearchValue] = useState('');

	const onSearch = (val) => {
		setSearchValue(val);
	};

	const onChange = (value) => {
		value !== null ? assignCategory(category.id, value) : deleteCategory(category.id);
	};

	return (
		<div>
			<Select defaultValue={ category.category }
			        style={ { width: 200 } }
			        showSearch
			        optionFilterProp='children'
			        filterOption={ (input, option) => (
				        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
			        ) }
			        onChange={ onChange }
			        onSearch={ onSearch }
			>
				<Select.Option value={ null }>Пусто</Select.Option>
				{ iziCategories.filter(category => category.parent.department.id === selectedDepartmentId).map(iziCategory =>
					<Select.Option value={ iziCategory.id } key={ iziCategory.id }>
						{ iziCategory.name }
					</Select.Option>
				) }
			</Select>
			<CreateCategory
				createCategory={ createCategory } languages={ languages }
				parents={ parents } createIziClass={ createIziClass }
				selectedDepartmentId={ selectedDepartmentId }
				selectedDepartmentName={ selectedDepartmentName }
			/>
		</div>
	);
};

CategoriesSelector.propTypes = {
	category: PropTypes.object.isRequired,
	assignCategory: PropTypes.func.isRequired,
	createCategory: PropTypes.func.isRequired,
	deleteCategory: PropTypes.func.isRequired,
	createIziClass: PropTypes.func.isRequired,
	iziCategories: PropTypes.array.isRequired,
	parents: PropTypes.array.isRequired,
	languages: PropTypes.array.isRequired,
	selectedDepartmentId: PropTypes.number.isRequired,
	selectedDepartmentName: PropTypes.string
};

export default CategoriesSelector;