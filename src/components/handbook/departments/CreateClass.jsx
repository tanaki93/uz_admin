import React from 'react';
import * as PropTypes from 'prop-types';
import { PlusOutlined } from '@ant-design/icons';
import { Modal, Button, Input, Table, Checkbox } from 'antd';

class CreateClass extends React.Component {
	state = {
		visible: false,
		name: '',
		code: '',
		languages: []
	};

	UNSAFE_componentWillReceiveProps(nextProps) {
		const data = [];

		nextProps.languages.map(language => data.push({
			is_active: true,
			lang_id: language.id,
			lang_name: language.name,
			lang_code: language.code,
			translation: null,
		}));

		this.setState({
			languages: data
		});
	}

	onChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};


	onTableChange = (id, e) => {
		const languages = this.state.languages;
		languages.map(language => language.lang_id === id && (language[e.target.name] = e.target.value));
		this.setState({ languages });
	};

	showModal = () => {
		const data = [];

		this.props.languages.map(language => data.push({
			is_active: true,
			lang_id: language.id,
			lang_name: language.name,
			lang_code: language.code,
			translation: null,
		}));

		this.setState({
			visible: true,
			languages: data
		});
	};

	handleOk = () => {
		const { name, languages, code } = this.state;
		const data = [];

		languages.map(language => data.push({
			is_active: language.is_active,
			lang_id: language.lang_id,
			translation: language.translation,
		}));
		if (name && code) {
			this.props.createClass(this.props.departmentId, {
				name,
				code,
				languages: data,
				position: null,
				is_active: true,
				department_id: parseInt(this.props.departmentId)
			});

			this.setState({
				visible: false,
				name: '',
				code: '',
				languages: []
			});
		}
	};

	handleCancel = e => {
		this.setState({
			visible: false,
		});
	};

	render() {
		const { name, languages, code } = this.state;

		const columns = [
			{
				title: 'Актив',
				render: language => <Checkbox checked={ language.is_active } name='is_active'
				                              onChange={ e => this.onTableChange(language.lang_id, e) }/>
			},
			{
				title: 'Страна',
				dataIndex: 'lang_code'
			},
			{
				title: 'Перевод',
				render: language => <Input defaultValue={ language.translation } name='translation'
				                           onChange={ e => this.onTableChange(language.lang_id, e) }/>
			},
		];

		return (
			<>
				<Button type="primary" onClick={ this.showModal } icon={ <PlusOutlined/> }/>
				<Modal
					title={ `Создание класса ${ this.props.selectedDepartmentName }` }
					visible={ this.state.visible }
					onOk={ this.handleOk }
					onCancel={ this.handleCancel }
				>
					<table className={ 'base-table' }>
						<thead>
						<tr>
							<td>Код</td>
							<td>Название</td>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td>
								<Input value={ code } name={ 'code' } onChange={ this.onChange }/>
							</td>
							<td>
								<Input value={ name } name={ 'name' } onChange={ this.onChange }/>
							</td>
						</tr>
						</tbody>
					</table>
					<Table className={ 'izi-table' } columns={ columns } dataSource={ languages } rowKey='lang_id'/>
				</Modal>
			</>
		);
	}
}

CreateClass.propTypes = {
	createClass: PropTypes.func.isRequired,
	languages: PropTypes.array.isRequired,
	departmentId: PropTypes.number.isRequired,
	selectedDepartmentName: PropTypes.string
};

export default CreateClass;