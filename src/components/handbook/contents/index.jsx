import React from 'react';
import { Typography, Table, Row, Col } from 'antd';

const { Title } = Typography;

class Departments extends React.Component {
	render() {

		const columns = [
			{
				title: 'Состав поставщика',
			},
			{
				title: 'Состав IZI',
			}
		];

		return (
			<>
				<Row type="flex" justify="space-between">
					<Col><Title level={ 3 }>Составы</Title></Col>
				</Row>
				<br/>
				<Row type="flex" justify="space-between" gutter={ 8 }>
					<Col span={ 12 }>
						<Table className={ 'izi-table' } columns={ columns }
						       dsize='small'
						       rowKey={ 'id' }/>
					</Col>
				</Row>
			</>
		);
	}
}

export default Departments;