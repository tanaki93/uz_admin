import React from 'react';
import * as PropTypes from 'prop-types';
import { Typography, Table, Row, Col, Radio } from 'antd';
import SizeSelector from './SizeSelector';

const { Title } = Typography;

class Departments extends React.Component {
	state = {
		assigned: true,
		selectedDepartmentCategories: [],
		selectedDepartmentId: null,
		selectedDepartmentName: '',
	};

	UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
		if (this.props.brandDepartments !== nextProps.brandDepartments) {
			this.setState({
				brandDepartments: nextProps.brandDepartments
			});
		}
	}

	selectDepartment = (id, name, categories) => {
		this.setState({
			selectedDepartmentId: id,
			selectedDepartmentName: name,
			selectedDepartmentCategories: categories
		});
		this.props.fetchClasses(id);
	};

	handleAssignedChange = e => {
		this.setState({ assigned: e.target.value });
	};

	render() {
		const { assigned } = this.state;
		const { iziSizes } = this.props;
		const sizes = assigned ? this.props.sizes.filter(item => item.izi_size !== null) : this.props.sizes.filter(item => item.izi_size === null);

		const columns = [
			{
				title: 'Размер поставщика',
				dataIndex: 'name'
			},
			{
				title: 'Размер IZI',
				render: size => <SizeSelector
					size={ size }
					sizes={ iziSizes }
					languages={ this.props.languages }
					assignSize={ this.props.assignSize }
					createSize={ this.props.createSize }
				/>
			}
		];

		return (
			<>
				<Row type="flex" justify="space-between">
					<Col><Title level={ 3 }>Размеры</Title></Col>
				</Row>
				<Radio.Group value={ assigned } onChange={ this.handleAssignedChange }>
					<Radio.Button value={ true }>Назначенные</Radio.Button>
					<Radio.Button value={ false }>Не назначенные</Radio.Button>
				</Radio.Group>
				<br/>
				<br/>
				<Row type="flex" justify="space-between" gutter={ 8 }>
					<Col span={ 12 }>
						<Table className={ 'izi-table' } columns={ columns } loading={ this.props.fetching }
						       dataSource={ sizes } size='small'
						       rowKey={ 'id' }/>
					</Col>
				</Row>
			</>
		);
	}
}

Departments.propTypes = {
	fetching: PropTypes.bool.isRequired,
	sizes: PropTypes.array.isRequired,
	iziSizesFetching: PropTypes.bool.isRequired,
	iziSizes: PropTypes.array.isRequired,
	languages: PropTypes.array.isRequired,
	assignSize: PropTypes.func.isRequired,
	createSize: PropTypes.func.isRequired
};

export default Departments;