import React from 'react';
import * as PropTypes from 'prop-types';
import { Select } from 'antd';
import CreateSize from '../../bases/sizes/CreateSize';

class SizeSelector extends React.Component {
	state = {
		selectInputValue: ''
	};

	onSearch = (val) => {
		this.setState({
			selectInputValue: val
		});
	};

	onChange = (value) => {
		this.props.assignSize(this.props.size.id, value);
	};


	render() {
		const { size, sizes, languages } = this.props;

		return (
			<div>
				<Select defaultValue={ size.izi_size ? size.izi_size.id : '' }
				        style={ { width: 200 } }
				        showSearch
				        optionFilterProp="children"
				        filterOption={ (input, option) => (
					        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
				        ) }
				        onChange={ value => this.onChange(value) }
				        onSearch={ this.onSearch }
				>
					{ sizes.map(size =>
						<Select.Option value={ size.id } key={ size.id }>
							{ size.name }
						</Select.Option>
					) }
				</Select>
				<CreateSize createSize={ this.props.createSize }
				            languages={ languages }
				            sizes={ sizes }
				            custom/>
			</div>
		);
	}
};

SizeSelector.propTypes = {
	size: PropTypes.object.isRequired,
	sizes: PropTypes.array.isRequired,
	languages: PropTypes.array.isRequired,
	assignSize: PropTypes.func.isRequired,
	createSize: PropTypes.func.isRequired,
};

export default SizeSelector;