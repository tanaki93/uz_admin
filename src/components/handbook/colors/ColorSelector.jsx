import React, { useState } from 'react';
import * as PropTypes from 'prop-types';
import { Select } from 'antd';
import CreateColor from '../../bases/colors/CreateColor';

const ColorSelector = (props) => {
	const [input, setInput] = useState('');
	const { color, colors, assignColor, createColor, languages } = props;

	const onSearch = (val) => {
		setInput(val);
	};

	const onChange = (value) => {
		assignColor(color.id, value);
	};

	return (
		<div>
			<Select defaultValue={ color.izi_colour ? color.izi_colour.id : null }
			        style={ { width: 200 } }
			        showSearch
			        optionFilterProp='children'
			        filterOption={ (input, option) => (
				        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
			        ) }
			        onChange={ onChange }
			        onSearch={ onSearch }
			        notFoundContent={
				        <CreateColor colors={ colors } languages={ languages } createColor={ createColor }/>
			        }
			>
				{ colors.map(color =>
					<Select.Option value={ color.id } key={ color.id }>
						{ color.name }
					</Select.Option>
				) }
			</Select>
		</div>
	);
};

ColorSelector.propTypes = {
	color: PropTypes.object.isRequired,
	colors: PropTypes.array.isRequired,
	assignColor: PropTypes.func.isRequired,
	createColor: PropTypes.func.isRequired,
	languages: PropTypes.array.isRequired
};

export default ColorSelector;