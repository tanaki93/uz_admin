import React, { useState } from 'react';
import * as PropTypes from 'prop-types';
import { Table, PageHeader, Tabs } from 'antd';
import ColorSelector from './ColorSelector';

const { TabPane } = Tabs;

const Departments = (props) => {
	const [assigned, assign] = useState(true);
	const { fetching, colors, iziColors, languages, assignColor, createColor } = props;

	const handleAssignedChange = (value) => {
		switch (+value) {
			default:
			case 0: assign(false); break;
			case 1: assign(true); break;
		}
	};

	const _colors = assigned ? colors.filter(item => item.izi_colour !== null) : colors.filter(item => item.izi_colour === null);

	const columns = [
		{
			title: 'Цвет поставщика',
			dataIndex: 'name'
		},
		{
			title: 'Цвет IZI',
			render: color => <ColorSelector
				color={ color }
				languages={ languages }
				colors={ iziColors }
				assignColor={ assignColor }
				createColor={ createColor }
			/>
		}
	];

	return (
		<>
			<PageHeader ghost={ false }title='Цвета'
			            footer={
				            <Tabs size='small' defaultActiveKey='1' onChange={ handleAssignedChange }>
					            <TabPane tab='Назначенные' key='1'/>
					            <TabPane tab='Не назначенные' key='0'/>
				            </Tabs>
			            }
			/>
			<div className='page-wrapper'>
				<Table className={ 'izi-table' } columns={ columns } loading={ fetching }
				       dataSource={ _colors } size='small' rowKey={ 'id' }/>
			</div>
		</>
	);
};

Departments.propTypes = {
	fetching: PropTypes.bool.isRequired,
	colors: PropTypes.array.isRequired,
	iziColors: PropTypes.array.isRequired,
	languages: PropTypes.array.isRequired,
	assignColor: PropTypes.func.isRequired,
	createColor: PropTypes.func.isRequired,
};

export default Departments;