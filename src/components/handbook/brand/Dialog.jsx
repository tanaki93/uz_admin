import React, { useEffect, useState, createRef } from 'react';
import * as PropTypes from 'prop-types';
import { Modal, Input, Select, Row, Col, Table, Form } from 'antd';

const Dialog = (props) => {
	const [countriesState, setCountriesState] = useState([]);
	const { visible, close, create, edit, selected, currencies } = props;
	const formRef = createRef();

	useEffect(() => {
		if (selected.id === undefined) {
			const countries = [];

			countries.map(country => props.countries.push({
				country_code: country.code,
				country_id: country.id,
				country_name: country.name,
				mark_up: null,
				round_digit: null,
				round_to: null,
			}));

			setCountriesState(countries);
		} else {
			setCountriesState(selected.countries);
		}
	}, [selected]);

	const onTableChange = (id, e) => {
		const countries = [...countriesState];
		const toSet = countries.findIndex(c => c.country_id === id);
		countries[toSet] = { ...countries[toSet], [e.target.name]: e.target.value };
		setCountriesState(countries);
	};

	const handleOk = () => {
		const form = formRef.current;
		const countriesData = [];

		form.validateFields().then(values => {
			countriesState.map(country => countriesData.push({
				country_id: country.country_id,
				round_to: country.round_to,
				round_digit: country.round_digit,
				mark_up: country.mark_up
			}));

			const data = {
				...values,
				countries: countriesData,
				is_active: selected.is_active || false
			};

			selected.id === undefined ? create(data) : edit(selected.id, data);

			form.resetFields();
			close();
		});
	};

	const columns = [
		{
			title: 'Страна',
			dataIndex: 'country_code'
		},
		{
			title: 'Markup',
			render: country => <Input defaultValue={ country.mark_up } name='mark_up'
			                          onChange={ e => onTableChange(country.country_id, e) }/>
		},
		{
			title: 'Скругление',
			render: country => <Input defaultValue={ country.round_digit } name='round_digit'
			                          onChange={ e => onTableChange(country.country_id, e) }/>
		},
		{
			title: 'Скругление на',
			render: country => <Input defaultValue={ country.round_to } name='round_to'
			                          onChange={ e => onTableChange(country.country_id, e) }/>
		},
	];

	const defaultRules = [{ required: true, message: 'Это обязательное поле' }];

	const formItemLayout = {
		labelCol: {
			xl: { span: 24 },
		},
		wrapperCol: {
			xl: { span: 24 },
		},
	};

	return (
		<Modal
			width={ '90vw' }
			title={ selected.id ? 'Изменение бренда ' + selected.name : 'Создание бренда' }
			visible={ visible }
			onOk={ handleOk }
			onCancel={ close }
		>
			{ visible &&
			<>
				<Form
					{ ...formItemLayout }
					ref={ formRef }
					initialValues={ {
						code: selected.code || '',
						name: selected.name || '',
						currency_id: selected.currency?.id || '',
						link: selected.link || '',
						delivery_price: selected.delivery_price || '',
						delivery_day: selected.delivery_day || '',
						delivery_percent: selected.delivery_percent || '',
					} }
				>
					<Row gutter={ 24 }>
						<Col span={ 6 }>
							<Form.Item label='Код' name='code' rules={ defaultRules }>
								<Input/>
							</Form.Item>
						</Col>
						<Col span={ 6 }>
							<Form.Item label='Название *ПУ' name='name' rules={ defaultRules }>
								<Input/>
							</Form.Item>
						</Col>
						<Col span={ 6 }>
							<Form.Item label='Валюта' name='currency_id' rules={ defaultRules }>
								<Select>
									{
										currencies.map(currency =>
											<Select.Option key={ 'currency_' + currency.id } value={ currency.id }>
												{ currency.name }
											</Select.Option>
										)
									}
								</Select>
							</Form.Item>
						</Col>
						<Col span={ 6 }>
							<Form.Item label='Ссылка' name='link' rules={ defaultRules }>
								<Input/>
							</Form.Item>
						</Col>
						<Col span={ 6 }>
							<Form.Item label='Срок доставки' name='delivery_day' rules={ defaultRules }>
								<Input/>
							</Form.Item>
						</Col>
						<Col span={ 6 }>
							<Form.Item label='Цена доставки за 1 кг (кг/$)' name='delivery_price'
							           rules={ defaultRules }>
								<Input/>
							</Form.Item>
						</Col>
						<Col span={ 6 }>
							<Form.Item label='% от суммы доставки, на доп. расходы' name='delivery_percent'
							           rules={ defaultRules }>
								<Input/>
							</Form.Item>
						</Col>
					</Row>
				</Form>
				<Table className={ 'izi-table' } pagination={ false } columns={ columns } dataSource={ countriesState }
				       rowKey='country_id'/>
			</>
			}
		</Modal>
	);
};

Dialog.propTypes = {
	visible: PropTypes.bool.isRequired,
	close: PropTypes.func.isRequired,
	create: PropTypes.func.isRequired,
	edit: PropTypes.func.isRequired,
	selected: PropTypes.object,
	currencies: PropTypes.array.isRequired,
	countries: PropTypes.array.isRequired,
};

export default Dialog;