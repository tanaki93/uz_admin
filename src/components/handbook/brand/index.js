import React, { useRef, useState } from 'react';
import * as PropTypes from 'prop-types';
import { EditOutlined, PlusOutlined, UnorderedListOutlined } from '@ant-design/icons';
import { Typography, Table, Switch, Button, Row, Col, PageHeader } from 'antd';
import { Link } from 'react-router-dom';
import Dialog from './Dialog';

const { Title } = Typography;

const Brand = (props) => {
	const wrapper = useRef(null);
	const [visible, setVisible] = useState(false);
	const [selected, setSelected] = useState({});
	const { refreshBrand, createBrand, editBrand, fetching, data, currencies, countries } = props;

	const showModal = (selected) => {
		setVisible(true);
		selected && setSelected(selected);
	};

	const closeModal = () => {
		setVisible(false);
		setSelected({});
	};

	const triggerActive = (brand, checked) => {
		editBrand(brand.id, {
			name: brand.name,
			link: brand.link,
			is_active: checked,
			code: brand.code,
			countries: brand.countries,
			currency_id: brand.currency.id
		});
	};

	const columns = [
		{
			title: 'Активен',
			render: brand => <Switch defaultChecked={ brand.is_active }
			                         onChange={ checked => triggerActive(brand, checked) }/>
		},
		{
			title: 'Код',
			dataIndex: 'code'
		},
		{
			title: 'Название',
			dataIndex: 'name'
		},
		{
			title: 'Валюта ',
			dataIndex: 'currency',
			render: currency => currency ? currency.code : ''
		},
		{
			render: brand => <Row type={ 'flex' } justify={ 'end' }>
				<Button onClick={ () => showModal(brand) } icon={ <EditOutlined/> }>
					Изменить
				</Button>
				&nbsp;
				<Link to={ `brand/${ brand.id }/departments` }>
					<Button type='primary' icon={ <UnorderedListOutlined/> }>Отделения</Button>
				</Link>
			</Row>
		}
	];

	return (
		<>
			<PageHeader
				ghost={ false }
				title='Поставщики'
				extra={ [
					<Button key='createBrand' type='primary' onClick={ showModal } icon={ <PlusOutlined/> }>
						Создать
					</Button>
				] }
			/>
			<div className='page-wrapper page-wrapper--small' ref={ wrapper }>
				<Table
					scroll={ { y: wrapper.current?.clientHeight - 114 } }
					pagination={ false }
					className='izi-table'
					size='small'
					rowKey='id'
					columns={ columns }
					dataSource={ data }
					loading={ fetching }
				/>
			</div>
			<Dialog
				visible={ visible }
				close={ closeModal }
				create={ createBrand }
				edit={ editBrand }
				selected={ selected }
				currencies={ currencies }
				countries={ countries }
			/>
		</>
	);
};

Brand.propTypes = {
	refreshBrand: PropTypes.func.isRequired,
	createBrand: PropTypes.func.isRequired,
	editBrand: PropTypes.func.isRequired,
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	currencies: PropTypes.array.isRequired,
	countries: PropTypes.array.isRequired
};

export default Brand;