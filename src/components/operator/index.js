import React, { useEffect } from 'react';
import * as PropTypes from 'prop-types';
import { Typography, Table, Row, Col, Button, PageHeader } from 'antd';
import moment from 'moment';
import { Link } from 'react-router-dom';
import * as processingSteps from './../../constants/processingSteps';
import { useIntl } from 'react-intl';

const { Title } = Typography;

const DocumentsComponent = (props) => {
	const intl = useIntl();
	const { fetching, fetchDocuments, assignDocument, documents, all } = props;

	useEffect(() => {
		fetchDocuments();
	}, []);

	const renderStatus = (status) => {
		switch (status) {
			default:
				return <span style={ { color: 'blue' } }>В обработке</span>;
			case processingSteps.END:
				return <span style={ { color: 'orange' } }>Готово на проверку</span>;
			case processingSteps.APPROVED:
				return <span style={ { color: 'green' } }>в IZISHOP</span>;
		}
	};

	const columns = [
		{
			title: 'ID',
			dataIndex: 'id'
		},
		{
			title: intl.formatMessage({ id: 'products-check.details.brand' }),
			dataIndex: 'brand'
		},
		{
			title: 'Отделение',
			dataIndex: 'department'
		},
		{
			title: 'Кол-во товаров',
			render: item => <><span>{ item.products }</span> <br/>
				(<span style={ { color: 'green' } }>{ item.products - item.no_stock_or_price }</span> / <span
					style={ { color: 'red' } }>{ item.no_stock_or_price }</span>)</>
		},
		{
			title: intl.formatMessage({ id: 'packages-check.list.updated-at' }),
			dataIndex: 'updated_at',
			render: updatedAt => moment(updatedAt).format('DD MMM YYYY, HH:mm')
		},
		{
			title: 'Статус',
			dataIndex: 'step',
			render: step => renderStatus(step)
		},
		{
			render: item => item.step < processingSteps.END &&
				<Link to={ `operator-documents/${ item.id }` }>Перейти</Link>
		},
	];

	const columns2 = [
		{
			title: 'ID',
			dataIndex: 'id'
		},
		{
			title: intl.formatMessage({ id: 'products-check.details.brand' }),
			dataIndex: 'brand'
		},
		{
			title: 'Отделение',
			dataIndex: 'department'
		},
		{
			title: 'Кол-во товаров',
			render: item => <><span>{ item.products }</span> (<span
				style={ { color: 'green' } }>{ item.products - item.no_stock_or_price }</span>/<span
				style={ { color: 'red' } }>{ item.no_stock_or_price }</span>)</>
		},
		{
			title: intl.formatMessage({ id: 'packages-check.list.updated-at' }),
			dataIndex: 'updated_at',
			render: updatedAt => moment(updatedAt).format('DD MMM YYYY, HH:mm')
		},
		{
			title: 'Статус',
			dataIndex: 'step',
			render: step => renderStatus(step)
		},
		{
			render: doc => <Button disabled={ doc.user }
			                       onClick={ () => assignDocument(doc.id) }>{ doc.user ? 'На обработке' : 'Взять на обработку' }</Button>
		},
	];
	return (
		<>
			<PageHeader
				ghost={ false }
				title='Документы'
			/>
			<div className='page-wrapper page-wrapper--small'>
				<Row type='flex' gutter={ [16, 32] }>
					<Col span={ 24 }>
						<Title level={ 4 }>Документы для обработки</Title>
						<Table loading={ fetching } columns={ columns2 }
						       dataSource={ all.filter(doc => !doc.user) } size='small' rowKey={ 'id' }/>
					</Col>
					<Col span={ 24 }>
						<Title level={ 4 }>В обработке</Title>
						<Table loading={ fetching } columns={ columns } dataSource={ documents }
						       size='small'
						       rowKey={ 'id' }/>
					</Col>
				</Row>
			</div>
		</>
	);
};

DocumentsComponent.propTypes = {
	fetchDocuments: PropTypes.func.isRequired,
	assignDocument: PropTypes.func.isRequired,
	fetching: PropTypes.bool.isRequired,
	documents: PropTypes.array.isRequired,
	all: PropTypes.array.isRequired,
};

export default DocumentsComponent;