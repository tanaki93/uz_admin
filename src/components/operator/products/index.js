import '../../../assets/styles/slider.css';
import React, { useEffect, useRef, useState } from 'react';
import { useHistory } from 'react-router-dom';
import * as PropTypes from 'prop-types';
import { useIntl } from 'react-intl';
import { LoadingOutlined } from '@ant-design/icons';
import { Icon as LegacyIcon } from '@ant-design/compatible';
import { Row, Col, Button, Modal, PageHeader, Form, Table } from 'antd';
import Steps from './Steps';
import Filter from './Filter';
import BulkUpdater from './BulkUpdater';
import moment from 'moment';
import * as processingSteps from './../../../constants/processingSteps';
import Selector from './Selector';
import EditOutlined from '@ant-design/icons/lib/icons/EditOutlined';
import Details from './Details';

const DocumentProducts = (props) => {
	const wrapper = useRef(null);
	const intl = useIntl();
	const history = useHistory();
	const [selected, setSelected] = useState([]);
	const [comments, setComments] = useState(false);
	const {
		fetching, documentId, products, hierarchy, fetchProducts, processProducts, processProduct, bulkProducts,
		createContentBase, languages, contents, count
	} = props;

	const dropSelection = () => {
		setSelected([]);
	};

	const showModal = () => {
		setComments(true);
	};

	const handleOk = e => {
		setComments(false);
	};

	const handleCancel = e => {
		setComments(false);
	};

	const getSelectedKeys = (selection) => {
		setSelected(selection);
	};

	const processDocument = () => {
		processProducts(documentId);
		window.scrollTo(0, 0);
	};

	const [visible, toggleVisible] = useState(false);
	const [product, setProduct] = useState({});
	const _processProduct = (id, optionName, optionId) => {
		processProduct(id, { option: optionName, id: optionId });
	};

	const _getSelectedKeys = (selection) => {
		getSelectedKeys(selection);
	};

	const onClickHandler = (object, index) => {
		setProduct(object);
		toggleVisible(true);
	};

	const close = e => {
		toggleVisible(false);
	};

	const getData = (type, departmentId) => {
		if (type === 'department') {
			return hierarchy.brand?.departments;
		}
		if (type === 'category') {
			return hierarchy.brand?.departments.find(dep => dep.id === departmentId)?.categories;
		}

		return [];
	};

	const columns = [
		{
			title: 'Картинка',
			render: product =>
				<img alt={ product.title } src={ product.images[0] } style={ { width: 50, height: 70 } }/>
		},
		{
			title: intl.formatMessage({ id: 'products-check.details.brand' }),
			dataIndex: 'brand',
			render: brand => brand ? brand.name : '',
			width: 80
		},
		{
			title: 'Название',
			dataIndex: 'title',
			width: 240
		},
		{
			title: 'Отделение',
			dataIndex: ['izi_department', 'name'],
			key:'department',
			editable: true,
			width: 200
		},
		{
			title: 'Категория',
			dataIndex: ['izi_category', 'name'],
			key:'category',
			editable: true,
			width: 200
		},
		{
			title: 'Цвета',
			dataIndex: ['colour', 'name'],
			key:'colour',
			editable: true,
			width: 200
		},
		{
			title: intl.formatMessage({ id: 'products-check.details.composition' }),
			dataIndex: ['content', 'name'],
			key:'content',
			editable: true
		},
		{
			title: intl.formatMessage({ id: 'packages.list.details' }),
			render: record => <a
				onClick={ () => onClickHandler(record) }>{ intl.formatMessage({ id: 'packages.list.details' }) }</a>,
			width: 100
		},
	];

	const EditableCell = ({ title, editable, children, dataIndex, record, handleSave, ...restProps }) => {
		const [editing, setEditing] = useState(false);

		const toggleEdit = () => {
			setEditing(!editing);
		};

		const save = async (value) => {
			try {
				toggleEdit();
				switch (dataIndex[0]) {
					case 'izi_department': _processProduct(record.id, 'department', value); break;
					case 'izi_category': _processProduct(record.id, 'category', value); break;
					default: _processProduct(record.id, dataIndex[0], value); break;
				}

			} catch (errInfo) {
				console.log('Save failed:', errInfo);
			}
		};

		const blur = async => {
			toggleEdit();
		};

		let data = [];

		switch (dataIndex ? dataIndex[0] : '') {
			default:
			case 'izi_department':
				data = getData('department');
				break;
			case 'izi_category':
				data = getData('category', record.izi_department.id);
				break;
			case 'colour':
				data = hierarchy.colours;
				break;
			case 'content':
				data = hierarchy.contents;
				break;
		}

		let childNode = children;

		if (editable) {
			childNode = editing ? (
				<Form initialValues={ { [dataIndex[0]]: record[dataIndex[0]]?.id } }>
					<Form.Item style={ { margin: 0, } } name={ dataIndex[0] }
					           rules={ [{ required: true, message: `${ title } is required.` }] }>
						<Selector
							id={ product.id }
							type={ dataIndex[0] }
							data={ data }
							record={ record }
							onChange={ save }
							onBlur={ blur }
							processProduct={ _processProduct }
						/>
					</Form.Item>
				</Form>
			) : (
				<div className='editable-cell-value-wrap' style={ { paddingRight: 24 } } onClick={ toggleEdit }>
					{ children } <EditOutlined style={ { color: 'red' } }/>
				</div>
			);
		}

		return <td { ...restProps }>{ childNode }</td>;
	};

	const components = {
		body: {
			cell: EditableCell,
		}
	};

	const _columns = columns.map(col => {
		if (!col.editable) {
			return col;
		}

		return {
			...col,
			onCell: record => ({
				record,
				editable: col.editable,
				dataIndex: col.dataIndex,
				title: col.title,
				handleSave: processProduct,
			}),
		};
	});

	const rowSelection = {
		setProduct,
		onChange: (selectedRowKeys) => {
			_getSelectedKeys(selectedRowKeys);
		}
	};

	console.log(hierarchy.step)

	return (
		<>
			<PageHeader
				onBack={ () => history.push('/users') }
				ghost={ false }
				title='Обработка документа'
				extra={ <Steps step={ hierarchy.step }/> }
				subTitle={
					<>
						<span
							style={ { marginRight: '15px' } }><b>{ intl.formatMessage({ id: 'products-check.details.brand' }) }: </b>{ hierarchy.brand && hierarchy.brand.name }</span>
						<span style={ { marginRight: '15px' } }><b>Отделение: </b>{ hierarchy.department_name }</span>
					</>
				}
			>
				<Row type='flex' gutter={ [16, 16] }>
					<Col span={ 24 }>
						{
							hierarchy.comments && hierarchy.comments[0] &&
							<>
								&nbsp;
								<Button className={ 'decline-btn' } onClick={ showModal }
								        icon={ <LegacyIcon type={ 'message' }/> }>
									Посмотреть комментарий
								</Button>
								<Modal
									title={ intl.formatMessage({ id: 'products-check.comment.title' }) }
									visible={ comments }
									onOk={ handleOk }
									onCancel={ handleCancel }
									footer={ [
										<Button key='submit' type='primary' onClick={ handleOk }>
											Закрыть
										</Button>,
									] }
								>
									{ hierarchy.comments && hierarchy.comments[hierarchy.comments.length - 1] && `${ moment(hierarchy.comments[hierarchy.comments.length - 1].updated_at).format('DD MMM YYYY') }: ${ hierarchy.comments[hierarchy.comments.length - 1].text }` }
								</Modal>
							</>
						}
					</Col>
					<Col span={ 24 }>
						<Filter
							fetching={ fetching }
							hierarchy={ hierarchy }
							fetchProducts={ fetchProducts }
							dropSelection={ dropSelection }
							count={ count }
							documentId={ documentId }
							createContentBase={ createContentBase }
							languages={ languages }
							contents={ contents }
						/>
					</Col>
				</Row>
				<Row align='bottom'>
					<Col span={ 6 }>
						<BulkUpdater
							dropSelection={ dropSelection }
							step={ hierarchy.step || 0 }
							selected={ selected }
							hierarchy={ hierarchy }
							bulkProducts={ bulkProducts }
							documentId={ documentId }
						/>
					</Col>
					<Col span={ 6 } offset={ 6 }>
						{ count ? `Выбрано ${ selected.length }/${ count }` :
							<LoadingOutlined/> }
					</Col>
					<Col span={ 6 }>
						<Row type={ 'flex' } justify={ 'end' }>
							{ hierarchy.step < processingSteps.END &&
							<Button type={ 'primary' } onClick={ processDocument }
							        icon={ <LegacyIcon type={ 'check-circle' }/> }>
								Обработать
							</Button>
							}
						</Row>
					</Col>
				</Row>
			</PageHeader>
			<div className='page-wrapper page-wrapper--xxlarge' ref={ wrapper }>
				<Table
					scroll={ { y: wrapper.current?.clientHeight - 84 } }
					className={ 'izi-table' }
					rowSelection={ rowSelection }
					components={ components }
					columns={ _columns }
					pagination={ false }
					dataSource={ products }
					size='small'
					loading={ fetching }
					rowKey={ 'id' }
				/>
				<Details visible={ visible } product={ product } close={ close }/>
			</div>
		</>
	);
};

DocumentProducts.propTypes = {
	count:PropTypes.number.isRequired,
	fetching: PropTypes.bool.isRequired,
	documentId: PropTypes.string.isRequired,
	products: PropTypes.array.isRequired,
	hierarchy: PropTypes.object.isRequired,
	fetchProducts: PropTypes.func.isRequired,
	processProducts: PropTypes.func.isRequired,
	processProduct: PropTypes.func.isRequired,
	bulkProducts: PropTypes.func.isRequired,
	createContentBase: PropTypes.func.isRequired,
	languages: PropTypes.array.isRequired,
	contents: PropTypes.array.isRequired
};

export default DocumentProducts;