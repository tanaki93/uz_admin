import React, { useState } from 'react';
import * as PropTypes from 'prop-types';
import { Table, Form } from 'antd';
import Selector from './Selector';
import { useIntl } from 'react-intl';
import Details from './Details';
import EditOutlined from '@ant-design/icons/lib/icons/EditOutlined';

const Products = (props) => {
	const intl = useIntl();
	const [visible, toggleVisible] = useState(false);
	const [product, setProduct] = useState({});
	const { selectedRowKeys, getSelectedKeys, processProduct, hierarchy, products, fetching, wrapper } = props;
	const _processProduct = (id, optionName, optionId) => {
		processProduct(id, { option: optionName, id: optionId });
	};

	const _getSelectedKeys = (selection) => {
		getSelectedKeys(selection);
	};

	const onClickHandler = (object, index) => {
		setProduct(object);
		toggleVisible(true);
	};

	const handleCancel = e => {
		toggleVisible(false);
	};

	const getData = (type, departmentId) => {
		if (type === 'department') {
			return hierarchy.brand?.departments;
		}
		if (type === 'category') {
			return hierarchy.brand?.departments.find(dep => dep.id === departmentId)?.categories;
		}

		return [];
	};

	const columns = [
		{
			title: intl.formatMessage({ id: 'products-check.details.brand' }),
			dataIndex: 'brand',
			render: brand => brand ? brand.name : '',
			width: 80
		},
		{
			title: 'Название',
			dataIndex: 'title',
			width: 240
		},
		{
			title: 'Отделение',
			dataIndex: ['department', 'name'],
			editable: true,
			width: 200
		},
		{
			title: 'Категория',
			dataIndex: ['category', 'name'],
			editable: true,
			width: 200
		},
		{
			title: 'Цвета',
			dataIndex: ['colour', 'name'],
			editable: true,
			width: 200
		},
		{
			title: intl.formatMessage({ id: 'products-check.details.composition' }),
			dataIndex: ['content', 'name'],
			editable: true
		},
		{
			title: intl.formatMessage({ id: 'packages.list.details' }),
			render: record => <a
				onClick={ () => onClickHandler(record) }>{ intl.formatMessage({ id: 'packages.list.details' }) }</a>,
			width: 100
		},
	];

	const EditableCell = ({ title, editable, children, dataIndex, record, handleSave, ...restProps }) => {
		const [editing, setEditing] = useState(false);

		const toggleEdit = () => {
			setEditing(!editing);
		};

		const save = async (value) => {
			try {
				toggleEdit();
				_processProduct(record.id, dataIndex[0], value);
			} catch (errInfo) {
				console.log('Save failed:', errInfo);
			}
		};

		const blur = async => {
			toggleEdit();
		};

		let data = [];

		switch (dataIndex ? dataIndex[0] : '') {
			default:
			case 'department':
				data = getData('department', hierarchy.brand || {});
				break;
			case 'category':
				data = getData('category', hierarchy.brand || {}, record.izi_department.id);
				break;
			case 'colour':
				data = hierarchy.colours;
				break;
			case 'content':
				data = hierarchy.contents;
				break;
		}

		let childNode = children;

		if (editable) {
			childNode = editing ? (
				<Form initialValues={ { [dataIndex[0]]: record[dataIndex[0]]?.id } }>
					<Form.Item style={ { margin: 0, } } name={ dataIndex[0] }
					           rules={ [{ required: true, message: `${ title } is required.` }] }>
						<Selector
							id={ product.id }
							type={ dataIndex[0] }
							data={ data }
							record={ record }
							onChange={ save }
							onBlur={ blur }
							processProduct={ _processProduct }
						/>
					</Form.Item>
				</Form>
			) : (
				<div className='editable-cell-value-wrap' style={ { paddingRight: 24 } } onClick={ toggleEdit }>
					{ children } <EditOutlined style={ { color: 'red' } }/>
				</div>
			);
		}

		return <td { ...restProps }>{ childNode }</td>;
	};

	const components = {
		body: {
			cell: EditableCell,
		}
	};

	const _columns = columns.map(col => {
		if (!col.editable) {
			return col;
		}

		return {
			...col,
			onCell: record => ({
				record,
				editable: col.editable,
				dataIndex: col.dataIndex,
				title: col.title,
				handleSave: processProduct,
			}),
		};
	});

	const rowSelection = {
		selectedRowKeys,
		onChange: (selectedRowKeys) => {
			_getSelectedKeys(selectedRowKeys);
		}
	};
	return (
		<>
			<Table
				scroll={ { y: wrapper.current?.clientHeight - 84 } }
				className={ 'izi-table' }
				rowSelection={ rowSelection }
				components={ components }
				columns={ _columns }
				pagination={ false }
				dataSource={ products }
				size='small'
				loading={ fetching }
				rowKey={ 'id' }
			/>
			<Details visible={ visible } product={ product } close={ handleCancel }/>
		</>
	);
};

Products.propTypes = {
	selectedRowKeys: PropTypes.array.isRequired,
	getSelectedKeys: PropTypes.func.isRequired,
	processProduct: PropTypes.func.isRequired,
	hierarchy: PropTypes.object.isRequired,
	products: PropTypes.any,
	fetching: PropTypes.any.isRequired,
};

export default Products;