import React, { useRef, useState } from 'react';
import * as PropTypes from 'prop-types';
import { Icon as LegacyIcon } from '@ant-design/compatible';
import { Button, Cascader, Col, Form, Input, Pagination, Row, Select, Tooltip } from 'antd';
import CreateContent from '../../bases/contents/CreateContent';
import { useIntl } from 'react-intl';

const Filter = (props) => {
	const formRef = useRef(null);
	const form = formRef.current;
	const intl = useIntl();
	const cascaderRef = useRef(null);
	const [current, setCurrent] = useState(1);
	const { documentId, fetching, hierarchy, fetchProducts, dropSelection, count, createContentBase, languages, contents } = props;
	const _departments = [];
	hierarchy.brand && hierarchy.brand.departments.map(d => _departments.push({
		value: d.id, label: d.name, children: d.categories.map(x => {
			return { value: x.id, label: x.name };
		})
	}));

	const onClear = () => {
		form.resetFields();
		fetchProducts(documentId);
		dropSelection();
	};

	const onPageChange = (page) => {
		const { contentId, department, colourId, query } = form.getFieldsValue();
		setCurrent(page);
		fetchProducts(documentId, {
			query,
			department_id: department[0],
			category_id: department[1],
			colour_id: colourId,
			content_id: contentId,
			page
		});
	};

	const onFilterSubmit = () => {
		const { contentId, department, colourId, query } = form.getFieldsValue();
		setCurrent(1);
		dropSelection();
		fetchProducts(documentId, {
			query,
			department_id: department[0],
			category_id: department[1],
			colour_id: colourId,
			content_id: contentId,
			page: 1
		});
	};

	const layout = {
		labelCol: { span: 24 },
		wrapperCol: { span: 24 },
	};

	return (
		<Form
			ref={ formRef }
			initialValues={ {
				query: '',
				department: [null, null],
				colourId: null,
				contentId: null
			} }
			{ ...layout }
		>
			<Row type='flex' justify='space-between' align='bottom' gutter={ 16 }>
				<Col span={ 3 }>
					<Form.Item label='Поиск' name='query'>
						<Input.Search/>
					</Form.Item>
				</Col>
				<Col span={ 3 }>
					<Form.Item label='Отделение' name='department'>
						<Cascader ref={ cascaderRef } options={ _departments } changeOnSelect/>
					</Form.Item>
				</Col>
				<Col span={ 3 }>
					<Form.Item label={ intl.formatMessage({ id: 'products-check.details.color' }) } name='colourId'>
						<Select>
							<Select.Option value={ '' }>
								Все
							</Select.Option>
							<Select.Option value={ 0 }>
								{ intl.formatMessage({ id: 'products-check.selector.not-selected' }) }
							</Select.Option>
							{ hierarchy.colours && hierarchy.colours.map(colour =>
								<Select.Option key={ colour.id } value={ colour.id }>
									<Tooltip title={ colour.name } placement='left'>{ colour.name }</Tooltip>
								</Select.Option>
							) }
						</Select>
					</Form.Item>
				</Col>
				<Col span={ 6 }>
					<Form.Item label={ intl.formatMessage({ id: 'products-check.details.composition' }) }
					           name='contentId'>
						<Select notFoundContent={
							<CreateContent
								createContent={ createContentBase }
								languages={ languages }
								contents={ contents }
							/>
						}>
							<Select.Option value={ '' }>
								Все
							</Select.Option>
							<Select.Option value={ 0 }>
								{ intl.formatMessage({ id: 'products-check.selector.not-selected' }) }
							</Select.Option>
							{ hierarchy.contents && hierarchy.contents.map(item =>
								<Select.Option key={ item.id } value={ item.id }>
									<Tooltip title={ item.name } placement='left'>{ item.name }</Tooltip>
								</Select.Option>
							) }
						</Select>
					</Form.Item>
				</Col>
				<Col span={ 6 }>
					<Form.Item>
						<Button type={ 'primary' } onClick={ onFilterSubmit } disabled={ fetching }>
							{ intl.formatMessage({ id: 'packages-check.filter.filter' }) }
						</Button>
						&nbsp;
						<Button type={ 'default' } onClick={ onClear } icon={ <LegacyIcon type={ 'minus' }/> }
						        disabled={ fetching }>
							Очистить фильтр
						</Button>
					</Form.Item>
				</Col>
				<Col span={ 8 }>
					<Pagination
						current={ current } pageSize={ 50 } total={ count }
						onChange={ onPageChange } disabled={ fetching }
					/>
				</Col>
			</Row>
		</Form>
	);
};

Filter.propTypes = {
	documentId: PropTypes.string.isRequired,
	fetching: PropTypes.bool.isRequired,
	hierarchy: PropTypes.object.isRequired,
	fetchProducts: PropTypes.func.isRequired,
	dropSelection: PropTypes.func.isRequired,
	count: PropTypes.number.isRequired,
	createContentBase: PropTypes.func.isRequired,
	languages: PropTypes.array.isRequired,
	contents: PropTypes.array.isRequired,
};

export default Filter;