import React from 'react';
import { Steps } from 'antd';

const ProcessingSteps = (props) => {
	return <Steps size="small" current={ props.step }>
		<Steps.Step title="Начало"/>
		<Steps.Step title="Отделения"/>
		<Steps.Step title="Категории"/>
		<Steps.Step title="Цвета"/>
		<Steps.Step title="Состав"/>
		<Steps.Step title="Завершено"/>
	</Steps>;
};

export default ProcessingSteps;