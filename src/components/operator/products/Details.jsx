import React from 'react';
import * as PropTypes from 'prop-types';
import { Carousel, Checkbox, Col, Modal, Row } from 'antd';
import moment from 'moment';
import { useIntl } from 'react-intl';

const Details = (props) => {
	const intl = useIntl();
	const { visible, product, close } = props;

	return (
		<Modal
			width={ '1200px' }
			title='Обработка данных'
			visible={ visible }
			onCancel={ close }
			footer={ null }
		>
			{
				visible && product.id &&
				<>
					<Row gutter={ 16 }>
						<Col span={ 5 }>
							<Carousel autoplay>
								{ product.images.map(item => (
									<img src={ item } alt={ '' }/>
								)) }
							</Carousel>
						</Col>
						<Col span={ 19 }>
							<Row>
								<h3>Название:</h3>
								<Col span={ 12 }>
									<p>{ product.title }</p>
								</Col>
								<Col span={ 12 }>
									{ product.title }
								</Col>
							</Row>
							<Row>
								<h3>Описание:</h3>
								<Col span={ 12 }>
									<div dangerouslySetInnerHTML={ { __html: product.description } }/>
								</Col>
								<Col span={ 12 }>
									{ product.description }
								</Col>
							</Row>
						</Col>
					</Row>
					<br/>
					<Row gutter={ 16 }>
						<Col span={ 6 }>
							<h3>{ intl.formatMessage({ id: 'products-delivery.submitter.price' }) }</h3>
							<p>Оригинальная: <b>{ product.original_price } TL</b></p>
							<p>Цена продажи: <b>{ product.selling_price } TL</b></p>
							<p>Цена со скидкой: <b>{ product.discount_price } TL</b></p>
						</Col>

						<Col span={ 6 }>
							<Row gutter={ 16 }>
								<Col span={ 24 }>
									<h3>Атрибуты</h3>
								</Col>
								<Col span={ 24 }>
									<p><b>Отделение:</b> { product.department.name }</p>
								</Col>
								<Col span={ 24 }>
									<p><b>Категория:</b> { product.category.name }</p>
								</Col>
								<Col span={ 24 }>
									<p>
										<b>{ intl.formatMessage({ id: 'products-check.details.color' }) }:</b> { product.product.colour }
									</p>
								</Col>
								<Col span={ 24 }>
									<p>
										<b>{ intl.formatMessage({ id: 'products-check.details.composition' }) }:</b> { product.content && product.content.name }
									</p>
								</Col>
							</Row>
						</Col>
						<Col span={ 6 }>
							<Col span={ 24 }>
								<h3>Доставка</h3>
							</Col>
							<Col span={ 24 }>
								<p><b>Дата доставки:</b> { product.delivery_date }</p>
							</Col>
							<Col span={ 24 }>
								<p><Checkbox checked={ product.is_rush_delivery }/> <b>Быстрая доставка</b>
								</p>
							</Col>
							<Col span={ 24 }>
								<p><Checkbox checked={ product.is_free_argo }/> <b>Бесплатная доставка</b>
								</p>
							</Col>
						</Col>
					</Row>
					<br/>
					<Row type={ 'flex' } align={ 'middle' } justify={ 'space-between' }>
						<Col>
							<Row type={ 'flex' } gutter={ 16 }>
								<Col>
									<b>Создан:</b> { moment(product.created_at).format('DD MMM YYYY, HH:mm') }
								</Col>
								<Col>
									<b> Обновлен:</b> { moment(product.updated_at).format('DD MMM YYYY, HH:mm') }
								</Col>
							</Row>
						</Col>
					</Row>
				</>
			}
		</Modal>
	);
};

Details.propTypes = {
	visible: PropTypes.bool.isRequired,
	product: PropTypes.object.isRequired,
	close: PropTypes.func.isRequired
};

export default Details;