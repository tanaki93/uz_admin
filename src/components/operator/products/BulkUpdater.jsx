import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Button, Select } from 'antd';
import * as processingSteps from './../../../constants/processingSteps';
import { injectIntl } from 'react-intl';

const { Option, OptGroup } = Select;

class CategorySelector extends React.Component {
	state = {
		selectInputValue: '',
		options: []
	};

	onSearch = (val) => {
		this.setState({
			selectInputValue: val
		});
	};

	render() {
		const { data, onChange } = this.props;

		return (
			data &&
			<Select showSearch
			        optionFilterProp='children'
			        filterOption={ (input, option) => (
				        typeof (option.props.children) === 'string' && option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
			        ) }
			        onSearch={ this.onSearch }
			        style={ { width: '100%' } }
			        onChange={ value => onChange(value) }>
				{ data.map(department =>
					<OptGroup label={ department.name } key={ department.id }>
						{
							department.categories.map(category => (
								<Option key={ category.id } value={ category.id }>
									{ category.name }
								</Option>
							))
						}
					</OptGroup>
				) }
			</Select>
		);
	}
}

CategorySelector.propTypes = {
	data: PropTypes.object.isRequired,
	onChange: PropTypes.func.isRequired
};


class ContentSelector extends React.Component {
	state = {
		selectInputValue: '',
		options: []
	};

	onSearch = (val) => {
		this.setState({
			selectInputValue: val
		});
	};

	render() {
		const { data, onChange } = this.props;
		return (
			data &&
			<Select showSearch
			        optionFilterProp='children'
			        filterOption={ (input, option) => (
				        typeof (option.props.children) === 'string' && option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
			        ) }
			        onSearch={ this.onSearch }
			        style={ { width: '100%' } }
			        onChange={ value => onChange(value) }>
				{ data.map(item =>
					<Option key={ item.id } value={ item.id }>
						{ item.name }
					</Option>
				) }
			</Select>
		);
	}
}

ContentSelector.propTypes = {
	data: PropTypes.array.isRequired,
	onChange: PropTypes.func.isRequired
};

class BulkUpdater extends Component {
	state = {
		selectedDepartmentProcessId: null,
		selectedCategoryProcessId: null,
		selectedColourProcessId: null,
		selectedContentProcessId: null,
	};

	onDepartmentSelectProcess = (value) => {
		this.setState({
			'selectedDepartmentProcessId': value
		});
	};

	onCategorySelectProcess = (value) => {
		this.setState({
			'selectedCategoryProcessId': value
		});
	};

	onColourSelectProcess = (value) => {
		this.setState({
			'selectedColourProcessId': value
		});
	};

	onContentSelectProcess = (value) => {
		this.setState({
			'selectedContentProcessId': value
		});
	};

	onBulkUpdate = () => {
		const { step, selected, documentId } = this.props;
		const { selectedDepartmentProcessId, selectedCategoryProcessId, selectedColourProcessId, selectedContentProcessId } = this.state;
		let option = '';
		let id = '';

		switch (step) {
			default:
			case processingSteps.DEPARTMENT:
				option = 'department';
				id = selectedDepartmentProcessId;
				break;
			case processingSteps.CATEGORY:
				option = 'category';
				id = selectedCategoryProcessId;
				break;
			case processingSteps.COLOR:
				option = 'colour';
				id = selectedColourProcessId;
				break;
			case processingSteps.CONTENT:
				option = 'content';
				id = selectedContentProcessId;
				break;
		}

		const data = {
			products: selected,
			option,
			id
		};

		this.props.bulkProducts(documentId, data);
		this.props.dropSelection();
	};

	renderBulk = (step) => {
		const { hierarchy, intl } = this.props;
		const { selectedDepartmentProcessId, selectedCategoryProcessId, selectedColourProcessId, selectedContentProcessId } = this.state;

		switch (step) {
			case processingSteps.DEPARTMENT:
				return (
					<div style={ { flex: '1' } }>
						<Select
							placeholder='Отделение'
							style={ { width: '100%' } }
							onChange={ value => this.onDepartmentSelectProcess(value) }
							value={ selectedDepartmentProcessId }>
							{ hierarchy.brand && hierarchy.brand.departments.map(department =>
								<Option key={ department.id } value={ department.id }>
									{ department.name }
								</Option>
							) }
						</Select>
					</div>
				);
			case processingSteps.CATEGORY:
				return (
					<div style={ { flex: '1' } }>
						<CategorySelector
							placeholder='Категория'
							id={ selectedCategoryProcessId }
							defaultValue={ '' }
							data={ (hierarchy.brand && hierarchy.brand.departments) || [] }
							onChange={ value => this.onCategorySelectProcess(value) }
						/>
					</div>
				);
			case processingSteps.COLOR:
				return (
					<div style={ { flex: '1' } }>
						<Select
							placeholder={ intl.formatMessage({ id: 'products-check.details.color' }) }
							style={ { width: '100%' } }
							onChange={ value => this.onColourSelectProcess(value) }
							value={ selectedColourProcessId }>
							{ hierarchy.colours && hierarchy.colours.map(colour =>
								<Option key={ colour.id } value={ colour.id }>
									{ colour.name }
								</Option>
							) }
						</Select>
					</div>
				);
			case processingSteps.CONTENT:
				return (
					<div style={ { flex: '1' } }>
						<ContentSelector
							placeholder={ intl.formatMessage({ id: 'products-check.details.composition' }) }
							id={ selectedContentProcessId }
							data={ (hierarchy.contents && hierarchy.contents) || [] }
							onChange={ value => this.onContentSelectProcess(value) }
						/>
					</div>
				);
			default:
				return <div/>;
		}
	};

	render() {
		return (
			<>
				{ this.props.step < processingSteps.END &&
				<div style={ { display: 'flex', alignItems: 'flex-end' } }>
					{ this.renderBulk(this.props.step) }
					<Button type={ 'danger' } onClick={ this.onBulkUpdate }>
						Bulk update
					</Button>
				</div>
				}
			</>
		);
	}
}

BulkUpdater.propTypes = {
	dropSelection: PropTypes.func.isRequired,
	bulkProducts: PropTypes.func.isRequired,
	step: PropTypes.number.isRequired,
	selected: PropTypes.any.isRequired,
	hierarchy: PropTypes.any.isRequired,
	documentId: PropTypes.string.isRequired
};

export default (injectIntl)(BulkUpdater);