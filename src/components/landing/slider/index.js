import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Card, Checkbox, Col, Icon, Image, Modal, PageHeader, Row, Space, Form, Typography } from 'antd';
import PlusOutlined from '@ant-design/icons/lib/icons/PlusOutlined';
import List from 'antd/lib/list';
import Item from './item';
import { EditOutlined } from '@ant-design/icons';

const Slider = (props) => {
	const [visible, setVisible] = useState(false);
	const [toEdit, setEdit] = useState(null);
	const { fetching, data, fetchSliderAction, createSliderAction, editSliderAction } = props;

	const create = ({ link, image, smallImage }) => {
		createSliderAction({
			link,
			image,
			small_image: smallImage,
			is_active: false
		}).then(() => {
			setVisible(false)
			setEdit(false)
			fetchSliderAction()
		});
	};

	const edit = (sliderId, { link, image, smallImage }) => {
		editSliderAction(sliderId, {
			link,
			image,
			small_image: smallImage,
			is_active: false
		}).then(() => {
			setVisible(false)
			setEdit(false)
			fetchSliderAction()
		});
	};

	const check = (slide, checked) => {
		editSliderAction(slide.id, {
			link: slide.link,
			image: slide.image,
			small_image: slide.small_image,
			is_active: checked
		}).then(() => {
			fetchSliderAction()
		});
	};

	useEffect(() => {
		fetchSliderAction();
	}, []);

	return (
		<>
			<PageHeader ghost={ false } title={ 'Slider' }
			            extra={ <Button type='primary' icon={ <PlusOutlined/> }
			                            onClick={ () => {
			                            	setVisible(true);
				                            setEdit(null);
			                            } }>Добавить</Button> }
			            style={ { marginBottom: 24 } }/>
			<Item toEdit={ toEdit } visible={ visible } close={ () => {
				setVisible(false);
				setEdit(null);
			} } create={ create }
			      edit={ edit }/>
			<Row>
				<Col span={ 24 }>
					<Card>
						{ data && <List
							grid={ { gutter: 16, xs: 1, sm: 1, md: 2, lg: 3, xl: 3, xxl: 4, } }
							dataSource={ data }
							loading={ fetching }
							renderItem={ item => (
								<List.Item>
									<Card
										title={ item.link }
										extra={
											<Space size={ 12 }>
												<Space size={ 8 }>
													Активен: <Checkbox checked={ item.is_active } onChange={(e) => check(item, e.target.checked)}/>
												</Space>
												<Button type='primary' icon={ <EditOutlined/> }
												        onClick={ () => {
													        setVisible(true);
													        setEdit(item);
												        } }
												/>
											</Space>
										}>
										<Typography.Text>Desktop</Typography.Text>
										<Image src={ item.image }/>
										<Typography.Text>Mobile</Typography.Text>
										<Image src={ item.small_image }/>
									</Card>
								</List.Item>
							) }
						/> }
					</Card>
				</Col>
			</Row>
		</>
	);
};

Slider.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired
};

export default Slider;