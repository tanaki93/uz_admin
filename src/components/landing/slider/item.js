import React, { useEffect, useState } from 'react';
import { Modal, Upload, Form, Input } from 'antd';
import UploadOutlined from '@ant-design/icons/lib/icons/UploadOutlined';


const Item = (props) => {
	const [link, setLink] = useState('');
	const [image, setImage] = useState('');
	const [smallImage, setSmallImage] = useState('');

	const handleImageChange = ({ file }) => {
		file.status === 'done' && setImage(file.response.url);
	};

	const handleSmallImageChange = ({ file }) => {
		file.status === 'done' && setSmallImage(file.response.url);
	};

	const handleSubmit = () => {
		if (link && image && smallImage) {
			props.toEdit ? props.edit(props.toEdit.id, { link, image, smallImage }) : props.create({ link, image, smallImage });
		}
	};

	useEffect(() => {
		if (props.toEdit) {
			setLink(props.toEdit.link);
			setImage(props.toEdit.image);
			setSmallImage(props.toEdit.small_image);
		} else {
			setLink('');
			setImage('');
			setSmallImage('');
		}
	}, [props.toEdit]);

	return (
		<Modal
			title={ props.toEdit ? 'Изменить слайд' : 'Создать слайд' }
			visible={ props.visible }
			onCancel={ props.close }
			onOk={ handleSubmit }
		>
			<Form layout='vertical'>
				<Form.Item label={ 'Link' }>
					<Input value={ link } onChange={ (e) => setLink(e.target.value) }/>
				</Form.Item>
				<Form.Item label={ 'Desktop' }>
					<Upload
						name='image'
						listType='picture-card'
						className='avatar-uploader'
						showUploadList={ false }
						action='/api/v1/main/upload/'
						method='post'
						onChange={ handleImageChange }
					>
						{
							image
								? <img src={ image } alt='avatar' style={ { width: '100%' } }/>
								: <><UploadOutlined/> Upload</>
						}
					</Upload>
				</Form.Item>
				<Form.Item label={ 'Mobile' }>
					<Upload
						name='image'
						listType='picture-card'
						className='avatar-uploader'
						showUploadList={ false }
						action='/api/v1/main/upload/'
						method='post'
						onChange={ handleSmallImageChange }
					>
						{
							smallImage
								? <img src={ smallImage } alt='avatar' style={ { width: '100%' } }/>
								: <><UploadOutlined/> Upload</>
						}
					</Upload>
				</Form.Item>
			</Form>
		</Modal>
	);
};

export default Item;