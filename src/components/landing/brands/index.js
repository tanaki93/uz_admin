import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Checkbox, Icon, Table, Upload } from 'antd';

const Brands = (props) => {
	const [brands, setBrands] = useState([]);
	const { fetching, data, update } = props;

	useEffect(() => {
		setBrands([...data]);
	}, [data]);


	const handleUrlUpload = (name, item, fileList) => {
		let image = '';
		fileList.map(file => image = file.response.url);

		switch (name) {
			default:
			case 'db_image':
				update(item.id, {
					is_active: item.is_active,
					db_image: image,
					ds_image: item.ds_image,
					m_image: item.m_image
				});
				break;
			case 'ds_image':
				update(item.id, {
					is_active: item.is_active,
					db_image: item.db_image,
					ds_image: image,
					m_image: item.m_image
				});
				break;
			case 'm_image':
				update(item.id, {
					is_active: item.is_active,
					db_image: item.db_image,
					ds_image: item.ds_image,
					m_image: image
				});
				break;
		}
	};

	const handleImageRemove = (name, item) => {
		switch (name) {
			default:
			case 'db_image':
				update(item.id, {
					is_active: item.is_active,
					db_image: null,
					ds_image: item.ds_image,
					m_image: item.m_image
				});
				break;
			case 'ds_image':
				update(item.id, {
					is_active: item.is_active,
					db_image: item.db_image,
					ds_image: null,
					m_image: item.m_image
				});
				break;
			case 'm_image':
				update(item.id, {
					is_active: item.is_active,
					db_image: item.db_image,
					ds_image: item.ds_image,
					m_image: null
				});
				break;
		}
	};

	const triggerActive = (brand, checked) => {
		update(brand.id, {
			is_landing: checked
		});
	};

	const columns = [
		{
			title: 'Активен',
			render: brand => <Checkbox checked={ brand.is_landing }
			                           onChange={ e => triggerActive(brand, e.target.checked) }/>
		},
		{ key: 'name', dataIndex: 'name', title: 'Название' },
		{
			key: 'db_image', title: 'Большая',
			render: item => {
				const uploadProps = {
					action: `/api/v1/main/upload/`,
					method: 'post',
					name: 'image',
					onChange: ({ file, fileList }) => {
						if (file.status === 'done') {
							handleUrlUpload('db_image', item, fileList);
						}
					},
					onRemove: () => {
						handleImageRemove('db_image', item);
					}
				};
				return (
					<>
						{ item.db_image &&
						<img height={ 25 } src={ 'https://api.hurrem.uz/' + item.db_image } alt=''/> }
						<br/>
						<Upload { ...uploadProps }>
							<Button>
								<Icon type='upload'/> Upload
							</Button>
						</Upload>
					</>
				);

			}
		},
		{
			key: 'ds_image', title: 'Средняя',
			render: item => {
				const uploadProps = {
					action: `/api/v1/main/upload/`,
					method: 'post',
					name: 'image',
					onChange: ({ file, fileList }) => {
						if (file.status === 'done') {
							handleUrlUpload('ds_image', item, fileList);
						}
					},
					onRemove: () => {
						handleImageRemove('ds_image', item);
					}
				};
				return <>
					{ item.ds_image &&
					<img height={ 25 } src={ 'http://134.209.233.208:8000' + item.ds_image } alt=''/> }
					<br/>
					<Upload { ...uploadProps }>
						<Button>
							<Icon type='upload'/> Upload
						</Button>
					</Upload>
				</>;
			}
		},
		{
			key: 'm_image', title: 'Маленькая',
			render: item => {
				const uploadProps = {
					action: `/api/v1/main/upload/`,
					method: 'post',
					name: 'image',
					onChange: ({ file, fileList }) => {
						if (file.status === 'done') {
							handleUrlUpload('m_image', item, fileList);
						}
					},
					onRemove: () => {
						handleImageRemove('m_image', item);
					}
				};
				return <>
					{ item.m_image && <img height={ 25 } src={ 'http://134.209.233.208:8000' + item.m_image } alt=''/> }
					<br/>
					<Upload { ...uploadProps }>
						<Button>
							<Icon type='upload'/> Upload
						</Button>
					</Upload>
				</>;
			}
		}
	];


	return (
		<Table
			loading={ fetching }
			columns={ columns }
			dataSource={ brands }
		/>
	);
};

Brands.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	update: PropTypes.func.isRequired
};

export default Brands;