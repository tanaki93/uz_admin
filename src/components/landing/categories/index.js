import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import {
	Card, Checkbox,
	Image,
	PageHeader,
	Table,
	Upload
} from 'antd';
import UploadOutlined from '@ant-design/icons/lib/icons/UploadOutlined';

const Categories = (props) => {
	const { fetching, data, fetchLandingCategoriesAction, updateLandingCategoryAction, updateLandingSubcategoryAction } = props;

	useEffect(() => {
		fetchLandingCategoriesAction();
	}, []);

	const handleCategoryImageChange = (item, { file }) => {
		if (file.status === 'done') {
			if (item.parentId) {
				updateLandingSubcategoryAction(item.parentId, {
					link: item.link,
					image: file.response.url,
					small_image: item.small_image,
					is_landing: true,
					child_id: item.id
				}).then(() => fetchLandingCategoriesAction());
			} else {
				updateLandingCategoryAction(item.id, {
					link: item.link,
					image: file.response.url,
					small_image: item.small_image,
					is_landing: true
				}).then(() => fetchLandingCategoriesAction());
			}
		}

	};

	const handleCategorySmallImageChange = (item, { file }) => {
		if (file.status === 'done') {
			if (item.parentId) {
				updateLandingSubcategoryAction(item.parentId, {
					link: item.link,
					image: item.image,
					small_image: file.response.url,
					is_landing: true,
					child_id: item.id
				}).then(() => fetchLandingCategoriesAction());
			} else {
				updateLandingCategoryAction(item.id, {
					link: item.link,
					image: item.image,
					small_image: file.response.url,
					is_landing: true
				}).then(() => fetchLandingCategoriesAction());
			}
		}
	};

	const categoryVisible = (item, checked) => {
		if (item.parentId) {
			updateLandingSubcategoryAction(item.parentId, {
				link: item.link,
				image: item.image,
				small_image: item.small_image,
				is_landing: checked,
				child_id: item.id
			}).then(() => fetchLandingCategoriesAction());
		} else {
			updateLandingCategoryAction(item.id, {
				link: item.link,
				image: item.image,
				small_image: item.small_image,
				is_landing: checked
			}).then(() => fetchLandingCategoriesAction());
		}
	}

	const columns = [
		{
			title: 'Категория',
			id: 'name',
			key: 'name',
			dataIndex: 'name'
		},
		{
			title: 'Виден',
			id: 'visible',
			key: 'visible',
			render: item => <Checkbox checked={item.is_landing} onChange={(e) => categoryVisible(item, e.target.checked)}/>
		},
		{
			title: 'Desktop',
			id: 'image',
			key: 'image',
			render: item =>
				<Upload
					name='image'
					listType='picture-card'
					className='avatar-uploader'
					showUploadList={ false }
					action='/api/v1/main/upload/'
					method='post'
					onChange={ (e) => handleCategoryImageChange(item, e) }
				>
					{
						item.image
							? <Image
								width={ 90 }
								height={ 45 }
								src={ item.image }
							/>
							: <><UploadOutlined/> Upload</>
					}
				</Upload>
		},
		{
			title: 'Mobile',
			id: 'image',
			key: 'image',
			render: item =>
				<Upload
					name='image'
					listType='picture-card'
					className='avatar-uploader'
					showUploadList={ false }
					action='/api/v1/main/upload/'
					method='post'
					onChange={ (e) => handleCategorySmallImageChange(item, e) }
				>
					{
						item.small_image
							? <Image
								width={ 90 }
								height={ 45 }
								src={ item.small_image }
							/>
							: <><UploadOutlined/> Upload</>
					}
				</Upload>
		}
	];

	const remappedData = data.map(item => ({
		...item,
		childs: item.childs.map(child => ({ ...child, parentId: item.id }))
	}));

	return (
		<>
			<PageHeader ghost={ false } title={ 'Categories' }
			            style={ { marginBottom: 24 } }/>
			<Card>
				<Table loading={ fetching } dataSource={ remappedData } rowKey={ 'id' } columns={ columns }
				       expandable={ {
					       expandedRowRender: record =>
						       <Table dataSource={ record.childs } rowKey={ 'id' } columns={ columns }/>
				       } }
				/>
			</Card>

		</>
	);
};

Categories.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired
};

export default Categories;