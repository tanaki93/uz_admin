import React from 'react';
import * as PropTypes from 'prop-types';
import { EditOutlined } from '@ant-design/icons';
import { Icon as LegacyIcon } from '@ant-design/compatible';
import { Modal, Button, Input, Table, message } from 'antd';
import ColorPicker from './ColorPicker';

class EditColor extends React.Component {
	state = {
		visible: false,
		name: '',
		code: '',
		hex: '',
		languages: []
	};

	UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
		this.props.selectedColor !== nextProps.selectedColor &&
		this.setState({
			name: nextProps.selectedColor.name,
			code: nextProps.selectedColor.code,
			hex: nextProps.selectedColor.hex,
			languages: nextProps.selectedColor.languages
		});
	}

	onChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};

	onColorChange = (color) => {
		this.setState({ hex: color.hex });
	};

	onTableChange = (id, e) => {
		const languages = this.state.languages;
		languages.map(language => language.lang_id === id && (language[e.target.name] = e.target.value));
		this.setState({ languages });
	};

	showModal = () => {
		this.props.onSelectColor(this.props.selectedColor);
		const data = [];

		this.props.languages.map(language => data.push({
			lang_id: language.id,
			is_active: language.is_active,
			lang_name: language.name,
			lang_code: language.code,
			translation: language.translation,
		}));

		this.setState({
			visible: true,
			name: this.props.selectedColor.name,
			code: this.props.selectedColor.code,
			hex: this.props.selectedColor.hex,
			languages: data
		});
	};

	error = () => {
		message.error('Уже существует');
	};

	handleOk = e => {
		const { name, code, languages, hex } = this.state;
		const data = [];

		languages.map(language => data.push({
			is_active: true,
			lang_id: language.lang_id,
			translation: language.translation,
		}));

		if (code && name) {
			if (this.props.selectedColor.code && this.props.selectedColor.code.toLowerCase() === code.toLowerCase()) {
				this.props.editColor(this.props.selectedColor.id, { name, code, languages: data, hex });

				this.setState({
					visible: false,
					name: '',
					code: '',
					hex: '',
					languages: []
				});
			} else {
				if (!this.props.colors.find(entry => entry.code && entry.code.toLowerCase() === code.toLowerCase())) {
					this.props.editColor(this.props.selectedColor.id, { name, code, languages: data, hex });

					this.setState({
						visible: false,
						name: '',
						code: '',
						hex: '',
						languages: []
					});
				} else {
					this.error();
				}
			}
		}
	};

	handleCancel = e => {
		this.setState({
			visible: false,
		});
	};

	handleDelete = () => {
		this.props.deleteColor(this.props.selectedColor.id);
	};

	render() {
		const { name, languages, code } = this.state;

		const columns = [
			{
				title: 'Страна',
				dataIndex: 'lang_code'
			},
			{
				title: 'Перевод',
				render: language => <Input defaultValue={ language.translation } name='translation'
				                           onChange={ e => this.onTableChange(language.lang_id, e) }/>
			},
		];

		return (
			<>
				<Button onClick={ this.showModal } icon={ <EditOutlined/> }>
					Изменить
				</Button>
				<Modal
					title="Изменение цвета"
					visible={ this.state.visible }
					footer={
						<div style={ { display: 'flex', justifyContent: 'space-between' } }>
							<div>
								<Button type={ 'primary' } ghost onClick={ this.handleDelete }
								        disabled={ this.props.selectedColor.is_related }
								        icon={ <LegacyIcon type={ 'delete' }/> }>Удалить</Button>
							</div>
							<div>
								<Button onClick={ this.handleCancel }>Закрыть</Button>
								<Button type={ 'primary' } onClick={ this.handleOk }>Сохранить</Button>
							</div>
						</div>
					}
					onOk={ this.handleOk }
					onCancel={ this.handleCancel }
				>
					<table className={ 'base-table' }>
						<thead>
						<tr>
							<td>Код</td>
							<td>Название</td>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td>
								<Input value={ code } name={ 'code' } onChange={ this.onChange }/>
							</td>
							<td>
								<Input value={ name } name={ 'name' } onChange={ this.onChange }/>
							</td>
							<td>
								<ColorPicker onColorChange={ this.onColorChange }/>
							</td>
						</tr>
						</tbody>
					</table>
					<Table className={ 'izi-table' } columns={ columns } dataSource={ languages } rowKey='lang_id'
					       pagination={ false } size={ 'small' }/>
				</Modal>
			</>
		);
	}
}

EditColor.propTypes = {
	selectedColor: PropTypes.object.isRequired,
	onSelectColor: PropTypes.func.isRequired,
	deleteColor: PropTypes.func.isRequired,
	editColor: PropTypes.func.isRequired,
	languages: PropTypes.array.isRequired,
	colors: PropTypes.array.isRequired,
};

export default EditColor;