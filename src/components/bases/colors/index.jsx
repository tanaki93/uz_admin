import React from 'react';
import * as PropTypes from 'prop-types';
import { Typography, Table, Row, Col, Checkbox, Upload, Button, Icon } from 'antd';
import CreateColor from './CreateColor';
import EditColor from './EditColor';
import { injectIntl } from 'react-intl';

const { Title } = Typography;

class Colors extends React.Component {
	state = {
		selected: {},
	};

	onSelectColor = (color) => {
		this.setState({
			selected: color
		});
	};

	handleUrlUpload = (item, fileList) => {
		const data = [];
		let image = '';

		fileList.map(file => image = file.response.url);
		item.languages.map(language => data.push({
			is_active: true,
			lang_id: language.lang_id,
			translation: language.translation,
		}));

		this.props.editColor(item.id, { name: item.name, code: item.code, languages: data, hex: item.hex, image });
	};

	handleImageRemove = (item) => {
		const data = [];
		item.languages.map(language => data.push({
			is_active: true,
			lang_id: language.lang_id,
			translation: language.translation,
		}));

		this.props.editColor(item.id, {
			name: item.name, code: item.code,
			languages: data, hex: item.hex, image: null
		});
	};

	render() {
		const { languages, data, createColor, editColor, fetching, intl } = this.props;

		const columns = [
			{
				title: 'ID',
				dataIndex: 'id'
			},
			{
				title: 'Название',
				dataIndex: 'name'
			},
			{
				title: 'Код',
				dataIndex: 'code',
				render: code => code || 'Не указан'
			},
			{
				title: intl.formatMessage({ id: 'products-check.details.color' }),
				dataIndex: 'hex',
				render: hex => <div style={ { backgroundColor: hex, height: '35px' } }/>
			},
			{
				render: color => <EditColor
					deleteColor={ this.props.deleteColor }
					editColor={ editColor }
					selectedColor={ this.state.selected }
					onSelectColor={ () => this.onSelectColor(color) }
					languages={ languages }
					colors={ data }
				/>
			},
			{
				title: 'Связан',
				dataIndex: 'is_related',
				render: val => <Checkbox checked={ val }/>
			},
			{
				title: 'Картинка',
				render: item => {
					const uploadProps = {
						defaultFileList: item.image && [
							{
								uid: '-1',
								name: 'uploaded',
								status: 'done',
									url: 'https://magicbox.izishop.kg' + item.image,
							},
						],
						action: `/call/main/upload/`,
						method: 'post',
						name: 'image',
						onChange: ({ file, fileList }) => {
							if (file.status === 'done') {
								this.handleUrlUpload(item, fileList);
							}
						},
						onRemove: () => {
							this.handleImageRemove(item);
						}
					};
					return <Upload { ...uploadProps }>
						<Button>
							<Icon type="upload"/> Upload
						</Button>
					</Upload>;
				}
			}
		];

		return (
			<Row type="flex" justify="start" gutter={ [16, 16] }>
				<Col><Title level={ 3 }>Цвета IZI</Title></Col>
				<Col span={ 12 }><CreateColor createColor={ createColor } languages={ languages }
				                              colors={ data }/></Col>
				<Col span={ 12 }>
					<Table className={ 'izi-table' } columns={ columns } dataSource={ data.sort((a, b) => a.id - b.id) }
					       size='small'
					       loading={ fetching }
					       rowKey={ 'id' }/>
				</Col>
			</Row>
		);
	}
}

Colors.propTypes = {
	createColor: PropTypes.func.isRequired,
	editColor: PropTypes.func.isRequired,
	deleteColor: PropTypes.func.isRequired,
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	languages: PropTypes.array.isRequired,
};

export default (injectIntl)(Colors);