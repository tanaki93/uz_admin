import React, { useRef, useState } from 'react';
import * as PropTypes from 'prop-types';
import { EditOutlined, PlusOutlined, UploadOutlined } from '@ant-design/icons';
import { Typography, Table, Checkbox, InputNumber, Row, Col, Upload, Button, PageHeader } from 'antd';
import { Link, useHistory } from 'react-router-dom';
import Dialog from './Dialog';

const { Title } = Typography;

const Category = (props) => {
	const history = useHistory();
	const wrapper = useRef(null);
	const [visible, setVisible] = useState(false);
	const [selected, setSelected] = useState({});

	const { createCategory, editCategory, deleteCategory, uploadCategoryImage, fetching, data, languages, parentId, departmentId } = props;

	const showModal = (selected) => {
		setVisible(true);
		selected && setSelected(selected);
	};

	const closeModal = () => {
		setVisible(false);
		setSelected({});
		this.setState({
			visible: false,
			selected: {}
		});
	};

	const check = (selected, e) => {
		const data = [];

		selected.languages.map(language => data.push({
			is_active: language.is_active,
			lang_id: language.lang_id,
			translation: language.translation,
		}));

		this.props.editCategory(selected.id, {
			name: selected.name,
			average_weight: selected.average_weight,
			position: selected.position,
			code: selected.code,
			is_active: e.target.checked,
			languages: data,
			parent: this.props.parentId
		});
	};

	const positionChange = (item, value) => {
		const data = [];

		item.languages.map(language => data.push({
			is_active: language.is_active,
			lang_id: language.lang_id,
			translation: language.translation,
		}));

		editCategory(item.id, {
			name: item.name,
			average_weight: item.average_weight,
			code: item.code,
			languages: data,
			position: value,
			is_active: item.is_active,
			parent: parentId
		});
	};

	const columns = [
		{
			title: 'Актив',
			render: item => <Checkbox checked={ item.is_active } onClick={ e => check(item, e) }/>
		},
		{
			title: 'Порядок',
			render: item => <InputNumber value={ item.position }
			                             onChange={ value => positionChange(item, value) }/>
		},
		{
			title: 'Название',
			dataIndex: 'name'
		},
		{
			title: 'Средний вес (кг)',
			dataIndex: 'average_weight'
		},
		{
			title: 'Код',
			dataIndex: 'code'
		},
		{
			render: category =>
				<Row type={ 'flex' } justify={ 'end' }>
					<Button type='dashed' onClick={ () => this.showModal(category) } icon={ <EditOutlined/> }>
						Изменить
					</Button>
				</Row>

		},
		{
			title: 'Картинка',
			render: item => {
				const uploadProps = {
					action: `/call/operator/izishop/categories/${ item.id }/`,
					method: 'post',
					headers: {
						Authorization: `Token ${ localStorage.getItem('Token') }`,
					},
					name: 'size_table',
					onChange: ({ file, fileList }) => {
						if (file.status === 'done') {
							uploadCategoryImage(item, fileList);
						}
					},
					fileList: item.size_table && [
						{
							uid: '-1',
							name: 'table',
							status: 'done',
							url: 'https://magicbox.izishop.kg' + item.size_table,
						},
					],
				};
				return (
					<Upload { ...uploadProps }>
						<Button>
							<UploadOutlined/> Upload
						</Button>
					</Upload>
				);
			}
		}
	];

	return (
		<>
			<PageHeader
				title={ `Категории ${ data[0]?.parent.department.name } ${ data[0]?.parent.name }` }
				ghost={ false }
				onBack={ () => history.push(`/store/departments/${ departmentId }/parents`) }
				extra={ <Button type='primary' onClick={ showModal } icon={ <PlusOutlined/> }>
					Создать категорию
				</Button> }
			/>
			<Dialog
				data={ data.sort((a, b) => a.position - b.position) }
				visible={ visible }
				selected={ selected }
				create={ createCategory }
				edit={ editCategory }
				delete={ deleteCategory }
				close={ closeModal }
				parentId={ parentId }
				languages={ languages }
			/>
			<div className='page-wrapper page-wrapper--small' ref={ wrapper }>
				<Table
					scroll={ { y: wrapper.current?.clientHeight - 84 } }
					className={ 'izi-table' }
					columns={ columns }
					dataSource={ data }
					size='small'
					rowKey={ 'id' }
					loading={ fetching }
				/>
			</div>
		</>
	);
};

Category.propTypes = {
	createCategory: PropTypes.func.isRequired,
	editCategory: PropTypes.func.isRequired,
	deleteCategory: PropTypes.func.isRequired,
	uploadCategoryImage: PropTypes.func.isRequired,
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	languages: PropTypes.array.isRequired,
	parentId: PropTypes.string.isRequired,
	departmentId: PropTypes.string.isRequired,
};

export default Category;