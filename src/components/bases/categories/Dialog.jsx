import React from 'react';
import * as PropTypes from 'prop-types';
import { Form, Icon as LegacyIcon } from '@ant-design/compatible';

import { Modal, Button, Input, Table, Row, Col } from 'antd';

class Dialog extends React.Component {
	state = {
		languages: []
	};

	UNSAFE_componentWillReceiveProps(nextProps) {
		if (nextProps.selected.id === undefined) {
			const data = [];

			nextProps.languages.map(language => data.push({
				is_active: true,
				lang_id: language.id,
				lang_name: language.name,
				lang_code: language.code,
				translation: null,
			}));

			this.setState({
				languages: data
			});
		} else {
			this.setState({
				languages: nextProps.selected.languages,
			});
		}
	}

	onTableChange = (id, e) => {
		const languages = this.state.languages;
		languages.map(language => language.lang_id === id && (language[e.target.name] = e.target.value));
		this.setState({ languages });
	};

	isCodeExists = (rule, value, callback) => {
		if (value !== this.props.selected.code && this.props.data.find(entry => entry.code.toLowerCase().replace(/\s/g, '') === value.toLowerCase().replace(/\s/g, ''))) {
			callback('Такой код существует!');
		} else {
			callback();
		}
	};

	handleOk = () => {
		const { languages } = this.state;
		const langData = [];
		this.props.form.validateFields((err, values) => {
			if (err) {
				return;
			}

			languages.map(language => langData.push({
				is_active: true,
				lang_id: language.lang_id,
				translation: language.translation,
			}));

			const data = {
				code: values.code.replace(/\s/g, ''),
				name: values.name,
				average_weight: values.average_weight,
				languages: langData,
				position: this.props.selected.position || null,
				is_active: this.props.selected.is_active || false,
				parent_id: parseInt(this.props.parentId)
			};

			this.props.selected.id === undefined ?
				this.props.create(this.props.parentId, data) :
				this.props.edit(this.props.selected.id, data);

			this.props.form.resetFields();
			this.props.close();
		});
	};

	handleDelete = () => {
		this.props.delete(this.props.selected.id);
		this.props.close();
	};

	render() {
		const { languages } = this.state;
		const { visible, selected } = this.props;
		const { getFieldDecorator } = this.props.form;

		const columns = [
			{
				title: 'Страна',
				dataIndex: 'lang_code'
			},
			{
				title: 'Перевод',
				render: language => <Input defaultValue={ language.translation } name='translation'
				                           onChange={ e => this.onTableChange(language.lang_id, e) }/>
			},
		];

		return (
			<Modal
				title={ selected.id ? 'Изменение категории ' + selected.name : 'Создание категории' }
				visible={ visible }
				footer={
					selected.id &&
					<div style={ { display: 'flex', justifyContent: 'space-between' } }>
						<div>
							<Button type={ 'primary' } ghost onClick={ this.handleDelete }
							        icon={ <LegacyIcon type={ 'delete' }/> }>Удалить</Button>
						</div>
						<div>
							<Button onClick={ this.handleCancel }>Закрыть</Button>
							<Button type={ 'primary' } onClick={ this.handleOk }>Сохранить</Button>
						</div>
					</div>
				}
				onOk={ this.handleOk }
				onCancel={ this.props.close }
			>
				{
					visible && <Form>
						<table className={ 'base-table' }>
							<thead>
							<tr>
								<td>Код</td>
								<td>Название</td>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>
									<Form.Item>
										{ getFieldDecorator('code', {
											initialValue: selected.code || '',
											rules: [
												{ required: true, message: 'Это обязательное поле' },
												{ validator: this.isCodeExists },
											],
										})(
											<Input name={ 'code' } onChange={ this.onChange }/>
										) }
									</Form.Item>
								</td>
								<td>
									<Form.Item>
										{ getFieldDecorator('name', {
											initialValue: selected.name || '',
											rules: [
												{ required: true, message: 'Это обязательное поле' },
											],
										})(
											<Input name={ 'name' } onChange={ this.onChange }/>
										) }
									</Form.Item>
								</td>
							</tr>
							</tbody>
						</table>
					</Form> }
				{ visible &&
				<Table className={ 'izi-table' } pagination={ false } columns={ columns } dataSource={ languages }
				       rowKey='lang_id'/>
				}
				<br/>
				<Form>
					<Row gutter={ 24 }>
						<Col span={ 24 }>
							<Form.Item label="Средний вес (кг)">
								{ getFieldDecorator('average_weight', {
									initialValue: selected.average_weight || '',
									rules: [
										{ required: true, message: 'Это обязательное поле' },
									],
								})(
									<Input name={ 'average_weight' }/>
								) }
							</Form.Item>
						</Col>
					</Row>
				</Form>
			</Modal>
		);
	}
}

Dialog.propTypes = {
	data: PropTypes.array.isRequired,
	visible: PropTypes.bool.isRequired,
	selected: PropTypes.object.isRequired,
	parentId: PropTypes.any.isRequired,
	languages: PropTypes.array.isRequired,
	edit: PropTypes.func.isRequired,
	delete: PropTypes.func.isRequired,
	create: PropTypes.func.isRequired,
	close: PropTypes.func.isRequired,
};

const DialogForm = Form.create({ name: 'category' })(Dialog);

export default DialogForm;