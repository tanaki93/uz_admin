import React, { useEffect, useState } from 'react';
import * as PropTypes from 'prop-types';
import { DeleteOutlined } from '@ant-design/icons';
import { Modal, Button, Input, Table, Form, Col, Row } from 'antd';

const Dialog = (props) => {
	const formRef = React.createRef();
	const [langList, setLangList] = useState([]);
	const { data, visible, selected, create, remove, close, languages, edit } = props;

	useEffect(() => {
		if (selected.id) {
			setLangList(selected.languages);
		} else {
			const data = [];
			languages.map(language => data.push({
				is_active: true,
				lang_id: language.id,
				lang_name: language.name,
				lang_code: language.code,
				translation: null,
			}));
			setLangList(data);
		}
	}, [selected, languages]);

	const onTableChange = (id, e) => {
		const languages = langList;
		languages.map(language => language.lang_id === id && (language[e.target.name] = e.target.value));
		setLangList(languages);
	};

	const codeExists = (rule, value, callback) => {
		if (value !== selected.code && data.find(entry => entry.code.toLowerCase().replace(/\s/g, '') === value.toLowerCase().replace(/\s/g, ''))) {
			callback('Такой код существует!');
		} else {
			callback();
		}
	};

	const handleOk = () => {
		const form = formRef.current;
		const langData = [];

		form.validateFields().then(values => {
			langList.map(language => langData.push({
				is_active: true,
				lang_id: language.lang_id,
				translation: language.translation,
			}));

			const data = {
				code: values.code.replace(/\s/g, ''),
				name: values.name,
				languages: langData,
				position: selected.position || null,
				is_active: selected.is_active || false
			};

			selected.id === undefined ?
				create(data) :
				edit(selected.id, data);

			form.resetFields();
			close();
		});
	};

	const handleDelete = () => {
		remove(selected.id);
		close();
	};

	const columns = [
		{
			title: 'Страна',
			dataIndex: 'lang_code'
		},
		{
			title: 'Перевод',
			render: language => <Input defaultValue={ language.translation } name='translation'
			                           onBlur={ e => onTableChange(language.lang_id, e) }/>
		},
	];


	return (
		<Modal
			title='Отделения'
			visible={ visible }
			footer={
				selected.id &&
				<div style={ { display: 'flex', justifyContent: 'space-between' } }>
					<div>
						<Button type={ 'primary' } ghost onClick={ handleDelete }
						        icon={ <DeleteOutlined/> }>Удалить</Button>
					</div>
					<div>
						<Button onClick={ close }>Закрыть</Button>
						<Button type={ 'primary' } onClick={ handleOk }>Сохранить</Button>
					</div>
				</div>
			}
			onCancel={ close }
		>
			{
				visible && <Form
					ref={ formRef }
					initialValues={ {
						code: selected.code || '',
						name: selected.name || ''
					} }
					onFinish={ handleOk }
				>
					<Row gutter={ 8 }>
						<Col span={ 12 }>
							<Form.Item label='Код' name='code' rules={ [
								{ required: true, message: 'Это обязательное поле' },
								{ validator: codeExists },
							] }>
								<Input/>
							</Form.Item>
						</Col>

						<Col span={ 12 }>
							<Form.Item label='Название' name='name' rules={ [
								{ required: true, message: 'Это обязательное поле' },
							] }>
								<Input/>
							</Form.Item>
						</Col>
					</Row>
				</Form>
			}

			{ langList.length > 0 &&
			<Table className={ 'izi-table' } columns={ columns } dataSource={ langList } rowKey='lang_id'/>
			}
		</Modal>
	);
};

Dialog.propTypes = {
	data: PropTypes.array.isRequired,
	visible: PropTypes.bool.isRequired,
	selected: PropTypes.object.isRequired,
	create: PropTypes.func.isRequired,
	remove: PropTypes.func.isRequired,
	close: PropTypes.func.isRequired,
	edit: PropTypes.func.isRequired,
	languages: PropTypes.array.isRequired
};

export default Dialog;