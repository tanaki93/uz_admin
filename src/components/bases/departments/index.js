import React from 'react';
import * as PropTypes from 'prop-types';
import { EditOutlined, PlusOutlined } from '@ant-design/icons';
import { Typography, Table, Checkbox, InputNumber, Button, Row, Col } from 'antd';
import { Link } from 'react-router-dom';
import Dialog from './Dialog';

const { Title } = Typography;

class DepartmentComponent extends React.Component {
	state = {
		visible: false,
		data: [],
		selected: {}
	};

	showModal = (selected) => {
		this.setState({
			visible: true
		});

		selected && this.setState({
			selected
		});
	};

	closeModal = () => {
		this.setState({
			visible: false,
			selected: {}
		});
	};

	UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
		this.setState({
			data: nextProps.data
		});
	}

	onSelectDepartment = (department) => {
		this.setState({
			selected: department
		});
	};

	check = (selected, e) => {
		const data = [];

		selected.languages.map(language => data.push({
			lang_id: language.lang_id,
			is_active: language.is_active,
			translation: language.translation,
		}));

		this.props.editDepartment(selected.id, {
			name: selected.name,
			code: selected.code,
			languages: data,
			position: selected.position,
			is_active: e.target.checked
		});
	};

	positionChange = (selected, value) => {
		const data = [];

		selected.languages.map(language => data.push({
			lang_id: language.lang_id,
			is_active: language.is_active,
			translation: language.translation,
		}));

		this.props.editDepartment(selected.id, {
			name: selected.name,
			code: selected.code,
			languages: data,
			position: value,
			is_active: selected.is_active
		});
	};

	render() {
		const { visible, selected } = this.state;
		const { data, createDepartment, editDepartment, deleteDepartment, languages } = this.props;

		const columns = [
			{
				title: 'Актив',
				render: item => <Checkbox checked={ item.is_active } onClick={ e => this.check(item, e) }/>
			},
			{
				title: 'Порядок',
				render: item => <InputNumber value={ item.position }
				                             onChange={ value => this.positionChange(item, value) }/>
			},
			{
				title: 'Название',
				dataIndex: 'name'
			},
			{
				title: 'Код',
				dataIndex: 'code'
			},
			{
				render: department =>
					<Row type={ 'flex' } justify={ 'end' }>
						<Button type="dashed" onClick={ () => this.showModal(department) } icon={ <EditOutlined/> }>
							Изменить
						</Button>
						&nbsp;
						<Link to={ `/store/departments/${ department.id }/parents` }><Button
							type={ 'primary' }>Классы</Button></Link>
					</Row>

			}
		];

		return (
			<>
				<Row type="flex" gutter={ 16 }>
					<Col><Title level={ 3 }>Отделения IZI</Title></Col>
					<Col span={ 12 }>
						<Button type="primary" onClick={ this.showModal } icon={ <PlusOutlined/> }>
							Создать отделение
						</Button>
					</Col>
				</Row>
				<br/>
				<Row type="flex" gutter={ 16 }>
					<Col span={ 8 }>
						<Table
							className={ 'izi-table' }
							columns={ columns }
							dataSource={ this.state.data }
							size='small'
							loading={ this.props.fetching }
							rowKey={ 'id' }
							pagination={ false }
						/>
					</Col>
				</Row>
				<Dialog
					data={ data }
					visible={ visible }
					selected={ selected }
					create={ createDepartment }
					edit={ editDepartment }
					remove={ deleteDepartment }
					close={ this.closeModal }
					languages={ languages }
				/>
			</>
		);
	}
}

DepartmentComponent.propTypes = {
	createDepartment: PropTypes.func.isRequired,
	editDepartment: PropTypes.func.isRequired,
	deleteDepartment: PropTypes.func.isRequired,
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	languages: PropTypes.array.isRequired,
};

export default DepartmentComponent;