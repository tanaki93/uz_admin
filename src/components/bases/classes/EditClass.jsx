import React from 'react';
import * as PropTypes from 'prop-types';
import { EditOutlined } from '@ant-design/icons';
import { Icon as LegacyIcon } from '@ant-design/compatible';
import { Modal, Button, Input, Table, message } from 'antd';

class EditClass extends React.Component {
	state = {
		visible: false,
		name: '',
		code: '',
		languages: []
	};

	UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
		this.props.selectedClass !== nextProps.selectedClass &&
		this.setState({
			name: nextProps.selectedClass.name,
			code: nextProps.selectedClass.code,
			languages: nextProps.selectedClass.languages
		});
	}

	onChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};

	onTableChange = (id, e) => {
		const languages = this.state.languages;
		languages.map(language => language.lang_id === id && (language[e.target.name] = e.target.value));
		this.setState({ languages });
	};

	showModal = () => {
		this.setState({
			visible: true,
		});
		this.props.selectClass(this.props.selectedClass);
	};

	error = () => {
		message.error('Такое отделение уже существует');
	};

	handleOk = () => {
		const { name, languages, code } = this.state;
		const department = this.props.selectedClass;
		const data = [];

		languages.map(language => data.push({
			is_active: true,
			lang_id: language.lang_id,
			translation: language.translation,
		}));

		if (code && name) {
			if (this.props.selectedClass.code && this.props.selectedClass.code.toLowerCase() === code.toLowerCase()) {
				this.props.editClass(this.props.selectedClass.id, {
					name,
					code,
					languages: data,
					position: department.position,
					is_active: department.is_active,
					department_id: this.props.departmentId
				});
				this.setState({
					visible: false,
					name: '',
					code: ''
				});
			} else {
				if (!this.props.classes.find(entry => entry.code.toLowerCase() === code.toLowerCase())) {
					this.props.editClass(this.props.selectedClass.id, {
						name,
						code,
						languages: data,
						position: department.position,
						is_active: department.is_active,
						department_id: this.props.departmentId
					});
					this.setState({
						visible: false,
						name: '',
						code: ''
					});
				} else {
					this.error();
				}
			}
		}
	};

	handleCancel = e => {
		this.setState({
			visible: false,
		});
	};

	handleDelete = () => {
		this.props.deleteClass(this.props.selectedClass.id);
	};

	render() {
		const { name, languages, code } = this.state;

		const columns = [
			{
				title: 'Страна',
				dataIndex: 'lang_code'
			},
			{
				title: 'Перевод',
				render: language => <Input defaultValue={ language.translation } name='translation'
				                           onChange={ e => this.onTableChange(language.lang_id, e) }/>
			},
		];

		return (
			<>
				<Button type="dashed" onClick={ this.showModal } icon={ <EditOutlined/> }>
					Изменить
				</Button>
				<Modal
					title="Изменение класса"
					visible={ this.state.visible }
					footer={
						<div style={ { display: 'flex', justifyContent: 'space-between' } }>
							<div>
								<Button type={ 'primary' } ghost onClick={ this.handleDelete }
								        icon={ <LegacyIcon type={ 'delete' }/> }>Удалить</Button>
							</div>
							<div>
								<Button onClick={ this.handleCancel }>Закрыть</Button>
								<Button type={ 'primary' } onClick={ this.handleOk }>Сохранить</Button>
							</div>
						</div>
					}
					onOk={ this.handleOk }
					onCancel={ this.handleCancel }
				>
					<table className={ 'base-table' }>
						<thead>
						<tr>
							<td>Код</td>
							<td>Название</td>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td>
								<Input value={ code } name={ 'code' } onChange={ this.onChange }/>
							</td>
							<td>
								<Input value={ name } name={ 'name' } onChange={ this.onChange }/>
							</td>
						</tr>
						</tbody>
					</table>
					<Table columns={ columns } dataSource={ languages } rowKey='lang_id'/>
				</Modal>
			</>
		);
	}
}

EditClass.propTypes = {
	editClass: PropTypes.func.isRequired,
	selectedClass: PropTypes.object.isRequired,
	selectClass: PropTypes.func.isRequired,
	deleteClass: PropTypes.func.isRequired,
	departmentId: PropTypes.string.isRequired,
	languages: PropTypes.array.isRequired,
	classes: PropTypes.array.isRequired
};

export default EditClass;