import React from 'react';
import * as PropTypes from 'prop-types';
import { Typography, Table, Checkbox, InputNumber, Button, Row, Col } from 'antd';
import CreateClass from './CreateClass';
import EditClass from './EditClass';
import { Link } from 'react-router-dom';

const { Title } = Typography;

class ClassesComponent extends React.Component {
	state = {
		data: [],
		selected: {},
		title: ''
	};


	UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
		const title = nextProps.data[0] ? nextProps.data[0].department.name : '';
		this.setState({
			data: nextProps.data,
			title
		});
	}

	onSelectClass = (department) => {
		this.setState({
			selected: department
		});
	};

	check = (selected, e) => {
		const data = [];

		selected.languages.map(language => data.push({
			is_active: language.is_active,
			lang_id: language.lang_id,
			translation: language.translation,
		}));

		this.props.editClass(selected.id, {
			name: selected.name,
			position: selected.position,
			code: selected.code,
			is_active: e.target.checked,
			languages: data,
			departmentId: this.props.departmentId
		});
	};

	positionChange = (selected, value) => {
		const data = [];

		selected.languages.map(language => data.push({
			is_active: language.is_active,
			lang_id: language.lang_id,
			translation: language.translation,
		}));

		this.props.editClass(selected.id, {
			name: selected.name,
			code: selected.code,
			languages: data,
			position: value,
			is_active: selected.is_active,
			departmentId: this.props.departmentId
		});
	};

	render() {
		const columns = [
			{
				title: 'Актив',
				render: item => <Checkbox checked={ item.is_active } onClick={ e => this.check(item, e) }/>
			},
			{
				title: 'Порядок',
				render: item => <InputNumber value={ item.position }
				                             onChange={ value => this.positionChange(item, value) }/>
			},
			{
				title: 'Название',
				dataIndex: 'name'
			},
			{
				title: 'Код',
				dataIndex: 'code'
			},
			{
				render: _class =>
					<Row type={ 'flex' } justify={ 'end' }>
						<EditClass
							classes={ this.props.data }
							departmentId={ this.props.departmentId }
							editClass={ this.props.editClass }
							selectedClass={ this.state.selected }
							selectClass={ () => this.onSelectClass(_class) }
							deleteClass={ this.props.deleteClass }
							languages={ this.props.languages }
						/>
						&nbsp;
						<Link to={ `/store/departments/${ this.props.departmentId }/parents/${ _class.id }` }><Button
							type={ 'primary' }>Категории</Button></Link>
					</Row>

			}
		];

		return (
			<>
				<Row type="flex" gutter={ 16 }>
					<Col span={ 12 }>
						<Link to={ '/store/departments/' }>Назад</Link>
					</Col>
				</Row>
				<Row type="flex" gutter={ 16 }>
					<Col><Title level={ 3 }>Классы { this.state.title }</Title></Col>
					<Col span={ 12 }>
						<CreateClass
							classes={ this.props.data }
							createClass={ this.props.createClass }
							departmentId={ this.props.departmentId }
							languages={ this.props.languages }
						/>
					</Col>
				</Row>
				<br/>
				<Row type="flex" gutter={ 16 }>
					<Col span={ { xs: 24, sm: 16, md: 8, lg: 8 } }>
						<Table className={ 'izi-table' } columns={ columns }
						       dataSource={ this.state.data.sort((a, b) => a.position - b.position) } size='small'
						       rowKey={ 'id' } loading={ this.props.fetching }/>
					</Col>
				</Row>
			</>
		);
	}
}

ClassesComponent.propTypes = {
	createClass: PropTypes.func.isRequired,
	editClass: PropTypes.func.isRequired,
	deleteClass: PropTypes.func.isRequired,
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	languages: PropTypes.array.isRequired,
	departmentId: PropTypes.string.isRequired,
};

export default ClassesComponent;