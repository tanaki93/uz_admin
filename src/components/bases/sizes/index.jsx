import React from 'react';
import * as PropTypes from 'prop-types';
import { Table, Row, Col, Checkbox, PageHeader } from 'antd';
import CreateSize from './CreateSize';
import EditSize from './EditSize';

class Sizes extends React.Component {
	state = {
		selected: {}
	};

	onSelectSize = (size) => {
		this.setState({
			selected: size
		});
	};

	render() {
		const { languages, data, createSize, editSize, fetching } = this.props;

		const columns = [
			{
				title: 'ID',
				dataIndex: 'id',
				width: 50
			},
			{
				title: 'Название',
				dataIndex: 'name'
			},
			{
				title: 'Код',
				dataIndex: 'code',
				render: code => code || 'Не указан',
				width: 100
			},
			{
				render: size => <EditSize
					deleteSize={ this.props.deleteSize }
					editSize={ editSize }
					selectedSize={ this.state.selected }
					onSelectSize={ () => this.onSelectSize(size) }
					languages={ languages }
					size={ data }
				/>,
				width: 150
			},
			{
				title: 'Связан',
				dataIndex: 'is_related',
				render: val => <Checkbox checked={ val }/>,
				width: 100
			},
		];

		return (
			<Row type="flex" justify="start" gutter={ [16, 16] }>
				<Col span={ 24 }>
					<PageHeader title='Размеры IZI' ghost={ false } extra={ [
						<CreateSize createSize={ createSize } languages={ languages } sizes={ data }/>
					] }/>
				</Col>
				<Col span={8}>
					<Table
						scroll={{ y: window.innerHeight - 300 }}
						className={ 'izi-table' }
						columns={ columns }
						dataSource={ data }
						size='small'
						loading={ fetching }
						rowKey={ 'id' }
					/>
				</Col>
			</Row>
		);
	}
}

Sizes.propTypes = {
	createSize: PropTypes.func.isRequired,
	editSize: PropTypes.func.isRequired,
	deleteSize: PropTypes.func.isRequired,
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	languages: PropTypes.array.isRequired,
};

export default Sizes;