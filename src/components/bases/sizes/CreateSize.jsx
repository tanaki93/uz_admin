import React from 'react';
import * as PropTypes from 'prop-types';
import { PlusOutlined } from '@ant-design/icons';
import { Modal, Button, Input, Table, message } from 'antd';

class CreateSize extends React.Component {
	state = {
		visible: false,
		name: '',
		code: '',
		languages: []
	};

	onChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};

	onTableChange = (id, e) => {
		const languages = this.state.languages;
		languages.map(language => language.lang_id === id && (language[e.target.name] = e.target.value));
		this.setState({ languages });
	};

	showModal = () => {
		const data = [];
		this.props.languages.map(language => data.push({
			is_active: true,
			lang_id: language.id,
			lang_name: language.name,
			lang_code: language.code,
			translation: null,
		}));

		this.setState({
			languages: data,
			visible: true,
		});
	};

	error = () => {
		message.error('Такое отделение уже существует');
	};

	handleOk = e => {
		const { name, code, languages } = this.state;
		const data = [];

		languages.map(language => data.push({
			lang_id: language.lang_id,
			is_active: language.is_active || true,
			translation: language.translation,
		}));

		if (name && code) {
			if (!this.props.sizes.find(color => color.code && color.code.toLowerCase() === code.toLowerCase())) {
				this.props.createSize({ name, code, languages: data });

				this.setState({
					visible: false,
					name: '',
					code: '',
					languages: []
				});
			} else {
				this.error();
			}
		}
	};

	handleCancel = e => {
		this.setState({
			visible: false,
		});
	};

	render() {
		const { name, languages, code } = this.state;

		const columns = [
			{
				title: 'Страна',
				dataIndex: 'lang_code'
			},
			{
				title: 'Перевод',
				render: language => <Input defaultValue={ language.translation } name='translation'
				                           onChange={ e => this.onTableChange(language.lang_id, e) }/>
			},
		];

		return (
			<>
				{ this.props.custom ? <Button type="primary" onClick={ this.showModal } icon={ <PlusOutlined/> }/> :
					<Button type="primary" onClick={ this.showModal } icon={ <PlusOutlined/> }>
						Создать размер
					</Button> }

				<Modal
					title="Создание размера"
					visible={ this.state.visible }
					onOk={ this.handleOk }
					onCancel={ this.handleCancel }
				>
					<table className={ 'base-table' }>
						<thead>
						<tr>
							<td>Код</td>
							<td>Название</td>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td>
								<Input value={ code } name={ 'code' } onChange={ this.onChange }/>
							</td>
							<td>
								<Input value={ name } name={ 'name' } onChange={ this.onChange }/>
							</td>
						</tr>
						</tbody>
					</table>
					<br/>
					<Table className={ 'izi-table' } columns={ columns } dataSource={ languages } rowKey='lang_id'
					       pagination={ false } size={ 'small' }/>
				</Modal>
			</>
		);
	}
}

CreateSize.propTypes = {
	createSize: PropTypes.func.isRequired,
	languages: PropTypes.array.isRequired,
	sizes: PropTypes.array.isRequired,
	custom: PropTypes.bool
};

export default CreateSize;