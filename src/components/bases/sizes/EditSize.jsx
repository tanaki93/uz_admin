import React from 'react';
import * as PropTypes from 'prop-types';
import { EditOutlined } from '@ant-design/icons';
import { Icon as LegacyIcon } from '@ant-design/compatible';
import { Modal, Button, Input, Table, message } from 'antd';

class EditSize extends React.Component {
	state = {
		visible: false,
		name: '',
		code: '',
		languages: []
	};

	UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
		this.props.selectedSize !== nextProps.selectedSize &&
		this.setState({
			name: nextProps.selectedSize.name,
			code: nextProps.selectedSize.code,
			languages: nextProps.selectedSize.languages
		});
	}

	onChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};

	onTableChange = (id, e) => {
		const languages = this.state.languages;
		languages.map(language => language.lang_id === id && (language[e.target.name] = e.target.value));
		this.setState({ languages });
	};

	showModal = () => {
		this.props.onSelectSize(this.props.selectedSize);
		const data = [];

		this.props.languages.map(language => data.push({
			lang_id: language.id,
			is_active: language.is_active || true,
			lang_name: language.name,
			lang_code: language.code,
			translation: language.translation,
		}));

		this.setState({
			visible: true,
			name: this.props.selectedSize.name,
			code: this.props.selectedSize.code,
			languages: data
		});
	};

	error = () => {
		message.error('Такой цвет уже существует');
	};

	handleOk = e => {
		const { name, code, languages } = this.state;
		const data = [];

		languages.map(language => data.push({
			is_active: true,
			lang_id: language.lang_id,
			translation: language.translation || '',
		}));

		if (code && name) {
			if (this.props.selectedSize.code && this.props.selectedSize.code.toLowerCase() === code.toLowerCase()) {
				this.props.editSize(this.props.selectedSize.id, { name, code, languages: data });

				this.setState({
					visible: false,
					name: '',
					code: '',
					languages: []
				});
			} else {
				if (!this.props.size.find(entry => entry.code && entry.code.toLowerCase() === code.toLowerCase())) {
					this.props.editSize(this.props.selectedSize.id, { name, code, languages: data });

					this.setState({
						visible: false,
						name: '',
						code: '',
						languages: []
					});
				} else {
					this.error();
				}
			}
		}
	};

	handleCancel = e => {
		this.setState({
			visible: false,
		});
	};

	handleDelete = () => {
		this.props.deleteSize(this.props.selectedSize.id);
	};

	render() {
		const { name, languages, code } = this.state;

		const columns = [
			{
				title: 'Страна',
				dataIndex: 'lang_code'
			},
			{
				title: 'Перевод',
				render: language => <Input defaultValue={ language.translation } name='translation'
				                           onChange={ e => this.onTableChange(language.lang_id, e) }/>
			},
		];

		return (
			<>
				<Button onClick={ this.showModal } icon={ <EditOutlined/> }>
					Изменить
				</Button>
				<Modal
					title="Изменение размера"
					visible={ this.state.visible }
					footer={
						<div style={ { display: 'flex', justifyContent: 'space-between' } }>
							<div>
								<Button type={ 'primary' } ghost onClick={ this.handleDelete }
								        disabled={ this.props.selectedSize.is_related }
								        icon={ <LegacyIcon type={ 'delete' }/> }>Удалить</Button>
							</div>
							<div>
								<Button onClick={ this.handleCancel }>Закрыть</Button>
								<Button type={ 'primary' } onClick={ this.handleOk }>Сохранить</Button>
							</div>
						</div>
					}
					onOk={ this.handleOk }
					onCancel={ this.handleCancel }
				>
					<table className={ 'base-table' }>
						<thead>
						<tr>
							<td>Код</td>
							<td>Название</td>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td>
								<Input value={ code } name={ 'code' } onChange={ this.onChange }/>
							</td>
							<td>
								<Input value={ name } name={ 'name' } onChange={ this.onChange }/>
							</td>
						</tr>
						</tbody>
					</table>
					<Table className={ 'izi-table' } columns={ columns } dataSource={ languages } rowKey='lang_id'
					       pagination={ false } size={ 'small' }/>
				</Modal>
			</>
		);
	}
}

EditSize.propTypes = {
	selectedSize: PropTypes.object.isRequired,
	onSelectSize: PropTypes.func.isRequired,
	deleteSize: PropTypes.func.isRequired,
	editSize: PropTypes.func.isRequired,
	languages: PropTypes.array.isRequired,
	size: PropTypes.array.isRequired,
};

export default EditSize;