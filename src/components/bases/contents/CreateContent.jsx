import React from 'react';
import * as PropTypes from 'prop-types';
import { PlusOutlined } from '@ant-design/icons';
import { Modal, Button, Input, Table, message } from 'antd';

class CreateContent extends React.Component {
	state = {
		visible: false,
		name: '',
		code: '',
		languages: []
	};

	UNSAFE_componentWillReceiveProps(nextProps) {
		const data = [];

		nextProps.languages.map(language => data.push({
			lang_id: language.id,
			lang_name: language.name,
			lang_code: language.code,
			translation: null,
		}));

		this.setState({
			languages: data
		});
	}

	onChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};

	onTableChange = (id, e) => {
		const languages = this.state.languages;
		languages.map(language => language.lang_id === id && (language[e.target.name] = e.target.value));
		this.setState({ languages });
	};

	showModal = () => {
		this.setState({
			visible: true,
		});
	};

	error = () => {
		message.error('Уже существует');
	};

	handleOk = () => {
		const { name, languages, code } = this.state;
		const data = [];

		languages.map(language => data.push({
			is_active: true,
			lang_id: language.lang_id,
			translation: language.translation,
		}));

		if (name && code) {
			if (!this.props.contents.find(entry => entry.code.toLowerCase() === code.toLowerCase())) {
				this.props.createContent({ name, code, languages: data, position: null, is_active: false });
				this.setState({
					visible: false,
					name: '',
					code: '',
					is_active: false,
					languages: []
				});
			} else {
				this.error();
			}
		}
	};

	handleCancel = e => {
		this.setState({
			visible: false,
		});
	};

	render() {
		const { name, languages, code } = this.state;

		const columns = [
			{
				title: 'Страна',
				dataIndex: 'lang_code'
			},
			{
				title: 'Перевод',
				render: language => <Input defaultValue={ language.translation } name='translation'
				                           onChange={ e => this.onTableChange(language.lang_id, e) }/>
			},
		];

		return (
			<>
				<Button type="primary" onClick={ this.showModal } icon={ <PlusOutlined/> }>
					Создать
				</Button>
				<Modal
					title="Создание состава"
					visible={ this.state.visible }
					onOk={ this.handleOk }
					onCancel={ this.handleCancel }
				>
					<table className={ 'base-table' }>
						<thead>
						<tr>
							<td>Код</td>
							<td>Название</td>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td>
								<Input value={ code } name={ 'code' } onChange={ this.onChange }/>
							</td>
							<td>
								<Input value={ name } name={ 'name' } onChange={ this.onChange }/>
							</td>
						</tr>
						</tbody>
					</table>
					<Table columns={ columns } dataSource={ languages } rowKey='lang_id'/>
				</Modal>
			</>
		);
	}
}

CreateContent.propTypes = {
	createContent: PropTypes.func.isRequired,
	languages: PropTypes.array.isRequired,
	contents: PropTypes.array.isRequired
};

export default CreateContent;