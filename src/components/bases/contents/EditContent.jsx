import React from 'react';
import * as PropTypes from 'prop-types';
import { EditOutlined } from '@ant-design/icons';
import { Icon as LegacyIcon } from '@ant-design/compatible';
import { Modal, Button, Input, Table, message } from 'antd';

class EditContent extends React.Component {
	state = {
		visible: false,
		name: '',
		code: '',
		languages: []
	};

	onChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};

	UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
		this.props.selectedContent !== nextProps.selectedContent &&
		this.setState({
			name: nextProps.selectedContent.name,
			code: nextProps.selectedContent.code,
			languages: nextProps.selectedContent.languages
		});
	}


	onTableChange = (id, e) => {
		const languages = this.state.languages;
		languages.map(language => language.lang_id === id && (language[e.target.name] = e.target.value));
		this.setState({ languages });
	};

	showModal = () => {
		this.setState({
			visible: true,
			name: this.props.selectedContent.name,
			code: this.props.selectedContent.code,
			languages: this.props.selectedContent.languages
		});

		this.props.selectContent(this.props.selectedContent);
	};

	error = () => {
		message.error('Уже существует');
	};

	handleOk = () => {
		const { name, languages, code } = this.state;
		const content = this.props.selectedContent;
		const data = [];

		languages.map(language => data.push({
			lang_id: language.lang_id,
			is_active: language.is_active || true,
			translation: language.translation,
		}));

		if (name && code) {
			if (this.props.selectedContent.code.toLowerCase() === code.toLowerCase()) {
				this.props.editContent(this.props.selectedContent.id, {
					name,
					code,
					languages: data,
					position: content.position,
					is_active: content.is_active
				});
				this.setState({
					visible: false,
					name: '',
					code: '',
					is_active: false,
					languages: []
				});
			} else {
				if (!this.props.contents.find(entry => entry.code.toLowerCase() === code.toLowerCase())) {
					this.props.editContent(this.props.selectedContent.id, {
						name,
						code,
						languages: data,
						position: content.position,
						is_active: content.is_active
					});
					this.setState({
						visible: false,
						name: '',
						code: '',
						is_active: false,
						languages: []
					});
				} else {
					this.error();
				}
			}
		}
	};

	handleCancel = e => {
		this.setState({
			visible: false,
			name: '',
			code: '',
			is_active: false,
			languages: []
		});
	};

	handleDelete = () => {
		this.props.deleteContent(this.props.selectedContent.id);
	};

	render() {
		const { name, languages, code } = this.state;

		const columns = [
			{
				title: 'Страна',
				dataIndex: 'lang_code'
			},
			{
				title: 'Перевод',
				render: language => <Input defaultValue={ language.translation } name='translation'
				                           onChange={ e => this.onTableChange(language.lang_id, e) }/>
			},
		];

		return (
			<>
				<Button type="dashed" onClick={ this.showModal } icon={ <EditOutlined/> }>
					Изменить
				</Button>
				<Modal
					title="Изменение состава"
					visible={ this.state.visible }
					footer={
						<div style={ { display: 'flex', justifyContent: 'space-between' } }>
							<div>
								<Button type={ 'primary' } ghost onClick={ this.handleDelete }
								        icon={ <LegacyIcon type={ 'delete' }/> }>Удалить</Button>
							</div>
							<div>
								<Button onClick={ this.handleCancel }>Закрыть</Button>
								<Button type={ 'primary' } onClick={ this.handleOk }>Сохранить</Button>
							</div>
						</div>
					}
					onCancel={ this.handleCancel }
				>
					<table className={ 'base-table' }>
						<thead>
						<tr>
							<td>Код</td>
							<td>Название</td>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td>
								<Input value={ code } name={ 'code' } onChange={ this.onChange }/>
							</td>
							<td>
								<Input value={ name } name={ 'name' } onChange={ this.onChange }/>
							</td>
						</tr>
						</tbody>
					</table>
					<Table columns={ columns } dataSource={ languages } rowKey='lang_id'/>
				</Modal>
			</>
		);
	}
}

EditContent.propTypes = {
	editContent: PropTypes.func.isRequired,
	selectedContent: PropTypes.object.isRequired,
	selectContent: PropTypes.func.isRequired,
	deleteContent: PropTypes.func.isRequired,
	languages: PropTypes.array.isRequired,
	contents: PropTypes.array.isRequired
};

export default EditContent;