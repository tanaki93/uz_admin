import React from 'react';
import * as PropTypes from 'prop-types';
import { Typography, Table, Row, Col } from 'antd';
import CreateContent from './CreateContent';
import EditContent from './EditContent';

const { Title } = Typography;

class ContentComponent extends React.Component {
	state = {
		data: [],
		selected: {}
	};

	UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
		this.setState({
			data: nextProps.data
		});
	}

	onSelectContent = (content) => {
		this.setState({
			selected: content
		});
	};

	check = (selected, e) => {
		const data = [];

		selected.languages.map(language => data.push({
			lang_id: language.lang_id,
			is_active: language.is_active,
			translation: language.translation,
		}));

		this.props.editContent(selected.id, {
			name: selected.name,
			code: selected.code,
			languages: data,
			position: selected.position,
			is_active: e.target.checked
		});
	};

	positionChange = (selected, value) => {
		const data = [];

		selected.languages.map(language => data.push({
			lang_id: language.lang_id,
			is_active: language.is_active,
			translation: language.translation,
		}));

		this.props.editContent(selected.id, {
			name: selected.name,
			code: selected.code,
			languages: data,
			position: value,
			is_active: selected.is_active
		});
	};

	render() {
		const columns = [
			{
				title: 'Название',
				dataIndex: 'name'
			},
			{
				title: 'Код',
				dataIndex: 'code'
			},
			{
				render: content =>
					<Row type={ 'flex' } justify={ 'end' }>
						<EditContent
							contents={ this.props.data }
							editContent={ this.props.editContent }
							selectedContent={ this.state.selected }
							selectContent={ () => this.onSelectContent(content) }
							deleteContent={ this.props.deleteContent }
							languages={ this.props.languages }
						/>
					</Row>

			}
		];

		return (
			<>
				<Row type="flex" gutter={ 16 }>
					<Col><Title level={ 3 }>Составы IZI</Title></Col>
					<Col span={ 12 }>
						<CreateContent
							contents={ this.props.data }
							createContent={ this.props.createContent }
							languages={ this.props.languages }
						/>
					</Col>
				</Row>
				<br/>
				<Row type="flex" gutter={ 16 }>
					<Col span={ { xs: 24, sm: 16, md: 8, lg: 8 } }>
						<Table className={ 'izi-table' } columns={ columns } dataSource={ this.state.data } size='small'
						       loading={ this.props.fetching }
						       rowKey={ 'id' }/>
					</Col>
				</Row>
			</>
		);
	}
}

ContentComponent.propTypes = {
	createContent: PropTypes.func.isRequired,
	editContent: PropTypes.func.isRequired,
	deleteContent: PropTypes.func.isRequired,
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	languages: PropTypes.array.isRequired,
};

export default ContentComponent;