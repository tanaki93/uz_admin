import React from 'react';
import PropTypes from 'prop-types';
import { Layout, Menu, Select } from 'antd';
import * as Locales from '../../constants/locales';
import { LogoutOutlined, UserOutlined } from '@ant-design/icons';

const Header = (props) => {
	const { languageId, changeLocaleAction, logout, user } = props;

	const localeChange = (locale) => {
		changeLocaleAction({ languageId: locale });
	};

	return (
		<Layout.Header className='header'>
			<Select onChange={ localeChange } defaultValue={ languageId } size='small'>
				<Select.Option value={ Locales.RU }>
					Русский
				</Select.Option>
				<Select.Option value={ Locales.TR }>
					Türk
				</Select.Option>
			</Select>
			<Menu mode='horizontal' theme="dark">
				<Menu.SubMenu icon={ <UserOutlined/> } title={ user.username }>
					<Menu.Item key='/logout' onClick={ logout }>
						<LogoutOutlined/>
						<span>Выйти</span>
					</Menu.Item>
				</Menu.SubMenu>
			</Menu>
		</Layout.Header>
	);
};

Header.propTypes = {
	changeLocaleAction: PropTypes.func.isRequired,
	logout: PropTypes.func.isRequired,
	languageId: PropTypes.number.isRequired,
	user: PropTypes.object.isRequired
};

export default Header;