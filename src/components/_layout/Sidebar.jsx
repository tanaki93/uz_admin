import React, { useEffect, useState } from 'react';
import * as PropTypes from 'prop-types';
import { Layout, Menu } from 'antd';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { useIntl } from 'react-intl';
import logoSmall from '../../assets/images/logo_small.svg';
import logo from '../../assets/images/logo.svg';
import * as userTypes from '../../constants/userType';
import {
	DatabaseOutlined, HomeOutlined, BarcodeOutlined, PlusOutlined, CalculatorOutlined,
	CarryOutOutlined, DeploymentUnitOutlined, DownloadOutlined, UploadOutlined, RollbackOutlined, SettingOutlined
} from '@ant-design/icons';

const { Sider } = Layout;
const { SubMenu } = Menu;


const Sidebar = (props) => {
	const intl = useIntl();
	const history = useHistory();
	const match = useRouteMatch();
	const [collapsed, setCollapsed] = useState(false);
	const [current, setCurrent] = useState(match.path);
	const [openKeys, setOpenKeys] = useState([]);
	const { userType } = props;

	const rootSubmenuKeys = ['sub2', 'sub3', 'sub4'];

	const onOpenChange = openKeys => {
		const latestOpenKey = openKeys.find(key => openKeys.indexOf(key) === -1);
		if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
			setOpenKeys(openKeys);
		} else {
			latestOpenKey ? setOpenKeys([latestOpenKey]) : setOpenKeys([]);
		}
	};

	useEffect(() => {
		match.path.includes('/brand/:brandId/departments') && setCurrent('/');
	}, [match]);

	const handleClick = e => {
		setCurrent(e.key);
	};

	return (
		<Sider collapsible collapsed={ collapsed } onCollapse={ setCollapsed }>
			{/*<img alt='logo' src={ collapsed ? logoSmall : logo } className='logo'/>*/}
			<h1 style={{marginLeft: 60,marginTop:20, marginBottom: 15, color:'white'}}>Hurrem</h1>
			<Menu theme='dark' defaultSelectedKeys={ ['/'] }
			      onClick={ handleClick }
			      openKeys={ openKeys }
			      onOpenChange={ onOpenChange }
			      selectedKeys={ [current] } mode='inline'>
				{
					userType !== userTypes.CONTROLLER && userType !== userTypes.MARKETEER &&
					<Menu.Item key='/' onClick={ () => history.push('/') }>
						<HomeOutlined/>
						<span>Главная</span>
					</Menu.Item>
				}
				{
					userType === userTypes.ADMIN &&
					<Menu.Item key='/documents/create'
					           onClick={ () => history.push('/documents/create') }>
						<PlusOutlined/>
						<span>Создание документа</span>
					</Menu.Item>
				}
				{
					userType === userTypes.ADMIN &&
					<SubMenu
						key='sub3'
						title={
							<>
								<SettingOutlined/>
								<span>Администрирование</span>
							</>
						}
					>
						<Menu.Item key='/countries'
						           onClick={ () => history.push('/countries') }>Страны</Menu.Item>
						<Menu.Item key='/cities'
						           onClick={ () => history.push('/cities') }>Города</Menu.Item>
						<Menu.Item key='/points'
						           onClick={ () => history.push('/points') }>Пункты</Menu.Item>
						<Menu.Item key='/currencies'
						           onClick={ () => history.push('/currencies') }>Валюты</Menu.Item>
						<Menu.Item key='/languages'
						           onClick={ () => history.push('/languages') }>Языки</Menu.Item>
						<Menu.Item key='/users'
						           onClick={ () => history.push('/users') }>Пользователи</Menu.Item>
					</SubMenu>
				}
				{
					userType === userTypes.ADMIN &&
					<SubMenu
						key='sub10'
						title={
							<>
								<RollbackOutlined/>
								<span>Возвраты</span>
							</>
						}
					>
						<Menu.Item key='/refund'
						           onClick={ () => history.push('/refund') }>Hurrem</Menu.Item>
						<Menu.Item key='/refund/client'
						           onClick={ () => history.push('/refund/client') }>От
							клиентов</Menu.Item>
					</SubMenu>
				}
				{
					userType === userTypes.ADMIN &&
					<SubMenu
						key='sub2'
						title={
							<>
								<DatabaseOutlined/>
								<span>Справочники (Vendor)</span>
							</>
						}
					>

						<Menu.Item key='/brand' onClick={ () => history.push('/brand') }>
							<span>Поставщики</span>
						</Menu.Item>
						<Menu.Item key='/colors' onClick={ () => history.push('/colors') }>
							Цвета
						</Menu.Item>
						<Menu.Item key='/sizes' onClick={ () => history.push('/sizes') }>
							Размеры
						</Menu.Item>
						<Menu.Item key='/contents' onClick={ () => history.push('/contents') }>
							Составы
						</Menu.Item>
					</SubMenu>
				}
				{
					userType === userTypes.ADMIN &&
					<SubMenu
						key='sub4'
						title={
							<>
								<DatabaseOutlined/>
								<span>Справочники (Hurrem)</span>
							</>
						}
					>
						<Menu.Item key='/store/departments'
						           onClick={ () => history.push('/store/departments') }>
							Отделения
						</Menu.Item>
						<Menu.Item key='/sizes-base' onClick={ () => history.push('/base/sizes') }>
							Размеры
						</Menu.Item>
						<Menu.Item key='/colors-base' onClick={ () => history.push('/base/colors') }>
							Цвета
						</Menu.Item>
						<Menu.Item key='/contents-base' onClick={ () => history.push('/base/contents') }>
							Составы
						</Menu.Item>
					</SubMenu>
				}

				{
					userType === userTypes.ADMIN &&
					<SubMenu
						key='subland'
						title={
							<>
								<DatabaseOutlined/>
								<span>Лэндинг</span>
							</>
						}
					>

						<Menu.Item key='/landing/slider' onClick={ () => history.push('/landing/slider') }>
							<span>Слайдер</span>
						</Menu.Item>

						<Menu.Item key='/landing/categories' onClick={ () => history.push('/landing/categories') }>
							<span>Изображения категорий</span>
						</Menu.Item>
					</SubMenu>
				}
				{
					userType === userTypes.ADMIN &&
					<Menu.Item key='/products' onClick={ () => history.push('/products') }>
						<BarcodeOutlined/>
						<span>Товары Vendor</span>
					</Menu.Item>
				}
				{
					userType === userTypes.ADMIN &&
					<Menu.Item key='/izi-products' onClick={ () => history.push('/izi-products') }>
						<BarcodeOutlined/>
						<span>Товары Hurrem</span>
					</Menu.Item>
				}
				{
					userType === userTypes.MARKETEER &&
					<Menu.Item key='/marketing' onClick={ () => history.push('/marketing') }>
						<BarcodeOutlined/>
						<span>Товары</span>
					</Menu.Item>
				}
				{
					userType === userTypes.MARKETEER &&
					<Menu.Item key='/marketing/only' onClick={ () => history.push('/marketing/only') }>
						<BarcodeOutlined/>
						<span>Товары для маркетолога</span>
					</Menu.Item>
				}
				{
					userType === userTypes.ADMIN &&
					<Menu.Item key='/orders' onClick={ () => history.push('/orders') }>
						<DownloadOutlined/>
						<span>Заказы</span>
					</Menu.Item>
				}
				{
					userType === userTypes.CONTROLLER &&
					<Menu.Item key='/packages-check' onClick={ () => history.push('/packages-check') }>
						<CarryOutOutlined/>
						<span>{ intl.formatMessage({ id: 'packages-check.title' }) }</span>
					</Menu.Item>
				}
				{
					userType === userTypes.CONTROLLER &&
					<Menu.Item key='/products-check' onClick={ () => history.push('/products-check') }>
						<CarryOutOutlined/>
						<span>{ intl.formatMessage({ id: 'products-check.title' }) }</span>
					</Menu.Item>
				}
				{
					userType === userTypes.CONTROLLER &&
					<Menu.Item key='/product-delivery'
					           onClick={ () => history.push('/product-delivery') }>
						<UploadOutlined/>
						<span>{ intl.formatMessage({ id: 'products-delivery.title' }) }</span>
					</Menu.Item>
				}
				{
					userType === userTypes.CONTROLLER &&
					<Menu.Item key='/packages' onClick={ () => history.push('/packages') }>
						<DeploymentUnitOutlined/>
						<span>{ intl.formatMessage({ id: 'packages.title' }) }</span>
					</Menu.Item>
				}
				{
					userType === userTypes.ADMIN &&
					<Menu.Item key='/logistics' onClick={ () => history.push('/logistics') }>
						<CalculatorOutlined/>
						<span>Логистика</span>
					</Menu.Item>
				}
			</Menu>
		</Sider>
	);
};

Sidebar.propTypes = {
	userType: PropTypes.number.isRequired
};

export default Sidebar;