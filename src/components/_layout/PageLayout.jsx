import React from 'react';
import * as PropTypes from 'prop-types';
import { Route, withRouter } from 'react-router-dom';
import { Layout } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Sidebar from './Sidebar';
import Header from './Header';
import compose from 'recompose/compose';
import { changeLocale as changeLocaleAction } from 'actions/locale';
import { loseAuth as loseAuthAction, reAuth as reAuthAction } from 'actions/creators/auth';

const { Content } = Layout;

class PageLayout extends React.Component {

	UNSAFE_componentWillReceiveProps(nextProps) {
		localStorage.getItem('Token');
		if (localStorage.getItem('Token') === undefined || localStorage.getItem('Token') === null) {
			this.props.history.push('/login');
		}
	}

	componentDidMount() {
		if (localStorage.getItem('Token') || this.props.auth.get('status') === undefined) {
			this.props.reAuthAction();
		}
	}

	logout = () => {
		this.props.loseAuthAction();
	};

	localeChange = (locale) => {
		this.props.changeLocaleAction({ languageId: locale });
	};

	render() {
		const { changeLocaleAction, languageId, component: Component, ...rest } = this.props;

		return (
			<Route { ...rest } render={ matchProps => (
				<Layout style={ { minHeight: '100vh' } }>
					<Sidebar userType={ this.props.user.user_type }/>
					<Layout>
						<Header
							user={ this.props.user }
							languageId={ languageId } changeLocaleAction={ changeLocaleAction }
							logout={ this.logout }
						/>
						<Content style={ { margin: '25px 0' } }>
							<div style={ {
								padding: '0 24px',
								height: 'calc(100vh - 114px)',
								overflow: 'auto'
							} }>
								<Component { ...matchProps }/>
							</div>
						</Content>
					</Layout>
				</Layout>
			) }/>
		);
	}
}

PageLayout.propTypes = {
	loseAuthAction: PropTypes.func.isRequired,
	reAuthAction: PropTypes.func.isRequired,
	auth: PropTypes.object.isRequired,
	user: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
	auth: state.application.get('auth'),
	user: state.application.getIn(['auth', 'user']),
	languageId: state.application.get('languageId')
});

const mapDispatchToProps = dispatch => bindActionCreators({
	loseAuthAction,
	reAuthAction,
	changeLocaleAction
}, dispatch);

export default compose(
	withRouter,
	connect(mapStateToProps, mapDispatchToProps)
)(PageLayout);
