import React from 'react';
import * as PropTypes from 'prop-types';
import { Select } from 'antd';

class Selector extends React.Component {
	state = {
		selectInputValue: '',
		options: []
	};

	onSearch = (val) => {
		this.setState({
			selectInputValue: val
		});
	};

	render() {
		const { data, onChange, value, disabled, onBlur } = this.props;

		return (
			<Select
				open
				autoFocus={ true }
				disabled={ disabled }
				value={ value }
				style={ { minWidth: '120px', width: '100%' } }
				showSearch
				optionFilterProp='children'
				filterOption={ (input, option) => (
					option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
				) }
				onSearch={ this.onSearch }
				onChange={ onChange }
				onBlur={ onBlur }
			>
				{ data.map(item =>
					<Select.Option key={ item.id } value={ item.id }>
						{ item.name }
					</Select.Option>
				) }
			</Select>
		);
	}
}

Selector.propTypes = {
	data: PropTypes.array.isRequired,
	type: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired,
	record: PropTypes.object.isRequired,
};

export default Selector;