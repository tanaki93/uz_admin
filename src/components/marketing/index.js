import React, { useEffect, useRef, useState } from 'react';
import * as PropTypes from 'prop-types';
import { Table, Checkbox, PageHeader, Form } from 'antd';
import Filter from './Filter';
import Selector from './Selector';
import Details from './Details';
import { useIntl } from 'react-intl';
import EditOutlined from '@ant-design/icons/lib/icons/EditOutlined';

const IziProducts = (props) => {
	const intl = useIntl();
	const wrapper = useRef(null);
	const [visible, setVisible] = useState(false);
	const [product, setProduct] = useState({});
	const [products, setProducts] = useState([]);
	const {
		count, current, fetching, hierarchy, fetchingDepartments, departments, fetchingColors, colors,
		fetchingBrands, brands, fetchProducts, editProduct, hitProduct, marketing, setMarketing
	} = props;


	useEffect(() => {
		setProducts([...props.products]);
	}, [props]);

	const handleOk = e => {
		setVisible(false);
	};

	const onClickHandler = (product) => {
		setVisible(true);
		setProduct(product);
	};

	const handleCancel = e => {
		setVisible(false);
	};

	const hit = (e) => {
		hitProduct(e.target.id, e.target.checked);
	};

	const mark = (e) => {
		setMarketing(e.target.id, e.target.checked);
	};

	const columns = [
		{
			title: 'Марк',
			render: item => <Checkbox id={ item.id } checked={ item.is_marketolog } onClick={ mark }/>,
			width: 40,
		},
		{
			title: 'Картинка',
			render: product => product.images &&
				<img alt={ product.title } src={ product.images[0] } style={ { width: 50, height: 70 } }/>,
			width: 80
		},
		{
			title: intl.formatMessage({ id: 'products-check.details.brand' }),
			dataIndex: ['brand', 'name'],
			width: 100
		},
		{
			title: 'Название',
			dataIndex: 'title',
			width: 100
		},
		{
			title: 'Отделение',
			dataIndex: ['department', 'name']
		},

		{
			title: 'Категория',
			dataIndex: ['category', 'name']
		},
		{
			title: 'Цвета',
			dataIndex: ['colour', 'name']
		},
		{
			title: intl.formatMessage({ id: 'products-check.details.composition' }),
			editable: editProduct,
			dataIndex: ['content', 'name']
		},
		{
			title: 'Цена продажи',
			dataIndex: 'selling_price',
			width: 100
		},
		{
			title: 'Цена Hurrem',
			dataIndex: 'izi_price',
			width: 100
		},
		{
			title: 'Ссылка',
			dataIndex: 'link',
			render: link => <a href={ link.url } target={ '_blank' }>Ссылка</a>,
			width: 100
		},
		{
			title: 'Ссылка Hurrem',
			dataIndex: 'izi_link',
			key: 'izi_link',
			width: 100,
			render: link => <a href={ link } target={ '_blank' }>Ссылка</a>
		},
		{
			title: 'Хит',
			render: item => <Checkbox id={ item.id } checked={ item.is_hit } onClick={ hit }/>,
			width: 40,
		},
		{
			title: intl.formatMessage({ id: 'packages.list.details' }),
			render: record => <a onClick={ () => onClickHandler(record) }>
				{ intl.formatMessage({ id: 'packages.list.details' }) }
			</a>,
			width: 100
		},
	];

	return (
		<>
			<PageHeader
				title='Список маркетолога'
				ghost={ false }
				footer={
					<Filter
						marketing
						fetchingBrands={ fetchingBrands }
						brands={ brands }
						fetchingDepartments={ fetchingDepartments }
						departments={ departments }
						fetchingColors={ fetchingColors }
						colors={ colors }
						fetching={ fetching }
						hierarchy={ hierarchy }
						fetchProducts={ fetchProducts }
						count={ count }
						current={ current }
					/>
				}
			/>
			<div className='page-wrapper page-wrapper--xlarge' ref={ wrapper }>
				<Table
					scroll={ { y: wrapper.current?.clientHeight - 114 } }
					loading={ fetching }
					className={ 'izi-table' }
					pagination={ false }
					columns={ columns }
					dataSource={ products }
					size='small'
					rowKey={ 'id' }
				/>
			</div>
			<Details product={ product } visible={ visible } handleOk={ handleOk } handleCancel={ handleCancel }/>
		</>
	);
};

IziProducts.propTypes = {
	marketing: PropTypes.bool,
	fetching: PropTypes.bool.isRequired,
	fetchingColors: PropTypes.bool.isRequired,
	fetchingBrands: PropTypes.bool.isRequired,
	fetchingDepartments: PropTypes.bool.isRequired,
	fetchingContents: PropTypes.bool.isRequired,
	products: PropTypes.array.isRequired,
	contents: PropTypes.array.isRequired,
	departments: PropTypes.array.isRequired,
	brands: PropTypes.array.isRequired,
	colors: PropTypes.array.isRequired,
	count: PropTypes.number.isRequired,
	pages: PropTypes.number.isRequired,
	current: PropTypes.number.isRequired,
	hierarchy: PropTypes.object.isRequired,
	fetchProducts: PropTypes.func.isRequired,
	hitProduct: PropTypes.func,
	editProduct: PropTypes.func,
	activateProduct: PropTypes.func,
	setMarketing: PropTypes.func
};

export default IziProducts;