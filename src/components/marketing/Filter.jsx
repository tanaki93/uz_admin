import React, { useRef, useState } from 'react';
import * as PropTypes from 'prop-types';
import { Icon as LegacyIcon } from '@ant-design/compatible';
import { Button, Col, Input, Pagination, Row, Select, Form, Cascader } from 'antd';
import { injectIntl } from 'react-intl';

const Filter = (props) => {
	const formRef = useRef(null);
	const cascaderRef = useRef(null);
	const form = formRef.current;
	const [page, setPage] = useState(1);
	const {
		fetchingDepartments, departments, fetchingColors, colors, fetchingBrands, brands, intl, fetchProducts, marketing
	} = props;

	const _departments = [];
	departments.map(d => _departments.push({
		value: d.id, label: d.name, children: d.categories.map(x => {
			return { value: x.id, label: x.name };
		})
	}));

	const onReset = () => {
		props.fetchProducts('', '', '', '', '', 1);
	};

	const onPageChange = (page) => {
		setPage(page);
		const { brandId, department, colourId, query, isMarketing } = form.getFieldsValue();
		fetchProducts({
			brandId,
			departmentId: department[0] || '',
			categoryId: department[1] || '',
			colourId,
			query,
			page: page,
			isMarketing
		});
	};

	const onFilterSubmit = () => {
		const { brandId, department, colourId, query, isMarketing } = form.getFieldsValue();
		fetchProducts({
			brandId,
			departmentId: department[0] || '',
			categoryId: department[1] || '',
			colourId,
			query,
			page: 1,
			isMarketing
		});
	};

	return (
		<Form
			ref={ formRef }
			initialValues={ {
				query: '',
				brandId: '',
				department: [],
				categoryId: '',
				colourId: '',
				isMarketing: ''
			} }
			onReset={ onReset }
			onFinish={ onFilterSubmit }
		>
			<Row gutter={ 8 }>
				<Col span={ 8 }>
					<Form.Item label='Поиск' name='query'>
						<Input.Search onSearch={ onFilterSubmit } enterButton/>
					</Form.Item>
				</Col>
			</Row>
			<Row gutter={ 8 }>
				<Col span={ 4 }>
					<Form.Item label={ intl.formatMessage({ id: 'products-check.details.brand' }) } name='brandId'>
						<Select loading={ fetchingBrands } style={ { width: '100%' } }>
							<Select.Option value={ '' }>
								Все
							</Select.Option>
							<Select.Option value={ 0 }>
								{ intl.formatMessage({ id: 'products-check.selector.not-selected' }) }
							</Select.Option>
							{ brands.map(item =>
								<Select.Option key={ item.id } value={ item.id }>
									{ item.name }
								</Select.Option>
							) }
						</Select>
					</Form.Item>
				</Col>
				<Col span={ 8 }>
					<Form.Item label='Отделение' name='department'>
						<Cascader ref={ cascaderRef } options={ _departments } changeOnSelect
						          disabled={ fetchingDepartments }/>
					</Form.Item>
				</Col>
				<Col span={ 4 }>
					<Form.Item label={ intl.formatMessage({ id: 'products-check.details.color' }) } name='colourId'>
						<Select loading={ fetchingColors } style={ { width: '100%' } }>
							<Select.Option value={ '' }>
								Все
							</Select.Option>
							<Select.Option value={ 0 }>
								{ intl.formatMessage({ id: 'products-check.selector.not-selected' }) }
							</Select.Option>
							{ colors.map(item =>
								<Select.Option key={ item.id } value={ item.id }>
									{ item.name }
								</Select.Option>
							) }
						</Select>
					</Form.Item>
				</Col>
				<Col span={ 8 }>
					<Row type='flex' justify='end' gutter={ 16 }>
						<Button type={ 'primary' } onClick={ onFilterSubmit } disabled={ props.fetching }>
							{ intl.formatMessage({ id: 'packages-check.filter.filter' }) }
						</Button>
						&nbsp;
						<Button type={ 'default' } onClick={ onReset } icon={ <LegacyIcon type={ 'minus' }/> }
						        disabled={ props.fetching }>
							Очистить фильтр
						</Button>
					</Row>
				</Col>
				<Col span={ 24 }>
					<Row type={ 'flex' } align='middle' justify={ 'space-between' } gutter={ [16, 16] }>
						<Col>
							<Pagination current={ page } pageSize={ 200 } total={ props.count }
							            onChange={ onPageChange } disabled={ props.fetching }/>
						</Col>
						<Col>
							Всего в списке: { props.fetching ? 'Загрузка...' : props.count }
						</Col>
					</Row>
				</Col>
			</Row>
		</Form>
	);
};

Filter.propTypes = {
	marketing: PropTypes.bool,
	fetching: PropTypes.bool.isRequired,
	fetchProducts: PropTypes.func.isRequired,
	count: PropTypes.number.isRequired,
	fetchingDepartments: PropTypes.bool.isRequired,
	departments: PropTypes.array.isRequired,
	fetchingColors: PropTypes.bool.isRequired,
	colors: PropTypes.array.isRequired,
	fetchingBrands: PropTypes.bool.isRequired,
	brands: PropTypes.array.isRequired,
};

export default (injectIntl)(Filter);