import React from 'react';
import { Select } from 'antd';
import * as PropTypes from 'prop-types';

class CategorySelector extends React.Component {
	state = {
		selectInputValue: '',
		options: []
	};

	onSearch = (val) => {
		this.setState({
			selectInputValue: val
		});
	};

	render() {
		const { data, defaultValue, id, type } = this.props;
		return (
			<Select
				value={ defaultValue }
				showSearch
				optionFilterProp="children"
				filterOption={ (input, option) => (
					typeof (option.props.children) === 'string' && option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
				) }
				onSearch={ this.onSearch }
				style={ { width: '100%', color: !defaultValue.name ? 'inherit' : 'red' } }
				onChange={ (value) => this.props.processProduct(id, type, value) }>
				{ data && data.categories.map(category => (
					<Select.Option key={ category.id } value={ category.id }>
						{ category.name }
					</Select.Option>
				)) }
			</Select>
		);
	}
}

CategorySelector.propTypes = {
	data: PropTypes.object,
	type: PropTypes.string.isRequired,
	defaultValue: PropTypes.any.isRequired,
	processProduct: PropTypes.func.isRequired,
	id: PropTypes.number.isRequired,
	selectedDepartment: PropTypes.any.isRequired
};

export default CategorySelector;