import React from 'react';
import * as PropTypes from 'prop-types';
import { Button, Table } from 'antd';
import { useIntl } from 'react-intl';

const List = (props) => {
	const intl = useIntl();
	const { fetching, createDocuments, brands, wrapper } = props;

	const columns = [
		{
			title: intl.formatMessage({ id: 'products-check.details.brand' }),
			dataIndex: 'name'
		},
		{
			title: 'Всего товаров',
			dataIndex: 'total_count'
		},
		{
			title: 'Всего докуметов',
			dataIndex: 'document_count'
		},
		{
			title: 'Обработано',
			dataIndex: 'processed_count'
		},
		{
			title: 'В обработке',
			dataIndex: 'in_process_count'
		},
		{
			title: 'Не обработано',
			dataIndex: 'out_process_count'
		},
		{
			render: item =>
				<Button disabled={ item.out_process_count === 0 } type={ 'primary' }
				        onClick={ () => createDocuments({ brand_id: item.id }) }>
					Создать новый документ
				</Button>
		}
	];

	return (
		<Table
			loading={ fetching }
			scroll={ { y: wrapper.current?.clientHeight - 84 } }
			className={ 'izi-table' }
			columns={ columns }
			dataSource={ brands }
			pagination={ false }
		/>
	);
};

List.propTypes = {
	fetching: PropTypes.bool.isRequired,
	brands: PropTypes.array.isRequired,
	createDocuments: PropTypes.func.isRequired,
	wrapper: PropTypes.object.isRequired
};

export default List;