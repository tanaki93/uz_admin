import React, { useRef } from 'react';
import * as PropTypes from 'prop-types';
import { Col, PageHeader, Row } from 'antd';
import List from './List';

const Create = (props) => {
	const wrapper = useRef(null);
	const { fetching, brands, createDocuments } = props;

	return (
		<>
			<PageHeader
				ghost={ false }
				title="Создание документа"
			/>
			<div className='page-wrapper page-wrapper--small' ref={ wrapper }>
				<Row type="flex">
					<Col span={ 24 }>
						<List fetching={ fetching } brands={ brands } createDocuments={ createDocuments }
						      wrapper={ wrapper }/>
					</Col>
				</Row>
			</div>
		</>
	);
};

Create.propTypes = {
	fetching: PropTypes.bool.isRequired,
	brands: PropTypes.array.isRequired,
	createDocuments: PropTypes.func.isRequired
};

export default Create;