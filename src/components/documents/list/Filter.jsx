import React, { useState } from 'react';
import * as PropTypes from 'prop-types';
import { Select, Button, DatePicker, Form } from 'antd';
import { useIntl } from 'react-intl';

const { RangePicker } = DatePicker;

const Filter = (props) => {
	const intl = useIntl();
	const [brandId, setBrandId] = useState('');
	const [userId, setUserId] = useState('');
	const [dateFrom, setDateFrom] = useState('');
	const [dateTo, setDateTo] = useState('');
	const { users, brands, fetchingBrands, fetchingUsers, fetchDocuments } = props;

	const handleDateSelect = (value, dateString) => {
		setDateFrom(dateString[0]);
		setDateTo(dateString[1]);
	};

	const handleSubmit = () => {
		fetchDocuments(userId, brandId, dateFrom, dateTo);
	};

	return (
		<Form layout='inline'>
			<Form.Item label={ intl.formatMessage({ id: 'products-check.details.brand' }) }>
				<Select defaultValue={ '' } onChange={ setBrandId }
				        style={ { width: '200px' } } loading={ fetchingBrands }>
					<Select.Option value={ '' }>Все</Select.Option>
					{ brands.map(brand => <Select.Option value={ brand.id }
					                                     key={ brand.id }>{ brand.name }</Select.Option>) }
				</Select>
			</Form.Item>
			<Form.Item label={ 'Ответственный' }>
				<Select defaultValue={ '' } onChange={ setUserId }
				        style={ { width: '200px' } } loading={ fetchingUsers }>
					<Select.Option value={ '' }>Все</Select.Option>
					{ users.map(user => <Select.Option
						value={ user.id }
						key={ user.id }>{ user.first_name + ' ' + user.last_name }</Select.Option>) }
				</Select>
			</Form.Item>
			<Form.Item label={ intl.formatMessage({ id: 'packages-check.filter.date' }) }>
				<RangePicker onChange={ handleDateSelect } allowClear/>
			</Form.Item>
			<Button onClick={ handleSubmit } type='primary'>
				{ intl.formatMessage({ id: 'packages-check.filter.filter' }) }
			</Button>
		</Form>
	);
};

Filter.propTypes = {
	fetchDocuments: PropTypes.func.isRequired,
	users: PropTypes.array.isRequired,
	brands: PropTypes.array.isRequired,
	fetchingBrands: PropTypes.bool.isRequired,
	fetchingUsers: PropTypes.bool.isRequired,
};

export default Filter;