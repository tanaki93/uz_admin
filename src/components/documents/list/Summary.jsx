import React from 'react';
import * as PropTypes from 'prop-types';
import { Checkbox, Table } from 'antd';
import { Link } from 'react-router-dom';
import moment from 'moment';
import * as processingSteps from './../../../constants/processingSteps';
import { useIntl } from 'react-intl';

const Summary = (props) => {
	const intl = useIntl();
	const { fetching, documents, wrapper, setNew } = props;

	const renderStatus = (status) => {
		switch (status) {
			default:
				return <span style={ { color: 'blue' } }>В обработке</span>;
			case processingSteps.END:
				return <span style={ { color: 'orange' } }>Готово на проверку</span>;
			case processingSteps.APPROVED:
				return <span style={ { color: 'green' } }>в IZISHOP</span>;
		}
	};

	const set = (e) => {
		setNew(e.target.id, e.target.checked);
	};

	const columns = [
		{
			title: 'New',
			render: item => <Checkbox id={ item.id } checked={ item.is_new } onClick={ set }/>,
			width: 70,
		},
		{
			title: 'ID',
			dataIndex: 'id',
			width: 70
		},
		{
			title: intl.formatMessage({ id: 'products-check.details.brand' }),
			dataIndex: 'brand',
			render: entry => entry.name || '',
			width: 150
		},
		{
			title: 'Отделение',
			dataIndex: 'department',
			render: entry => entry ? entry.name : '',
			width: 150
		},
		{
			title: 'Ответственный',
			dataIndex: 'user',
			render: user => user ?
				`${ user.first_name || 'Имя' } ${ user.last_name || 'Фамилия' }` :
				intl.formatMessage({ id: 'packages-check.filter.date' })
		},
		{
			title: 'Кол-во товаров',
			render: item => <>
				<span>{ item.products }</span> <br/>
				(<span style={ { color: 'green' } }>{ item.products - item.no_stock_or_price }</span> / <span
				style={ { color: 'red' } }>{ item.no_stock_or_price }</span>)
			</>
		},
		{
			title: 'Дата изменения',
			dataIndex: 'updated_at',
			render: date => moment(date).format('YYYY-MM-DD')
		},
		{
			title: 'Статус',
			dataIndex: 'step',
			render: step => renderStatus(step)
		},
		{
			render: doc => doc.step < processingSteps.APPROVED &&
				<Link to={ `/documents/${ doc.id }` }>Проверить</Link>,
			width: 150
		}
	];

	return (
		<Table
			scroll={ { y: wrapper.current?.clientHeight - 116 } }
			pagination={ false }
			dataSource={ documents }
			loading={ fetching }
			columns={ columns }
			rowKey={ 'id' }
		/>
	);
};


Summary.propTypes = {
	documents: PropTypes.array.isRequired,
	fetching: PropTypes.bool.isRequired,
	wrapper: PropTypes.object.isRequired,
	setNew: PropTypes.func.isRequired
};

export default Summary;