import React, { useEffect, useRef, useState } from 'react';
import * as PropTypes from 'prop-types';
import { PageHeader, Tabs } from 'antd';
import Summary from './Summary';
import Filter from './Filter';
import * as processingSteps from 'constants/processingSteps';

const { TabPane } = Tabs;

const Documents = (props) => {
	const [documents, setDocuments] = useState([]);
	const wrapper = useRef(null);
	const { fetching, fetchingBrands, brands, fetchingUsers, users, setNew } = props;

	useEffect(() => {
		setDocuments([...props.documents]);
	}, [props]);

	const handleSwitch = (type) => {
		switch (+type) {
			default:
			case 1:
				setDocuments(props.documents);
				break;
			case 2:
				setDocuments(props.documents.filter(doc => doc.step === processingSteps.END));
				break;
			case 3:
				setDocuments(props.documents.filter(doc => doc.step < processingSteps.END));
				break;
			case 4:
				setDocuments(props.documents.filter(doc => doc.step === processingSteps.APPROVED));
				break;
		}
	};

	return (
		<>
			<PageHeader
				ghost={ false }
				title='Документы'
				footer={
					<Tabs size='small' defaultActiveKey='1' onChange={ handleSwitch }>
						<TabPane tab='Все' key='1'/>
						<TabPane tab='На проверку' key='2'/>
						<TabPane tab='В обработке' key='3'/>
						<TabPane tab='Проверенные' key='4'/>
					</Tabs>
				}
			>
				<Filter
					fetchDocuments={ props.fetchDocuments }
					fetchingBrands={ fetchingBrands }
					brands={ brands }
					fetchingUsers={ fetchingUsers }
					users={ users }
				/>
			</PageHeader>
			<div className='page-wrapper' ref={ wrapper }>
				<Summary documents={ documents } fetching={ fetching } wrapper={ wrapper } setNew={ setNew }/>
			</div>
		</>
	);
};

Documents.propTypes = {
	setNew: PropTypes.func.isRequired,
	fetchDocuments: PropTypes.func.isRequired,
	fetching: PropTypes.bool.isRequired,
	documents: PropTypes.array.isRequired,
	fetchingBrands: PropTypes.bool.isRequired,
	brands: PropTypes.array.isRequired,
	fetchingUsers: PropTypes.bool.isRequired,
	users: PropTypes.array.isRequired,
};

export default Documents;