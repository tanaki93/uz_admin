import React, { useState, useEffect } from 'react';
import * as PropTypes from 'prop-types';
import { Icon as LegacyIcon } from '@ant-design/compatible';
import { Button, Col, Input, Pagination, Row, Select, Tooltip } from 'antd';
import { useIntl } from 'react-intl';

const Filter = (props) => {
	const intl = useIntl();
	const [query, setQuery] = useState('');
	const [departmentId, setDepartmentId] = useState(null);
	const [categoryId, setCategoryId] = useState(null);
	const [colourId, setColourId] = useState(null);
	const [contentId, setContentId] = useState(null);
	const [categories, setCategories] = useState([]);
	const [current, setCurrent] = useState(1);
	const { fetching, hierarchy, fetchProducts, count, documentId } = props;

	useEffect(() => {
		hierarchy.department_id !== undefined ?
			setCategories(getCategories(hierarchy.brand.departments, hierarchy.department_id))
			: setCategories([]);

	}, [hierarchy]);


	const onDepartmentSelect = (value) => {
		setDepartmentId(value);
		setCategories(getCategories(hierarchy.brand.departments, value));
		setCategoryId(null);
	};

	const getCategories = (data, id) => {
		if (data.find(entry => entry.id === id)) {
			return data.find(entry => entry.id === id) && data.find(entry => entry.id === id).categories;
		}
		return [];
	};

	const onClear = () => {
		setColourId(null);
		setCategoryId(null);
		setDepartmentId(null);
		setQuery('');
		setCurrent(1);
		fetchProducts(documentId);
	};

	const onPageChange = (page) => {
		setCurrent(page);
		fetchProducts(documentId, {
			query, department_id: departmentId, category_id: categoryId, colour_id: colourId, content_id: contentId, page
		});
	};

	const onChangeSearch = e => {
		setQuery(e.target.value);
	};

	const onFilterSubmit = () => {
		setCurrent(1);

		fetchProducts(documentId, {
			query, department_id: departmentId, category_id: categoryId, colour_id: colourId, content_id: contentId, page: current || 1
		});
	};

	return (
		<div className='documents-filter'>
			<Row type="flex" align='bottom' gutter={ [16, 16] }>
				<Col span={ 18 }>
					<Row type="flex" gutter={ 16 }>
						<Col span={ 8 }>
							Поиск:
							<Input.Search name={ 'query' } value={ query } onChange={ onChangeSearch }/>
						</Col>
						<Col span={ 4 }>
							Отделение:
							<Select
								style={ { width: '100%' } }
								onChange={ onDepartmentSelect }
								value={ departmentId }
							>
								<Select.Option value={ null }>
									Все
								</Select.Option>
								<Select.Option value={ 0 }>
									Не указан
								</Select.Option>
								{ hierarchy.brand?.departments.map(department =>
									<Select.Option key={ department.id } value={ department.id }>
										<Tooltip title={ department.name }
										         placement="left">{ department.name }</Tooltip>
									</Select.Option>
								) }
							</Select>
						</Col>
						<Col span={ 4 }>
							Категория:
							<Select
								style={ { width: '100%' } }
								onChange={ setCategoryId }
								value={ categoryId }
								disabled={ departmentId === '' }
							>
								<Select.Option value={ null }>
									Все
								</Select.Option>
								<Select.Option value={ 0 }>
									Не указан
								</Select.Option>
								{ categories.map(category =>
									<Select.Option key={ category.id } value={ category.id }>
										<Tooltip title={ category.name }
										         placement="left">{ category.name }</Tooltip>
									</Select.Option>
								) }
							</Select>
						</Col>
						<Col span={ 4 }>
							{ intl.formatMessage({ id: 'products-check.details.color' }) }:
							<Select
								style={ { width: '100%' } }
								onChange={ setColourId }
								value={ colourId }
							>
								<Select.Option value={ null }>
									Все
								</Select.Option>
								<Select.Option value={ 0 }>
									Не указан
								</Select.Option>
								{ hierarchy.colours?.map(colour =>
									<Select.Option key={ colour.id } value={ colour.id }>
										<Tooltip title={ colour.name } placement="left">{ colour.name }</Tooltip>
									</Select.Option>
								) }
							</Select>
						</Col>
						<Col span={ 4 }>
							{ intl.formatMessage({ id: 'products-check.details.composition' }) }:
							<Select
								style={ { width: '100%' } }
								onChange={ setContentId }
								value={ contentId }
							>
								<Select.Option value={ null }>
									Все
								</Select.Option>
								<Select.Option value={ 0 }>
									Не указан
								</Select.Option>
								{ hierarchy.contents?.map(content =>
									<Select.Option key={ content.id } value={ content.id }>
										<Tooltip title={ content.name } placement="left">
											{ content.name }
										</Tooltip>
									</Select.Option>
								) }
							</Select>
						</Col>
					</Row>
				</Col>
				<Col span={ 6 }>
					<Row type="flex" justify='end' gutter={ 16 }>
						<Button type={ 'primary' } onClick={ onFilterSubmit } disabled={ fetching }>
							{ intl.formatMessage({ id: 'packages-check.filter.filter' }) }
						</Button>
						&nbsp;
						<Button type={ 'default' } onClick={ onClear } icon={ <LegacyIcon type={ 'minus' }/> }
						        disabled={ fetching }>
							Очистить фильтр
						</Button>
					</Row>
				</Col>
				<Col span={ 24 }>
					<Pagination
						current={ current }
						pageSize={ 200 }
						total={ count }
						onChange={ onPageChange }
						disabled={ fetching }
					/>
				</Col>
			</Row>
		</div>
	);
};

Filter.propTypes = {
	fetching: PropTypes.bool.isRequired,
	hierarchy: PropTypes.object.isRequired,
	fetchProducts: PropTypes.func.isRequired,
	count: PropTypes.number.isRequired,
	documentId: PropTypes.string.isRequired
};

export default Filter;