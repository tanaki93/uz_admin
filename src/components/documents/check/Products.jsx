import React, { useState } from 'react';
import * as PropTypes from 'prop-types';
import { Checkbox, Table, Row, Col, Carousel, Modal, Button, Typography } from 'antd';
import moment from 'moment';
import { useIntl } from 'react-intl';

const { Paragraph } = Typography;

const Products = (props) => {
	const intl = useIntl();
	const [visible, setVisible] = useState(false);
	const [product, setProduct] = useState({});
	const { fetching, products, wrapper } = props;

	const onClickHandler = (object) => {
		setProduct(object)
		setVisible(true)
	};

	const handleCancel = e => {
		setVisible(false)
	};

	const columns = [
		{
			title: 'Картинка',
			render: product =>
				<img alt={ product.title } src={ product.images[0] } style={ { width: 50, height: 70 } }/>
		},
		{
			title: intl.formatMessage({ id: 'products-check.details.brand' }),
			dataIndex: 'brand',
			render: brand => brand.name
		},
		{
			title: 'Название',
			dataIndex: 'title',
		},
		{
			title: 'Описание',
			dataIndex: 'description',
			render: text =>
				<Paragraph ellipsis={ { rows: 3 } }>
					<div dangerouslySetInnerHTML={ { __html: text } }/>
				</Paragraph>
		},
		{
			title: 'Отделение',
			dataIndex: 'izi_department',
			render: item => item.name
		},
		{
			title: 'Категория',
			dataIndex: 'izi_category',
			render: item => item.name
		},
		{
			title: 'Цвета',
			dataIndex: 'colour',
			render: item => item.name
		},
		{
			title: intl.formatMessage({ id: 'products-check.details.composition' }),
			dataIndex: 'content',
			render: item => item.name
		},
		{
			title: 'Оригинальная цена',
			dataIndex: 'original_price'
		},
		{
			title: 'Цена со скидкой',
			dataIndex: 'discount_price'
		},
		{
			title: 'Цена продажи',
			dataIndex: 'selling_price'
		},
		{
			title: 'Ссылка',
			dataIndex: 'link',
			render: link => <a href={ link.url } target={ '_blank' }>Ссылка</a>
		},
		{
			title: intl.formatMessage({ id: 'packages.list.details' }),
			render: record =>
				<Button onClick={ () => onClickHandler(record) }>
					{ intl.formatMessage({ id: 'packages.list.details' }) }
				</Button>
		},
	];

	return (
		<>
			<Table
				scroll={ { y: wrapper.current?.clientHeight - 104 } }
				columns={ columns }
				pagination={ false }
				dataSource={ products }
				size='small'
				loading={ fetching }
				rowKey={ 'id' }
			/>
			<Modal
				width={ '1200px' }
				title="Обработка данных"
				visible={ visible }
				onCancel={ handleCancel }
				footer={ null }
			>
				{
					product.id &&
					<>
						<Row gutter={ 16 }>
							<Col span={ 5 }>
								<Carousel autoplay>
									{ product.images.map(item => (
										<img src={ item } alt={ '' }/>
									)) }
								</Carousel>
							</Col>
							{/*<Col span={ 19 }>*/}
							{/*	<Row>*/}
							{/*		<h3>Название:</h3>*/}
							{/*		<Col span={ 12 }>*/}
							{/*			<p>{ product.title }</p>*/}
							{/*		</Col>*/}
							{/*		<Col span={ 12 }>*/}
							{/*			{ title }*/}
							{/*		</Col>*/}
							{/*	</Row>*/}
							{/*	<Row>*/}
							{/*		<h3>Описание:</h3>*/}
							{/*		<Col span={ 12 }>*/}
							{/*			<div dangerouslySetInnerHTML={ { __html: product.description } }/>*/}
							{/*		</Col>*/}
							{/*		<Col span={ 12 }>*/}
							{/*			{ description }*/}
							{/*		</Col>*/}
							{/*	</Row>*/}
							{/*</Col>*/}
						</Row>
						<br/>
						<Row gutter={ 16 }>
							{/*<Col span={ 12 }>*/}
							{/*	<Row gutter={ 16 }>*/}
							{/*		<Col span={ 12 }>*/}
							{/*			<h3>Цена</h3>*/}
							{/*		</Col>*/}
							{/*		<Col span={ 12 }>*/}
							{/*			<h3>Цена IZI</h3>*/}
							{/*		</Col>*/}
							{/*		<Col span={ 12 }>*/}
							{/*			<p>Оригинальная: <b>{ product.original_price } TL</b></p>*/}
							{/*		</Col>*/}
							{/*		<Col span={ 12 }>*/}
							{/*			<p>{ original_price }</p>*/}
							{/*		</Col>*/}
							{/*		<Col span={ 12 }>*/}
							{/*			<p>Цена продажи: <b>{ product.selling_price } TL</b></p>*/}
							{/*		</Col>*/}
							{/*		<Col span={ 12 }>*/}
							{/*			<p>{ selling_price }</p>*/}
							{/*		</Col>*/}
							{/*		<Col span={ 12 }>*/}
							{/*			<p>Цена со скидкой: <b>{ product.discount_price } TL</b></p>*/}
							{/*		</Col>*/}
							{/*		<Col span={ 12 }>*/}
							{/*			<p>{ discount_price }</p>*/}
							{/*		</Col>*/}
							{/*	</Row>*/}

							{/*</Col>*/}

							<Col span={ 6 }>
								<Row gutter={ 16 }>
									<Col span={ 24 }>
										<h3>Атрибуты</h3>
									</Col>
									<Col span={ 24 }>
										<p><b>Отделение:</b> { product.department.name }</p>
									</Col>
									<Col span={ 24 }>
										<p><b>Категория:</b> { product.category.name }</p>
									</Col>
									<Col span={ 24 }>
										<p>
											<b>{ intl.formatMessage({ id: 'products-check.details.color' }) }:</b> { product.colour.name }
										</p>
									</Col>
									<Col span={ 24 }>
										<p>
											<b>{ intl.formatMessage({ id: 'products-check.details.composition' }) }:</b> { product.content.name }
										</p>
									</Col>
								</Row>
							</Col>
							<Col span={ 6 }>
								<Col span={ 24 }>
									<h3>Доставка</h3>
								</Col>
								<Col span={ 24 }>
									<p><b>Дата доставки:</b> { product.delivery_date }</p>
								</Col>
								<Col span={ 24 }>
									<p><Checkbox checked={ product.is_rush_delivery }/> <b>Быстрая доставка</b>
									</p>
								</Col>
								<Col span={ 24 }>
									<p><Checkbox checked={ product.is_free_argo }/> <b>Бесплатная доставка</b>
									</p>
								</Col>
							</Col>
						</Row>
						<br/>
						<Row type={ 'flex' } align={ 'middle' } justify={ 'space-between' }>
							<Col>
								<Row type={ 'flex' } gutter={ 16 }>
									<Col>
										<a target={ '_blank' } href={ product.link.url }>Ссылка на товар</a>
									</Col>
									<Col>
										<b>Создан:</b> { moment(product.created_at).format('DD MMM YYYY, HH:mm') }
									</Col>
									<Col>
										<b> Обновлен:</b> { moment(product.updated_at).format('DD MMM YYYY, HH:mm') }
									</Col>
								</Row>
							</Col>
						</Row>
					</>
				}
			</Modal>
		</>
	);
};

Products.propTypes = {
	products: PropTypes.any,
	fetching: PropTypes.any.isRequired,
	wrapper: PropTypes.object.isRequired
};

export default Products;