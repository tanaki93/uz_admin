import '../../../assets/styles/slider.css';
import React, { Fragment, useRef, useState } from 'react';
import * as PropTypes from 'prop-types';
import { useIntl } from 'react-intl';
import { Icon as LegacyIcon } from '@ant-design/compatible';
import { Button, Input, Modal, PageHeader, Popover } from 'antd';
import Filter from './Filter';
import Products from './Products';
import * as processingSteps from './../../../constants/processingSteps';

const Check = (props) => {
	const wrapper = useRef(null);
	const intl = useIntl();
	const [step] = useState(0);
	const [comments, setComments] = useState('');
	const [approving, setApproving] = useState(false);
	const [discarding, setDiscarding] = useState(false);
	const { fetching, documentId, products, hierarchy, fetchProducts, sendBack, approve } = props;

	const handleOk = () => {
		if (approving) {
			approve(documentId);
		}
		if (discarding) {
			sendBack(documentId, comments);
			setComments('');
		}
		close();
	};

	const close = () => {
		setApproving(false);
		setDiscarding(false);
	};

	const handleVisibleChange = visible => {
		setApproving(visible);
	};

	return (
		<>
			<PageHeader
				ghost={ false }
				title="Проверка документа"
				subTitle={
					<Fragment>
						<span style={ { display: 'inline-block', marginTop: 3, marginRight: 15 } }>
							<b>{ intl.formatMessage({ id: 'products-check.details.brand' }) }: </b>
							{ hierarchy.brand && hierarchy.brand.name }
						</span>
						<span style={ { display: 'inline-block', marginTop: 3, marginRight: 15 } }>
							<b>Отделение: </b>{ hierarchy.department_name }
						</span>
					</Fragment>
				}
				extra={ [
					<Button key='decline' className={ 'decline-btn' } onClick={ () => setDiscarding(false) }
					        icon={ <LegacyIcon type={ 'arrow-left' }/> }>
						Отправить на обработку снова
					</Button>,
					<Popover
						key='accept'
						content={ <Button onClick={ () => handleOk('toApprove') }>OK</Button> }
						title="Одобрить документ?"
						trigger="click"
						visible={ approving }
						onVisibleChange={ handleVisibleChange }
					>
						<Button className={ 'accept-btn' }>
							Завершить проверку и отправить в IZISHOP
						</Button>
					</Popover>
					// <span key='brand' style={ { marginRight: '15px' } }>
					// 	<b>{ intl.formatMessage({ id: 'products-check.details.brand' }) }: </b>
					// 	{ hierarchy.brand && hierarchy.brand.name }
					// </span>,
					// <span key='department_name'>
					// 	<b>Отделение: </b>{ hierarchy.department_name }
					// </span>
				] }
			>
				<Filter
					fetching={ fetching }
					page={ products.page }
					count={ products.count }
					step={ hierarchy.step }
					hierarchy={ hierarchy }
					fetchProducts={ fetchProducts }
					documentId={ documentId }
				/>
			</PageHeader>
			{ products.step === processingSteps.END &&
			<Modal
				title={ intl.formatMessage({ id: 'products-check.comment.title' }) }
				visible={ discarding }
				onOk={ () => handleOk('toDiscard') }
				okText='Отправить'
				cancelText='Отмена'
				onCancel={ close }
			>
				<Input.TextArea rows={ 3 } name={ 'comments' } onChange={ setComments }/>
			</Modal>
			}
			<div className='page-wrapper page-wrapper--large' ref={ wrapper }>
				<Products
					step={ step }
					hierarchy={ hierarchy }
					products={ products.products || [] }
					fetching={ fetching }
					wrapper={ wrapper }
				/>
			</div>
		</>
	);
};

Check.propTypes = {
	fetching: PropTypes.bool.isRequired,
	documentId: PropTypes.string.isRequired,
	products: PropTypes.object.isRequired,
	hierarchy: PropTypes.object.isRequired,
	fetchProducts: PropTypes.func.isRequired,
	sendBack: PropTypes.func.isRequired,
	approve: PropTypes.func.isRequired,
};

export default Check;