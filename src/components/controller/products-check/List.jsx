import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Button, Table } from 'antd';
import moment from 'moment';
import Details from './Details';
import PackageStatusSelector from './packageStatusSelector';
import { injectIntl } from 'react-intl';

class List extends Component {
	state = {
		selected: [],
		showDetails: false,
		details: {},
	};

	openDetailsModal = (item) => {
		this.setState({
			showDetails: true,
			details: item
		});
	};

	closeDetailsModal = () => {
		this.setState({
			showDetails: false,
			details: {}
		});
	};

	selectKeys = (selected) => {
		this.setState({
			selected
		});
	};


	render() {
		const { fetching, data, changeProductStatus, setProductComment, intl } = this.props;
		const { showDetails, details } = this.state;

		const columns = [
			{
				title: intl.formatMessage({ id: 'packages-check.filter.number' }),
				dataIndex: 'stage',
				key: 'package_id'
			},
			{
				title: intl.formatMessage({ id: 'products-check.list.id' }),
				dataIndex: ['product', 'id'],
				key: 'product_id'
			},
			{
				title: intl.formatMessage({ id: 'products-check.list.product' }),
				dataIndex: ['product', 'title'],
				key: 'product_name'
			},
			{
				title: intl.formatMessage({ id: 'products-check.list.details' }),
				render: item => <Button type={ 'link' }
				                        onClick={ () => this.openDetailsModal(item) }>{ intl.formatMessage({ id: 'packages.list.details' }) }</Button>
			},
			{
				title: intl.formatMessage({ id: 'packages-check.list.updated-at' }),
				dataIndex: 'updated',
				key: 'package_updated_at',
				render: date => moment(date).format('DD.MM.YYYY | HH:mm')
			},
			{
				title: intl.formatMessage({ id: 'products-check.list.state' }),
				key: 'package_stage',
				render: pack =>
					<PackageStatusSelector
						changeStatus={ changeProductStatus }
						setComment={ setProductComment }
						pack={ pack }
					/>
			},
		];

		const rowSelection = {
			onChange: (selectedRowKeys) => {
				this.selectKeys(selectedRowKeys);
			}
		};


		return (
			<>
				<Table
					loading={ fetching }
					rowSelection={ rowSelection }
					className={ 'izi-table' }
					columns={ columns }
					dataSource={ data.sort((a, b) => new Date(b.updated) - new Date(a.updated)) }
					bordered
					size="small"
					rowKey={ 'id' }
				/>
				<Details
					visible={ showDetails }
					close={ this.closeDetailsModal }
					data={ details }
				/>

			</>

		);
	}
}

List.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	changeProductStatus: PropTypes.func.isRequired,
	setProductComment: PropTypes.func.isRequired
};

export default (injectIntl)(List);