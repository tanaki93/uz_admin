import React, { Component } from 'react';
import { Carousel, Col, Modal, Row } from 'antd';
import * as PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';

class Details extends Component {
	render() {
		const { data, visible, close, intl } = this.props;

		return (
			<Modal
				destroyOnClose
				visible={ visible }
				footer={ false }
				onCancel={ close }
				width={ '700px' }
			>
				{ data.id && <Row gutter={ 16 }>
					<Col span={ 8 }>
						<Carousel autoplay>
							{
								data.product.images.map(image => <img src={ image } alt={ data.id.image }/>)
							}
						</Carousel>
					</Col>
					<Col span={ 16 }>
						<b>{ intl.formatMessage({ id: 'products-check.list.product' }) }:</b> { data.product.title }
						<br/>
						<b>{ intl.formatMessage({ id: 'products-check.list.id' }) }:</b> { data.product.id } <br/>
						<hr/>
						<b>{ intl.formatMessage({ id: 'products-check.details.brand' }) }:</b> { data.product.brand.name }
						<br/>
						<b>{ intl.formatMessage({ id: 'products-check.details.size' }) }:</b> { data.size.name } <br/>
						<b>{ intl.formatMessage({ id: 'products-check.details.color' }) }:</b> { data.product.colour.name }
						<br/>
						<b>{ intl.formatMessage({ id: 'products-check.details.composition' }) }:</b> { data.product.content.name }
						<br/>
						<a href={ data.link }>{ intl.formatMessage({ id: 'products-check.details.izi-link' }) }</a>
					</Col>
				</Row> }
			</Modal>
		);
	}
}

Details.propTypes = {
	visible: PropTypes.any.isRequired,
	close: PropTypes.any.isRequired,
	data: PropTypes.any.isRequired,
};

export default (injectIntl)(Details);