import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Form } from '@ant-design/compatible';
import { Select } from 'antd';
import * as productStatus from '../../../constants/productCheckStatus';
import Comment from './Comment';
import { injectIntl } from 'react-intl';

class PackageStatusSelector extends Component {
	state = {
		showComment: false,
		status: '',
	};

	componentDidMount() {
		this.setState({
			status: this.props.pack.checking_status
		});
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			status: nextProps.pack.checking_status
		});
	}

	openCommentsModal = () => {
		this.setState({
			showComment: true
		});
	};

	closeCommentsModal = () => {
		this.setState({
			showComment: false
		});
	};

	onChange = (value) => {
		this.setState({
			status: value
		});

		if (value !== 0) {
			if (value === productStatus.GOOD) {
				this.props.changeStatus(this.props.pack.id, productStatus.GOOD);
			} else {
				this.openCommentsModal();
			}
		}
	};

	render() {
		const { showComment, status } = this.state;
		const { pack, setComment, changeStatus, intl } = this.props;

		return (
			<>
				<Form style={ { display: 'flex' } }>
					<Form.Item style={ { margin: 0 } }>
						<Select value={ this.state.status }
						        placeholder={ intl.formatMessage({ id: 'products-check.selector.selected' }) }
						        style={ { width: '120px' } }
						        onChange={ value => this.onChange(value) }>
							<Select.Option
								value={ 0 }>{ intl.formatMessage({ id: 'products-check.selector.not-selected' }) }</Select.Option>
							<Select.Option
								value={ productStatus.GOOD }>{ intl.formatMessage({ id: 'products-check.selector.ok' }) }</Select.Option>
							<Select.Option
								value={ productStatus.DEFECT }>{ intl.formatMessage({ id: 'products-check.selector.defect' }) }</Select.Option>
							<Select.Option
								value={ productStatus.ERROR }>{ intl.formatMessage({ id: 'products-check.selector.wrong' }) }</Select.Option>
						</Select> &nbsp;
					</Form.Item>
				</Form>
				<Comment
					pack={ pack }
					visible={ showComment }
					setProductComment={ setComment }
					changeStatus={ changeStatus }
					status={ status }
					close={ this.closeCommentsModal }
				/>
			</>

		);
	}
}

PackageStatusSelector.propTypes = {
	changeStatus: PropTypes.func.isRequired,
	setComment: PropTypes.func.isRequired,
	pack: PropTypes.object.isRequired
};

export default (injectIntl)(PackageStatusSelector);