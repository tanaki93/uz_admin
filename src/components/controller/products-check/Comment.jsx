import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Button, Input, Modal } from 'antd';
import { injectIntl } from 'react-intl';

class Comment extends Component {
	state = {
		comment: ''
	};

	submit = () => {
		this.props.setProductComment(this.props.pack.id, this.state.comment);
		this.props.changeStatus(this.props.pack.id, this.props.status);
		this.props.close();
		this.setState({
			comment: ''
		});
	};

	onChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};

	render() {
		const { comment } = this.state;
		const { intl } = this.props;

		return (
			<Modal
				title={ `${ intl.formatMessage({ id: 'products-check.comment.title' }) } ${ this.props.status === -1 ? `(${ intl.formatMessage({ id: 'products-check.selector.defect' }) })` : `(${ intl.formatMessage({ id: 'products-check.selector.wrong' }) })` }` }
				visible={ this.props.visible }
				onCancel={ this.props.close }
				footer={ [
					<Button disabled={ comment === '' } key="submit" type="primary" onClick={ this.submit }>
						{ intl.formatMessage({ id: 'products-check.comment.send' }) }
					</Button>
				] }
			>
				<Input.TextArea rows={ 5 } value={ comment } name='comment' onChange={ this.onChange }/>
			</Modal>
		);
	}
}

Comment.propTypes = {
	pack: PropTypes.object.isRequired,
	visible: PropTypes.bool.isRequired,
	setProductComment: PropTypes.func.isRequired,
	changeStatus: PropTypes.func.isRequired,
	status: PropTypes.number.isRequired,
	close: PropTypes.func.isRequired
};

export default (injectIntl)(Comment);