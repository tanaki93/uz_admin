import React, { Component, Fragment } from 'react';
import * as PropTypes from 'prop-types';
import { Col, PageHeader, Row, Tabs } from 'antd';
import Filter from './Filter';
import List from './List';
import { injectIntl } from 'react-intl';
import * as productStatus from '../../../constants/productCheckStatus';

const { TabPane } = Tabs;

class ProductsCheck extends Component {
	state = {
		data: [],
		type: 1
	};

	UNSAFE_componentWillReceiveProps(nextProps) {
		this.setState({
			data: nextProps.data.filter(item => item.checking_status === productStatus.WAIT)
		});
	}

	handleSwitch = (type) => {
		switch (+type) {
			default:
			case 100:
				this.setState({
					data: this.props.data,
					type
				});
				break;
			case productStatus.ERROR:
				this.setState({
					data: this.props.data.filter(item => item.checking_status === productStatus.ERROR),
					type
				});
				break;
			case productStatus.DEFECT:
				this.setState({
					data: this.props.data.filter(item => item.checking_status === productStatus.DEFECT),
					type
				});
				break;
			case productStatus.WAIT:
				this.setState({
					data: this.props.data.filter(item => item.checking_status === productStatus.WAIT),
					type
				});
				break;
			case productStatus.GOOD:
				this.setState({
					data: this.props.data.filter(item => item.checking_status === productStatus.GOOD),
					type
				});
				break;
		}
	};

	render() {
		const { intl, fetching, fetchProductsToCheck, changeProductStatus, setProductComment } = this.props;
		const { data } = this.state;

		return (
			<Fragment>
				<Row type="flex" gutter={ [16, 16] }>
					<Col span={ 24 }>
						<PageHeader
							ghost={ false }
							title={ intl.formatMessage({ id: 'products-check.title' }) }
							footer={
								<Tabs size='large' defaultActiveKey={ '' + productStatus.WAIT }
								      onChange={ this.handleSwitch }>
									<TabPane tab="Статус не выбран" key={ '' + productStatus.WAIT }/>
									<TabPane tab="Товары в порядке" key={ '' + productStatus.GOOD }/>
									<TabPane tab="Брак" key={ '' + productStatus.DEFECT }/>
									<TabPane tab="Не тот товар" key={ '' + productStatus.ERROR }/>
									<TabPane tab="Все" key={ '' + 100 }/>
								</Tabs>
							}
						>
							<Filter fetchProductsToCheck={ fetchProductsToCheck }/>
						</PageHeader>
					</Col>
					<Col span={ 24 }>
						<List
							fetching={ fetching }
							data={ data }
							changeProductStatus={ changeProductStatus }
							setProductComment={ setProductComment }
						/>
					</Col>
				</Row>
			</Fragment>
		);
	}
}

ProductsCheck.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	fetchProductsToCheck: PropTypes.func.isRequired,
	changeProductStatus: PropTypes.func.isRequired,
	setProductComment: PropTypes.func.isRequired
};

export default (injectIntl)(ProductsCheck);