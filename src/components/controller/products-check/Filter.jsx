import React from 'react';
import * as PropTypes from 'prop-types';
import { Form } from '@ant-design/compatible';
import { Button, Col, DatePicker, Input, Row } from 'antd';
import { injectIntl } from 'react-intl';

const { RangePicker } = DatePicker;

class Filter extends React.Component {
	state = {
		bulk: 0,
		query: '',
		number: '',
		dateFrom: '',
		dateTo: ''
	};

	handleChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};

	handleSelect = (name, value) => {
		this.setState({
			[name]: value
		});
	};

	handleDateSelect = (value, dateString) => {
		this.setState({
			dateFrom: dateString[0],
			dateTo: dateString[1]
		});
	};


	handleSubmit = () => {
		const { query, number, dateFrom, dateTo } = this.state;
		this.props.fetchProductsToCheck({ query, number, dateFrom, dateTo });
	};

	handleReset = () => {
		this.setState({
			query: '',
			number: '',
			dateFrom: '',
			dateTo: ''
		}, () => this.props.fetchProductsToCheck());
	};

	render() {
		const { intl } = this.props;
		const formItemLayout = {
			labelCol: {
				xl: { span: 24 },
			},
			wrapperCol: {
				xl: { span: 24 },
			},
		};

		return (
			<Row type={ 'flex' }>
				<Col>
					<Form layout="inline" { ...formItemLayout }>
						<Form.Item label='Название товара'>
							<Input name={ 'query' } onChange={ this.handleChange } value={ this.state.query }/>
						</Form.Item>
						<Form.Item label={ intl.formatMessage({ id: 'packages-check.filter.date' }) }>
							<RangePicker onChange={ this.handleDateSelect } allowClear/>
						</Form.Item>
						<Form.Item label={ intl.formatMessage({ id: 'packages-check.filter.number' }) }>
							<Input name={ 'number' } onChange={ this.handleChange } value={ this.state.number }/>
						</Form.Item>
						<Form.Item label={ <span>&nbsp;</span> }>
							<Button onClick={ this.handleSubmit }
							        type={ 'primary' }>{ intl.formatMessage({ id: 'packages-check.filter.filter' }) }</Button>
						</Form.Item>
						<Form.Item label={ <span>&nbsp;</span> }>
							<Button
								onClick={ this.handleReset }>{ intl.formatMessage({ id: 'packages-check.filter.drop' }) }</Button>
						</Form.Item>
					</Form>
				</Col>
			</Row>

		);
	}
}

Filter.propTypes = {
	fetchProductsToCheck: PropTypes.func.isRequired
};

export default (injectIntl)(Filter);