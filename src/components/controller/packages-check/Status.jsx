import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Button } from 'antd';
import * as packageStatus from '../../../constants/packageStatus';
import { injectIntl } from 'react-intl';

class Status extends Component {
	changePackageStatus = (packageId, status) => {
		this.props.changePackageStatus(packageId, status);
	};

	render() {
		const { pack, intl } = this.props;
		return (
			<>
				<Button
					className={ 'submit-btn' }
					onClick={ () => this.changePackageStatus(pack.id, packageStatus.RECEIVED) }
					disabled={ pack.status === packageStatus.RECEIVED }
				>
					{ intl.formatMessage({ id: 'packages-check.status.received' }) }
				</Button>
				{ pack.status === packageStatus.RECEIVED &&
				<Button
					onClick={ () => this.changePackageStatus(pack.id, packageStatus.ORDERED) }
					type='link'
				>
					{ intl.formatMessage({ id: 'packages-check.status.not-received' }) }
				</Button>
				}
			</>
		);
	}
}

Status.propTypes = {
	pack: PropTypes.object.isRequired,
	changePackageStatus: PropTypes.func.isRequired
};

export default (injectIntl)(Status);