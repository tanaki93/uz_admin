import React, { Component, Fragment } from 'react';
import * as PropTypes from 'prop-types';
import { Col, PageHeader, Row, Tabs } from 'antd';
import Filter from './Filter';
import List from './List';
import { injectIntl } from 'react-intl';

const { TabPane } = Tabs;

class PackagesCheck extends Component {
	state = {
		data: [],
		type: 1
	};

	UNSAFE_componentWillReceiveProps(nextProps) {
		this.setState({
			data: nextProps.data.filter(item => item.status === 1)
		});
	}

	handleSwitch = (type) => {
		switch (+type) {
			default:
			case 0:
				this.setState({
					data: this.props.data,
					type
				});
				break;
			case 1:
				this.setState({
					data: this.props.data.filter(item => item.status === 1),
					type
				});
				break;
			case 2:
				this.setState({
					data: this.props.data.filter(item => item.status === 2),
					type
				});
				break;
		}
	};

	render() {
		const { fetching, fetchPackages, changePackageStatus, intl } = this.props;
		const { data } = this.state;

		return (
			<Fragment>
				<Row type="flex" gutter={ [16, 16] }>
					<Col span={ 24 }>
						<PageHeader
							ghost={ false }
							title={ intl.formatMessage({ id: 'packages-check.title' }) }
							footer={
								<Tabs size='large' defaultActiveKey='1' onChange={ this.handleSwitch }>
									<TabPane tab="В ожидании" key='1'/>
									<TabPane tab="Полученные" key='2'/>
									<TabPane tab="Все" key='0'/>
								</Tabs>
							}
						>
							<Filter fetchPackages={ fetchPackages }/>
						</PageHeader>
					</Col>
					<Col span={ 24 }>
						<List
							fetching={ fetching }
							data={ data }
							changePackageStatus={ changePackageStatus }
						/>
					</Col>
				</Row>
			</Fragment>
		);
	}
}

PackagesCheck.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	fetchPackages: PropTypes.func.isRequired,
	changePackageStatus: PropTypes.func.isRequired
};

export default (injectIntl)(PackagesCheck);