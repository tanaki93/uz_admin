import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Col, Row, Typography } from 'antd';
import Filter from './Filter';
import List from './List';
import { injectIntl } from 'react-intl';

const { Title } = Typography;


class PackagesCheck extends Component {
	render() {
		const {
			fetching, data, fetchPackages, changePackageStatus, intl
		} = this.props;


		return (
			<>
				<Row type="flex" gutter={ 16 }>
					<Col><Title level={ 3 }>{ intl.formatMessage({ id: 'packages-check.title' }) }</Title></Col>
				</Row>
				<Row gutter={ [16, 24] }>
					<Col span={ 24 }>
						<Filter fetchPackages={ fetchPackages }/>
					</Col>
					<Col span={ 24 }>
						<List
							fetching={ fetching }
							data={ data }
							changePackageStatus={ changePackageStatus }
						/>
					</Col>
				</Row>
			</>
		);
	}
}

PackagesCheck.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	fetchPackages: PropTypes.func.isRequired,
	changePackageStatus: PropTypes.func.isRequired
};

export default (injectIntl)(PackagesCheck);