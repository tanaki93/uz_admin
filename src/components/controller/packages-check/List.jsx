import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Button, Col, Row, Table } from 'antd';
import moment from 'moment';
import * as packageStatus from '../../../constants/packageStatus';
import Status from './Status';
import { injectIntl } from 'react-intl';
import Details from '../../products/Details';

class List extends Component {
	state = {
		visible: false,
		product: {}
	};

	handleOk = e => {
		this.setState({
			visible: false,
		});
	};


	handleCancel = e => {
		this.setState({
			visible: false,
		});
	};

	onClickHandler = (object, index) => {
		this.setState({
			product: object,
			visible: true
		});
	};

	renderPackageStatus = (status, intl) => {
		switch (status) {
			default:
			case packageStatus.WAIT:
				return <span style={ { color: '#F2994A' } }>{ intl.formatMessage({id: 'packages-check.list.stage'}) }</span>;
			case packageStatus.ORDERED:
				return <span style={ { color: '#F2994A' } }>{ intl.formatMessage({id: 'packages-check.status.ordered'}) }</span>;
			case packageStatus.RECEIVED:
				return <span style={ { color: '#219653' } }>{ intl.formatMessage({id: 'packages-check.status.received'}) }</span>;
		}
	};

	selectKeys = (selected) => {
		this.setState({
			selected
		});
	};

	render() {
		const { fetching, data, changePackageStatus, intl } = this.props;
		const { product, visible } = this.state;

		const columns = [
			{
				title: intl.formatMessage({ id: 'packages-check.filter.number' }),
				dataIndex: 'number',
				key: 'package_id'
			},
			{
				title: intl.formatMessage({ id: 'packages-check.list.status' }),
				dataIndex: 'status',
				key: 'package_status',
				render: status => this.renderPackageStatus(status, intl)
			},
			{
				title: intl.formatMessage({ id: 'packages-check.list.created-at' }),
				dataIndex: 'created',
				key: 'package_created_at',
				render: date => moment(date).format('DD.MM.YYYY | HH:mm')
			},
			{
				title: intl.formatMessage({ id: 'packages-check.list.updated-at' }),
				dataIndex: 'updated',
				key: 'package_updated_at',
				render: date => moment(date).format('DD.MM.YYYY | HH:mm')
			},
			{
				title: intl.formatMessage({ id: 'packages-check.list.stage' }),
				key: 'package_stage',
				render: pack => <Status pack={ pack } changePackageStatus={ changePackageStatus }/>
			},
		];

		const productColumns = [
			{
				title: 'Фото',
				dataIndex: ['product', 'images'],
				key: 'image',
				render: images => <img src={ images[0] } alt="" height={ 50 }/>,
				width: 100
			},
			{
				title: 'Продукт',
				dataIndex: ['product', 'title'],
				key: 'title',
				width: 200
			},
			{
				title: 'Кол-во',
				dataIndex: 'amount',
				key: 'amount',
				width: 100
			},
			{
				render: record => <Button
					onClick={ () => this.onClickHandler(record.product) }>{ intl.formatMessage({ id: 'packages.list.details' }) }</Button>
			}
		];


		return (
			<>
				<Table
					loading={ fetching }
					className='izi-table'
					columns={ columns }
					dataSource={ data.sort((a, b) => new Date(b.updated) - new Date(a.updated)) }
					bordered
					size='small'
					rowKey='id'
					expandedRowRender={ (item) =>
						<Row>
							<Col span={ 8 }>
								<Table
									columns={ productColumns }
									rowKey='id'
									dataSource={ item.products }
								/>
							</Col>
						</Row>
					}
				/>
				<Details product={ product } visible={ visible } handleOk={ this.handleOk }
				         handleCancel={ this.handleCancel }/>
			</>
		);
	}
}

List.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	changePackageStatus: PropTypes.func.isRequired
};

export default (injectIntl)(List);