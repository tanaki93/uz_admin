import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Button, Card, Col, Row, Table, Input } from 'antd';
import moment from 'moment';
import { injectIntl } from 'react-intl';

class Submitter extends Component {
	state = {
		weight: 0
	};

	submit = () => {
		const ids = [];
		this.props.selected.map(s => ids.push(s.id));
		this.props.createPackage(ids, this.state.weight);
		this.props.clearSelected();
	};

	onChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};

	render() {
		const { selected, intl } = this.props;
		const { weight } = this.state;

		const columns = [
			{
				title: intl.formatMessage({ id: 'products-check.list.product' }),
				dataIndex: ['product', 'title'],
				key: 'product_name'
			},
			{
				title: intl.formatMessage({ id: 'products-check.list.id' }),
				dataIndex: ['product', 'id'],
				key: 'product_id'
			},
			{
				title: intl.formatMessage({ id: 'packages-check.filter.number' }),
				dataIndex: ['package', 'number'],
				key: 'package_id'
			}
		];

		return (
			<>
				<Card>
					<Row type='flex' justify='space-between'>
						<Col><b>{ intl.formatMessage({ id: 'products-delivery.submitter.created-at' }) }</b></Col>
						<Col>{ moment().format('DD/MM/YYYY | HH:mm') }</Col>
					</Row>
					<br/>
					<Table dataSource={ selected } columns={ columns } size='small' pagination={ false }/>
					<hr/>
					<Row type='flex' justify='space-between'>
						<Col xs={ 12 }>
							<b>{ intl.formatMessage({ id: 'products-delivery.submitter.weight' }) }</b>
						</Col>
						<Col xs={ 12 }>
							<Input name='weight' type='number' value={ this.state.weight }
						                      onChange={ this.onChange }/>
						</Col>
					</Row>
				</Card>
				<Button
					block disabled={ !weight } className={ 'submit-btn' }
					onClick={ this.submit }>{ intl.formatMessage({ id: 'products-delivery.submitter.send' }) }</Button>
			</>
		);
	}
}

Submitter.propTypes = {
	selected: PropTypes.array.isRequired,
	createPackage: PropTypes.func.isRequired,
	clearSelected: PropTypes.func.isRequired,
};

export default (injectIntl)(Submitter);