import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Col, PageHeader, Row } from 'antd';
import Filter from './Filter';
import List from './List';
import { injectIntl } from 'react-intl';

class ProductsCheck extends Component {
	render() {
		const {
			fetching, data, fetchDeliveryProducts, createPackage, intl
		} = this.props;

		return (
			<>
				<Row type="flex" gutter={ [16, 16] }>
					<Col span={ 24 }>
						<PageHeader
							ghost={ false }
							title={ intl.formatMessage({ id: 'products-delivery.title' }) }
						>
							<Filter fetchDeliveryProducts={ fetchDeliveryProducts }/>
						</PageHeader>
					</Col>
					<Col span={ 24 }>
						<List
							fetching={ fetching }
							data={ data }
							createPackage={ createPackage }
						/>
					</Col>
				</Row>
			</>
		);
	}
}

ProductsCheck.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	fetchDeliveryProducts: PropTypes.func.isRequired,
	createPackage: PropTypes.func.isRequired
};

export default (injectIntl)(ProductsCheck);