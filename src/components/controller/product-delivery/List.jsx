import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Icon as LegacyIcon } from '@ant-design/compatible';
import { Button, Table, Col, Row } from 'antd';
import Submitter from './Submitter';
import {injectIntl} from "react-intl";
import * as productDeliveryStatus from 'constants/productDeliveryStatus';

class List extends Component {
	state = {
		selected: []
	};

	clearSelected = () => {
		this.setState({
			selected: []
		});
	};

	isSelected = (item) => {
		return this.state.selected.findIndex(x => x.id === item.id) !== -1;
	};

	selectToggle = (item) => {
		const selected = this.state.selected;
		const index = this.state.selected.findIndex(x => x.id === item.id);

		if (index === -1) {
			selected.push(item);
		} else {
			selected.splice(index, 1);
		}
		this.setState({ selected });
	};

	render() {
		const { selected } = this.state;
		const { fetching, data, createPackage, intl } = this.props;

		const columns = [
			{
				title: intl.formatMessage({ id: 'packages-check.filter.number' }),
				dataIndex: 'stage',
				key: 'package_id'
			},
			{
				title: intl.formatMessage({ id: 'products-check.list.id' }),
				dataIndex: 'product',
				key: 'order_id',
				render: product => product.id
			},
			{
				title: intl.formatMessage({ id: 'products-check.list.product' }),
				dataIndex: 'product',
				key: 'product_name',
				render: product => product.title
			},
			{
				key: 'delivery_button',
				render: item => <Button
					disabled={ item.delivery_status === productDeliveryStatus.SEND }
					icon={ <LegacyIcon type={ 'plus' }/> } onClick={ () => this.selectToggle(item) }
					type={ this.isSelected(item) ? 'primary' : 'default' }
				/>
			}
		];

		return (
			<Row type='flex' gutter={ 8 }>
				<Col span={ 14 }>
					<Table
						loading={ fetching }
						className={ 'izi-table' }
						columns={ columns }
						dataSource={ data }
						bordered
						size="small"
						rowKey={ 'id' }
					/>
				</Col>
				{ selected.length > 0 &&
				<Col span={ 10 }>
					<Submitter selected={ selected } createPackage={ createPackage }
					           clearSelected={ this.clearSelected }/>
				</Col>
				}
			</Row>
		);
	}
}

List.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	createPackage: PropTypes.func.isRequired
};

export default (injectIntl)(List);