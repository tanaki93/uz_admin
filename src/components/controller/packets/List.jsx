import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Button, Table, Radio } from 'antd';
import moment from 'moment';
import Details from './Details';
import * as productReceiveStatus from '../../../constants/productReceiveStatus';
import Flight from './Flight';
import { injectIntl } from 'react-intl';

class List extends Component {
	state = {
		selected: {},
		showDetails: false,
		showComment: false,
		commentType: 1,
	};

	renderProductReceiveStatus = (pack) => {
		const { intl } = this.props;
		const onChange = (e) => {
			this.props.changePacketStatus(pack.id, +e.target.value);
		};

		return (
			<Radio.Group defaultValue={ '' + pack.received_status } onChange={ onChange }>
				<Radio.Button value={ '' + productReceiveStatus.RECEIVED }>
					{ intl.formatMessage({ id: 'packages.list.received' }) }
				</Radio.Button>
				<Radio.Button value={ '' + productReceiveStatus.NOT_RECEIVED }>
					{ intl.formatMessage({ id: 'packages-check.status.not-received' }) }
				</Radio.Button>
			</Radio.Group>
		);
	};

	renderProductDeliveryStatus = (pack, intl) => {
		if (pack.flight === null) {
			return <span
				style={ { color: '#F2994A' } }>{ intl.formatMessage({ id: 'packages.delivery.urgent' }) }</span>;
		} else {
			return <>
				<span style={ { color: '#219653' } }>{ intl.formatMessage({ id: 'packages.delivery.sent' }) }</span>
				<Button type='link'
				        onClick={ () => this.props.createFlight(pack.id, '') }>{ intl.formatMessage({ id: 'packages.delivery.not-sent' }) }</Button>
			</>;
		}
	};

	openDetailsModal = (item) => {
		this.setState({
			showDetails: true,
			selected: item
		});
	};

	closeDetailsModal = () => {
		this.setState({
			showDetails: false,
			selected: {}
		});
	};

	render() {
		const { fetching, data, intl } = this.props;

		const columns = [
			{
				title: intl.formatMessage({ id: 'packages.details.id' }),
				dataIndex: 'id',
				key: 'package_id'
			},
			{
				title: intl.formatMessage({ id: 'products-delivery.submitter.weight' }),
				dataIndex: 'weight',
				key: 'package_weight'
			},
			{
				title: intl.formatMessage({ id: 'packages.list.composition' }),
				key: 'package_contents',
				render: item => <Button type={ 'link' }
				                        onClick={ () => this.openDetailsModal(item) }>{ intl.formatMessage({ id: 'packages.list.details' }) }</Button>
			},
			{
				title: intl.formatMessage({ id: 'packages-check.list.updated-at' }),
				dataIndex: 'updated',
				key: 'package_updated_at',
				render: date => moment(date).format('DD.MM.YYYY | HH:mm')
			},
			{
				title: intl.formatMessage({ id: 'packages.list.receive-status' }),
				key: 'package_received_status',
				render: pack => this.renderProductReceiveStatus(pack, intl)
			},
			{
				title: intl.formatMessage({ id: 'packages.list.flight-number' }),
				key: 'packageFlightNumber',
				render: packet => <Flight packet={ packet } createFlight={ this.props.createFlight }/>
			},
			{
				title: intl.formatMessage({ id: 'packages.list.delivery-status' }),
				key: 'package_delivery_status',
				render: pack => this.renderProductDeliveryStatus(pack, intl)
			},
		];

		const { showDetails, selected } = this.state;

		return (
			<>
				<Table
					loading={ fetching }
					className={ 'izi-table' }
					columns={ columns }
					dataSource={ data.sort((a, b) => (b.updated_at) - (a.updated_at)) }
					bordered
					size="small"
					rowKey={ 'id' }
				/>
				<Details
					visible={ showDetails }
					close={ this.closeDetailsModal }
					selected={ selected }
				/>
			</>

		);
	}
}

List.propTypes = {
	fetching: PropTypes.any.isRequired,
	data: PropTypes.any.isRequired,
	changePacketStatus: PropTypes.any.isRequired,
	createFlight: PropTypes.any.isRequired
};

export default (injectIntl)(List);