import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Col, PageHeader, Row, Tabs, Typography } from 'antd';
import Filter from './Filter';
import List from './List';
import { injectIntl } from 'react-intl';
import * as productStatus from '../../../constants/productCheckStatus';

const { Title } = Typography;

class Packets extends Component {
	render() {
		const {
			fetching, data, fetchPackets, changePacketStatus, createFlight, intl
		} = this.props;
		return (
			<>
				<Row type="flex" gutter={ [16, 16] }>
					<Col span={ 24 }>
						<PageHeader
							ghost={ false }
							title={intl.formatMessage({id: 'packages.title'})}
						>
							<Filter fetchPackets={ fetchPackets }/>
						</PageHeader>
					</Col>
					<Col span={ 24 }>
						<List
							fetching={ fetching }
							data={ data }
							changePacketStatus={ changePacketStatus }
							createFlight={ createFlight }
						/>
					</Col>
				</Row>
			</>
		);
	}
}

Packets.propTypes = {
	fetching: PropTypes.any.isRequired,
	data: PropTypes.any.isRequired,
	fetchPackets: PropTypes.any.isRequired,
	changePacketStatus: PropTypes.any.isRequired,
	createFlight: PropTypes.any.isRequired
};

export default (injectIntl)(Packets);