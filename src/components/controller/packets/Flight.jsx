import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Button, Input } from 'antd';
import * as productReceiveStatus from '../../../constants/productReceiveStatus';

class Flight extends Component {
	state = {
		number: '',
		money: ''
	};

	componentWillReceiveProps(nextProps, nextContext) {
		this.setState({
			number: nextProps.packet.flight?.number || '',
			money: nextProps.packet.money || ''
		});
	}

	componentDidMount() {
		if (this.props.packet.flight) {
			this.setState({
				number: this.props.packet.flight.number,
				money: this.props.packet.money
			});
		}
	}

	onChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};

	submit = () => {
		this.props.createFlight(this.props.packet.id, this.state.number, this.state.money);
	};

	render() {
		const { packet } = this.props;
		const { number, money } = this.state;

		return (
			<Input.Group compact>
				<Input style={{ width: 100 }} value={ money } name='money' onChange={ this.onChange } placeholder='Цена'
				       disabled={ packet.received_status !== productReceiveStatus.RECEIVED || this.props.packet.flight}/>
				<Input style={{ width: 300 }} value={ number } name='number' onChange={ this.onChange } placeholder='Номер рейса'
				       disabled={ packet.received_status !== productReceiveStatus.RECEIVED || this.props.packet.flight}/>
				<Button onClick={ this.submit } className={ 'submit-btn' }
				        disabled={ packet.received_status !== productReceiveStatus.RECEIVED || this.props.packet.flight }>Отправить</Button>
			</Input.Group>
		);
	}
}

Flight.propTypes = {
	packet: PropTypes.object,
	createFlight: PropTypes.func.isRequired
};

export default Flight;