import React, { Component } from 'react';
import { Col, Modal, Row, Table } from 'antd';
import * as PropTypes from 'prop-types';
import moment from 'moment';
import { injectIntl } from 'react-intl';

class Details extends Component {
	render() {
		const { visible, close, selected, intl } = this.props;

		const columns = [
			{
				title: intl.formatMessage({ id: 'products-check.list.product' }),
				dataIndex: 'product',
				key: 'product_name',
				render: item => item.title
			},
			{
				title: intl.formatMessage({ id: 'packages-check.filter.number' }),
				dataIndex: 'package',
				key: 'package_id',
				render: item => item.id
			},
			{
				title: intl.formatMessage({ id: 'products-check.list.id' }),
				dataIndex: 'product',
				key: 'order_id',
				render: item => item.id
			},
			{
				title: intl.formatMessage({ id: 'products-delivery.submitter.price' }),
				dataIndex: 'product',
				key: 'product.shipping_price',
				render: item => item.shipping_price
			},
			{
				title: intl.formatMessage({ id: 'products-delivery.submitter.weight' }),
				dataIndex: 'weight'
			}
		];

		return (
			<Modal
				destroyOnClose
				visible={ visible }
				footer={ false }
				onCancel={ close }
				width={ '700px' }
			>
				<Row type='flex' justify='space-between'>
					<Col><b>{ intl.formatMessage({ id: 'packages.details.id' }) }</b></Col>
					<Col>{ selected.id }</Col>
				</Row>
				<Row type='flex' justify='space-between'>
					<Col><b>{ intl.formatMessage({ id: 'products-delivery.submitter.created-at' }) }</b></Col>
					<Col>{ moment(selected.created).format('DD/MM/YYYY | HH:mm') }</Col>
				</Row>
				<br/>
				<Table dataSource={ selected.products } columns={ columns } size='small' pagination={ false }/>
				<hr/>
				<Row type='flex' justify='space-between'>
					<Col><b>{ intl.formatMessage({ id: 'packages.details.weight' }) }</b></Col>
					<Col>{ selected.total_weight }</Col>
				</Row>
				<Row type='flex' justify='space-between'>
					<Col><b>{ intl.formatMessage({ id: 'packages.details.price' }) }</b></Col>
					<Col>{ selected.total_price }</Col>
				</Row>
			</Modal>
		);
	}
}

Details.propTypes = {
	visible: PropTypes.any.isRequired,
	close: PropTypes.any.isRequired,
	selected: PropTypes.object.isRequired
};

export default (injectIntl)(Details);