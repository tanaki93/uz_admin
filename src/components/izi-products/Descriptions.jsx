import React, { Fragment } from 'react';
import * as PropTypes from "prop-types";
import {Carousel, Checkbox, Input, Modal, Row, Table} from 'antd';

class Descriptions extends React.Component {
	state = {
		descriptions: []
	};

	handleOk = e => {
		const { descriptions } = this.state;
		const data = [];
		descriptions.map(description => data.push({
			lang_id: description.lang_id,
			translation: description.description,
		}));
		this.props.editDescription(this.props.product, data)
		this.props.handleCancel()
	};

	handleCancel = e => {
		this.props.handleCancel()
	};


	componentWillReceiveProps(nextProps) {
		const data = [];

		nextProps.descriptions.map(description => data.push({
			lang_id: description.lang_id,
			lang_name: description.lang_name,
			lang_code: description.lang_code,
			description: description.description_text,
		}));

		this.setState({
			descriptions: data
		})
	}
	onTableChange = (id, e) => {
		const descriptions = this.state.descriptions;
		descriptions.map(description => description.lang_id === id && (description[e.target.name] = e.target.value));
		this.setState({ descriptions })
	};
	render() {
		const { descriptions } = this.state;
		const columns = [
			{
				title: 'Язык',
				dataIndex: 'lang_code'
			},
			{
				title: 'Описание',
				render: description => <Input.TextArea value={ description.description } name='description'
										   onChange={ e => this.onTableChange(description.lang_id, e) }/>
			},
		];
		return (
			<Modal
				width={ '600px' }
				title={ 'Описания' }
				visible={ this.props.visible }
				onOk={ this.handleOk }
				onCancel={ this.handleCancel }
			>
				<Table className={ 'izi-table' } columns={ columns } dataSource={ descriptions } rowKey='lang_id'
					   pagination={ false } size={ 'small' }/>

			</Modal>
		)
	}
}

Descriptions.propTypes = {
	descriptions: PropTypes.array.isRequired,
	product: PropTypes.number.isRequired,
	visible: PropTypes.bool.isRequired,
	handleOk: PropTypes.func.isRequired,
	handleCancel: PropTypes.func.isRequired,
	editDescription: PropTypes.func.isRequired,
};

export default (Descriptions)