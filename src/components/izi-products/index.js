import React, { useEffect, useRef, useState } from 'react';
import * as PropTypes from 'prop-types';
import {Table, Checkbox, PageHeader, Form, Button} from 'antd';
import Filter from './Filter';
import Selector from './Selector';
import Details from './Details';
import { useIntl } from 'react-intl';
import EditOutlined from '@ant-design/icons/lib/icons/EditOutlined';
import Descriptions from "./Descriptions";
const IziProducts = (props) => {
	const intl = useIntl();
	const wrapper = useRef(null);
	const [visible, setVisible] = useState(false);
	const [show, setShow] = useState(false);
	const [product, setProduct] = useState({});
	const [descriptions, setDescriptions] = useState([]);
	const [products, setProducts] = useState([]);
	const {
		count, current, fetching, hierarchy, fetchingDepartments, fetchingContents, departments, fetchingColors, colors, contents,
		fetchingBrands, brands, fetchProducts, editProduct, activateProduct, hitProduct, editDescription
	} = props;

	useEffect(() => {
		setProducts([...props.products]);
	}, [props]);

	const handleOk = e => {
		setVisible(false);
		setShow(false);
	};
	const onClickDescriptionHandler = (product) => {
		setShow(true);
		setProduct(product);
		setDescriptions(product.descriptions);
	};

	const onClickHandler = (product) => {
		setVisible(true);
		setProduct(product);
	};

	const handleCancel = e => {
		setVisible(false);
		setShow(false);
	};

	const processProduct = (id, option, value) => {
		editProduct(id, option, value);
	};

	const check = (e) => {
		activateProduct(e.target.id, e.target.checked);
	};

	const hit = (e) => {
		hitProduct(e.target.id, e.target.checked);
	};
	function getDescriptions(record){
		var items = []
		for (const [index, value] of record.descriptions.entries()) {
			if (value.description_text !== ''){
				items.push(<span key={index} style={{marginLeft: 4}}>{value.lang_code}</span>)
			}
		}
		if (items.length === 0) {
			items.push(<span style={{marginLeft: 4}}>{'empty'}</span>)
		}
		return items;
	}
	const columns = [
		{
			render: item => <Checkbox checked={ item.is_sellable } onClick={ check }/>,
			width: 40,
		},
		{
			title: 'Картинка',
			render: product => product.images &&
				<img alt={ product.title } src={ product.images[0] } style={ { width: 50, height: 70 } }/>,
			width: 80
		},
		{
			title: intl.formatMessage({ id: 'products-check.details.brand' }),
			dataIndex: ['brand', 'name'],
			width: 100
		},
		{
			title: 'Название',
			dataIndex: 'title',
			width: 100
		},
		{
			title: 'Отделение',
			editable: true,
			dataIndex: ['department', 'name']
		},

		{
			title: 'Категория',
			editable: true,
			dataIndex: ['category', 'name']
		},
		{
			title: 'Цвета',
			editable: true,
			dataIndex: ['colour', 'name']
		},
		{
			title: intl.formatMessage({ id: 'products-check.details.composition' }),
			editable: true,
			dataIndex: ['content', 'name']
		},
		{
			title: 'Цена продажи',
			dataIndex: 'selling_price',
			width: 100
		},
		{
			title: 'Цена Hurrem',
			dataIndex: 'izi_price',
			width: 100
		},
		{
			title: 'Ссылка',
			dataIndex: 'link',
			render: link => <a href={ link.url } target={ '_blank' }>Ссылка</a>,
			width: 100
		},
		{
			title: 'Ссылка Hurrem',
			dataIndex: 'izi_link',
			key: 'izi_link',
			width: 100,
			render: link => <a href={ link } target={ '_blank' }>Ссылка</a>
		},
		{
			title: 'Хит',
			render: item => <Checkbox id={ item.id } checked={ item.is_hit } onClick={ hit }/>,
			width: 40,
		},
		{
			title: 'Описания',
			render: record => <Button type={ 'default' } onClick={ () => onClickDescriptionHandler(record) }>{getDescriptions(record)}</Button>,
			width: 100
		},
		{
			title: intl.formatMessage({ id: 'packages.list.details' }),
			render: record => <a onClick={ () => onClickHandler(record) }>
				{ intl.formatMessage({ id: 'packages.list.details' }) }
			</a>,
			width: 100
		},
	];

	const EditableCell = ({ title, editable, children, dataIndex, record, handleSave, ...restProps }) => {
		const [editing, setEditing] = useState(false);

		const toggleEdit = () => {
			setEditing(!editing);
		};

		const save = async (value) => {
			try {
				toggleEdit();
				processProduct(record.id, dataIndex[0], value);
			} catch (errInfo) {
				console.log('Save failed:', errInfo);
			}
		};

		const blur = async => {
			toggleEdit();
		};

		let data = [];

		switch (dataIndex ? dataIndex[0] : '') {
			default:
			case 'department':
				data = departments;
				break;
			case 'category':
				data = departments.find(item => item.id === record.department.id)?.categories;
				break;
			case 'colour':
				data = colors;
				break;
			case 'content':
				data = contents;
				break;
		}

		let childNode = children;

		if (editable) {
			childNode = !fetchingDepartments && editing ? (
				<Form initialValues={ { [dataIndex[0]]: record[dataIndex[0]]?.id } }>
					<Form.Item style={ { margin: 0, } } name={ dataIndex[0] }
					           rules={ [{ required: true, message: `${ title } is required.` }] }>
						<Selector
							disabled={ fetchingDepartments || fetchingContents }
							type={ dataIndex[0] }
							data={ data }
							record={ record }
							onChange={ save }
							onBlur={ blur }
						/>
					</Form.Item>
				</Form>
			) : (
				<div className='editable-cell-value-wrap' style={ { paddingRight: 24 } } onClick={ toggleEdit }>
					{ children } <EditOutlined style={ { color: 'red' } }/>
				</div>
			);
		}

		return <td { ...restProps }>{ childNode }</td>;
	};

	const components = {
		body: {
			cell: EditableCell,
		}
	};

	const _columns = columns.map(col => {
		if (!col.editable) {
			return col;
		}

		return {
			...col,
			onCell: record => ({
				record,
				editable: col.editable,
				dataIndex: col.dataIndex,
				title: col.title,
				handleSave: processProduct,
			}),
		};
	});

	return (
		<>
			<PageHeader
				title='Товары IZI'
				ghost={ false }
				footer={
					<Filter
						fetchingBrands={ fetchingBrands }
						brands={ brands }
						fetchingDepartments={ fetchingDepartments }
						departments={ departments }
						fetchingColors={ fetchingColors }
						colors={ colors }
						fetching={ fetching }
						hierarchy={ hierarchy }
						fetchProducts={ fetchProducts }
						count={ count }
						current={ current }
					/>
				}
			/>
			<div className='page-wrapper page-wrapper--xlarge' ref={ wrapper }>
				<Table
					components={ components }
					scroll={ { y: wrapper.current?.clientHeight - 114 } }
					loading={ fetching }
					className={ 'izi-table' }
					pagination={ false }
					columns={ _columns }
					dataSource={ products }
					size='small'
					rowKey={ 'id' }
				/>
			</div>
			<Details product={ product } visible={ visible } handleOk={ handleOk } handleCancel={ handleCancel }/>
			<Descriptions descriptions={ descriptions } product={product.id} visible={ show } handleOk={ handleOk } handleCancel={ handleCancel }
						  editDescription={editDescription}/>
		</>
	);
};

IziProducts.propTypes = {
	fetching: PropTypes.bool.isRequired,
	fetchProducts: PropTypes.func.isRequired,
	hitProduct: PropTypes.func.isRequired,
	products: PropTypes.array.isRequired,
	fetchingDepartments: PropTypes.bool.isRequired,
	contents: PropTypes.array.isRequired,
	fetchingContents: PropTypes.bool.isRequired,
	departments: PropTypes.array.isRequired,
	fetchingColors: PropTypes.bool.isRequired,
	fetchingBrands: PropTypes.bool.isRequired,
	brands: PropTypes.array.isRequired,
	editProduct: PropTypes.func.isRequired,
	editDescription: PropTypes.func.isRequired,
	activateProduct: PropTypes.func.isRequired,
	colors: PropTypes.array.isRequired,
	count: PropTypes.number.isRequired,
	pages: PropTypes.number.isRequired,
	current: PropTypes.number.isRequired,
	hierarchy: PropTypes.object.isRequired
};

export default IziProducts;