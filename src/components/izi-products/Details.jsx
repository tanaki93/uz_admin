import React from 'react';
import * as PropTypes from 'prop-types';
import { Carousel, Checkbox, Col, Modal, Row } from 'antd';
import moment from 'moment';
import { injectIntl } from 'react-intl';

class Details extends React.Component {
	render() {
		const { product, intl } = this.props;
		return (
			<Modal
				width={ '1200px' }
				title={ product.title }
				visible={ this.props.visible }
				onOk={ this.props.handleOk }
				onCancel={ this.props.handleCancel }
				footer={ false }
			>
				{
					product.id &&
					<>
						<Row gutter={ 16 }>
							<Col span={ 5 }>
								<Carousel autoplay>
									{ product.images.map(item => (
										<img src={ item } alt={ '' }/>
									)) }
								</Carousel>
							</Col>
							<Col span={ 19 }>
								<Row>
									<h3>Название:</h3>
									<Col span={ 12 }>
										<p>{ product.title }</p>
									</Col>
									<Col span={ 12 }>
										{ product.title }
									</Col>
								</Row>
								<Row>
									<h3>Описание:</h3>
									<Col span={ 12 }>
										<div dangerouslySetInnerHTML={ { __html: product.description } }/>
									</Col>
									<Col span={ 12 }>
										{ product.description }
									</Col>
								</Row>
							</Col>
						</Row>
						<br/>
						<Row gutter={ 16 }>
							<Col span={ 12 }>
								<Row gutter={ 16 }>
									<Col span={ 12 }>
										<h3>{ intl.formatMessage({ id: 'products-delivery.submitter.price' }) }</h3>
									</Col>
									<Col span={ 12 }>
										<h3>Цена IZI</h3>
									</Col>
									<Col span={ 12 }>
										<p>Оригинальная: <b>{ product.original_price } TL</b></p>
									</Col>
									<Col span={ 12 }>
										<p>{ product.original_price }</p>
									</Col>
									<Col span={ 12 }>
										<p>Цена продажи: <b>{ product.selling_price } TL</b></p>
									</Col>
									<Col span={ 12 }>
										<p>{ product.selling_price }</p>
									</Col>
									<Col span={ 12 }>
										<p>Цена со скидкой: <b>{ product.discount_price } TL</b></p>
									</Col>
									<Col span={ 12 }>
										<p>{ product.discount_price }</p>
									</Col>
								</Row>

							</Col>

							<Col span={ 6 }>
								<Row gutter={ 16 }>
									<Col span={ 24 }>
										<h3>Атрибуты</h3>
									</Col>
									<Col span={ 24 }>
										<p>
											<b>Отделение:</b>{ product.department ? product.department.name : intl.formatMessage({ id: 'products-check.selector.not-selected' }) }
										</p>
									</Col>
									<Col span={ 24 }>
										<p>
											<b>Категория:</b>{ product.category ? product.category.name : intl.formatMessage({ id: 'products-check.selector.not-selected' }) }
										</p>
									</Col>
									<Col span={ 24 }>
										<p>
											<b>{ intl.formatMessage({ id: 'products-check.details.color' }) }:</b> { product.colour ? product.colour.name : intl.formatMessage({ id: 'products-check.selector.not-selected' }) }
										</p>
									</Col>
									<Col span={ 24 }>
										<p>
											<b>{ intl.formatMessage({ id: 'products-check.details.composition' }) }:</b> { product.content ? product.content.name : intl.formatMessage({ id: 'products-check.selector.not-selected' }) }
										</p>
									</Col>
								</Row>
							</Col>
							<Col span={ 6 }>
								<Col span={ 24 }>
									<h3>Доставка</h3>
								</Col>
								<Col span={ 24 }>
									<p><b>Дата доставки:</b> { product.delivery_date }</p>
								</Col>
								<Col span={ 24 }>
									<p><Checkbox checked={ product.is_rush_delivery }/> <b>Быстрая доставка</b>
									</p>
								</Col>
								<Col span={ 24 }>
									<p><Checkbox checked={ product.is_free_argo }/> <b>Бесплатная доставка</b>
									</p>
								</Col>
							</Col>
						</Row>
						<br/>
						<Row type={ 'flex' } align={ 'middle' } justify={ 'space-between' }>
							<Col>
								<Row type={ 'flex' } gutter={ 16 }>
									<Col>
										<a target={ '_blank' } href={ product.izi_link }>Ссылка на товар</a>
									</Col>
									<Col>
										<b>Создан:</b> { moment(product.created_at).format('DD MMM YYYY, HH:mm') }
									</Col>
									<Col>
										<b>Обновлен:</b> { moment(product.updated_at).format('DD MMM YYYY, HH:mm') }
									</Col>
								</Row>
							</Col>
						</Row>
					</>
				}
			</Modal>
		);
	}
}

Details.propTypes = {
	product: PropTypes.object.isRequired,
	visible: PropTypes.bool.isRequired,
	handleOk: PropTypes.func.isRequired,
	handleCancel: PropTypes.func.isRequired,
};

export default (injectIntl)(Details);