import React, { useEffect } from 'react';
import * as PropTypes from 'prop-types';
import * as authStatus from 'constants/auth';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Form, Input, Button, Card, message } from 'antd';

const Auth = (props) => {
	const { auth, fetching, status } = props;

	useEffect(() => {
		status === authStatus.INTERNAL_ERROR && error();
	}, [status]);

	const handleSubmit = values => {
		auth(values);
	};

	const error = () => {
		message.error('Логин или пароль неверны');
	};

	return (
		<Card className="login-form">
			<Form onFinish={ handleSubmit }>
				<Form.Item
					name='username'
					rules={ [{ required: true, message: 'Введите имя пользователя' }] }
				>
					<Input
						prefix={ <UserOutlined style={ { color: 'rgba(0,0,0,.25)' } }/> }
						placeholder="Имя пользователя"
					/>
				</Form.Item>
				<Form.Item
					name='password'
					rules={ [{ required: true, message: 'Введите пароль' }] }
				>
					<Input
						prefix={ <LockOutlined style={ { color: 'rgba(0,0,0,.25)' } }/> }
						type="password"
						placeholder="Пароль"
					/>
				</Form.Item>
				<Button type="primary" htmlType="submit" className="login-form-button" block
				        loading={ fetching }>
					Войти
				</Button>
			</Form>
		</Card>
	);
};

Auth.propTypes = {
	auth: PropTypes.func.isRequired,
	fetching: PropTypes.bool.isRequired,
	status: PropTypes.number.isRequired
};

export default Auth;