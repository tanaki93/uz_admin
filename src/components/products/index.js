import '../../assets/styles/slider.css';
import React from 'react';
import * as PropTypes from 'prop-types';
import { Typography, Table, Checkbox, Row, Col, Button } from 'antd';
import moment from 'moment';
import Filter from './Filter';
import Details from './Details';
import { injectIntl } from 'react-intl';

const { Title } = Typography;


class Index extends React.Component {
	state = {
		products: [],
		visible: false,
		product: {},
	};

	UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
		this.setState({
			products: nextProps.products,
		});
	}

	componentWillUnmount() {
		this.setState({
			products: [],
		});
	}

	handleOk = e => {
		this.setState({
			visible: false,
		});
	};

	onChange = (page) => {
		this.props.fetchProducts(page);
	};

	onClickHandler = (object, index) => {
		this.setState({
			product: object,
			visible: true
		});
	};

	handleCancel = e => {
		this.setState({
			visible: false,
		});
	};

	getPrice = price => {
		return parseInt((price * 15) + (price / 100 * 10));
	};

	check = (selected, e) => {
		this.props.activateProduct(selected.id, e.target.checked);
	};

	render() {
		const { intl } = this.props;
		const columns = [
			{
				title: 'Актив',
				render: item => <Checkbox checked={ item.is_active } onClick={ e => this.check(item, e) }/>,
				width: 100,
			},
			{
				title: 'ID',
				dataIndex: 'id',
				width: 100,
			},
			{
				title: 'Картинка',
				dataIndex: 'images',
				render: images => <img src={ images[0] } style={ { width: 50, height: 70 } } alt={ '' }/>
			},
			{
				title: 'Название',
				dataIndex: 'title',
			},
			{
				title: 'Ссылка',
				dataIndex: 'link',
				render: link => <a href={ link.url }>Ссылка</a>
			},
			{
				title: intl.formatMessage({ id: 'packages-check.list.updated-at' }),
				dataIndex: 'updated_at',
				render: updatedAt => moment(updatedAt).format('DD MMM YYYY, HH:mm')
			},
			{
				title: intl.formatMessage({ id: 'packages.list.details' }),
				render: record => <Button
					onClick={ () => this.onClickHandler(record) }>{ intl.formatMessage({ id: 'packages.list.details' }) }</Button>
			},
		];

		const { count, current, fetching, hierarchy, fetchingBrands, brands, fetchingColors, colors } = this.props;
		const { product, visible, products } = this.state;

		return (
			<>
				<Row type="flex" justify="space-between">
					<Col><Title level={ 3 }>Товары поставщиков</Title></Col>
				</Row>
				<Filter
					fetchingBrands={ fetchingBrands }
					brands={ brands }
					fetchingColors={ fetchingColors }
					colors={ colors }
					fetching={ fetching }
					hierarchy={ hierarchy }
					fetchProducts={ this.props.fetchProducts }
					count={ count }
					current={ current }
				/>
				<Row type="flex" justify="space-between">
					<Col span={ 24 }>
						<Table
							loading={ fetching }
							className={ 'izi-table' }
							scroll={ { y: 620 } }
							pagination={ false }
							columns={ columns }
							dataSource={ products }
							size='small'
							rowKey='id'
						/>
					</Col>
					<Details product={ product } visible={ visible } handleOk={ this.handleOk }
					         handleCancel={ this.handleCancel }/>
				</Row>
			</>
		);
	}
}

Index.propTypes = {
	fetching: PropTypes.bool.isRequired,
	fetchProducts: PropTypes.func.isRequired,
	products: PropTypes.array.isRequired,
	fetchingColors: PropTypes.bool.isRequired,
	activateProduct: PropTypes.func.isRequired,
	colors: PropTypes.array.isRequired,
	count: PropTypes.number.isRequired,
	pages: PropTypes.number.isRequired,
	current: PropTypes.number.isRequired,
	hierarchy: PropTypes.object.isRequired
};

export default (injectIntl)(Index);