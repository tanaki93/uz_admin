import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Icon as LegacyIcon } from '@ant-design/compatible';
import { Button, Col, Input, Pagination, Row, Select } from 'antd';
import { injectIntl } from 'react-intl';

class Filter extends Component {
	state = {
		query: '',
		brandId: '',
		departmentId: '',
		categoryId: '',
		colourId: '',
		current: 1
	};

	onSelect = (name, value) => {
		name === 'brandId' && this.setState({ departmentId: '', categoryId: '' });
		name === 'departmentId' && this.setState({ categoryId: '' });
		this.setState({
			[name]: value
		});
	};

	getCategories = (data, id) => {
		if (data.find(entry => entry.id === id)) {
			return data.find(entry => entry.id === id) && data.find(entry => entry.id === id).categories;
		}
		return [];
	};

	onClear = () => {
		this.setState({
			brandId: '',
			departmentId: '',
			categoryId: '',
			colourId: '',
			query: '',
			current: 1
		});
		this.props.fetchProducts('', '', '', '', '', 1);
	};

	onPageChange = (page) => {
		const { selectedDepartmentId, selectedCategoryId, selectedColourId, query } = this.state;
		this.setState({
			current: page,
		}, () => this.props.fetchProducts(
			this.props.documentId,
			query,
			selectedDepartmentId,
			selectedCategoryId,
			selectedColourId,
			this.state.current
		));
	};

	onChangeSearch = e => {
		this.setState({
			query: e.target.value
		});
	};

	onFilterSubmit = () => {
		const { brandId, departmentId, categoryId, colourId, query } = this.state;
		this.setState({
			current: 1
		});

		this.props.fetchProducts(
			brandId, departmentId, categoryId, colourId, query, 1
		);
	};

	render() {
		const {
			fetchingBrands, brands, fetchingColors, colors, intl
		} = this.props;

		const { brandId, departmentId, categoryId, colourId, query } = this.state;

		const brandIsEmpty = brandId === '' || brandId === 0;
		const departmentIsEmpty = departmentId === '' || departmentId === 0;

		return (
			<div className='documents-filter'>
				<Row type="flex" align='bottom' gutter={ 16 }>
					<Col span={ 18 }>
						<Row type="flex" gutter={ 16 }>
							<Col span={ 8 }>
								Поиск:
								<Input.Search name={ 'query' } value={ query } onChange={ this.onChangeSearch }/>
							</Col>
							<Col span={ 4 }>
								{ intl.formatMessage({ id: 'products-check.details.brand' }) }:
								<Select
									loading={ fetchingBrands }
									style={ { width: '100%' } }
									onChange={ value => this.onSelect('brandId', value) }
									value={ brandId }>
									<Select.Option value={ '' }>
										Все
									</Select.Option>
									<Select.Option value={ 0 }>
										{ intl.formatMessage({ id: 'products-check.selector.not-selected' }) }
									</Select.Option>
									{ brands.map(item =>
										<Select.Option key={ item.id } value={ item.id }>
											{ item.name }
										</Select.Option>
									) }
								</Select>
							</Col>
							<Col span={ 4 }>
								Отделение:
								<Select
									disabled={ brandIsEmpty }
									style={ { width: '100%' } }
									onChange={ value => this.onSelect('departmentId', value) }
									value={ departmentId }>
									<Select.Option value={ '' }>
										Все
									</Select.Option>
									<Select.Option value={ 0 }>
										{ intl.formatMessage({ id: 'products-check.selector.not-selected' }) }
									</Select.Option>
									{ brandId && brands.find(x => x.id === brandId).departments.map(item =>
										<Select.Option key={ item.id } value={ item.id }>
											{ item.name }
										</Select.Option>
									) }
								</Select>
							</Col>
							<Col span={ 4 }>
								Категория:
								<Select
									disabled={ brandIsEmpty || departmentIsEmpty }
									style={ { width: '100%' } }
									onChange={ value => this.onSelect('categoryId', value) }
									value={ categoryId }>
									<Select.Option value={ '' }>
										Все
									</Select.Option>
									<Select.Option value={ 0 }>
										{ intl.formatMessage({ id: 'products-check.selector.not-selected' }) }
									</Select.Option>
									{ brandId && departmentId && brands.find(x => x.id === brandId).departments.find(x => x.id === departmentId).categories.map(item =>
										<Select.Option key={ item.id } value={ item.id }>
											{ item.name }
										</Select.Option>
									) }
								</Select>
							</Col>
							<Col span={ 4 }>
								{ intl.formatMessage({ id: 'products-check.details.color' }) }:
								<Select
									loading={ fetchingColors }
									style={ { width: '100%' } }
									onChange={ value => this.onSelect('colourId', value) }
									value={ colourId }>
									<Select.Option value={ '' }>
										Все
									</Select.Option>
									<Select.Option value={ 0 }>
										{ intl.formatMessage({ id: 'products-check.selector.not-selected' }) }
									</Select.Option>
									{ colors.map(item =>
										<Select.Option key={ item.id } value={ item.id }>
											{ item.name }
										</Select.Option>
									) }
								</Select>
							</Col>
						</Row>
					</Col>
					<Col span={ 6 }>
						<Row type="flex" justify='end' gutter={ 16 }>
							<Button type={ 'primary' } onClick={ this.onFilterSubmit } disabled={ this.props.fetching }>
								{ intl.formatMessage({ id: 'packages-check.filter.filter' }) }
							</Button>
							&nbsp;
							<Button type={ 'default' } onClick={ this.onClear } icon={ <LegacyIcon type={ 'minus' }/> }
							        disabled={ this.props.fetching }>
								Очистить фильтр
							</Button>
						</Row>
					</Col>
				</Row>
				<br/>
				<Row type={ 'flex' } align='middle' justify={ 'space-between' }>
					<Col>
						<Pagination current={ this.state.current } pageSize={ 200 } total={ this.props.count }
						            onChange={ this.onPageChange } disabled={ this.props.fetching }/>
					</Col>
					<Col>
						Всего в списке: { this.props.fetching ? 'Загрузка...' : this.props.count }
					</Col>
				</Row>
			</div>
		);
	}
}

Filter.propTypes = {
	fetching: PropTypes.bool.isRequired,
	fetchProducts: PropTypes.func.isRequired,
	count: PropTypes.number.isRequired,
	fetchingBrands: PropTypes.bool.isRequired,
	brands: PropTypes.array.isRequired,
	fetchingColors: PropTypes.bool.isRequired,
	colors: PropTypes.array.isRequired,
};

export default (injectIntl)(Filter);