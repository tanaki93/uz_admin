import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Col, Row, Table } from 'antd';
import Submitter from './Submitter';
import { injectIntl } from 'react-intl';

class List extends Component {
	state = {
		selected: []
	};

	render() {
		const { fetching, data, intl } = this.props;

		const columns = [
			{
				title: 'Название',
				dataIndex: 'product',
				key: 'product_name',
				width: 200,
				render: item => item.title
			},
			{
				title: intl.formatMessage({ id: 'products-check.list.id' }),
				dataIndex: 'id',
				key: 'product_order_id',
			},
			{
				title: 'Кол-во',
				dataIndex: 'amount',
				key: 'product_count',
			},
			{
				title: 'Ссылка IZI',
				dataIndex: 'izi_link',
				key: 'izi_link',
				render: link => <a href={ link } target={ '_blank' }>Ссылка</a>
			},
			{
				title: 'Ссылка Vend',
				dataIndex: 'product',
				key: 'product_vend_link',
				render: product => <a href={ product.link.url } target={ '_blank' }>Ссылка</a>
			},
			{
				title: 'Цена дост.',
				dataIndex: 'shipping_price',
				key: 'shipping_price',
			},
			{
				title: 'Цена тов.',
				key: 'price',
				render: item => item.price / item.amount
			},
			{
				title: 'Сумма',
				key: 'product_sum',
				render: item => item.price + item.shipping_price
			}
		];

		const rowSelection = {
			onChange: (selectedRowKeys, selectedRows) => {
				this.setState({
					selected: selectedRows
				});
			}
		};

		return (
			<Row gutter={ [16, 24] }>
				<Col lg={ 12 }>
					<Table
						rowSelection={ rowSelection }
						pagination={ false }
						loading={ fetching }
						columns={ columns }
						dataSource={ data.products }
						bordered
						size="small"
						rowKey={ 'id' }
					/>
				</Col>
				<Col lg={ 12 }>
					{
						this.state.selected.length > 0 &&
						<Submitter orderId={ data.id } refund={ this.props.refund } selected={ this.state.selected }/>
					}
				</Col>
			</Row>
		);
	}
}

List.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	refund: PropTypes.func.isRequired
};

export default (injectIntl)(List);