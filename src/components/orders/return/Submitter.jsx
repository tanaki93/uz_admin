import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Button, Card, Col, Row, Table } from 'antd';
import moment from 'moment';
import { injectIntl } from 'react-intl';

class Submitter extends Component {
	state = {
		price: 0
	};

	componentDidMount() {
		const { selected } = this.props;
		let price = 0;
		selected.map(item => price += item.price);
		let shipping = 0;
		selected.map(item => shipping += item.shipping_price);
		this.setState({
			price: price + shipping
		});
	}

	submit = () => {
		this.props.refund({
			order_id: this.props.orderId,
			price: this.state.price,
			order_items: this.props.selected.map(item => item.id)
		});
	};

	onChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};

	render() {
		const { selected, intl } = this.props;
		let price = 0;
		selected.map(item => price += item.price);
		let shipping = 0;
		selected.map(item => shipping += item.shipping_price);

		const columns = [
			{
				title: intl.formatMessage({ id: 'products-check.list.product' }),
				dataIndex: 'product',
				key: 'product_name',
				render: item => item.title
			},
			{
				title: intl.formatMessage({ id: 'products-check.list.id' }),
				dataIndex: 'product',
				key: 'product_id',
				render: item => item.id
			},
			{
				title: 'Цена дост.',
				dataIndex: 'shipping_price',
				key: 'shipping_price',
			},
			{
				title: 'Цена тов.',
				key: 'price',
				render: item => item.price / item.amount
			},
		];

		return (
			<>
				<Card>
					<Row type='flex' justify='space-between'>
						<Col><b>{ intl.formatMessage({ id: 'products-delivery.submitter.created-at' }) }</b></Col>
						<Col>{ moment().format('DD/MM/YYYY | HH:mm') }</Col>
					</Row>
					<br/>
					<Table dataSource={ selected } columns={ columns } rowKey={ 'id' } size='small'
					       pagination={ false }/>
					<hr/>
					<Row type='flex' justify='space-between'>
						<Col/>
						<Col/>
						<Col>
							<b>Итого к возврату</b>
						</Col>
						<Col><b>{ price + shipping }</b></Col>
					</Row>
				</Card>
				<Button type={ 'primary' } block onClick={ this.submit }>Оформить возврат</Button>
			</>
		);
	}
}

Submitter.propTypes = {
	selected: PropTypes.array.isRequired,
	orderId: PropTypes.number.isRequired,
	refund: PropTypes.func.isRequired
};

export default (injectIntl)(Submitter);