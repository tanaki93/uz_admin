import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Button, Col, Row, Typography } from 'antd';
import List from './List';
import { Link } from 'react-router-dom';
import moment from 'moment';

const { Title } = Typography;

class OrderDetails extends Component {
	render() {
		const { fetching, data } = this.props;

		return (
			<>
				<Row type="flex" align='middle' gutter={ 16 }>
					<Col><Title level={ 3 }>Оформление возврата</Title></Col>
					<Col span={ 12 }>
						<Link to={ `/orders/${ data.id }` }>
							<Button size='small' type={ 'primary' }>Отмена</Button>
						</Link>
					</Col>
				</Row>
				<br/>
				<Row>
					<Col>
						<Row type="flex" gutter={ 16 } align={ 'middle' }>
							<Col><Title level={ 3 }>Заказ { data.id }</Title></Col>
							<Col>
								<b>Время заказа:</b> { moment(data.date).format('DD.MM.YYYY | HH:mm') }
							</Col>
							<Col>
								<b>Имя клиента:</b> { data.name }
							</Col>
						</Row>
					</Col>
				</Row>
				<br/>
				<List fetching={ fetching } data={ data } refund={ this.props.refund }/>
			</>
		);
	}
}

OrderDetails.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	refund: PropTypes.func.isRequired
};

export default OrderDetails;