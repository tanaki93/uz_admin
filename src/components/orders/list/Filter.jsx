import React from 'react';
import { Select, Button, DatePicker, Input, Form, Col, Row } from 'antd';
import * as PropTypes from 'prop-types';
import moment from 'moment';
import { useIntl } from 'react-intl';

const { RangePicker } = DatePicker;

const Filter = (props) => {
	const intl = useIntl();
	const formRef = React.createRef();
	const { fetchingClients, clients, fetchOrders } = props;

	const handleSubmit = () => {
		formRef.current.validateFields()
			.then(values => {
				fetchOrders({
					id: values.id,
					name: values.name,
					dateFrom: values.range ? moment(values.range[0]).format('YYYY-MM-DD') : '',
					dateTo: values.range ? moment(values.range[1]).format('YYYY-MM-DD') : ''
				});
			});
	};

	const handleReset = () => {
		formRef.current.resetFields();
		fetchOrders();
	};

	const formItemLayout = {
		labelCol: {
			xl: { span: 24 },
		},
		wrapperCol: {
			xl: { span: 24 },
		},
	};


	const tailLayout = {
		wrapperCol: { span: 24 }
	};

	return (
		<Form
			ref={ formRef }
			{ ...formItemLayout }
		>
			<Row gutter={ 16 } align='bottom'>
				<Col xl={ 6 } lg={ 6 } md={ 6 } xs={ 12 }>
					<Form.Item name='range' label={ intl.formatMessage({ id: 'packages-check.filter.date' }) }>
						<RangePicker allowClear style={ { width: '100%' } }/>
					</Form.Item>
				</Col>
				<Col xl={ 6 } lg={ 6 } md={ 6 } xs={ 12 }>
					<Form.Item label={ 'Номер заказа' } name='id'>
						<Input/>
					</Form.Item>
				</Col>
				<Col xl={ 6 } lg={ 6 } md={ 6 } xs={ 12 }>
					<Form.Item label={ 'Клиент' } name='name'>
						<Select
							loading={ fetchingClients }
							style={ { width: '100%' } }
						>
							<Select.Option value={ '' }>Все</Select.Option>
							{ clients.map((client, index) =>
								<Select.Option value={ client } key={ index }>{ client }</Select.Option>
							) }
						</Select>
					</Form.Item>
				</Col>
				<Col xl={ 6 } lg={ 6 } md={ 6 } xs={ 12 }>
					<Form.Item { ...tailLayout }>
						<Button onClick={ handleSubmit } type='primary'>
							{ intl.formatMessage({ id: 'packages-check.filter.filter' }) }
						</Button>
						<Button onClick={ handleReset }>
							{ intl.formatMessage({ id: 'packages-check.filter.drop' }) }
						</Button>
					</Form.Item>
				</Col>
			</Row>
		</Form>
	);
};

Filter.propTypes = {
	fetchOrders: PropTypes.func.isRequired,
	fetchingClients: PropTypes.bool.isRequired,
	clients: PropTypes.array.isRequired
};

export default Filter;