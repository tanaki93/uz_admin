import React, { useRef } from 'react';
import * as PropTypes from 'prop-types';
import { PlusOutlined } from '@ant-design/icons';
import { Button, PageHeader } from 'antd';
import Filter from './Filter';
import List from './List';

const Orders = (props) => {
	const wrapper = useRef(null);
	const { fetching, data, fetchingClients, clients, fetchOrders, assignMyself } = props;

	return (
		<>
			<PageHeader
				ghost={ false }
				title='Заказы'
				extra={ [
					<Button type='primary' icon={ <PlusOutlined/> }>
						Добавить заказ
					</Button>
				] }
				footer={
					<Filter
						fetchOrders={ fetchOrders }
						fetchingClients={ fetchingClients }
						clients={ clients }
					/>
				}
			>
			</PageHeader>
			<div className='page-wrapper page-wrapper--large' ref={ wrapper }>
				<List assignMyself={ assignMyself } fetching={ fetching } data={ data.reverse() }/>
			</div>
		</>
	);
};

Orders.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	fetchingClients: PropTypes.bool.isRequired,
	clients: PropTypes.array.isRequired,
	fetchOrders: PropTypes.func.isRequired,
	assignMyself: PropTypes.func.isRequired
};

export default Orders;