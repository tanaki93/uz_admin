import React, { useState } from 'react';
import * as PropTypes from 'prop-types';
import moment from 'moment';
import { Link } from 'react-router-dom';
import * as paymentStatus from '../../../constants/paymentStatus';
import * as processingStatus from '../../../constants/processingStatus';
import { Form } from '@ant-design/compatible';
import { Button, Col, Row, Table, Tag, Radio } from 'antd';

const List = (props) => {
	const [paymentFilter, setPaymentFilter] = useState('1');
	const [processFilter, setProcessFilter] = useState('0');
	const { fetching, data, assignMyself } = props;


	const handlePaymentFilterChange = (e) => {
		setPaymentFilter(e.target.value);
		paymentFilter !== '1' && setProcessFilter('0');
	};

	const handleProcessFilterChange = (e) => {
		setProcessFilter(e.target.value);
	};

	const renderPaymentStatus = (status) => {
		switch (status) {
			default:
			case paymentStatus.WAITING:
				return <Tag color='orange'>Ожидание</Tag>;
			case paymentStatus.NON_PAID:
				return <Tag color='red'>Не оплачен</Tag>;
			case paymentStatus.PAID:
				return <Tag color='green'>Оплачен</Tag>;
		}
	};

	const renderProcessingStatus = (status) => {
		switch (status) {
			default:
			case processingStatus.CANCELED:
				return <Tag color='red'>Отменен</Tag>;
			case processingStatus.WAIT:
				return <Tag color='orange'>Ожидание</Tag>;
			case processingStatus.IN_PROCESS:
				return <Tag color='blue'>В обработке</Tag>;
			case processingStatus.DONE:
				return <Tag color='green'>Обработан</Tag>;
			case processingStatus.REFUND:
				return <Tag color='#ED1B23'>Оформлен на возврат</Tag>;
		}
	};


	const columns = [
		{
			title: 'Заказ',
			dataIndex: 'id',
			key: 'order_id',
			render: id => <Link to={ `/orders/${ id }` }>{ id }</Link>
		},
		{
			title: 'Время заказа',
			dataIndex: 'date',
			key: 'order_date',
			render: date => moment(date).format('DD.MM.YYYY | HH:mm')
		},
		{
			title: 'Кол-во тов',
			dataIndex: 'count',
			key: 'order_count',
		},
		{
			title: 'Сумма',
			dataIndex: 'total_price',
			key: 'order_price',
		},
		{
			title: 'Имя клиента',
			dataIndex: 'name',
			key: 'order_client_name',
		},
		{
			title: 'Статус оплаты',
			dataIndex: 'payment_status',
			key: 'order_payment_status',
			render: status => renderPaymentStatus(status)
		},
		{
			title: 'Статус обработки',
			dataIndex: 'process_status',
			key: 'order_processing_status',
			render: status => renderProcessingStatus(status)
		},
		{
			key: 'order_processing_status',
			render: item => item.user ? `${ item.user.first_name } ${ item.user.last_name } (${ item.user.username })` :
				<Button disabled={ item.payment_status !== 1 } type={ 'primary' }
				        onClick={ () => assignMyself(item.id) }>
					Назначить себя
				</Button>
		},
		{
			title: 'Статус проверки',
			children: [
				{
					title: <div style={ { color: '#F2994A' } }>Ожидание</div>,
					dataIndex: 'statuses',
					key: 'order_status_waiting',
					render: statuses => <span style={ { color: '#F2994A' } }>{ statuses.waiting }</span>
				},
				{
					title: <div style={ { color: '#2F80ED' } }>В обработке</div>,
					dataIndex: 'statuses',
					key: 'order_status_processing',
					render: statuses => <span style={ { color: '#2F80ED' } }>{ statuses.in_process }</span>
				},
				{
					title: <div style={ { color: '#219653' } }>Обработан</div>,
					dataIndex: 'statuses',
					key: 'order_status_success',
					render: statuses => <span style={ { color: '#219653' } }>{ statuses.done }</span>
				},
				{
					title: <div style={ { color: '#ED1B23' } }>Отменен</div>,
					dataIndex: 'statuses',
					key: 'order_status_failure',
					render: statuses => <span style={ { color: '#ED1B23' } }>{ statuses.cancelled }</span>
				},
			]
		},
	];

	let paymentProcessed = data;
	let processProcessed = paymentProcessed;

	switch (+paymentFilter) {
		default:
		case 0:
			paymentProcessed = data;
			break;
		case 1:
			paymentProcessed = data.filter(item => item.payment_status === paymentStatus.PAID);
			break;
		case 2:
			paymentProcessed = data.filter(item => item.payment_status === paymentStatus.WAITING);
			break;
		case 3:
			paymentProcessed = data.filter(item => item.payment_status === paymentStatus.NON_PAID);
			break;
	}

	switch (+processFilter) {
		default:
		case 0:
			processProcessed = paymentProcessed;
			break;
		case 1:
			processProcessed = paymentProcessed.filter(item => item.process_status === processingStatus.WAIT);
			break;
		case 2:
			processProcessed = paymentProcessed.filter(item => item.process_status === processingStatus.IN_PROCESS);
			break;
		case 3:
			processProcessed = paymentProcessed.filter(item => item.process_status === processingStatus.DONE);
			break;
		case 4:
			processProcessed = paymentProcessed.filter(item => item.process_status === processingStatus.CANCELED);
			break;

	}

	const formItemLayout = {
		labelCol: {
			xl: { span: 24 },
		},
		wrapperCol: {
			xl: { span: 24 },
		},
	};

	return (
		<>
			<Form { ...formItemLayout }>
				<Row gutter={ [16, 24] }>
					<Col>
						<Form.Item label={ 'Статус оплаты' }>
							<Radio.Group onChange={ handlePaymentFilterChange } value={ paymentFilter }>
								<Radio.Button value='1'>Оплачен</Radio.Button>
								<Radio.Button value='2'>В ожидании</Radio.Button>
								<Radio.Button value='3'>Не оплачен</Radio.Button>
								<Radio.Button value='0'>Все</Radio.Button>
							</Radio.Group>
						</Form.Item>
					</Col>
					<Col>
						<Form.Item label={ 'Статус обработки' }>
							<Radio.Group onChange={ handleProcessFilterChange } value={ processFilter } disabled={paymentFilter === '1'}>
								<Radio.Button value='0'>Все</Radio.Button>
								<Radio.Button value='1'>В ожидании</Radio.Button>
								<Radio.Button value='2'>В обработке</Radio.Button>
								<Radio.Button value='3'>Обработан</Radio.Button>
								<Radio.Button value='4'>Отменен</Radio.Button>
							</Radio.Group>
						</Form.Item>
					</Col>
				</Row>
			</Form>

			<Table
				loading={ fetching }
				className={ 'izi-table' }
				columns={ columns }
				dataSource={ processProcessed }
				bordered
				size='small'
				rowKey={ 'id' }
			/>
		</>
	);
};

List.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	assignMyself: PropTypes.func.isRequired
};
export default List;