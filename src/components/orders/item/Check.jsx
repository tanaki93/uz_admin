import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

class Check extends Component {
	render() {
		const { id, date, name, products, address, price, transactions, shipping_price } = this.props.payment;

		const success = transactions?.find(t => t.status === 'Успешная транзакция') || {};

		return (
			<div className="check">

				<div className="check__merchant">
						<span className="check__merchant__name">
							ОсОО «Питанга» - IZISHOP
						</span>
					<span className="check__merchant__address">
							ул. Тимирязева, д. 6/1. <br/>
							г Бишкек, Кыргызстан
						</span>
					<span className="check__merchant__date">
							Дата заказа: { moment(date).locale('ru').format('D MMMM, YYYYг.') }
						</span>
					<span className="check__merchant__site">
							www.izishop.kg
						</span>
				</div>

				<div className="check__order">
					<div className="check__order__number">
						номер заказа: { id }
					</div>
					<table className="check__order__info">
						<tr>
							<td rowSpan={ 2 }>
								<b>Адрес доставки</b>
								<br/>
								{ address + ', г Бишкек, Кыргызстан' || 'Адрес не указано' }
								<br/>
								<br/>
								<b>Тип доставки</b>
								<br/>
								Стандартный
							</td>
							<td>
								<table className={ 'check__order__items' }>
									<thead>
									<tr>
										<th>Товар</th>
										<th>Цена</th>
									</tr>
									</thead>
									<tbody>
									{ products?.map(item =>
										<tr>
											<td>{ `${ item.product.izi_category.name } ${ item.product.brand.name } (${ item.product.colour.name } / ${ item.size.name } )` }</td>
											<td>{ item.price }с</td>
										</tr>
									) }

									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td style={ { textAlign: 'right' } }>
								<table className={ 'check__order__total' }>
									<tbody>
									<tr>
										<td>Цена товаров:</td>
										<td>{ price }с</td>
									</tr>
									<tr>
										<td>Цена доставки:</td>
										<td>{ shipping_price }с</td>
									</tr>
									<tr>
										<td></td>
										<td>-------</td>
									</tr>
									<tr style={ { fontWeight: 'bold' } }>
										<td>Итого:</td>
										<td>{ price + shipping_price }с</td>
									</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</table>
				</div>

				<div className="check__payment">
					<div className="check__payment__number">
						Информация об оплате
					</div>
					<table className="check__payment__info">
						<tr>
							<td>
								<b>Метод оплаты</b>
								<br/>
								{ success.pg_payment_system } | { success.pan || success.trans_id }
								<br/>
								{ name || 'Имя не указано' }
								<br/>
								{ success.auth_code && `Код авторизации: ${ success.auth_code }` }
								<br/>
								Тип транзакции: Покупка
								<br/>
								<br/>
								<b>Биллинг адрес</b>
								<br/>
								{ address + ', г Бишкек, Кыргызстан' || 'Адрес не указано' }
							</td>
							<td style={ { textAlign: 'right' } }>
								<table className={ 'check__order__total' }>
									<tbody>
									<tr>
										<td>Цена товаров:</td>
										<td>{ price }с</td>
									</tr>
									<tr>
										<td>Цена доставки:</td>
										<td>{ shipping_price }с</td>
									</tr>
									<tr>
										<td/>
										<td>-------</td>
									</tr>
									<tr style={ { fontWeight: 'bold' } }>
										<td>Итого:</td>
										<td>{ price + shipping_price }с</td>
									</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</table>
				</div>
			</div>
		);
	}
}

Check.propTypes = {
	payment: PropTypes.object.isRequired
};

export default Check;