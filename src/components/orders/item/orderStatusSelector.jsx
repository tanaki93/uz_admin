import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import * as productStatus from '../../../constants/productStatus';
import { Radio } from 'antd';

class OrderStatusSelector extends Component {
	render() {
		return (
			<Radio.Group onChange={ this.onChange } value={ this.props.item.product_status }>
				<Radio style={ { color: '#F2994A' } } value={ productStatus.WAIT }>
					В ожидании
				</Radio>
				<Radio style={ { color: '#2F80ED' } } value={ productStatus.IN_PROCESS }>
					В обработке
				</Radio>
				<Radio style={ { color: '#219653' } } value={ productStatus.DONE }>
					Обработан
				</Radio>
				<Radio style={ { color: '#ED1B23' } } value={ productStatus.CANCELED }>
					Отменён
				</Radio>
			</Radio.Group>
		);
	}
}

OrderStatusSelector.propTypes = {
	product_status: PropTypes.object.isRequired,
};

export default OrderStatusSelector;