import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Form } from '@ant-design/compatible';

import { Button, Input } from 'antd';
import * as packageStatus from '../../../constants/packageStatus';
import * as productStatus from '../../../constants/productStatus';

class Stage extends Component {
	state = {
		stage: ''
	};

	componentDidMount() {
		this.props.product.package !== null &&
		this.setState({
			stage: this.props.product.package.number
		});
	}

	onChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};

	changeStage = () => {
		this.props.changeStage(this.props.product.id, this.state.stage);
	};

	removeStage = () => {
		this.props.changeStage(this.props.product.id, '');
	};

	render() {
		const { product } = this.props;
		const { stage } = this.state;

		return (
			<div>
				<Form style={ { display: 'flex' } }>
					<Form.Item style={ { margin: 0 } }>
						<Input
							style={ {
								width: '150px'
							} }
							onChange={ this.onChange }
							name='stage'
							value={ stage }
							disabled={ product.package_status !== packageStatus.WAIT || product.product_status === productStatus.REFUND }/>
					</Form.Item>
					<Form.Item style={ { margin: 0 } }>
						<Button type={ 'primary' }
						        onClick={ this.changeStage }
						        disabled={ product.package_status !== packageStatus.WAIT || product.product_status === productStatus.REFUND }>
							Заказать
						</Button>
					</Form.Item>
				</Form>
				{
					product.package_status === packageStatus.ORDERED && product.package_status === packageStatus.WAIT &&
					<Button type={ 'link' }
					        onClick={ this.removeStage }>
						Не заказан
					</Button>
				}
			</div>
		);
	}
}

Stage.propTypes = {
	product: PropTypes.object.isRequired,
	changeStage: PropTypes.func.isRequired
};

export default Stage;