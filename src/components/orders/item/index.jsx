import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Button, Card, Col, Descriptions, Modal, Row, Typography } from 'antd';
import List from './List';
import { Link } from 'react-router-dom';
import moment from 'moment';
import * as processingStatus from '../../../constants/productStatus';
import Check from './Check';
import ReactToPrint from 'react-to-print';

const { Title } = Typography;

class OrderDetails extends Component {
	state = { visible: false };

	showModal = () => {
		this.setState({
			visible: true,
		});
	};

	handleOk = e => {
		this.setState({
			visible: false,
		});
	};

	handleCancel = e => {
		this.setState({
			visible: false,
		});
	};

	render() {
		const { fetching, data, changeProductStatus, changeStage } = this.props;

		const paymentTypeRenderer = (type) => {
			switch (type) {
				default:
				case 0:
					return '';
				case 1:
					return 'PayBox';
				case 2:
					return 'Demir Bank';
			}
		};

		return (
			<>
				<Modal
					width={ 960 }
					centered
					title="Чек"
					visible={ this.state.visible }
					onCancel={ this.handleCancel }
					footer={
						<ReactToPrint
							trigger={ () => <Button type='primary'>Распечатать чек</Button> }
							content={ () => this.componentRef }
						/>
					}
				>
					<Check payment={ this.props.data } ref={ el => (this.componentRef = el) }/>
				</Modal>
				<Card>
					<Row>
						<Col span={ 24 }>
							<Link to={ `/orders` }>Назад</Link>
						</Col>
					</Row>
					<Row>
						<Col>
							<Row type="flex" gutter={ 16 } align={ 'middle' }>
								<Col><Title level={ 3 }>Заказ { data.id }</Title></Col>
								<Col>
									<b>Время заказа:</b> { moment(data.date).format('DD.MM.YYYY | HH:mm') }
								</Col>
								<Col>
									<b>Имя клиента:</b> { data.name }
								</Col>
							</Row>
						</Col>
					</Row>
					<hr/>
					<br/>
					<Row gutter={ [16, 24] }>
						<Col span={ 24 }>
							<Descriptions className={ 'izi-desc' } layout="vertical" size='small'
							              column={ { xxl: 6, xl: 4, lg: 4, md: 4, sm: 2, xs: 1 } }>
								<Descriptions.Item label="Адрес доставки">{ data.address }</Descriptions.Item>
								<Descriptions.Item label="Почта">{ data.email }</Descriptions.Item>
								<Descriptions.Item label="Телефон">{ data.phone }</Descriptions.Item>
								{data.payment_type &&<Descriptions.Item
									label="Метод платежа">{ paymentTypeRenderer(data.payment_type) }</Descriptions.Item>}
							</Descriptions>
						</Col>
						<Col>
							<List
								fetching={ fetching } data={ data.products }
								changeProductStatus={ changeProductStatus } changeStage={ changeStage }
							/>
						</Col>
						<Col span={ 24 }>
							<Row type="flex" gutter={ 16 } align={ 'end' }>
								<Col>
									<Button onClick={ this.showModal }>
										Чек
									</Button>
									<Button className={ 'submit-btn' } type={ 'primary' }
									        onClick={ () => this.props.changeOrderStatus(data.id, processingStatus.DONE) }>Обработан</Button>
									<Button type={ 'link' }
									        onClick={ () => this.props.changeOrderStatus(data.id, processingStatus.CANCELED) }>Отменен</Button>
									<Link to={ `${ data.id }/refund` }><Button type={ 'primary' }>Оформить
										возврат</Button></Link>
								</Col>
							</Row>
						</Col>
					</Row>
				</Card>
			</>
		);
	}
}

OrderDetails.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	changeProductStatus: PropTypes.func.isRequired,
	changeOrderStatus: PropTypes.func.isRequired,
	changeStage: PropTypes.func.isRequired
};

export default OrderDetails;