import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Modal, Table } from 'antd';
import StatusRenderer from '../../../shared/statusRenderer';
import Stage from './Stage';
import * as productStatus from '../../../constants/productStatus';
import { injectIntl } from 'react-intl';

class List extends Component {

	changeDeliveryStatus = (id, value) => {
		this.props.changeProductStatus(id, 'delivery_status', value);
	};

	showComment = (comments) => {
		Modal.confirm({
			title: 'Комментарии',
			content: comments.length > 0 ? comments.map((item, index) =>
				<p key={ 'comment_' + index }>{ item.comment }</p>
			) : 'Нет комментарий',
			cancelText: false,
		});
	};

	renderStatus = (status) => {
		switch (status) {
			default:
			case productStatus.WAIT:
				return <span style={ { color: '#F2994A' } }>В ожидании</span>;

			case productStatus.IN_PROCESS:
				return <span style={ { color: '#2F80ED' } }>В обработке</span>;

			case productStatus.DONE:
				return <span style={ { color: '#219653' } }>Обработан</span>;

			case productStatus.REFUND:
				return <span style={ { color: '#ff0000' } }>Оформлен возврат</span>;

			case productStatus.CANCELED:
				return <span style={ { color: '#ED1B23' } }>Отменён</span>;
		}
	};

	render() {
		const { fetching, data, intl } = this.props;
		const statusRenderer = new StatusRenderer();

		const columns = [
			{
				title: 'Название',
				dataIndex: 'product',
				key: 'product_name',
				width: 300,
				fixed: 'left',
				render: product => product.title
			},
			{
				title: intl.formatMessage({ id: 'products-check.list.id' }),
				dataIndex: 'id',
				key: 'product_order_id',
				width: 100,
				fixed: 'left',
			},
			{
				title: intl.formatMessage({ id: 'products-check.details.size' }),
				dataIndex: 'size',
				key: 'product_size',
				width: 100,
				render: size => size.name
			},
			{
				title: 'Кол-во',
				dataIndex: 'amount',
				key: 'product_count',
				width: 100,
			},
			{
				title: 'Ссылка IZI',
				dataIndex: 'izi_link',
				key: 'izi_link',
				width: 100,
				render: link => <a href={ link } target={ '_blank' }>Ссылка</a>
			},
			{
				title: 'Ссылка Vend',
				dataIndex: 'product',
				key: 'product_vend_link',
				width: 100,
				render: product => <a href={ product.link.url } target={ '_blank' }>Ссылка</a>
			},
			{
				title: intl.formatMessage({ id: 'packages.list.details' }),
				children: [
					{
						title: 'Статус товара',
						dataIndex: 'product_status',
						width: 150,
						render: status => this.renderStatus(status)
					},
					{
						title: 'Цена дост.',
						dataIndex: 'product',
						key: 'product.shipping_price',
						width: 100,
						render: product => product.shipping_price
					},
					{
						title: 'Цена тов.',
						key: 'price',
						width: 100,
						render: item => item.price / item.amount
					},
					{
						title: 'Сумма',
						key: 'product_sum',
						width: 100,
						render: item => item.price
					},
				]
			},
			{
				title: 'Обработка',
				children: [
					{
						title: 'Этап',
						key: 'package',
						width: 250,
						render: product => <Stage product={ product } changeStage={ this.props.changeStage }/>

					},
					{
						title: 'Статус',
						dataIndex: 'package_status',
						key: 'package_status',
						width: 100,
						render: status => statusRenderer.renderPackageStatus(status, intl)
					},
				]
			},
			{
				title: 'Проверка',
				children: [
					{
						title: 'Состояние',
						key: 'checking_status',
						width: 100,
						render: product => statusRenderer.renderProductCheckStatus(product.checking_status, intl)
					},
					{
						title: 'Статус отпр. на доставку',
						dataIndex: 'delivery_status',
						key: 'delivery_status',
						width: 100,
						render: status => statusRenderer.renderProductDeliveryStatus(status, intl)
					},
				]
			}
		];

		return (
			<Table
				className={ 'izi-table' }
				pagination={ false }
				loading={ fetching }
				columns={ columns }
				dataSource={ data }
				bordered
				size="small"
				rowKey={ 'id' }
				scroll={ { x: 2000 } }
			/>
		);
	}
}

List.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	changeProductStatus: PropTypes.func.isRequired,
	changeStage: PropTypes.func.isRequired
};

export default (injectIntl)(List);
