import React from 'react';
import { Table, Tag } from 'antd';
import * as userType from '../../../../constants/userType';
import { Link } from 'react-router-dom';
import EditUser from './EditUser';
import * as PropTypes from 'prop-types';
import { useTextFilter } from 'shared/stringSearch';

const List = (props) => {
	const { fetching, data, deleteUser, editUser, wrapper } = props;

	const renderTag = (type) => {
		switch (type) {
			case userType.CONTROLLER:
				return <Tag color="green">Контролёр</Tag>;
			case userType.ADMIN:
				return <Tag color="purple">Администратор</Tag>;
			case userType.OPERATOR:
				return <Tag color="blue">Оператор</Tag>;
			default:
			case userType.CLIENT:
				return <Tag color="red">Клиент</Tag>;
		}
	};

	const columns = [
		{
			title: 'Логин',
			render: user =>
				user.user_type === userType.CLIENT ?
					<Link to={ '/users/' + user.id }>
						{ user.username }
					</Link> : user.username,
			...useTextFilter('username'),
		},
		{
			title: 'Имя',
			key: 'name',
			render: user => `${ user.first_name } ${ user.last_name }`,
			...useTextFilter('first_name'),
		},
		{
			title: 'Почта',
			key: 'email',
			dataIndex: 'email'
		},
		{
			title: 'Тип',
			key: 'user_type',
			dataIndex: 'user_type',
			render: type => renderTag(type)
		},
		{
			title: 'Телефон',
			key: 'phone',
			dataIndex: 'phone'
		},
		{
			align: 'right',
			render: user => <EditUser editUser={ editUser } deleteUser={ deleteUser } user={ user }/>
		}
	];

	return (
		<Table
			rowKey='id'
			scroll={ { y: wrapper.current?.clientHeight - 168 } }
			className='izi-table' columns={ columns }
			loading={ fetching }
			dataSource={ data }
		/>
	);
};

List.propTypes = {
	data: PropTypes.array.isRequired,
	deleteUser: PropTypes.func.isRequired,
	editUser: PropTypes.func.isRequired,
	fetching: PropTypes.bool.isRequired,
	wrapper: PropTypes.object.isRequired
};

export default List;