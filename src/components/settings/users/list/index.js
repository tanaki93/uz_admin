import React, { useEffect, useRef, useState } from 'react';
import * as PropTypes from 'prop-types';
import { PageHeader, Tabs } from 'antd';
import * as userType from 'constants/userType';
import CreateUser from './CreateUser';
import List from './List';

const Users = (props) => {
	const wrapper = useRef(null);
	const [data, setData] = useState([]);
	const { createUser, fetching, deleteUser, editUser } = props;

	useEffect(() => {
		setData(props.data.filter(user => user.user_type === userType.CLIENT));
	}, [props.data])

	const handleChange = (value) => {
		switch (+value) {
			default:
			case 2:
				setData(props.data.filter(user => user.user_type === userType.CLIENT));
				break;
			case 1:
				setData(props.data.filter(user => user.user_type !== userType.CLIENT));
				break;
		}
	};

	return (
		<>
			<PageHeader
				ghost={ false }
				title="Пользователи"
				extra={ [
					<CreateUser key='createUser' createUser={ createUser } users={ data }/>
				] }
				footer={
					<Tabs defaultActiveKey="2" onChange={ handleChange }>
						<Tabs.TabPane tab="Клиенты" key="2"/>
						<Tabs.TabPane tab="Сотрудники" key="1"/>
					</Tabs>
				}
			/>
			<div className='page-wrapper page-wrapper--mid' ref={ wrapper }>
				<List fetching={ fetching } data={ data } deleteUser={ deleteUser } editUser={ editUser } wrapper={ wrapper }/>
			</div>
		</>
	);
};

Users.propTypes = {
	data: PropTypes.array.isRequired,
	createUser: PropTypes.func.isRequired,
	deleteUser: PropTypes.func.isRequired,
	editUser: PropTypes.func.isRequired,
	fetching: PropTypes.bool.isRequired
};

export default Users;