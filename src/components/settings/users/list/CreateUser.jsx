import React, { useState } from 'react';
import * as PropTypes from 'prop-types';
import { PlusOutlined } from '@ant-design/icons';
import { Form, Modal, Button, Input, Select, message } from 'antd';
import * as userType from '../../../../constants/userType';

const CreateUser = (props) => {
	const [visible, setVisible] = useState(false);
	const { createUser, users } = props;

	const showModal = () => {
		setVisible(true);
	};

	const error = () => {
		message.error('Такой пользователь уже существует');
	};

	const handleOk = values => {
		if (!users.find(user => user.username === values.username)) {
			createUser(values);
		} else {
			error();
		}
		setVisible(false);
	};

	const handleCancel = e => {
		setVisible(false);
	};

	const formItemLayout = {
		labelCol: { span: 8 },
		wrapperCol: { span: 16 },
	};

	const tailLayout = {
		wrapperCol: { span: 24 },
		style: { marginBottom: 0 }
	};

	return (
		<>
			<Button type="primary" onClick={ showModal } icon={ <PlusOutlined/> }>
				Добавить пользователя
			</Button>
			<Modal
				title="Добавить пользователя"
				visible={ visible }
				onCancel={ handleCancel }
				footer={
					null
				}
			>
				<Form onFinish={ handleOk } { ...formItemLayout }>
					<Form.Item label="Имя пользователя" required name='username'
					           rules={ [{ required: true, message: 'Введите имя пользователя' }] }>
						<Input/>
					</Form.Item>
					<Form.Item label="Пароль" required name='password'
					           rules={ [{ required: true, message: 'Введите пароль' }] }>
						<Input type='password'/>
					</Form.Item>
					<Form.Item label="Имя" required name='first_name'
					           rules={ [{ required: true, message: 'Введите имя' }] }>
						<Input/>
					</Form.Item>
					<Form.Item label="Фамилия" required name='last_name'
					           rules={ [{ required: true, message: 'Введите фамилию' }] }>
						<Input/>
					</Form.Item>
					<Form.Item label="Тип" required name='user_type'
					           rules={ [{ required: true, message: 'Выберите тип' }] }>
						<Select style={ { width: '100%' } }>
							<Select.Option value={ userType.OPERATOR }>Оператор</Select.Option>
							<Select.Option value={ userType.ADMIN }>Администратор</Select.Option>
							<Select.Option value={ userType.CONTROLLER }>Контролёр</Select.Option>
							<Select.Option value={ userType.CLIENT }>Клиент</Select.Option>
							<Select.Option value={ userType.MARKETEER }>Маркетолог</Select.Option>
						</Select>
					</Form.Item>
					<Form.Item label="Email" required name='email'
					           rules={ [{ required: true, message: 'Введите почту' }] }>
						<Input/>
					</Form.Item>
					<Form.Item label="Телефон" required name='phone'
					           rules={ [{ required: true, message: 'Введите номер телефона' }] }>
						<Input/>
					</Form.Item>
					<Form.Item { ...tailLayout }>
						<Button type="primary" htmlType="submit" className="login-form-button" block>
							Подтведить
						</Button>
					</Form.Item>
				</Form>
			</Modal>
		</>
	);
};

CreateUser.propTypes = {
	createUser: PropTypes.func.isRequired,
	users: PropTypes.array.isRequired,
};

export default CreateUser;