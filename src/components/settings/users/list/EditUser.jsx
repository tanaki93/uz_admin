import React, { useState } from 'react';
import * as PropTypes from 'prop-types';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { Modal, Button, Input, Select, Form, Row, Col, Popconfirm } from 'antd';
import * as userType from '../../../../constants/userType';

const EditUser = (props) => {
	const [visible, setVisible] = useState(false);
	const { user, editUser, deleteUser } = props;

	const showModal = () => {
		setVisible(true);
	};

	const handleOk = values => {
		editUser(user.id, values);
		setVisible(false);
	};

	const handleCancel = e => {
		setVisible(false);
	};

	const handleDelete = () => {
		deleteUser(user.id);
		setVisible(false);
	};

	const formItemLayout = {
		labelCol: { span: 8 },
		wrapperCol: { span: 14 },
	};

	const tailLayout = {
		wrapperCol: { span: 24 },
		style: { marginBottom: 0 }
	};


	return (
		<>
			<Button onClick={ showModal } icon={ <EditOutlined/> }>
				Изменить
			</Button>
			<Modal
				title="Изменить пользователя"
				visible={ visible }
				onCancel={ handleCancel }
				footer={ null }
			>
				<Form
					{ ...formItemLayout }
					footer={ null }
					initialValues={ {
						user_type: user.user_type,
						first_name: user.first_name,
						last_name: user.last_name,
						email: user.email,
						phone: user.phone,
					} }
					onFinish={ handleOk }
				>
					<Form.Item
						label="Имя"
						required
						name='first_name'
						rules={ [{ required: true, message: 'Введите имя' }] }
					>
						<Input/>
					</Form.Item>
					<Form.Item
						label="Фамилия"
						required
						name='last_name'
						rules={ [{ required: true, message: 'Введите фамилию' }] }
					>
						<Input/>
					</Form.Item>
					<Form.Item
						label="Тип"
						required
						name='user_type'
						rules={ [{ required: true, message: 'Выберите тип' }] }
					>
						<Select style={ { width: '100%' } }>
							<Select.Option value={ userType.OPERATOR }>Оператор</Select.Option>
							<Select.Option value={ userType.ADMIN }>Администратор</Select.Option>
							<Select.Option value={ userType.CONTROLLER }>Контролёр</Select.Option>
							<Select.Option value={ userType.CLIENT }>Клиент</Select.Option>
							<Select.Option value={ userType.MARKETEER }>Маркетолог</Select.Option>
						</Select>
					</Form.Item>
					<Form.Item
						label="Email"
						required
						name='email'
						rules={ [{ required: true, message: 'Введите почту' }] }
					>
						<Input/>
					</Form.Item>
					<Form.Item
						label="Телефон"
						required
						name='phone'
						rules={ [{ required: true, message: 'Введите номер телефона' }] }
					>
						<Input/>
					</Form.Item>
					<Form.Item { ...tailLayout }>
						<Row type='flex' gutter={ 8 }>
							<Col span={ 6 }>
								<Popconfirm
									title="Вы уверены?"
									onConfirm={ handleDelete }
									okText="Да"
									cancelText="Нет"
									disabled={ user.is_related || user.user_type === userType.ADMIN }
								>
									<Button
										block
										type={ 'primary' } ghost
										icon={ <DeleteOutlined/> }
										disabled={ user.is_related || user.user_type === userType.ADMIN }
									>
										Удалить
									</Button>
								</Popconfirm>
							</Col>
							<Col span={ 18 }>
								<Button type="primary" htmlType="submit" className="login-form-button" block>
									Подтведить
								</Button>
							</Col>
						</Row>
					</Form.Item>
				</Form>
			</Modal>
		</>
	);
};

EditUser.propTypes = {
	editUser: PropTypes.func.isRequired,
	deleteUser: PropTypes.func.isRequired,
	user: PropTypes.object.isRequired
};

export default EditUser;