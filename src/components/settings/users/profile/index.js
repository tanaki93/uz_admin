import React, { useRef } from 'react';
import { withRouter } from 'react-router-dom';
import * as PropTypes from 'prop-types';
import { Descriptions, PageHeader } from 'antd';
import List from './List';

const Profile = (props) => {
	const wrapper = useRef(null);
	const { fetching, orders, data, history } = props;

	const renderGender = (genderId) => {
		switch (genderId) {
			default:
			case 0:
				return 'Не указан';
			case 1:
				return 'Мужчина';
			case 2:
				return 'Женщина';
		}
	};

	return (
		<>
			<PageHeader
				onBack={ () => history.push('/users') }
				title={ `Информация о пользователе ${ data.first_name + ' ' + data.last_name || '' }` }
				ghost={ false }
			>
				<Descriptions size='small' column={ { xxl: 3, xl: 3, lg: 3, md: 2, sm: 2, xs: 1 } }>
					<Descriptions.Item label="Логин">{ data.username }</Descriptions.Item>
					<Descriptions.Item label="Телефон">{ data.phone }</Descriptions.Item>
					<Descriptions.Item label="Пол">{ renderGender(data.gender) }</Descriptions.Item>
				</Descriptions>
			</PageHeader>
			<div className='page-wrapper' ref={ wrapper }>
				<List fetching={ fetching } orders={ orders }/>
			</div>
		</>

	);
};

Profile.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.object.isRequired,
	orders: PropTypes.array.isRequired
};

export default withRouter(Profile);