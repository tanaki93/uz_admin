import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';
import moment from 'moment';
import { useIntl } from 'react-intl';

const List = (props) => {
	const intl = useIntl();
	const { fetching, orders } = props;

	const columns = [
		{
			title: 'Имя',
			key: 'name',
			dataIndex: 'name'
		},
		{
			title: 'Почта',
			key: 'email',
			dataIndex: 'email'
		},
		{
			title: 'Телефон',
			key: 'phone',
			dataIndex: 'phone'
		},
		{
			title: 'Адрес',
			key: 'address',
			dataIndex: 'address'
		},
		{
			title: 'Статус доставки',
			key: 'status',
			dataIndex: 'status',
		},
		{
			title: 'Статус оплаты',
			key: 'payment_status',
			dataIndex: 'payment_status',
		},
		{
			title: 'Дата заказа',
			key: 'date',
			dataIndex: 'date',
			render: date => moment(date).format('DD.MM.YYYY HH:mm')
		},
		{
			title: 'Цена доставки',
			key: 'shipping_price',
			dataIndex: 'shipping_price'
		},
		{
			title: 'Итого',
			key: 'total_price',
			dataIndex: 'total_price',
		},
	];

	const productColumns = [
		{
			title: 'Товар',
			dataIndex: 'product',
			key: 'productName',
			render: item =>
				<a href={ item.izi_link } target={ '_blank' }>
					{ `${ item.izi_category.languages.find(lang => lang.lang_code === 'RUS').translation || '' } ${ item.brand.name || '' }` }
				</a>
		},
		{
			title: 'Кол-во',
			key: 'amount',
			dataIndex: 'amount'
		},
		{
			title: 'Цена доставки',
			key: 'shipping_price',
			dataIndex: 'shipping_price'
		},
		{
			title: intl.formatMessage({ id: 'products-delivery.submitter.price' }),
			key: 'price',
			dataIndex: 'price'
		},
		{
			title: 'Статус доствки',
			key: 'delivery_status',
			dataIndex: 'delivery_status'
		},
	];

	return (
		<Table loading={ fetching } dataSource={ orders } columns={ columns } rowKey={ 'id' }
		       expandedRowRender={ order =>
			       <Table dataSource={ order.products } rowKey={ 'id' } columns={ productColumns }/>
		       }
		/>
	);
};

List.propTypes = {
	fetching: PropTypes.bool.isRequired,
	orders: PropTypes.array.isRequired
};

export default List;