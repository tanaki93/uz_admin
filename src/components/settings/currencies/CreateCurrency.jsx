import React from 'react';
import * as PropTypes from 'prop-types';
import { PlusOutlined } from '@ant-design/icons';
import { Modal, Button, Input, message } from 'antd';

class CreateCurrency extends React.Component {
	state = {
		visible: false,
		name: '',
		code: '',
		code_name: '',
	};

	onChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};

	showModal = () => {
		this.setState({
			visible: true,
		});
	};

	error = () => {
		message.error('Такая валюта уже существует');
	};

	handleOk = e => {
		const { name, code, code_name } = this.state;

		if (name && code && code_name) {
			if (!this.props.currencies.find(currency => currency.code.toLowerCase() === code.toLowerCase())) {
				this.props.createCurrency({ name, code, code_name });
				this.setState({
					visible: false,
					name: '',
					code: '',
					code_name: '',
				});
			} else {
				this.error();
			}
		}
	};

	handleCancel = e => {
		this.setState({
			visible: false,
			name: '',
			code: '',
			code_name: ''
		});
	};

	render() {
		const { name, code, code_name } = this.state;

		return (
			<>
				{ this.props.custom ?
					<Button type="primary" onClick={ this.showModal } icon={ <PlusOutlined/> } style={{marginBottom: 24}}/> :
					<Button type="primary" onClick={ this.showModal } icon={ <PlusOutlined/> } style={{marginBottom: 24}}>
						Создать валюту
					</Button>
				}

				<Modal
					title="Создание валюты"
					visible={ this.state.visible }
					onOk={ this.handleOk }
					onCancel={ this.handleCancel }
				>
					<table className={ 'base-table' }>
						<thead>
						<tr>
							<td>Код</td>
							<td>Название</td>
							<td>Символ</td>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td>
								<Input value={ code } name={ 'code' } onChange={ this.onChange }/>
							</td>
							<td>
								<Input value={ name } name={ 'name' } onChange={ this.onChange }/>
							</td>
							<td>
								<Input value={ code_name } name={ 'code_name' } onChange={ this.onChange }/>
							</td>
						</tr>
						</tbody>
					</table>
				</Modal>
			</>
		);
	}
}

CreateCurrency.propTypes = {
	createCurrency: PropTypes.func.isRequired,
	currencies: PropTypes.array.isRequired,
	custom: PropTypes.bool
};

export default CreateCurrency;