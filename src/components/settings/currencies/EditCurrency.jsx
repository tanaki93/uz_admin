import React from 'react';
import * as PropTypes from 'prop-types';
import { EditOutlined } from '@ant-design/icons';
import { Icon as LegacyIcon } from '@ant-design/compatible';
import { Modal, Button, Row, Col, Input, InputNumber, DatePicker, message } from 'antd';
import moment from 'moment';
import { injectIntl } from 'react-intl';
import Statistics from './Statistics';

class EditCurrency extends React.Component {
	state = {
		visible: false,
		name: '',
		code: '',
		code_name: '',
		is_active: false,
		exchanges: []
	};

	UNSAFE_componentWillReceiveProps(nextProps) {
		const { selected } = nextProps;
		this.setState({
			name: selected.name,
			code: selected.code,
			code_name: selected.code_name,
			is_active: selected.is_active,
			exchanges: selected.exchanges
		});
	};

	onChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};

	showModal = () => {
		const { selected } = this.props;
		selected.exchanges.map(ex => ex.date === moment().format('YYYY-MM-DD'));
		this.setState({
			visible: true,
			name: selected.name,
			code: selected.code,
			code_name: selected.code_name,
			is_active: selected.is_active,
			exchanges: selected.exchanges
		});
	};

	error = () => {
		message.error('Такая валюта уже существует');
	};

	handleOk = e => {
		const { name, code, is_active, code_name } = this.state;
		if (name && code && code_name) {
			if (this.props.selected.code.toLowerCase() === code.toLowerCase()) {
				this.props.editCurrency(this.props.selected.id, { is_active, name, code, code_name });
				this.setState({
					visible: false,
					name: '',
					code: '',
					code_name: '',
					is_active: false,
				});
			} else {
				if (this.props.selected.code.toLowerCase() !== code.toLowerCase() && !this.props.currencies.find(currency => currency.code.toLowerCase() === code.toLowerCase())) {
					this.props.editCurrency(this.props.selected.id, { is_active, name, code, code_name });
					this.setState({
						visible: false,
						name: '',
						code: '',
						code_name: '',
						is_active: false,
					});
				} else {
					this.error();
				}
			}
		}
	};

	handleCancel = e => {
		this.setState({
			visible: false,
			name: '',
			code: '',
			code_name: '',
			is_active: false,
		});
	};

	handleDelete = e => {
		this.props.deleteCurrency(this.props.selected.id);
		this.setState({
			visible: false
		});
	};

	handleExchangeValueChange = (value, id, index) => {
		const exchanges = this.state.exchanges;
		id ? exchanges.find(ex => ex.id === id).value = value : exchanges[index].value = value;
		this.setState({
			exchanges
		});
	};

	handleExchangeDateChange = (value, id, index) => {
		const exchanges = this.state.exchanges;
		id ? exchanges.find(ex => ex.id === id).date = value : exchanges[index].date = value;
		this.setState({
			exchanges
		});
	};

	submitExchange = (exchange) => {
		const date = exchange.date ? moment(exchange.date).format('YYYY-MM-DD') : moment().format('YYYY-MM-DD');
		if (exchange.id) {
			this.props.editExchange(exchange.id, {
				value: exchange.value,
				from_currency_id: exchange.from,
				to_currency_id: exchange.to,
				date
			});
		} else {
			exchange.value &&
			this.props.createExchange({
				value: exchange.value,
				from_currency_id: exchange.from,
				to_currency_id: exchange.to,
				date
			});
		}
	};

	render() {
		const { intl } = this.props;
		const { name, code, exchanges, code_name } = this.state;

		return (
			<>
				<Button type="dashed" onClick={ this.showModal } icon={ <EditOutlined/> }>
					Изменить
				</Button>
				<Modal
					width={ '50vw' }
					onCancel={ this.handleCancel }
					title="Изменение валюты"
					visible={ this.state.visible }
					footer={
						<div style={ { display: 'flex', justifyContent: 'space-between' } }>
							<div>
								<Button type={ 'primary' } ghost onClick={ this.handleDelete }
								        disabled={ this.props.selected.is_related }
								        icon={ <LegacyIcon type={ 'delete' }/> }>Удалить</Button>
							</div>
							<div>
								<Button onClick={ this.handleCancel }>Закрыть</Button>
								<Button type={ 'primary' } onClick={ this.handleOk }>Сохранить</Button>
							</div>
						</div>
					}
				>
					<Row type='flex' gutter={ [16, 16] }>
						<Col span={ 24 }>
							<table className={ 'base-table' }>
								<thead>
								<tr>
									<td>Код</td>
									<td>Название</td>
									<td>Символ</td>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td>
										<Input value={ code } name={ 'code' } onChange={ this.onChange }/>
									</td>
									<td>
										<Input value={ name } name={ 'name' } onChange={ this.onChange }/>
									</td>
									<td>
										<Input value={ code_name } name={ 'code_name' } onChange={ this.onChange }/>
									</td>
								</tr>
								</tbody>
							</table>
						</Col>
						<Col span={ 24 }>
							<table className={ 'base-table' }>
								<thead>
								<tr>
									<td>Валюта</td>
									<td>Курс</td>
									<td>{ intl.formatMessage({ id: 'packages-check.filter.date' }) }</td>
									<td></td>
								</tr>
								</thead>
								<tbody>
								{
									exchanges && exchanges.map((exchange, index) =>
										<tr>
											<td>{ exchange.toName }</td>
											<td><InputNumber min={ 0 }
											                 onChange={ value => this.handleExchangeValueChange(value, exchange.id, index) }
											                 value={ exchange.value }/></td>
											<td><DatePicker allowClear={ false }
											                onChange={ (date, dateString) => this.handleExchangeDateChange(dateString, exchange.id, index) }
											                defaultValue={ moment() }/></td>
											<td>
												<Button type={ 'primary' } loading={ this.props.fetchingExchanges }
												        onClick={ () => this.submitExchange(exchange) }>Обновить
													курс</Button>
												<Statistics statistics={ exchange.values }/>
											</td>
										</tr>
									)
								}
								</tbody>
							</table>
						</Col>
					</Row>
				</Modal>
			</>
		);
	}
}

EditCurrency.propTypes = {
	editCurrency: PropTypes.func.isRequired,
	deleteCurrency: PropTypes.func.isRequired,
	createExchange: PropTypes.func.isRequired,
	editExchange: PropTypes.func.isRequired,
	fetchingExchanges: PropTypes.bool.isRequired,
	selected: PropTypes.object
};

export default (injectIntl)(EditCurrency);