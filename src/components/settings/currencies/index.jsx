import React, { Component } from 'react';
import { Checkbox, Col, Row, Table, Typography } from 'antd';
import * as PropTypes from 'prop-types';
import CreateCurrency from './CreateCurrency';
import EditCurrency from './EditCurrency';

const { Title } = Typography;

class Currencies extends Component {
	check = (selected, e) => {
		this.props.editCurrency(selected.id, {
			is_active: e.target.checked,
			code: selected.code,
			code_name: selected.code_name,
			name: selected.name
		});
	};

	render() {
		const { data, fetching, dataExchanges } = this.props;

		const columns = [
			{
				title: 'Актив',
				render: item => <Checkbox checked={ item.is_active } onClick={ e => this.check(item, e) }/>
			},
			{
				title: 'Код',
				dataIndex: 'code'
			},
			{
				title: 'Название',
				dataIndex: 'name'
			},
			{
				title: 'Символ',
				dataIndex: 'code_name'
			},
			{
				render: currency => <div style={ { display: 'flex', justifyContent: 'flex-end' } }>
					<EditCurrency
						createExchange={ this.props.createExchange }
						editExchange={ this.props.editExchange }
						deleteCurrency={ this.props.deleteCurrency }
						editCurrency={ this.props.editCurrency }
						fetchingExchanges={ this.props.fetchingExchanges }
						selected={ currency }
						currencies={ data }
						exchanges={ dataExchanges }
					/>
				</div>
			}
		];


		return (
			<>
				<Row type="flex" gutter={ 8 }>
					<Col><Title level={ 3 }>Валюты</Title></Col>
					<Col><CreateCurrency currencies={ data } createCurrency={ this.props.createCurrency }/></Col>
				</Row>
				<br/>
				<Row type="flex" justify="space-between">
					<Col xl={ 8 } lg={ 12 } md={ 24 }>
						<Table className={ 'izi-table' } columns={ columns } dataSource={ data } size='small'
						       loading={ fetching }
						       rowKey={ 'id' }/>
					</Col>
				</Row>
			</>
		);
	}
}

Currencies.propTypes = {
	data: PropTypes.array.isRequired,
	fetching: PropTypes.bool.isRequired,
	fetchingExchanges: PropTypes.bool.isRequired,
	dataExchanges: PropTypes.array.isRequired,
	createCurrency: PropTypes.func.isRequired,
	deleteCurrency: PropTypes.func.isRequired,
	createExchange: PropTypes.func.isRequired,
	editExchange: PropTypes.func.isRequired,
	editCurrency: PropTypes.func.isRequired
};

export default Currencies;