import React, {Component } from 'react';
import * as PropTypes from 'prop-types';
import { Table, Button, Modal } from 'antd';
import { injectIntl } from 'react-intl';

class Statistics extends Component {
	state = { visible: false };

	showModal = () => {
		this.setState({
			visible: true,
		});
	};

	handleOk = e => {
		this.setState({
			visible: false,
		});
	};

	handleCancel = e => {
		this.setState({
			visible: false,
		});
	};

	render() {
		const { intl } = this.props;
		const columns = [
			{
				title: '#',
				dataIndex: 'id'
			}, {
				title: intl.formatMessage({ id: 'packages-check.filter.date' }),
				dataIndex: 'date',
			}, {
				title: 'Курс',
				dataIndex: 'value'
			},
		];

		return (
			<>
				<Button type="link" onClick={ this.showModal } disabled={ this.props.statistics.length === 0 }>
					Статистика
				</Button>
				<Modal
					title="Статистика"
					visible={ this.state.visible }
					onOk={ this.handleOk }
					onCancel={ this.handleCancel }
				>
					<Table size={ 'small' } dataSource={ this.props.statistics } columns={ columns } rowKey={ 'id' }/>
				</Modal>
			</>

		);
	}
}

Statistics.propTypes = {
	statistics: PropTypes.array.isRequired
};

export default (injectIntl)(Statistics);