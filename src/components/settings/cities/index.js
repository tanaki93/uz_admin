import React, { useState } from 'react';
import { PageHeader, Table, Checkbox, Button } from 'antd';
import * as PropTypes from 'prop-types';
import Dialog from './Dialog';
import { EditOutlined, PlusOutlined } from '@ant-design/icons';

const Cities = (props) => {
	const [visible, setVisible] = useState(false);
	const [selected, setSelected] = useState(null);
	const { fetching, data, countries, createCity, editCity, deleteCity } = props;

	const show = () => {
		setVisible(true);
	};

	const hide = () => {
		setVisible(false);
		setSelected(null);
	};

	const assignToEdit = (entry) => {
		show();
		setSelected(entry);
	};

	const check = (selected, e) => {
		editCity(selected.id, {
			country_id: selected.country.id,
			name: selected.name,
			delivery_price: selected.delivery_price,
			delivery_percent: selected.delivery_percent,
			delivery_day: selected.delivery_day,
			is_active: e.target.checked
		});
	};

	const columns = [
		{
			title: 'Актив',
			render: item => <Checkbox checked={ item.is_active } onClick={ e => check(item, e) }/>
		},
		{
			title: 'Название',
			dataIndex: 'name'
		},
		{
			title: 'Доставка',
			children: [
				{
					title: 'Цена ($)',
					dataIndex: 'delivery_price'
				},
				{
					title: 'Процент',
					dataIndex: 'delivery_percent'
				},
				{
					title: 'Длительность (дни)',
					dataIndex: 'delivery_day'
				}
			]
		},
		{
			title: 'Страна',
			dataIndex: 'country',
			render: item => item ? `${ item.name }` : ''
		},
		{
			render: selected =>
				<div style={ { display: 'flex', justifyContent: 'flex-end' } }>
					<Button type='dashed' onClick={ () => assignToEdit(selected) } icon={ <EditOutlined/> }>
						Изменить
					</Button>
				</div>
		}
	];

	return (
		<>
			<PageHeader
				ghost={ false }
				title='Города'
				extra={ [
					<Button key={ 'createCity' } type='primary' onClick={ show } icon={ <PlusOutlined/> }>
						Создать город
					</Button>
				] }
			/>
			<div className='page-wrapper page-wrapper--small'>
				<Table
					className={ 'izi-table' }
					columns={ columns }
					dataSource={ data }
					size='small'
					rowKey={ 'id' }
					loading={ fetching }
				/>
			</div>
			<Dialog
				visible={ visible }
				hide={ hide }
				selected={ selected }
				data={ data }
				countries={ countries }
				createCity={ createCity }
				editCity={ editCity }
				deleteCity={ deleteCity }
			/>
		</>
	);
};

Cities.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	countries: PropTypes.array.isRequired,
	createCity: PropTypes.func.isRequired,
	editCity: PropTypes.func.isRequired,
	deleteCity: PropTypes.func.isRequired
};

export default Cities;