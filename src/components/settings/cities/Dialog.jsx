import React from 'react';
import * as PropTypes from 'prop-types';
import { Modal, Row, Col, Input, Select, message, Form, Button } from 'antd';
import { Icon as LegacyIcon } from '@ant-design/compatible';

const Dialog = (props) => {
	const formRef = React.createRef();

	const { visible, hide, selected, data, countries, createCity, editCity, deleteCity } = props;

	const handleDelete = e => {
		deleteCity(selected.id);
		hide();
	};

	const handleEdit = (form, values) => {
		editCity(selected.id, { ...values, is_active: selected.is_active });
		form.resetFields();
		hide();
	};

	const handleCreate = (form, values) => {
		createCity({ ...values, is_active: false });
		form.resetFields();
		hide();
	};

	const onFinish = () => {
		const form = formRef.current;

		form.validateFields()
			.then(values => {
				const hasSame = data.find(entry => entry.name.toLowerCase() === values.name.toLowerCase());
				if (selected) {
					handleEdit(form, values);
				} else {
					if (hasSame) {
						message.error('Такой город уже существует');
					} else {
						handleCreate(form, values);
					}
				}
			});
	};

	const defaultRules = [{ required: true, message: 'Обязательно для заполнения' }];

	return (
		<>
			<Modal
				width={ '50vw' }
				title={ selected ? 'Изменение города' : 'Создание города' }
				visible={ visible }
				onCancel={ hide }
				footer={
					<Row justify='space-between'>
						<Col>
							{ selected &&
							<Button
								type='primary' ghost onClick={ handleDelete }
								icon={ <LegacyIcon type={ 'delete' }/> }
							>
								Удалить
							</Button>
							}
						</Col>
						<Col>
							<Button onClick={ onFinish } type='primary'>
								{ selected ? 'Изменить' : 'Создать' }
							</Button>
						</Col>
					</Row>
				}
			>
				{
					visible &&
					<Form
						ref={ formRef }
						onFinish={ onFinish }
						labelCol={ { span: 24 } }
						wrapperCol={ { span: 24 } }
						initialValues={ selected && {
							name: selected.name,
							delivery_price: selected.delivery_price,
							delivery_percent: selected.delivery_percent,
							delivery_day: selected.delivery_day,
							country_id: selected.country?.id,
						} }
					>
						<Row gutter={ 16 }>
							<Col xl={ 12 } lg={ 12 } md={ 24 } xs={ 24 }>
								<Form.Item label='Название' name='name' rules={ defaultRules }>
									<Input/>
								</Form.Item>
							</Col>
							<Col xl={ 12 } lg={ 12 } md={ 24 } xs={ 24 }>
								<div style={ { display: 'flex', alignItems: 'flex-end' } }>
									<Form.Item label='Страна' name='country_id' style={ { flexGrow: '1' } }>
										<Select>
											<Select.Option value={ 0 }>Пусто</Select.Option>
											{ countries.map(item =>
												<Select.Option value={ item.id } key={ 'country_' + item.id }>
													{ item.name }
												</Select.Option>
											) }
										</Select>
									</Form.Item>
								</div>
							</Col>
							<Col xl={ 8 } lg={ 8 } md={ 24 } xs={ 24 }>
								<Form.Item label='Цена доставки ($)' name='delivery_price' rules={ defaultRules }>
									<Input/>
								</Form.Item>
							</Col>
							<Col xl={ 8 } lg={ 8 } md={ 24 } xs={ 24 }>
								<Form.Item label='Процент с доставки' name='delivery_percent' rules={ defaultRules }>
									<Input/>
								</Form.Item>
							</Col>
							<Col xl={ 8 } lg={ 8 } md={ 24 } xs={ 24 }>
								<Form.Item label='Длительность (дни)' name='delivery_day' rules={ defaultRules }>
									<Input/>
								</Form.Item>
							</Col>
						</Row>
					</Form>
				}
			</Modal>
		</>
	);
};

Dialog.propTypes = {
	visible: PropTypes.bool.isRequired,
	hide: PropTypes.func.isRequired,
	selected: PropTypes.object,
	data: PropTypes.array.isRequired,
	countries: PropTypes.array.isRequired,
	createCity: PropTypes.func.isRequired,
	editCity: PropTypes.func.isRequired,
	deleteCity: PropTypes.func.isRequired
};

export default Dialog;