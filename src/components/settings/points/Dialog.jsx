import React from 'react';
import * as PropTypes from 'prop-types';
import { Modal, Row, Col, Input, Select, message, Form, Button } from 'antd';
import { Icon as LegacyIcon } from '@ant-design/compatible';

const Dialog = (props) => {
	const formRef = React.createRef();

	const { visible, hide, selected, data, cities, createPoint, editPoint, deletePoint } = props;

	const handleDelete = e => {
		deletePoint(selected.id);
		hide();
	};

	const handleEdit = (form, values) => {
		editPoint(selected.id, { ...values, is_active: selected.is_active });
		form.resetFields();
		hide();
	};

	const handleCreate = (form, values) => {
		createPoint({ ...values, is_active: false });
		form.resetFields();
		hide();
	};

	const onFinish = () => {
		const form = formRef.current;

		form.validateFields()
			.then(values => {
				const hasSame = data.find(entry => entry.name.toLowerCase() === values.name.toLowerCase());
				if (selected) {
					handleEdit(form, values);
				} else {
					if (hasSame) {
						message.error('Такой пункт уже существует');
					} else {
						handleCreate(form, values);
					}
				}
			});
	};

	const defaultRules = [{ required: true, message: 'Обязательно для заполнения' }];

	return (
		<>
			<Modal
				width={ '50vw' }
				title={ selected ? 'Изменение пункта' : 'Создание пункта' }
				visible={ visible }
				onCancel={ hide }
				footer={
					<Row justify='space-between'>
						<Col>
							{ selected &&
							<Button
								type='primary' ghost onClick={ handleDelete }
								icon={ <LegacyIcon type={ 'delete' }/> }
							>
								Удалить
							</Button>
							}
						</Col>
						<Col>
							<Button onClick={ onFinish } type='primary'>
								{ selected ? 'Изменить' : 'Создать' }
							</Button>
						</Col>
					</Row>
				}
			>
				{
					visible &&
					<Form
						ref={ formRef }
						onFinish={ onFinish }
						labelCol={ { span: 24 } }
						wrapperCol={ { span: 24 } }
						initialValues={ selected && {
							name: selected.name,
							delivery_price: selected.delivery_price,
							lat: selected.lat,
							lng: selected.lng,
							country_id: selected.country?.id,
						} }
					>
						<Row gutter={ 16 }>
							<Col xl={ 12 } lg={ 12 } md={ 24 } xs={ 24 }>
								<Form.Item label='Название' name='name' rules={ defaultRules }>
									<Input/>
								</Form.Item>
							</Col>
							<Col xl={ 12 } lg={ 12 } md={ 24 } xs={ 24 }>
								<div style={ { display: 'flex', alignItems: 'flex-end' } }>
									<Form.Item label='Город' name='city_id' style={ { flexGrow: '1' } }>
										<Select>
											<Select.Option value={ 0 }>Пусто</Select.Option>
											{ cities.map(item =>
												<Select.Option value={ item.id } key={ 'city_' + item.id }>
													{ item.name }
												</Select.Option>
											) }
										</Select>
									</Form.Item>
								</div>
							</Col>
							<Col xl={ 8 } lg={ 8 } md={ 24 } xs={ 24 }>
								<Form.Item label='Цена доставки ($)' name='delivery_price' rules={ defaultRules }>
									<Input/>
								</Form.Item>
							</Col>
							<Col xl={ 8 } lg={ 8 } md={ 24 } xs={ 24 }>
								<Form.Item label='Долгота' name='lat' rules={ defaultRules }>
									<Input/>
								</Form.Item>
							</Col>
							<Col xl={ 8 } lg={ 8 } md={ 24 } xs={ 24 }>
								<Form.Item label='Широта' name='lng' rules={ defaultRules }>
									<Input/>
								</Form.Item>
							</Col>
						</Row>
					</Form>
				}
			</Modal>
		</>
	);
};

Dialog.propTypes = {
	visible: PropTypes.bool.isRequired,
	hide: PropTypes.func.isRequired,
	selected: PropTypes.object,
	data: PropTypes.array.isRequired,
	cities: PropTypes.array.isRequired,
	createPoint: PropTypes.func.isRequired,
	editPoint: PropTypes.func.isRequired,
	deletePoint: PropTypes.func.isRequired
};

export default Dialog;