import React, { useState } from 'react';
import { PageHeader, Table, Checkbox, Button } from 'antd';
import * as PropTypes from 'prop-types';
import Dialog from './Dialog';
import { EditOutlined, PlusOutlined } from '@ant-design/icons';

const Points = (props) => {
	const [visible, setVisible] = useState(false);
	const [selected, setSelected] = useState(null);

	const { fetching, data, cities, createPoint, editPoint, deletePoint } = props;

	const show = () => {
		setVisible(true);
	};

	const hide = () => {
		setVisible(false);
		setSelected(null);
	};

	const assignToEdit = (entry) => {
		show();
		setSelected(entry);
	};

	const check = (selected, e) => {
		editPoint(selected.id, {
			city_id: selected.city.id,
			name: selected.name,
			delivery_price: selected.delivery_price,
			lat: selected.lat,
			lng: selected.lng,
			is_active: e.target.checked
		});
	};

	const columns = [
		{
			title: 'Актив',
			render: item => <Checkbox checked={ item.is_active } onClick={ e => check(item, e) }/>
		},
		{
			title: 'Название',
			dataIndex: 'name'
		},
		{
			title: 'Цена ($)',
			dataIndex: 'delivery_price'
		},
		{
			title: 'Долгота',
			dataIndex: 'lat'
		},
		{
			title: 'Широта',
			dataIndex: 'lng'
		},
		{
			title: 'Город',
			dataIndex: 'city',
			render: item => item ? `${ item.name }` : ''
		},
		{
			render: selected =>
				<div style={ { display: 'flex', justifyContent: 'flex-end' } }>
					<Button type='dashed' onClick={ () => assignToEdit(selected) } icon={ <EditOutlined/> }>
						Изменить
					</Button>
				</div>
		}
	];

	return (
		<>
			<PageHeader
				ghost={ false }
				title='Пункты'
				extra={ [
					<Button key={ 'createCity' } type='primary' onClick={ show } icon={ <PlusOutlined/> }>
						Создать пункт
					</Button>
				] }
			/>
			<div className='page-wrapper page-wrapper--small'>
				<Table
					className={ 'izi-table' }
					columns={ columns }
					dataSource={ data }
					size='small'
					rowKey={ 'id' }
					loading={ fetching }
				/>
			</div>
			<Dialog
				visible={ visible }
				hide={ hide }
				selected={ selected }
				data={ data }
				cities={ cities }
				createPoint={ createPoint }
				editPoint={ editPoint }
				deletePoint={ deletePoint }
			/>
		</>
	);
};

Points.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	cities: PropTypes.array.isRequired,
	createPoint: PropTypes.func.isRequired,
	editPoint: PropTypes.func.isRequired,
	deletePoint: PropTypes.func.isRequired
};

export default Points;