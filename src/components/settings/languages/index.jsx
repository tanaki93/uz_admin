import React, { Component } from 'react';
import { Checkbox, Col, Row, Table, Typography } from 'antd';
import * as PropTypes from 'prop-types';
import CreateLanguage from './CreateLanguage';
import EditLanguage from './EditLanguage';

const { Title } = Typography;

class Languages extends Component {
	check = (selected, e) => {
		this.props.editLanguage(selected.id, {
			is_active: e.target.checked,
			code: selected.code,
			name: selected.name
		});
	};

	render() {
		const columns = [
			{
				title: 'Актив',
				render: item => <Checkbox checked={ item.is_active } onClick={ e => this.check(item, e) }/>
			},
			{
				title: 'Код',
				dataIndex: 'code'
			},
			{
				title: 'Название',
				dataIndex: 'name'
			},
			{
				render: currency => <EditLanguage languages={ this.props.data }
				                                  deleteLanguage={ this.props.deleteLanguage }
				                                  editLanguage={ this.props.editLanguage } selected={ currency }/>
			}
		];

		const { fetching, data } = this.props;

		return (
			<>
				<Row type="flex" gutter={ 8 }>
					<Col><Title level={ 3 }>Языки</Title></Col>
					<Col><CreateLanguage languages={ data } createLanguage={ this.props.createLanguage }/></Col>
				</Row>
				<br/>
				<Row type="flex" justify="space-between">
					<Col span={ 12 }>
						<Table className={ 'izi-table' } columns={ columns } dataSource={ data } size='small'
						       loading={ fetching }
						       rowKey={ 'id' }/>
					</Col>
				</Row>
			</>
		);
	}
}

Languages.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	createLanguage: PropTypes.func.isRequired,
	deleteLanguage: PropTypes.func.isRequired,
	editLanguage: PropTypes.func.isRequired
};

export default Languages;