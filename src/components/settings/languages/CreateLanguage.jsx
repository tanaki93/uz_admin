import React from 'react';
import * as PropTypes from 'prop-types';
import { PlusOutlined } from '@ant-design/icons';
import { Modal, Button, Input, message } from 'antd';

class CreateLanguage extends React.Component {
	state = {
		visible: false,
		name: '',
		code: ''
	};

	onChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};

	showModal = () => {
		this.setState({
			visible: true,
		});
	};

	error = () => {
		message.error('Такой язык уже существует');
	};

	handleOk = e => {
		const { name, code } = this.state;
		if (name && code) {
			if (!this.props.languages.find(language => language.code.toLowerCase() === code.toLowerCase())) {
				this.props.createLanguage({ name, code });
				this.setState({
					visible: false,
					name: '',
					code: ''
				});
			} else {
				this.error();
			}
		}
	};

	handleCancel = e => {
		this.setState({
			visible: false,
			name: '',
			code: ''
		});
	};

	render() {
		const { name, code } = this.state;

		return (
			<>
				{ this.props.custom ?
					<Button type="primary" onClick={ this.showModal } icon={ <PlusOutlined/> } style={{marginBottom: 24}}/> :
					<Button type="primary" onClick={ this.showModal } icon={ <PlusOutlined/> } style={{marginBottom: 24}}>
						Создать язык
					</Button>
				}

				<Modal
					centered
					title="Создание языка"
					visible={ this.state.visible }
					onOk={ this.handleOk }
					onCancel={ this.handleCancel }
				>
					<table className={ 'base-table' }>
						<thead>
						<tr>
							<td>Код</td>
							<td>Название</td>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td>
								<Input value={ code } name={ 'code' } onChange={ this.onChange }/>
							</td>
							<td>
								<Input value={ name } name={ 'name' } onChange={ this.onChange }/>
							</td>
						</tr>
						</tbody>
					</table>
				</Modal>
			</>
		);
	}
}

CreateLanguage.propTypes = {
	createLanguage: PropTypes.func.isRequired,
	languages: PropTypes.array.isRequired,
	custom: PropTypes.bool.isRequired
};

export default CreateLanguage;