import React from 'react';
import * as PropTypes from 'prop-types';
import { EditOutlined } from '@ant-design/icons';
import { Icon as LegacyIcon } from '@ant-design/compatible';
import { Modal, Button, Input, message } from 'antd';

class EditLanguage extends React.Component {
	state = {
		visible: false,
		name: '',
		code: ''
	};

	dropToInitial = () => {
		this.setState({
			visible: false,
			name: '',
			code: ''
		});
	};

	componentDidMount() {
		const { selected } = this.props;
		this.setState({
			name: selected.name,
			code: selected.code
		});
	};

	onChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};

	showModal = () => {
		const { selected } = this.props;
		this.setState({
			visible: true,
			name: selected.name,
			code: selected.code
		});
	};

	error = () => {
		message.error('Такой язык уже существует');
	};

	handleOk = e => {
		const { name, code } = this.state;
		if (this.props.selected.code.toLowerCase() === code.toLowerCase()) {
			this.props.editLanguage(this.props.selected.id, { name, code });
			this.dropToInitial();
		} else {
			if (name && code) {
				if (!this.props.languages.find(language => language.code.toLowerCase() === code.toLowerCase())) {
					this.props.editLanguage(this.props.selected.id, { name, code });
					this.dropToInitial();
				} else {
					this.error();
				}
			}
		}
	};

	handleCancel = e => {
		this.setState({
			visible: false
		});
	};

	handleDelete = e => {
		this.props.deleteLanguage(this.props.selected.id);
	};

	render() {
		const { name, code } = this.state;

		return (
			<>
				<Button type="dashed" onClick={ this.showModal } icon={ <EditOutlined/> }>
					Изменить
				</Button>
				<Modal
					title="Изменение языка"
					visible={ this.state.visible }
					footer={
						<div style={ { display: 'flex', justifyContent: 'space-between' } }>
							<div>
								<Button type={ 'primary' } ghost onClick={ this.handleDelete }
								        disabled={ this.props.selected.is_related }
								        icon={ <LegacyIcon type={ 'delete' }/> }>Удалить</Button>
							</div>
							<div>
								<Button onClick={ this.handleCancel }>Закрыть</Button>
								<Button type={ 'primary' } onClick={ this.handleOk }>Сохранить</Button>
							</div>
						</div>
					}
					onCancel={ this.handleCancel }
				>
					<table className={ 'base-table' }>
						<thead>
						<tr>
							<td>Код</td>
							<td>Название</td>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td>
								<Input value={ code } name={ 'code' } onChange={ this.onChange }/>
							</td>
							<td>
								<Input value={ name } name={ 'name' } onChange={ this.onChange }/>
							</td>
						</tr>
						</tbody>
					</table>
				</Modal>
			</>
		);
	}
}

EditLanguage.propTypes = {
	editLanguage: PropTypes.func.isRequired,
	deleteLanguage: PropTypes.func.isRequired,
	selected: PropTypes.object,
	languages: PropTypes.array.isRequired
};

export default EditLanguage;