import React from 'react';
import * as PropTypes from 'prop-types';
import { Modal, Row, Col, Input, Select, message, Form, Button } from 'antd';
import CreateLanguage from '../languages/CreateLanguage';
import CreateCurrency from '../currencies/CreateCurrency';
import { Icon as LegacyIcon } from '@ant-design/compatible';

const Dialog = (props) => {
	const formRef = React.createRef();

	const { visible, hide, selected, createCountry, editCountry, deleteCountry, createCurrency, createLanguage, currencies, languages, countries } = props;

	const handleDelete = e => {
		deleteCountry(selected.id);
		hide();
	};

	const handleEdit = (form, values) => {
		editCountry(selected.id, { ...values, is_active: selected.is_active });
		form.resetFields();
		hide();
	};

	const handleCreate = (form, values) => {
		createCountry({ ...values, is_active: false });
		form.resetFields();
		hide();
	};

	const onFinish = () => {
		const form = formRef.current;

		form.validateFields()
			.then(values => {
				const hasSame = countries.find(entry => entry.code.toLowerCase() === values.code.toLowerCase());
				if (selected) {
					handleEdit(form, values);
				} else {
					if (hasSame) {
						message.error('Такая страна уже существует');
					} else {
						handleCreate(form, values);
					}
				}
			});
	};

	const defaultRules = [{ required: true, message: 'Обязательно для заполнения' }];

	return (
		<>
			<Modal
				width={ '50vw' }
				title={ selected ? 'Изменение cтраны' : 'Создание cтраны' }
				visible={ visible }
				onCancel={ hide }
				footer={
					<Row justify='space-between'>
						<Col>
							{ selected &&
							<Button
								type='primary' ghost onClick={ handleDelete }
								disabled={ selected.is_related }
								icon={ <LegacyIcon type={ 'delete' }/> }
							>
								Удалить
							</Button>
							}
						</Col>
						<Col>
							<Button onClick={ onFinish } type='primary'>
								{ selected ? 'Изменить' : 'Создать' }
							</Button>
						</Col>
					</Row>
				}
			>
				{
					visible &&
					<Form
						ref={ formRef }
						onFinish={ onFinish }
						labelCol={ { span: 24 } }
						wrapperCol={ { span: 24 } }
						initialValues={ selected && {
							code: selected.code,
							name: selected.name,
							day_count_newest: selected.day_count_newest,
							delivery_price: selected.delivery_price,
							delivery_percent: selected.delivery_percent,
							delivery_day: selected.delivery_day,
							currency_id: selected.currency?.id,
							language_id: selected.language?.id
						} }
					>
						<Row gutter={16}>
							<Col xl={ 12 } lg={ 12 } md={ 24 } xs={ 24 }>
								<Form.Item label='Код' name='code' rules={ defaultRules }>
									<Input/>
								</Form.Item>
							</Col>
							<Col xl={ 12 } lg={ 12 } md={ 24 } xs={ 24 }>
								<Form.Item label='Название' name='name' rules={ defaultRules }>
									<Input/>
								</Form.Item>
							</Col>
							<Col xl={ 8 } lg={ 8 } md={ 24 } xs={ 24 }>
								<Form.Item label='Цена доставки ($)' name='delivery_price' rules={ defaultRules }>
									<Input/>
								</Form.Item>
							</Col>
							<Col xl={ 8 } lg={ 8 } md={ 24 } xs={ 24 }>
								<Form.Item label='Процент с доставки' name='delivery_percent' rules={ defaultRules }>
									<Input/>
								</Form.Item>
							</Col>
							<Col xl={ 8 } lg={ 8 } md={ 24 } xs={ 24 }>
								<Form.Item label='Длительность (дни)' name='delivery_day' rules={ defaultRules }>
									<Input/>
								</Form.Item>
							</Col>
							<Col xl={ 8 } lg={ 8 } md={ 24 } xs={ 24 }>
								<Form.Item label='Свежесть (дни)' name='day_count_newest' rules={ defaultRules }>
									<Input/>
								</Form.Item>
							</Col>
							<Col span={ 24 }>
								<div style={ { display: 'flex', alignItems: 'flex-end' } }>
									<Form.Item label='Валюта' name='currency_id' style={ { flexGrow: '1' } }>
										<Select>
											<Select.Option value={ 0 }>Пусто</Select.Option>
											{ currencies.map(currency =>
												<Select.Option value={ currency.id } key={ 'currency_' + currency.id }>
													{ `${ currency.name } (${ currency.code })` }
												</Select.Option>
											) }
										</Select>
									</Form.Item>

									<CreateCurrency createCurrency={ createCurrency } currencies={ currencies } custom/>
								</div>
							</Col>
							<Col span={ 24 }>
								<div style={ { display: 'flex', alignItems: 'flex-end' } }>
									<Form.Item label='Язык' name='language_id' style={ { flexGrow: '1' } }>
										<Select>
											<Select.Option value={ 0 }>Пусто</Select.Option>
											{ languages.map(language =>
												<Select.Option value={ language.id } key={ 'language_' + language.id }>
													{ `${ language.name } (${ language.code })` }
												</Select.Option>
											) }
										</Select>
									</Form.Item>
									<CreateLanguage createLanguage={ createLanguage } languages={ languages } custom/>
								</div>
							</Col>
						</Row>
					</Form>
				}
			</Modal>
		</>
	);
};

Dialog.propTypes = {
	visible: PropTypes.bool.isRequired,
	hide: PropTypes.func.isRequired,
	selected: PropTypes.object,
	createCountry: PropTypes.func.isRequired,
	editCountry: PropTypes.func.isRequired,
	deleteCountry: PropTypes.func.isRequired,
	createCurrency: PropTypes.func.isRequired,
	createLanguage: PropTypes.func.isRequired,
	currencies: PropTypes.array.isRequired,
	languages: PropTypes.array.isRequired,
	countries: PropTypes.array.isRequired,
};

export default Dialog;