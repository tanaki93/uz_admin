import React, { useState } from 'react';
import { PageHeader, Table, Checkbox, Button } from 'antd';
import * as PropTypes from 'prop-types';
import Dialog from './Dialog';
import { EditOutlined, PlusOutlined } from '@ant-design/icons';

const Countries = (props) => {
	const [visible, setVisible] = useState(false);
	const [selected, setSelected] = useState(null);
	const { fetching, data, currencies, languages, createCountry, editCountry, createLanguage, createCurrency, deleteCountry } = props;

	const show = () => {
		setVisible(true);
	};

	const hide = () => {
		setVisible(false);
		setSelected(null);
	};

	const assignToEdit = (entry) => {
		show();
		setSelected(entry);
	};

	const check = (selected, e) => {
		editCountry(selected.id, {
			is_active: e.target.checked,
			code: selected.code,
			name: selected.name,
			currency_id: selected.currency.id,
			language_id: selected.language.id
		});
	};

	const columns = [
		{
			title: 'Актив',
			render: item => <Checkbox checked={ item.is_active } onClick={ e => check(item, e) }/>
		},
		{
			title: 'Код',
			dataIndex: 'code'
		},
		{
			title: 'Название',
			dataIndex: 'name'
		},
		{
			title: 'Доставка',
			children: [
				{
					title: 'Цена ($)',
					dataIndex: 'delivery_price'
				},
				{
					title: 'Процент',
					dataIndex: 'delivery_percent'
				},
				{
					title: 'Длительность (дни)',
					dataIndex: 'delivery_day'
				}
			]
		},
		{
			title: 'Свежесть (дни)',
			dataIndex: 'day_count_newest'
		},
		{
			title: 'Валюта',
			dataIndex: 'currency',
			render: currency => currency ? `${ currency.name } ${ currency.code }` : ''
		},
		{
			title: 'Язык',
			dataIndex: 'language',
			render: language => language ? `${ language.name } ${ language.code }` : ''
		},
		{
			render: selected =>
				<div style={ { display: 'flex', justifyContent: 'flex-end' } }>
					<Button type='dashed' onClick={ () => assignToEdit(selected) } icon={ <EditOutlined/> }>
						Изменить
					</Button>
				</div>
		}
	];

	return (
		<>
			<PageHeader
				ghost={ false }
				title='Страны'
				extra={ [
					<Button key={ 'createCountry' } type='primary' onClick={ show } icon={ <PlusOutlined/> }>
						Создать страну
					</Button>
				] }
			/>
			<div className='page-wrapper page-wrapper--small'>
				<Table
					className={ 'izi-table' }
					columns={ columns }
					dataSource={ data }
					size='small'
					rowKey={ 'id' }
					loading={ fetching }
				/>
			</div>
			<Dialog
				visible={ visible }
				hide={ hide }
				selected={ selected }
				countries={ data }
				createCountry={ createCountry }
				editCountry={ editCountry }
				deleteCountry={ deleteCountry }
				createLanguage={ createLanguage }
				createCurrency={ createCurrency }
				currencies={ currencies }
				languages={ languages }
			/>
		</>
	);
};

Countries.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	currencies: PropTypes.array.isRequired,
	languages: PropTypes.array.isRequired,
	createCountry: PropTypes.func.isRequired,
	editCountry: PropTypes.func.isRequired,
	createLanguage: PropTypes.func.isRequired,
	createCurrency: PropTypes.func.isRequired,
	deleteCountry: PropTypes.func.isRequired
};

export default Countries;