import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Button, Table } from 'antd';
import StatusRenderer from '../../../shared/statusRenderer';
import * as logisticsReceiveStatus from '../../../constants/logisticsReceiveStatus';
import * as logisticsDeliverStatus from '../../../constants/logisticsDeliverStatus';
import moment from 'moment';
import SendOrder from './sendOrder';
import { injectIntl } from 'react-intl';


class List extends Component {
	receive = (id) => {
		this.props.changeLogisticsStatus(id, 'logistic_receive_status', logisticsReceiveStatus.RECEIVED);
	};

	nonReceive = (id) => {
		this.props.changeLogisticsStatus(id, 'logistic_receive_status', logisticsReceiveStatus.WAIT);
	};

	delivered = (id) => {
		this.props.changeLogisticsStatus(id, 'logistic_deliver_status', logisticsDeliverStatus.DELIVERED);
	};

	nonDelivered = (id) => {
		this.props.changeLogisticsStatus(id, 'logistic_deliver_status', logisticsDeliverStatus.SENT);
	};

	render() {
		const statusRenderer = new StatusRenderer();
		const { data, createDeliveryStatus, changeLogisticsStatus, intl } = this.props;

		const columns = [
			{
				title: 'Название',
				dataIndex: 'product',
				key: 'product_name',
				width: 200,
				fixed: 'left',
				render: item => item.title
			},
			{
				title: intl.formatMessage({ id: 'products-check.list.id' }),
				dataIndex: 'product',
				key: 'product_order_id',
				width: 100,
				fixed: 'left',
				render: item => item.id
			},
			{
				title: 'Кол-во',
				dataIndex: 'amount',
				key: 'amount',
				width: 100,
			},
			{
				title: 'Ссылка IZI',
				dataIndex: 'izi_link',
				key: 'izi_link',
				width: 100,
				render: link => <a href={ link } target={ '_blank' }>Ссылка</a>
			},
			{
				title: 'Ссылка Vend',
				dataIndex: 'product',
				key: 'product_vend_link',
				width: 100,
				render: product => <a href={ product.link.url } target={ '_blank' }>Ссылка</a>
			},
			{
				title: intl.formatMessage({ id: 'packages.list.flight-number' }),
				children: [
					{
						title: 'Номер',
						dataIndex: 'flight',
						key: 'flight',
						width: 100,
						render: flight => flight && flight.number
					},
					{
						title: 'Статус доставки',
						dataIndex: 'logistic_receive_status',
						key: 'logistic_receive_status',
						width: 200,
						render: status => statusRenderer.renderLogisticsReceiveStatus(status, intl)
					},
					{
						title: '',
						key: 'product_shipping_price',
						width: 120,
						render: item => <>
							{ item.logistic_receive_status === logisticsReceiveStatus.WAIT &&
							<Button className={ 'submit-btn' }
							        onClick={ () => this.receive(item.id) }>Получено</Button> }
							{ item.logistic_receive_status === logisticsReceiveStatus.RECEIVED &&
							<Button type={ 'link' }
							        onClick={ () => this.nonReceive(item.id) }>{ intl.formatMessage({ id: 'packages-check.status.not-received' }) }</Button> }
						</>
					},
					{
						title: 'Дата доставки',
						dataIndex: 'receive_date',
						key: 'receive_date',
						width: 200,
						render: date => date && moment(date).format('DD.MM.YYYY | HH:mm')
					}
				]
			},
			{
				title: 'Выдача',
				children: [
					{
						title: 'Статус выдачи',
						dataIndex: 'logistic_deliver_status',
						key: 'logistic_deliver_status',
						width: 150,
						render: status => statusRenderer.renderLogisticsDeliverStatus(status, intl)
					},
					{
						key: 'sendOrder',
						width: 250,
						render: item => <SendOrder
							item={ item }
							createDeliveryStatus={ createDeliveryStatus }
							changeLogisticsStatus={ changeLogisticsStatus }
						/>
					},
					{
						title: 'Дата выдачи',
						dataIndex: 'send_date',
						width: 200,
						render: date => date && moment(date).format('DD.MM.YYYY | HH:mm')
					},
				]
			},
			{
				title: 'Доставка',
				children: [
					{
						title: 'Статус',
						key: 'product_check_state',
						width: 120,
						render: item => <>
							{ item.logistic_deliver_status !== logisticsDeliverStatus.DELIVERED &&
							<Button className={ 'submit-btn' }
							        disabled={ item.logistic_deliver_status === logisticsDeliverStatus.DELIVERED }
							        onClick={ () => this.delivered(item.id) }>Доставлен</Button>
							}
							{ item.logistic_deliver_status === logisticsDeliverStatus.DELIVERED &&
							<Button type={ 'link' }
							        disabled={ item.logistic_deliver_status !== logisticsDeliverStatus.DELIVERED }
							        onClick={ () => this.nonDelivered(item.id) }>Не доставлен</Button>
							}
						</>
					},
					{
						title: 'Служба доставки',
						dataIndex: 'shipping_service',
						key: 'shipping_service',
						width: 300
					},
					{
						title: 'Дата доставки',
						dataIndex: 'delivery_date',
						key: 'delivery_date',
						width: 200,
						render: date => date && moment(date).format('DD.MM.YYYY | HH:mm')
					},
				]
			}
		];

		return (
			<Table
				className={ 'izi-table' }
				pagination={ false }
				columns={ columns }
				dataSource={ data.products }
				bordered
				size="small"
				rowKey={ 'id' }
				scroll={ { x: 2000, y: 600 } }
			/>
		);
	}
}

List.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.object.isRequired,
	changeLogisticsStatus: PropTypes.func.isRequired,
	createDeliveryStatus: PropTypes.func.isRequired
};

export default (injectIntl)(List);