import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Card, Col, Descriptions, Row, Typography } from 'antd';
import List from './List';
import { Link } from 'react-router-dom';
import moment from 'moment';

const { Title } = Typography;

class LogisticsManagement extends Component {
	render() {
		const { fetching, data, changeLogisticsStatus, createDeliveryStatus } = this.props;

		return (
			<Card>
				<Row>
					<Col span={ 12 }>
						<Link to={ `/logistics` }>Назад</Link>
					</Col>
				</Row>
				<Row>
					<Col>
						<Row type="flex" gutter={ 16 } align="middle">
							<Col><Title level={ 3 }>Заказ { data.id }</Title></Col>
							<Col>
								<b>Время заказа:</b> { moment(data.date).format('DD.MM.YYYY / HH:mm') }
							</Col>
							<Col>
								<b>Имя клиента:</b> { data.name }
							</Col>
						</Row>
					</Col>
				</Row>
				<hr/>
				<br/>
				<Row gutter={ [16, 24] }>
					<Col span={ 24 }>
						<Descriptions className={ 'izi-desc' } layout="vertical" size='small'
						              column={ { xxl: 6, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 } }>
							<Descriptions.Item label="Платежный адрес">{ data.address }</Descriptions.Item>
							<Descriptions.Item label="Адрес доставки">{ data.address }</Descriptions.Item>
							<Descriptions.Item label="Почта">{ data.email }</Descriptions.Item>
							<Descriptions.Item label="Телефон">{ data.phone }</Descriptions.Item>
							{/*<Descriptions.Item label="Метод доставки">Доставка до дома</Descriptions.Item>*/}
							{/*<Descriptions.Item label="Метод платежа">Оплата при доставке</Descriptions.Item>*/}
						</Descriptions>
					</Col>
					<Col span={ 24 }>
						<List
							fetching={ fetching }
							data={ data }
							changeLogisticsStatus={ changeLogisticsStatus }
							createDeliveryStatus={ createDeliveryStatus }
						/>
					</Col>
				</Row>
			</Card>
		);
	}
}

LogisticsManagement.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.object.isRequired,
	changeLogisticsStatus: PropTypes.func.isRequired,
	createDeliveryStatus: PropTypes.func.isRequired
};

export default LogisticsManagement;