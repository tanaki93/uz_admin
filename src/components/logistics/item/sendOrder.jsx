import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import * as logisticsReceiveStatus from '../../../constants/logisticsReceiveStatus';
import { Form } from '@ant-design/compatible';

import { Button, Input, Popover } from 'antd';
import * as logisticsDeliverStatus from '../../../constants/logisticsDeliverStatus';
import { injectIntl } from 'react-intl';

class SendOrder extends Component {
	state = {
		visible: false,
		shippingService: ''
	};

	hide = () => {
		this.setState({
			visible: false,
		});
	};

	handleVisibleChange = visible => {
		this.setState({ visible });
	};

	onChange = e => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};

	sent = (id) => {
		this.props.createDeliveryStatus(this.props.item.id, this.state.shippingService);
		this.props.changeLogisticsStatus(this.props.item.id, 'logistic_deliver_status', logisticsDeliverStatus.SENT);
	};

	nonSent = (id) => {
		this.props.changeLogisticsStatus(this.props.item.id, 'logistic_deliver_status', logisticsDeliverStatus.WAIT);
	};

	render() {
		const { item, intl } = this.props;
		const { shippingService } = this.state;

		return (
			<div>
				{ item.logistic_deliver_status === logisticsDeliverStatus.WAIT &&
				<Popover
					content={
						<Form>
							<Form.Item>
								<Input value={ shippingService } onChange={ this.onChange } name='shippingService'/>
							</Form.Item>
							<Form.Item>
								<Button onClick={ this.sent }>Отправить</Button>
							</Form.Item>
						</Form>
					}
					title="Сервим доставки"
					trigger="click"
					visible={ this.state.visible }
					onVisibleChange={ this.handleVisibleChange }
				>
					<Button type={ 'primary' }
					        disabled={ item.logistic_receive_status === logisticsReceiveStatus.WAIT }>Отправить</Button>
				</Popover>
				}

				{ item.logistic_deliver_status !== logisticsDeliverStatus.WAIT &&
				<Button type={ 'link' } onClick={ this.nonSent }
				        disabled={ item.logistic_receive_status === logisticsReceiveStatus.WAIT }>{ intl.formatMessage({ id: 'packages.delivery.not-sent' }) }</Button>
				}

			</div>
		);
	}
}

SendOrder.propTypes = {
	item: PropTypes.object.isRequired,
	createDeliveryStatus: PropTypes.func.isRequired,
	changeLogisticsStatus: PropTypes.func.isRequired,
};

export default (injectIntl)(SendOrder);