import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { PlusOutlined } from '@ant-design/icons';
import { Button, Col, Row, Typography } from 'antd';
import Filter from './Filter';
import List from './List';

const { Title } = Typography;

class Logistics extends Component {
	render() {
		const { fetching, data, fetchLogistics } = this.props;
		return (
			<>
				<Row type="flex" gutter={ 16 }>
					<Col><Title level={ 3 }>Логистика</Title></Col>
					<Col span={ 12 }>
						<Button type="primary" icon={ <PlusOutlined/> }>
							Добавить заказ
						</Button>
					</Col>
				</Row>
				<Row gutter={ [16, 24] }>
					<Col span={ 24 }>
						<Filter fetchLogistics={ fetchLogistics }/>
					</Col>
					<Col span={ 24 }>
						<List
							fetching={ fetching }
							data={ data }
						/>
					</Col>
				</Row>
			</>
		);
	}
}

Logistics.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired,
	fetchLogistics: PropTypes.func.isRequired
};

export default Logistics;