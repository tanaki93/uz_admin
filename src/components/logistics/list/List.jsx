import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Progress, Table } from 'antd';
import moment from 'moment';
import * as paymentStatus from '../../../constants/paymentStatus';
import { Link } from 'react-router-dom';
import { injectIntl } from 'react-intl';

class List extends Component {
	renderPaymentStatus = (status) => {
		switch (status) {
			default:
			case paymentStatus.NON_PAID:
				return <span style={ { color: '#ED1B23' } }>Не оплачен</span>;
			case paymentStatus.PAID:
				return <span style={ { color: '#219653' } }>Оплачен</span>;
		}
	};

	render() {
		const { fetching, data, intl } = this.props;
		const columns = [
			{
				title: 'Заказ',
				dataIndex: 'id',
				key: 'order_id',
				render: id => <Link to={ `/logistics/${ id }` }>{ id }</Link>
			},
			{
				title: intl.formatMessage({ id: 'packages-check.filter.date' }),
				dataIndex: 'date',
				key: 'order_time',
				render: time => moment(time).format('DD.MM.YYYY | HH:mm')
			},
			{
				title: 'Кол-во тов',
				dataIndex: 'amount',
				key: 'order_count',
			},
			{
				title: 'Сумма',
				dataIndex: 'price',
				key: 'order_price',
			},
			{
				title: 'Имя клиента',
				dataIndex: 'name',
				key: 'order_client_name',
			},
			{
				title: 'Статус оплаты',
				dataIndex: 'payment_status',
				key: 'order_payment_status',
				render: status => this.renderPaymentStatus(status)
			},
			{
				title: 'Статус доставки',
				key: 'logistic_receive_status',
				render: item => <>
					<span style={ {
						display: 'block',
						textAlign: 'center'
					} }>{ item.logistic_statuses.logistic_receive_status }/{ item.count }</span>
					<Progress percent={ (100 / item.count) * item.logistic_statuses.logistic_receive_status }
					          size="small" showInfo={ false }/>
				</>
			},
			{
				title: 'Статус обработки',
				key: 'logistic_deliver_status',
				render: item => <>
					<span style={ {
						display: 'block',
						textAlign: 'center'
					} }>{ item.logistic_statuses.logistic_deliver_status }/{ item.count }</span>
					<Progress percent={ (100 / item.count) * item.logistic_statuses.logistic_deliver_status }
					          size="small" showInfo={ false }/>
				</>
			}
		];

		return (
			<Table
				loading={ fetching }
				className={ 'izi-table' }
				columns={ columns }
				dataSource={ data }
				bordered
				size="small"
				rowKey={ 'id' }
			/>
		);
	}
}

List.propTypes = {
	fetching: PropTypes.bool.isRequired,
	data: PropTypes.array.isRequired
};

export default (injectIntl)(List);