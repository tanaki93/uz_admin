import React from 'react';
import * as PropTypes from 'prop-types';
import { Form } from '@ant-design/compatible';

import { Button, DatePicker, Input } from 'antd';
import { injectIntl } from 'react-intl';

const { RangePicker } = DatePicker;

class Filter extends React.Component {
	state = {
		number: '',
		dateFrom: '',
		dateTo: ''
	};

	handleChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};

	handleSelect = (name, value) => {
		this.setState({
			[name]: value
		});
	};

	handleDateSelect = (value, dateString) => {
		this.setState({
			dateFrom: dateString[0],
			dateTo: dateString[1]
		});
	};

	handleSubmit = () => {
		const { number, dateFrom, dateTo } = this.state;
		this.props.fetchLogistics({ number, dateFrom, dateTo });
	};

	handleReset = () => {
		this.setState({
			number: '',
			dateFrom: '',
			dateTo: ''
		}, () => this.props.fetchLogistics());
	};

	render() {
		const { intl } = this.props;
		const formItemLayout = {
			labelCol: {
				xl: { span: 24 },
			},
			wrapperCol: {
				xl: { span: 24 },
			},
		};

		return (
			<Form layout="inline" { ...formItemLayout }>
				<Form.Item label={ intl.formatMessage({ id: 'packages-check.filter.date' }) }>
					<RangePicker onChange={ this.handleDateSelect } allowClear/>
				</Form.Item>
				<Form.Item label={ intl.formatMessage({ id: 'packages-check.filter.number' }) }>
					<Input onChange={ this.handleChange } name='number' value={ this.state.number }/>
				</Form.Item>
				<Form.Item label={ <span>&nbsp;</span> }>
					<Button onClick={ this.handleSubmit }
					        type={ 'primary' }>{ intl.formatMessage({ id: 'packages-check.filter.filter' }) }</Button>
				</Form.Item>
				<Form.Item label={ <span>&nbsp;</span> }>
					<Button
						onClick={ this.handleReset }>{ intl.formatMessage({ id: 'packages-check.filter.drop' }) }</Button>
				</Form.Item>
			</Form>
		);
	}
}

Filter.propTypes = {
	fetchLogistics: PropTypes.func.isRequired
};

export default (injectIntl)(Filter);